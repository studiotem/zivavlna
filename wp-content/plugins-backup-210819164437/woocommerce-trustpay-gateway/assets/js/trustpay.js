var originalOpenPopup = openPopup;

openPopup = function () {
    var method_radio = jQuery('#payment_method_trustpay_gateway');
    var wire_radio = jQuery('#payment_method_trustpay_gateway_wire');
    var card_radio = jQuery('#payment_method_trustpay_gateway_card');
    var type;
    var form = jQuery('form.checkout');
    var iframe = jQuery('#TrustPayFrame');

    if (method_radio.is(':checked') && (wire_radio.is(':checked') || card_radio.is(':checked'))) {
        if (wire_radio.is(':checked')) {
            type = wire_radio.val();
        } else {
            type = card_radio.val();
        }
    }

    iframe.load(function () {
        try {
            originalOpenPopup();
        }
        catch (err) {
            console.error(err);
        }
    });

    form.addClass('processing');

    jQuery.ajax({
        type: 'POST',
        url: wc_checkout_params.checkout_url + '&get_trustpay_type_url=' + type,
        data: form.serialize(),
        dataType: 'json',
        async: false,
        success: function (result) {
            form.removeClass('processing');

            if ('success' === result.result && result.redirect) {
                iframe.attr('src', result.redirect);
            } else {
                jQuery('.woocommerce-NoticeGroup-checkout, .woocommerce-error, .woocommerce-message').remove();

                var error_message;

                if (result.messages) {
                    error_message = result.messages;
                } else {
                    error_message = '<div class="woocommerce-error">' + wc_checkout_params.i18n_checkout_error + '</div>';
                }

                form.prepend('<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout">' +
                    error_message +
                    '</div>');
                jQuery.scroll_to_notices(form);
            }
        }
    });
}
