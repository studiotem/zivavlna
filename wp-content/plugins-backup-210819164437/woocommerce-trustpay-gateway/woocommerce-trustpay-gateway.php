<?php

/**
 * Plugin Name: Woocommerce Trust Pay Gateway
 * Plugin URI:
 * Description: This plugin add the payment gateway which allow you to charge the Credit Card and make Instant bank transfer. <a href="https://www.trustpay.eu/">www.trustpay.eu</a>
 * Version: 1.0.3
 */

define('TRUSTPAY_DIR_NAME', 'woocommerce-trustpay-gateway');
define('TRUSTPAY_PATH', ABSPATH . '/wp-content/plugins/' . TRUSTPAY_DIR_NAME);

add_action( 'plugins_loaded', 'init_trustpay_gateway_class', 11 );
add_filter( 'woocommerce_payment_gateways', 'add_trustpay_gateway_class' );
add_filter( 'woocommerce_order_button_html', 'tp_custom_button_html' );

function init_trustpay_gateway_class() {
    require_once TRUSTPAY_PATH . '/src/class-wc-trustpay-gateway.php';
    require_once TRUSTPAY_PATH . '/src/locale.php';
}

function add_trustpay_gateway_class( $methods ) {
    $methods[] = 'WC_Trustpay_Gateway';
    return $methods;
}

function loadTrustPayLibrary() {
    require_once TRUSTPAY_PATH . '/src/lib/Payment.php';
    require_once TRUSTPAY_PATH . '/src/lib/PaymentWire.php';
    require_once TRUSTPAY_PATH . '/src/lib/PaymentCard.php';
    require_once TRUSTPAY_PATH . '/src/lib/Order.php';
    require_once TRUSTPAY_PATH . '/src/lib/PaymentType.php';
    require_once TRUSTPAY_PATH . '/src/lib/Signature.php';
    require_once TRUSTPAY_PATH . '/src/lib/Response.php';
    require_once TRUSTPAY_PATH . '/src/lib/ResponseWire.php';
    require_once TRUSTPAY_PATH . '/src/lib/ResponseCard.php';
    require_once TRUSTPAY_PATH . '/src/lib/ResultCodes.php';
}

function tp_custom_button_html( $button_html ) {
    $button_html = str_replace('class="', 'class="show-popup ', $button_html);
    return $button_html;
}
