<?php
use Trustpay\Order;
use Trustpay\Payment;
use Trustpay\PaymentWire;
use Trustpay\PaymentCard;
use Trustpay\PaymentType;
use Trustpay\ResultCodes;

class WC_Trustpay_Gateway extends WC_Payment_Gateway {
    
    private $live_account_id;
    private $live_secret_key;
    private $test_account_id;
    private $test_secret_key;
    private $sandbox;
    private $methods;
    private $currency;
    private $locale;
    private $show_logo;
    private $iframe;
    
    private $card_accepted_currencies = [
        'AUD',
        'CAD',
        'CZK',
        'DKK',
        'EUR',
        'GBP',
        'HRK',
        'HUF',
        'JPY',
        'NOK',
        'PLN',
        'RON',
        'SEK',
        'USD',
    ];

    public function __construct() {
        loadTrustPayLibrary();

        $this->id                   = 'trustpay_gateway';
        $this->icon                 = apply_filters( 'woocommerce_trustpay_icon', '' );
        $this->has_fields           = true;
        $this->method_title         = _x( 'Trust Pay', 'woocommerce' );
        $this->method_description   = __( 'Instant bank transfer and card payments. ', 'woocommerce' );

        $this->init_form_fields();
        $this->init_settings();

        $this->title                = $this->get_option( 'title' );
        $this->sandbox              = $this->get_option( 'sandbox' );
        
        $this->live_account_id      = $this->get_option( 'live_account_id' );
        $this->live_secret_key      = $this->get_option( 'live_secret_key' );
        $this->test_account_id      = $this->get_option( 'test_account_id' );
        $this->test_secret_key      = $this->get_option( 'test_secret_key' );
        $this->methods              = $this->get_option( 'methods' );
        $this->currency             = $this->get_option( 'currency' );
        $this->locale               = $this->get_option( 'locale' );
        $this->show_logo            = $this->get_option( 'show_logo' );
        $this->iframe               = $this->get_option( 'iframe' );

        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id,      array( $this, 'process_admin_options' ) );
        add_action( 'woocommerce_api_wc_gateway_trustpay_' . Payment::ENDPOINT_WIRE, array( $this, 'callback_wire' ) );
        add_action( 'woocommerce_api_wc_gateway_trustpay_' . Payment::ENDPOINT_CARD, array( $this, 'callback_card' ) );
        add_action( 'woocommerce_before_checkout_form',                              array( $this, 'checkout_response_error' ) );
        add_action( 'before_woocommerce_pay',                                        array( $this, 'checkout_response_error' ) );
    }
    
    public function isCardAcceptedCurrency($currency)
    {
        return in_array($currency, $this->card_accepted_currencies);
    }

    public function init_form_fields() {
        $this->form_fields = array(
            'enabled' => array(
                'title' => __( 'Enable/Disable', 'woocommerce' ),
                'type' => 'checkbox',
                'label' => __( 'Enable Trust Pay Payment', 'woocommerce' ),
                'default' => 'yes'
            ),
            'title' => array(
                'title' => __( 'Title', 'woocommerce' ),
                'type' => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
                'default' => __( 'Trust Pay', 'woocommerce' ),
                'desc_tip'      => true,
            ),
            'sandbox' => array(
                'title' => __( 'Sandbox', 'woocommerce' ),
                'type' => 'checkbox',
                'label' => __( 'Enable Sandbox', 'woocommerce' ),
                'default' => 'no'
            ),
            
            'live_account_id' => array(
                'title' => __( 'Live Account ID/Project ID', 'woocommerce' ),
                'type' => 'text',
                'default' => '',
            ),
            'live_secret_key' => array(
                'title' => __( 'Live Secret key', 'woocommerce' ),
                'type' => 'text',
                'default' => '',
            ),
            'test_account_id' => array(
                'title' => __( 'Test Account ID/Project ID', 'woocommerce' ),
                'type' => 'text',
                'default' => '',
            ),
            'test_secret_key' => array(
                'title' => __( 'Test Secret key', 'woocommerce' ),
                'type' => 'text',
                'default' => '',
            ),
            'methods' => array(
                'title' => __( 'Payment Methods', 'woocommerce' ),
                'type' => 'select',
                'defaul' => 'both',
                'options' => array(
                    'both' => __( 'Both', 'woocommerce' ),
                    'bank' => __( 'Bank Tranfer', 'woocommerce' ),
                    'card' => __( 'Credit Card', 'woocommerce' ),
                ),
            ),
            'currency' => array(
                'title' => __( 'Accepted Currency', 'woocommerce' ),
                'type' => 'select',
                'defaul' => 'EUR',
                'options' => array(
                    'EUR' => __( 'EUR', 'woocommerce' ),
                    'CZK' => __( 'CZK', 'woocommerce' ),
                ),
            ),
            'locale' => array(
                'title' => __( 'Localisation', 'woocommerce' ),
                'type' => 'select',
                'defaul' => 'EUR',
                'options' => array(
                    'en' => __( 'English', 'woocommerce' ),
                    'sk' => __( 'Slovensky', 'woocommerce' ),
                    'cs' => __( 'Česky', 'woocommerce' ),
                ),
            ),
            'show_logo' => array(
                'title' => __( 'Show Logos', 'woocommerce' ),
                'type' => 'checkbox',
                'label' => __( 'Show Logos in checkout confirmation options', 'woocommerce' ),
                'default' => 'yes'
            ),
            'iframe' => array(
                'title' =>  __( 'Iframe', 'woocommerce' ) ,
                'type' => 'checkbox',
                'label' => __( 'Please, read the <a href="https://doc.trustpay.eu" target="_blank">documentation</a> before switching iframe option on, or changing the standard woocommerce checkout', 'woocommerce' ),
                'default' => 'yes'
             ),
        );
    }

    public function checkout_response_error() {
        $error = false;

        if(isset($_GET['AcquirerResponseId']) && getTrustPayTranslation($_GET['AcquirerResponseId'], $this->getLocale())) {
            $error = getTrustPayTranslation($_GET['AcquirerResponseId'], $this->getLocale());
        }

        if ($error) { ?>
            <div class="woocommerce-error"><?php echo $error; ?></div>
            <?php
        }
    }

    public function payment_fields() {
        if(get_woocommerce_currency() == $this->getCurrency() || $this->isCardAcceptedCurrency(get_woocommerce_currency())) {
            ?>
            <fieldset id="<?php echo esc_attr($this->id); ?>_form" class='wc-payment-form'>
                <?php if(in_array($this->methods, ['both','bank']) && get_woocommerce_currency() == $this->getCurrency()): ?>
                <p class="form-row">
                    <label>
                        <input
                                type="radio"
                                name="trustpay_gateway_method"
                                value="<?php echo Payment::ENDPOINT_WIRE; ?>"
                                id="payment_method_trustpay_gateway_wire"
                                checked="checked"
                        />
                        <?php echo getTrustPayTranslation('bank_tranfer', $this->getLocale()); ?>
                        <?php if ($this->getShowLogo()) { ?>
                            <img src="<?php echo plugins_url(TRUSTPAY_DIR_NAME . '/assets/images/online-banking.png'); ?>" alt="" style="float:right"/>
                        <?php } ?>
                    </label>
                </p>
                <?php endif;?>
                <?php if(in_array($this->methods, ['both','card']) && $this->isCardAcceptedCurrency(get_woocommerce_currency())): ?>
                <p class="form-row">
                    <label>
                        <input
                                type="radio"
                                name="trustpay_gateway_method"
                                value="<?php echo Payment::ENDPOINT_CARD; ?>"
                                id="payment_method_trustpay_gateway_card"
                                <?php if(in_array($this->methods, ['card'])): ?>
                                    checked="checked"
                                <?php endif;?>
                        />
                        <?php echo getTrustPayTranslation('credit_card', $this->getLocale()); ?>
                        <?php if ($this->getShowLogo()) { ?>
                            <img src="<?php echo plugins_url(TRUSTPAY_DIR_NAME . '/assets/images/visa-master_card-maestro.png'); ?>" alt="" style="float:right"/>
                        <?php } ?>
                    </label>
                </p>
                <?php endif;?>
                <p></p>
            </fieldset>

            <?php if ( $this->getIframe() ) { ?>
                <script src="<?php echo plugins_url(TRUSTPAY_DIR_NAME . '/assets/js/tp-mapi.js'); ?>"></script>
                <script src="<?php echo plugins_url(TRUSTPAY_DIR_NAME . '/assets/js/trustpay.js'); ?>"></script>
                <iframe id="TrustPayFrame" style="width:0;height:0;border:none"></iframe>
            <?php } ?>
            <?php
        } else {
            ?>
            <div style="color:red"><?php echo __('Currency not supported', 'woocommerce');?>: <?php echo get_woocommerce_currency(); ?></div>
            <?php
        }
    }

    public function process_payment( $order_id ) {
        $order = new WC_Order( $order_id );

        if($paymentMethod = $this->getPaymentMethod()) {

            if(empty($_GET['get_trustpay_type_url'])) {
                global $woocommerce;
                $woocommerce->cart->empty_cart();

                if($order->get_status() != 'pending') {
                    $order->update_status('pending');
                }
            }

            $payment = $this->initPayment($order, $paymentMethod);

            return array(
                'result' => 'success',
                'redirect' => $payment->getUrl(PaymentType::PURCHASE)
            );
        } else {
            $order->update_status('failed');
            wc_add_notice(  'Choose Payment Method!', 'error' );
            return false;
        }
    }

    public function callback_wire() {
        $this->checkResponse('\Trustpay\ResponseWire', 'wire');
    }

    public function callback_card() {
        $this->checkResponse('\Trustpay\ResponseCard', 'card');
    }
    
    protected function checkResponse($class, $type) {
        $response = new $class($this->getSecretKey());
        $response->initData($_GET);

        if($response->validateSignature()) {
            $this->changeStatus(
                $response->getReference(),
                $response->getResultCode(),
                $response->getTransactionId(),
                $type
            );
        }

        exit;
    }

    protected function changeStatus($order_id, $resultCode, $transaction_id, $type) {
        
        $order = new WC_Order( $order_id );

        switch ($resultCode) {
            case ResultCodes::SUCCESS:
                $order->payment_complete($transaction_id);

                break;
            case ResultCodes::AUTHORIZED:
                if($type === 'card') $order->payment_complete($transaction_id);
                if($type === 'wire') $order->update_status('on-hold', __('Awaiting transaction', 'woocommerce'));

                break;
            case ResultCodes::PROCESSING:
                $order->update_status('on-hold', __('Awaiting transaction', 'woocommerce'));

                break;
        }
    }

    protected function getPaymentMethod() {
        $method = false;

        if(
                isset($_POST['trustpay_gateway_method']) &&
                in_array($_POST['trustpay_gateway_method'],
                    [
                        Payment::ENDPOINT_WIRE,
                        Payment::ENDPOINT_CARD
                    ]
                )
        ) {
            $method = $_POST['trustpay_gateway_method'];
        }

        return $method;
    }

    protected function initPayment($order, $paymentMethod) {
        $trustPayOrder = new Order();
        $trustPayOrder->setId($order->get_id());
        $trustPayOrder->setCurrency($order->get_currency());
        $trustPayOrder->setTotal($order->get_total());

        if($paymentMethod == Payment::ENDPOINT_WIRE) {
            $payment = new PaymentWire($this->getAccountId(), $this->getSecretKey());
        } else {
            $payment = new PaymentCard($this->getAccountId(), $this->getSecretKey());

            $trustPayOrder->setBillingCity($order->get_billing_city());
            $trustPayOrder->setBillingCountry($order->get_billing_country());
            $trustPayOrder->setBillingPostcode($order->get_billing_postcode());
            $trustPayOrder->setBillingStreet($order->get_billing_address_1());
            $trustPayOrder->setCardHolder($order->get_formatted_billing_full_name());
            $trustPayOrder->setEmail($order->get_billing_email());
        }


        if($this->getIframe()) {
            $cancelUrl = wc_get_checkout_url();
            $redirect = "false";
        } else {
            $cancelUrl = $order->get_checkout_payment_url();
            $redirect = "true";
        }
        error_log("----IS1:".$redirect);

        $payment->setOrder($trustPayOrder)
            ->setIsRedirect($redirect)
            ->setSandbox($this->getSandbox())
            ->setNotificationUrl(get_site_url() . '?wc-api=wc_gateway_trustpay_' . $paymentMethod)
            ->setReturnUrl($this->get_return_url( $order ))
            ->setCancelUrl($cancelUrl)
            ->setErrorUrl($cancelUrl)
            ->setLocalization($this->getLocale())
            ->setPaymentMethod($paymentMethod);

        return $payment;
    }

    private function getCurrency() {
        return $this->currency;
    }

    private function getAccountId() {
        return $this->getSandbox() ? $this->test_account_id : $this->live_account_id;
    }

    private function getSecretKey() {
        return $this->getSandbox() ? $this->test_secret_key : $this->live_secret_key;
    }

    private function getSandbox() {
        return $this->sandbox == 'yes' ? true : false;
    }

    private function getLocale() {
        return $this->locale;
    }

    private function getShowLogo() {
        return $this->show_logo == 'yes' ? true : false;
    }

    private function getIframe() {
        return $this->iframe == 'yes' ? true : false;
    }
}
