<?php
namespace Trustpay;

class PaymentType {
    const PURCHASE = 0;

    const CARD_ON_FILE_REGISTER = 1;

    const CARD_ON_FILE_PURCHASE = 2;

    const RECURRING_INITIAL_PAYMENT = 3;

    const RECURRING_SUBSEQUENT_PAYMENT = 4;

    const PREAUTHORIZATION = 5;

    const CAPTURE = 6;

    const REFUND = 8;

    const CARD_ON_FILE_PREAUTHORIZATION = 9;
}