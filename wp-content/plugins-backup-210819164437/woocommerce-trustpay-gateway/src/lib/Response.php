<?php
namespace Trustpay;

abstract class Response {
    protected $accountId;
    protected $amount;
    protected $authNumber;
    protected $cardExpiration;
    protected $cardId;
    protected $cardMask;
    protected $counterAccount;
    protected $counterAccountName;
    protected $currency;
    protected $orderId;
    protected $paymentId;
    protected $paymentRequestId;
    protected $reference;
    protected $resultCode;
    protected $signature;
    protected $type;

    protected $SignatureData;

    abstract public function getTransactionId();
    abstract public function validateSignature();
    abstract protected function getSigData();

    public function __construct($secretKey) {
        $this->SignatureData = new Signature($secretKey);
    }

    public function initData($data) {
        $variables = array(
            'accountId',
            'amount',
            'authNumber',
            'cardExpiration',
            'cardId',
            'cardMask',
            'counterAccount',
            'counterAccountName',
            'currency',
            'orderId',
            'paymentId',
            'paymentRequestId',
            'reference',
            'resultCode',
            'signature',
            'type'
        );

        foreach ($variables as $variable) {
            if(isset($data[ucfirst($variable)])) {
                $this->$variable = $data[ucfirst($variable)];
            }
        }
    }

    public function getReference() {
        return $this->reference;
    }

    public function getResultCode() {
        return $this->resultCode;
    }
}