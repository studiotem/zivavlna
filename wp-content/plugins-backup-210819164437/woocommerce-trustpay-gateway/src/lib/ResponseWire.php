<?php
namespace Trustpay;

class ResponseWire extends Response {
    public function getTransactionId() {
        return $this->paymentId;
    }

    public function validateSignature() {
        return $this->signature == $this->SignatureData->getSignature($this->getSigData());
    }

    protected function getSigData() {
        $data = array();

        $data[] = $this->accountId;
        $data[] = $this->amount;
        $data[] = $this->currency;
        $data[] = $this->type;
        $data[] = $this->resultCode;

        if($this->counterAccount) {
            $data[] = $this->counterAccount;
        }

        if($this->counterAccountName) {
            $data[] = $this->counterAccountName;
        }

        if($this->orderId) {
            $data[] = $this->orderId;
        }

        $data[] = $this->paymentId;
        $data[] = $this->reference;

        return $data;
    }
}