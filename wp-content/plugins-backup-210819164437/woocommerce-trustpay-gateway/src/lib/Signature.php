<?php
namespace Trustpay;

class Signature {
    protected $secret_key;

    public function __construct($secret_key) {
        $this->secret_key = $secret_key;
    }

    public function getSignature($data){
        return strtoupper(
            hash_hmac(
                'sha256',
                pack('A*', $this->getMessage($data)),
                pack('A*', $this->secret_key)
            )
        );
    }

    protected function getMessage($data) {
        return implode('/', $data);
    }
}