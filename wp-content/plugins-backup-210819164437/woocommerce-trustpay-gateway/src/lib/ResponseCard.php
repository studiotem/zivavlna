<?php
namespace Trustpay;

class ResponseCard extends Response {
    public function getTransactionId() {
        return $this->paymentRequestId;
    }

    public function validateSignature() {
        return $this->signature == $this->SignatureData->getSignature($this->getSigData());
    }

    protected function getSigData() {
        $data = array();

        $data[] = $this->accountId;
        $data[] = $this->amount;
        $data[] = $this->currency;
        $data[] = $this->reference;
        $data[] = $this->type;
        $data[] = $this->resultCode;
        $data[] = $this->paymentRequestId;

        if($this->cardId) {
            $data[] = $this->cardId;
        }

        if($this->cardMask) {
            $data[] = $this->cardMask;
        }

        if($this->cardExpiration) {
            $data[] = $this->cardExpiration;
        }

        if($this->authNumber) {
            $data[] = $this->authNumber;
        }

        return $data;
    }
}