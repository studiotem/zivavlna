<?php
namespace Trustpay;

class Payment {
    protected $account_id;
    protected $cancelUrl;
    protected $errorUrl;
    protected $localization;
    protected $notificationUrl;
    /* @var Order */
    protected $order;
    protected $paymentMethod;
    protected $returnUrl;
    protected $sandbox;
    protected $secret_key;
    protected $isRedirect;

    protected $Signature;

    const ENDPOINT_CARD = 'card';
    const ENDPOINT_WIRE = 'wire';
    
    /*
    const URL_CARD_LIVE_MERCHANT_PORTAL = "https://amapi.trustpay.eu/mapi5/Card/PayPopup";
    const URL_CARD_TEST_MERCHANT_PORTAL = "https://playground.trustpay.eu/mapi5/Card/PayPopup";

    const URL_WIRE_LIVE_MERCHANT_PORTAL = 'https://amapi.trustpay.eu/mapi5/wire/paypopup';
    const URL_WIRE_TEST_MERCHANT_PORTAL = 'https://playground.trustpay.eu/mapi5/wire/paypopup';
    
    const URL_CARD_LIVE_INTERNET_BANKING = "https://mapi.trustpay.eu/mapi5/Card/PayPopup";
    const URL_CARD_TEST_INTERNET_BANKING = "https://playground.trustpay.eu/mapi5/Card/PayPopup";

    const URL_WIRE_LIVE_INTERNET_BANKING = 'https://mapi.trustpay.eu/mapi5/wire/paypopup';
    const URL_WIRE_TEST_INTERNET_BANKING = 'https://playground.trustpay.eu/mapi5/wire/paypopup';
    */
    const URL_CARD_LIVE_MERCHANT_PORTAL = "https://amapi.trustpay.eu/mapi5/Card/PayPopup";
    const URL_CARD_TEST_MERCHANT_PORTAL = "https://amapi.trustpay.eu/mapi5/Card/PayPopup";

    const URL_WIRE_LIVE_MERCHANT_PORTAL = 'https://amapi.trustpay.eu/mapi5/wire/paypopup';
    const URL_WIRE_TEST_MERCHANT_PORTAL = 'https://amapi.trustpay.eu/mapi5/wire/paypopup';
    
    public function __construct($account_id, $secret_key) {
        $this->account_id = $account_id;
        $this->secret_key = $secret_key;
    }

    public function setCancelUrl($url) {
        $this->cancelUrl = $url;
        return $this;
    }

    public function setErrorUrl($url) {
        $this->errorUrl = $url;
        return $this;
    }

    public function setNotificationUrl($url) {
        $this->notificationUrl = $url;
        return $this;
    }

    public function setOrder(Order $order) {
        $this->order = $order;
        return $this;
    }

    public function setPaymentMethod($paymentMethod) {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    public function setReturnUrl($url) {
        $this->returnUrl = $url;
        return $this;
    }

    public function setSandbox($sandbox) {
        $this->sandbox = $sandbox;
        return $this;
    }

    public function setLocalization($localization) {
        $this->localization = $localization;
        return $this;
    }

    public function setIsRedirect($redirect) {
        $this->isRedirect = $redirect;
        return $this;
    }

    protected function getBaseUrl() {
        
        if($this->paymentMethod == self::ENDPOINT_WIRE) {
            return $this->sandbox ? self::URL_WIRE_TEST_MERCHANT_PORTAL : self::URL_WIRE_LIVE_MERCHANT_PORTAL;

        } elseif($this->paymentMethod == self::ENDPOINT_CARD) {
            return $this->sandbox ? self::URL_CARD_TEST_MERCHANT_PORTAL : self::URL_CARD_LIVE_MERCHANT_PORTAL;

        } else {
            throw new \Exception("Invalid Payment Method.");
        }
    }

    protected function getSignature() {
        if(!$this->Signature) {
            $this->Signature = new Signature($this->secret_key);
        }

        return $this->Signature;
    }

    protected function getIsRedirect() {
        return $this->isRedirect;
    }
}