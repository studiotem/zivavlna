<?php

function woocommerce_zasilkovna_shipping_init(){


if ( !class_exists( 'WC_Shipping_Method' ) ) 
      return;



 
		if ( ! class_exists( 'WC_Zasilkovna_Shipping_Method' ) ) {
			class WC_Zasilkovna_Shipping_Method extends WC_Shipping_Method {
				

                private $zasilkovna_option;

                private $zasilkovna_prices;
                
                private $zasilkovna_services;

                /**
				 * Constructor for your shipping class
				 *
				 * @access public
				 * @return void
				 */
				public function __construct( $instance_id = 0 ) {
        
                    $licence_status = get_option('woo-zasilkovna-licence');
                    if ( empty( $licence_status ) ) {
	                   return false;
                    }
        
        
					$this->id                 = 'zasilkovna'; 
                    $this->instance_id        = absint( $instance_id );
					$this->enabled            = "yes";

                    $this->supports           = array(
                        'shipping-zones',
                        'instance-settings',
                        'instance-settings-modal',
                    );

					$this->init();
                    $this->method_title     = __( 'Zásilkovna', 'zasilkovna' );
                    
					$this->method_description = $this->zasilkovna_title;
          
				}
 
				/**
				 * Init your settings
				 *
				 * @access public
				 * @return void
				 */
				function init() {
					// Load the settings API
					$this->init_form_fields();
					$this->init_settings(); 
          
                    $this->availability     = $this->get_option( 'availability' );
                    $this->countries 	    = $this->get_option( 'countries' );         
                    $this->cost             = $this->get_option( 'cost_zasilkovna' );
                    $this->cost_dobirka     = $this->get_option( 'cost_dobirka' );
                    $this->zasilkovna_title = $this->get_option( 'zasilkovna_title' );
                    $this->title            = $this->get_option( 'title' );
          
          
					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );

                    $this->zasilkovna_option   = get_option( 'zasilkovna_option' );
                    $this->zasilkovna_prices   = get_option( 'zasilkovna_prices' );
                    $this->zasilkovna_services = get_option( 'zasilkovna_services' );


				}
        
        
                function init_form_fields() {
					
					$this->instance_form_fields = array(
						'enabled' => array(
							'title'       => __( 'Povolit Zásilkovnu', 'zasilkovna' ),
							'type' 	      => 'checkbox',
							'label'       => __( 'Povolit tento způsob', 'zasilkovna' ),
							'default'     => 'no',
							),
						'title' => array(
							'title'       => __( 'Název dopravce', 'zasilkovna' ),
							'type'        => 'text',
							'description' => __( 'Název dopravce', 'zasilkovna' ),
							'default'     => __( 'Zásilkovna', 'zasilkovna' ),
							),
						'zasilkovna_title' => array(
							'title'       => __( 'Zásilkovna titulek', 'zasilkovna' ),
							'type'        => 'text',
							'description' => __( 'Text, který zákazník uvidí', 'zasilkovna' ),
							'default'     => __( 'Zásilkovna','zasilkovna' ),
						)
					);

				}
        
        
 
				/**
				 * calculate_shipping function.
				 *
				 */
				public function calculate_shipping( $package = array() ) {
        
                    $disable_product = Zasilkovna_Helper::is_disable_product_in_cart(); 
                    if( $disable_product === true ){ 
          
                        $rates = array(
                            'id'       => $this->id.'>charita',
                            'label'    => __('Zásilkovna', 'zasilkovna'),
                            'cost'     => 0,
                            'calc_tax' => 'per_order'
                        );
                        // Register the rate
                        $this->add_rate( $rates );
          
                    }else{
        
                        $zasilkovna_option   = get_option( 'zasilkovna_option' );
                        $zasilkovna_prices   = get_option( 'zasilkovna_prices' );
                        $zasilkovna_services = get_option( 'zasilkovna_services' );
          
                        $country = toret_get_customer_country();
                        $weight  = Zasilkovna_Helper::get_weight( WC()->cart->cart_contents_weight );
                        
                        $komplet_data = Zasilkovna_Helper::komplet_data();

                        $moje_country = strtolower($country);
                        if(('cz'== $moje_country)||('hu' == $moje_country)||('pl' == $moje_country)||('ro' == $moje_country)||('sk' == $moje_country)){
                            $cost = false;

                            if( $weight < 5 ){
                                $cost = Zasilkovna_Helper::isset_shipping( $zasilkovna_prices, 'zasilkovna-'.$moje_country.'-5kg' );
                            }elseif($weight > 4.99 && $weight < 10){
                                $cost = Zasilkovna_Helper::isset_shipping( $zasilkovna_prices, 'zasilkovna-'.$moje_country.'-10kg' );
                            }elseif($weight > 9.99 && $weight < 20){
                                $cost = Zasilkovna_Helper::isset_shipping( $zasilkovna_prices, 'zasilkovna-'.$moje_country.'-20kg' );
                            }elseif($weight > 19.99 && $weight < 30.01){
                                $cost = Zasilkovna_Helper::isset_shipping( $zasilkovna_prices, 'zasilkovna-'.$moje_country.'-30kg' );
                            }else{ 
                                $cost = '';
                            }
        
                            //Blokování Zásilkovny pro váhy na 10 Kg, pokud není povoleno v adminu
                            if( $weight > 10 ){
                                if( empty( $this->zasilkovna_option['max_weight'] ) ){
                                    return; 
                                }
                            }
        
                            if(!empty($cost)){
                                if( !empty( $zasilkovna_prices['zasilkovna-'.$moje_country.'-free'] ) ){
            
                                    $free = $zasilkovna_prices['zasilkovna-'.$moje_country.'-free'];
            
                                    //WooCommerce Multilingual compatibility
                                    $filtered_free = apply_filters( 'wcml_raw_price_amount', $free );
                                    if( !empty( $filtered_free ) ){ $free = $filtered_free; }
            
                                    //WooCommerce currency Switcher compatibility
                                    $filtered_free = apply_filters('woocs_exchange_value', $free);
                                    if( !empty( $filtered_free ) ){ $free = $filtered_free; }
            
                                    //WooCommerce currency Switcher compatibility
                                    $free = apply_filters('zasilkovana_free_shipping_filter', $free);
            
                                    $subtotal = WC()->cart->get_subtotal();
            
                                    if( !empty( $subtotal ) && $subtotal > $free ){
                                        $cost = 0;
                                    }
            
                                } 
            
                                $blokace = 0;
                                $items_blokace_veku = WC()->cart->get_cart();
                                foreach ( $items_blokace_veku as $item_ov ) {
                                    $product_id = $item_ov['data']->get_id();

                                    $product_kont = wc_get_product( $product_id );
                                    if ( $product_kont->is_type( 'variable' ) ) {
                                        $product_id = $product_kont->get_parent_id();  
                                    }


                                    $blokace_produkt = get_post_meta($product_id, '_zasilkovna_vypnuti', true); 
            
                                    if(!empty($blokace_produkt) && ($blokace_produkt == 'yes')){
                                        $blokace = 1;
                                    }
                                }

                                //Enable custom change price by filter 
                                $cost = apply_filters( 'zasilkovna_shipping_cost', $cost, $moje_country, $weight );
            
                                if( $cost === false ){ return false; }                  
            
                                if($cost == 0){
                                    $text = __('Zdarma', 'zasilkovna');
                                    $label = $this->zasilkovna_title . ': ' . apply_filters( 'zasilkovna_free_shipping_label', $text );
                                }else{
                                    $label = $this->zasilkovna_title;
                                }

                                $lic  = get_option( 'woo-zasilkovna-licence' );
                                if($lic == 'active'){

                                    if($blokace == 0){
                                        $rates = array(
                                            'id'       => $this->id.'>z-points',
                                            'label'    => $label,
                                            'cost'     => $cost,
                                            'calc_tax' => 'per_order'
                                        );
                                        // Register the rate
                                        $this->add_rate( $rates );
                                    }
                                }
                            }
                        }

                        foreach($komplet_data as $key => $data){
                            if($data['stat'] == $country){
                                if($data['deklarace'] != 1){
                                    $hmotnosti = (!empty($data['hmotnosti']) ? $data['hmotnosti'] : array(1,2,5,10));
                                    if($weight < max($hmotnosti)){
                                        $cost = 999999;
                                        foreach($hmotnosti as $hmotnost){
                                            if($weight < $hmotnost){
                                                $cost = Zasilkovna_Helper::isset_shipping( $zasilkovna_prices, $data['slug'] . '-' . sanitize_title($hmotnost) . 'kg' );    
                                                break;
                                            }
                                        }
                                        if( $weight > 10 ){
                                            if( empty( $this->zasilkovna_option['max_weight'] ) ){
                                                return; 
                                            }
                                        }
                                        if(!empty($cost)){
                                            if(!empty($zasilkovna_services['service-active-'.$key])){
                        
                                                if( !empty( $zasilkovna_prices[$data['slug'] . '-free'] ) ){
                            
                                                    $free = $zasilkovna_prices[$data['slug'] . '-free'];
                            
                                                    //WooCommerce Multilingual compatibility
                                                    $filtered_free = apply_filters( 'wcml_raw_price_amount', $free );
                                                    if( !empty( $filtered_free ) ){ $free = $filtered_free; }
                            
                                                    //WooCommerce currency Switcher compatibility
                                                    $filtered_free = apply_filters('woocs_exchange_value', $free);
                                                    if( !empty( $filtered_free ) ){ $free = $filtered_free; } 
                            
                                                    //WooCommerce currency Switcher compatibility
                                                    $free = apply_filters('zasilkovana_free_shipping_filter', $free);
                            
                                                    $subtotal = WC()->cart->get_subtotal();
                            
                                                    if( !empty( $subtotal ) && $subtotal > $free ){
                                                        $cost = 0;
                                                    }
                            
                                                }
                            
                                                //Enable custom change price by filter 
                                                $cost = apply_filters( $data['slug'] . '_shipping_cost', $cost, $country, $weight );
                            
                                                if( $cost === false ){ return false; }                  
                            
            
                                                $blokace = 0;
                                                $items_blokace_veku = WC()->cart->get_cart();
                                                foreach ( $items_blokace_veku as $item_ov ) {
                                                    $product_id = $item_ov['data']->get_id();
                            
                                                    $blokace_produkt = get_post_meta($product_id, '_' . $key . '_vypnuti', true); 
                            
                                                    if(!empty($blokace_produkt) && ($blokace_produkt == 'yes')){
                                                        $blokace = 1;
                                                    }
                                                }             
            
                                                if($cost == 0){
                                                    $text = __('Zdarma', 'zasilkovna');
                                                    $label = $zasilkovna_services['service-label-' . $key] . ': ' . apply_filters( 'zasilkovna_free_shipping_label', $text );
                                                }else{
                                                    $label = $zasilkovna_services['service-label-' . $key];
                                                }

                                                $lic  = get_option( 'woo-zasilkovna-licence' );
                                                if($lic == 'active'){
                                                
                                                    if($blokace == 0){
                                                        $rates = array(
                                                            'id'       => $this->id.'>' . $data['slug'],
                                                            'label'    => $label,
                                                            'cost'     => $cost,
                                                            'calc_tax' => 'per_order'
                                                        );
                                                        // Register the rate
                                                        $this->add_rate( $rates );
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    } 
          
				}                
  
			}//End class
		}
	
} 
add_action('plugins_loaded', 'woocommerce_zasilkovna_shipping_init');
 
	function add_woo_zasilkovna_shipping_method( $methods ) {
		$methods['zasilkovna'] = 'WC_Zasilkovna_Shipping_Method';
		return $methods;
	}
    add_filter( 'woocommerce_shipping_methods', 'add_woo_zasilkovna_shipping_method' );


  

?>