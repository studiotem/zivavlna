<div class="wrap zasilkovna-wrap">
  <h2><?php _e('Zásilkovna nastavení','zasilkovna'); ?></h2>
  
  
<?php 
//Save main setting
$this->save_setting();


$komplet_data = Zasilkovna_Helper::komplet_data();
$zasilkovna_kde = Zasilkovna_Helper::zasilkovna_kde();

$staty = array();
$staty_kont = array();
foreach($komplet_data as $key => $data){
    $staty[$data['stat']] = $data['statnazev'];
    $staty_kont[$data['stat']] = sanitize_title($data['statnazev']);
}
asort($staty_kont);
/**
 * Save country currency
 *
 */  

if( isset( $_POST['country_currency'] ) ){ 

    $country_currency = array();
    $default_array = array(
            'country_currency_bg',
            'country_currency_hu',
            'country_currency_de',
            'country_currency_pl',
            'country_currency_at',
            'country_currency_ru',
            'country_currency_sk'
        );


        foreach( $default_array as $item ){
            if( !empty( $_POST[$item] ) ){  
                $country_currency[$item] = sanitize_text_field( $_POST[$item] );
            }else{
                $country_currency[$item] = 'CZK';   
            }
        }

        if( !empty( $_POST['country_currency_deactivate'] ) ){  
            $country_currency['country_currency_deactivate'] = sanitize_text_field( $_POST['country_currency_deactivate'] );
        }
        
    update_option( 'zasilkovna_country_currency', $country_currency );

}

if( !empty( $_GET['pobocky'] ) ){

    Zasilkovna_Pobocky::get_service_branches(  $_GET['pobocky'] );
  
  }

/**
 * Save services data
 *
 */  
if(isset($_POST['services'])){ 

    $zasilkovna_services = array();
        foreach( Zasilkovna_Helper::komplet_data() as $key => $service ){
            if(!empty($_POST['service-label-'.$key])){  
              $zasilkovna_services['service-label-'.$key]  = $_POST['service-label-'.$key]; 
              //WPML save option fix
              if (function_exists ( 'icl_register_string' )){
                icl_register_string( 'Zasilkovna', $zasilkovna_services['service-label-'.$key], $_POST['service-label-'.$key] );
              }
            }
            if(!empty($_POST['service-active-'.$key])){ $zasilkovna_services['service-active-'.$key] = $_POST['service-active-'.$key]; }  
            if(!empty($_POST['service-poradi'])){ $zasilkovna_services['service-poradi'] = $_POST['service-poradi']; }          
        }

    update_option('zasilkovna_services',$zasilkovna_services);
    //wp_redirect(admin_url().'admin.php?page=control_xml');
}

//Save services price
$this->save_service_prices();

//Get and save branches
if(isset($_GET['check'])){

    Zasilkovna_Pobocky::get_branches();

}

$zasilkovna_mista     = get_option( 'zasilkovna_mista');
$zasilkovna_option    = get_option( 'zasilkovna_option');
$zasilkovna_mista_cz  = get_option( 'zasilkovna_mista_cz');
$zasilkovna_mista_sk  = get_option( 'zasilkovna_mista_sk');
$zasilkovna_mista_hu  = get_option( 'zasilkovna_mista_hu');
$zasilkovna_mista_pl  = get_option( 'zasilkovna_mista_pl');
$zasilkovna_mista_ro  = get_option( 'zasilkovna_mista_ro');

$zasilkovna_countries  = get_option( 'zasilkovna_countries' );

$licence_key  = get_option( 'woo-zasilkovna-licence-key' );
$lic  = get_option( 'woo-zasilkovna-licence' );
$licence_info = get_option( 'woo-zasilkovna-info' );

$zasilkovna_services = get_option('zasilkovna_services');
$country_currency = get_option('zasilkovna_country_currency');
 
$zasilkovna_prices = get_option( 'zasilkovna_prices' );

?>  
<div class="t-col-12 zasilkovna-main-wrap">
  <div class="toret-box box-info">
    <div class="box-header">
      <a href="https://documentation.toret.cz/zasilkovna" target="_blank" style="padding:14px;display:block;"><?php _e('Dokumentace a demo eshop pro vyzkoušení pluginu','zasilkovna'); ?> </a>
    </div>
    <div class="box-body">

        <ul class="zasilkovna-menu">
            <li>
                <a href="<?php echo admin_url(); ?>admin.php?page=zasilkovna&form=main-setting" class="<?php $this->get_active( 'main-setting' ); ?>">
                    <?php _e('Hlavní nastavení','zasilkovna'); ?>                
                </a>
            </li>
            <li>
                <a href="<?php echo admin_url(); ?>admin.php?page=zasilkovna&form=shipping-setting" class="<?php $this->get_active( 'shipping-setting' ); ?>">
                    <?php _e('Dopravci','zasilkovna'); ?>    
                </a>
            </li>

            <?php foreach($staty_kont as $key => $stat){ 
                if(!empty($zasilkovna_option['povolene_staty']) && in_array($key, $zasilkovna_option['povolene_staty'])){    
            ?>
                
                    <li>
                        <a href="<?php echo admin_url(); ?>admin.php?page=zasilkovna&form=<?php echo strtolower($key); ?>" class="<?php $this->get_active( strtolower($key) ); ?>">
                            <?php echo $staty[$key]; ?>                
                        </a >
                    </li>
                <?php }
            } ?>

        </ul>

        <?php 
            if(($this->get_form() == 'shipping_methods')||($this->get_form() == 'main')){
                include( 'parts/'.$this->get_form().'.php' ); 
            }else{
                ?>
                    <script type="text/javascript">
                        jQuery('document').ready(function(){
                            jQuery('.control-pobocky').click(function(){
                                var co = jQuery(this).data('value');
                                
                                jQuery('.pobocky-hide-' + co).toggle();
                                jQuery('.display-' + co).text(function(i, text){
                                    return text === "<?php echo __('Zobrazit pobočky','zasilkovna');?>" ? "<?php echo __('Skrýt pobočky','zasilkovna');?>" : "<?php echo __('Zobrazit pobočky','zasilkovna');?>";
                                });
                            });
                        });
                    </script>
                <?php
                $stat = $this->get_form();
                $zasilkovna_prices = get_option('zasilkovna_prices');
                $html = '<form method="post" action="" class="setting-form">
                            <input type="hidden" name="services_price" value="ok" />
                            <input type="hidden" name="services_price_country" value="' . $stat . '" />';

                if(in_array($stat, $zasilkovna_kde)){
                    $html .= '<table class="table-bordered">
                                <tr>
                                <th colspan="2" class="cena_top">' . __('Zásilkovna','zasilkovna') . '</th>
                                </tr>
                                <tr>
                                <th>' . __('Položka','zasilkovna') . '</th>
                                <th>' . __('Cena','zasilkovna') . '</th>
                                </tr>
                                <tr>
                                <td>' . __('Zásilka do 5 kg','zasilkovna') . '</td>
                                <td><input type="text" name="zasilkovna-' . $stat . '-5kg" value="' . (!empty($zasilkovna_prices['zasilkovna-' . $stat . '-5kg']) ? $zasilkovna_prices['zasilkovna-' . $stat . '-5kg'] : '') . '"></td>
                                </tr>
                                <tr>
                                <td>' . __('Zásilka do 10 kg','zasilkovna') . '</td>
                                <td><input type="text" name="zasilkovna-' . $stat . '-10kg" value="' . (!empty($zasilkovna_prices['zasilkovna-' . $stat . '-10kg']) ? $zasilkovna_prices['zasilkovna-' . $stat . '-10kg'] : '') . '"></td>
                                </tr>
                                <tr>
                                <td>' . __('Zásilka do 20 kg','zasilkovna') . '</td>
                                <td><input type="text" name="zasilkovna-' . $stat . '-20kg" value="' . (!empty($zasilkovna_prices['zasilkovna-' . $stat . '-20kg']) ? $zasilkovna_prices['zasilkovna-' . $stat . '-20kg'] : '') . '"></td>
                                </tr>
                                <tr>
                                <td>' . __('Zásilka do 30 kg','zasilkovna') . '</td>
                                <td><input type="text" name="zasilkovna-' . $stat . '-30kg" value="' . (!empty($zasilkovna_prices['zasilkovna-' . $stat . '-30kg']) ? $zasilkovna_prices['zasilkovna-' . $stat . '-30kg'] : '') . '"></td>
                                </tr>
                                <tr>
                                <td>' . __('Příplatek za dobírku','zasilkovna') . '</td>
                                <td><input type="text" name="zasilkovna-' . $stat . '-dobirka" value="' . (!empty($zasilkovna_prices['zasilkovna-' . $stat . '-dobirka']) ? $zasilkovna_prices['zasilkovna-' . $stat . '-dobirka'] : '') . '"></td>
                                </tr>
                                <tr>
                                <td>' . __('Doprava zdarma od','zasilkovna') . '</td>
                                <td><input type="text" name="zasilkovna-' . $stat . '-free" value="' . (!empty($zasilkovna_prices['zasilkovna-' . $stat . '-free']) ? $zasilkovna_prices['zasilkovna-' . $stat . '-free'] : '') . '"></td>
                                </tr>
                            </table>
                            <div class="clear"></div>';
                        $html .= '<input type="submit" class="button btn btn-success" value="' . __('Uložit','zasilkovna') . '" /><div class="clear"></div>';
                }

                if(in_array($stat, $zasilkovna_kde)){
                    if($stat == 'cz'){
                        $zasilkovna_mista_m = $zasilkovna_mista_cz;
                    }elseif($stat == 'hu'){
                        $zasilkovna_mista_m = $zasilkovna_mista_hu;
                    }elseif($stat == 'sk'){
                        $zasilkovna_mista_m = $zasilkovna_mista_sk;
                    }elseif($stat == 'pl'){
                        $zasilkovna_mista_m = $zasilkovna_mista_pl;
                    }elseif($stat == 'ro'){
                        $zasilkovna_mista_m = $zasilkovna_mista_ro;
                    }
                    $html .= '<div class="clear"></div>
                        
                        
                        <table class="table-bordered">
                            <tr>
                            <th>' . __('ID','zasilkovna'). '</th>
                            <th>' . __('Jméno','zasilkovna'). '</th>
                            <th>' . __('Provozovna','zasilkovna'). '</th>
                            <th>' . __('Ulice','zasilkovna'). '</th>
                            <th>' . __('Město','zasilkovna'). '</th>
                            <th>' . __('PSČ','zasilkovna'). '</th>
                            <th>' . __('Země','zasilkovna'). '</th>
                            </tr>';
                        $y = 0; 
                        if( !empty( $zasilkovna_mista_m ) ){
                            $y = 1;
                            foreach( $zasilkovna_mista_m as $key => $item ){ 
                                $html .= '<tr ' . ($y > 5 ? 'class="pobocky-hide pobocky-hide-zasilkovna"' : '') . ' >
                                            <td>' .  $item['id'] . '</td>
                                            <td>' .  $item['name'] . '</td>
                                            <td>' .  $item['place'] . '</td>
                                            <td>' .  $item['street'] . '</td>
                                            <td>' .  $item['city'] . '</td>
                                            <td>' .  $item['zip'] . '</td>
                                            <td>' .  $item['country'] . '</td>
                                        </tr>';
                                $y++;
                            } 
                        }
                          
                    $html .= '</table>  
                        ' . ($y > 5 ? '<div class="clear"></div><div class="btn btn-primary control-pobocky display-zasilkovna" data-value="zasilkovna">' . __('Zobrazit pobočky','zasilkovna'). '</div><div class="clear"></div>' : '') . '
                        <div class="clear"></div>
                        <a class="button btn btn-success" href="' . admin_url() . 'admin.php?page=zasilkovna&form=' . $stat . '&check=ok">' . __('Aktualizovat pobočky Zásilkovny','zasilkovna') . '</a> 
                        <div class="clear"></div>';     
                      
                }        


                foreach($komplet_data as $key => $data){
                    if(strtolower($data['stat']) == $stat){
                        $hmotnosti = (!empty($data['hmotnosti']) ? $data['hmotnosti'] : array(1,2,5,10));

                        $html .= '<table class="table-bordered">
                                <tr>
                                <th colspan="2" class="cena_top">' . $data['preklad'] . '</th>';
                        $html .= '</tr>
                                <tr>
                                <th>' . __('Položka','zasilkovna') . '</th>
                                <th>' . __('Cena','zasilkovna') . '</th>
                                </tr>';

                                if($data['deklarace'] > 0){
                                    $html .= '<tr><td colspan="2" class="zasilkovna-pozadavek">' . __('Dopravce nelze použít, protože zásilka musí mít specifickou celní deklaraci.','zasilkovna'). '</td></tr>';
                                }elseif($data['rozmery'] > 0){
                                    $html .= '<tr><td colspan="2" class="zasilkovna-pozadavek">' . __('Dopravce nelze použít, protože vyžaduje posílání informací o rozměrech balíku.','zasilkovna'). '</td></tr>';
                                }
                        foreach($hmotnosti as $hmotnost){
                            $html .= '<tr>
                                        <td>' . __('Zásilka do ','zasilkovna') . $hmotnost . ' kg</td>
                                        <td><input type="text" name="' . $data['slug'] . '-' . sanitize_title($hmotnost) . 'kg" value="' . (!empty($zasilkovna_prices[$data['slug'] . '-' . sanitize_title($hmotnost) . 'kg']) ? $zasilkovna_prices[$data['slug'] . '-' . sanitize_title($hmotnost) . 'kg'] : '') . '"';
                            if(($data['deklarace'] > 0)||($data['rozmery'] > 0)){
                                $html .= 'disabled';
                            }
                            $html .= '></td> 
                                    </tr>';
                        }
                                

                        $html .= '<tr>
                                <td>' . __('Příplatek za dobírku','zasilkovna') . '</td>
                                <td>';
                                if($data['dobirka'] > 0){
                                    if(!empty($zasilkovna_prices[$data['slug'] . '-dobirka'])){
                                        $html .= '<input type="text" name="' . $data['slug'] . '-dobirka" value="' . $zasilkovna_prices[$data['slug'] . '-dobirka'] . '"';
                                        if(($data['deklarace'] > 0)||($data['rozmery'] > 0)){
                                            $html .= 'disabled';
                                        }
                                        $html .= '>';
                                    }else{
                                        $html .= '<input type="text" name="' . $data['slug'] . '-dobirka" value=""';
                                        if(($data['deklarace'] > 0)||($data['rozmery'] > 0)){
                                            $html .= 'disabled';
                                        }
                                        $html .= '>';
                                    }
                                }else{
                                    $html .=  __('Tento dopravce nepřijímá dobírku','zasilkovna');
                                }
                        $html .= '</td>
                                </tr>
                                <tr>
                                <td>' . __('Doprava zdarma od','zasilkovna') . '</td>
                                <td><input type="text" name="' . $data['slug'] . '-free" value="' . (!empty($zasilkovna_prices[$data['slug'] . '-free']) ? $zasilkovna_prices[$data['slug'] . '-free'] : '') . '"';
                                if(($data['deklarace'] > 0)||($data['rozmery'] > 0)){
                                    $html .= 'disabled';
                                }
                                $html .= '></td>
                                </tr>
                            </table>
                            <div class="clear"></div>';

                        $html .= '<input type="submit" class="button btn btn-success" value="' . __('Uložit','zasilkovna') . '" /><div class="clear"></div>';

                        if(($data['pobocky'] == 1)&&($data['deklarace'] == 0)&&($data['rozmery'] == 0)){

                            $mista = get_option( 'zasilkovna_' . $key . '_branches', array() );


                            $i = 0;
                            $html .= '<table class="table-bordered">
                            <tr>
                                <th></th>
                                <th>' . __('ID','zasilkovna') . '</th>
                                <th>' . __('Ulice','zasilkovna') . '</th>
                                <th>' . __('Město','zasilkovna') . '</th>
                                <th>' . __('PSČ','zasilkovna') . '</th>
                                <th>' . __('Země','zasilkovna') . '</th>
                            </tr>';
                            if( !empty( $mista ) ){
                                $i = 1;
                                foreach( $mista as $key_m => $item ){ 
                                    $html .= '<tr ' . ($i > 5 ? 'class="pobocky-hide pobocky-hide-' . $data['slug'] . '"' : '') . ' >
                                                <td>' . $i . '</td>
                                                <td>' . $item['code'] . '</td>
                                                <td>' . $item['street'] . '</td>
                                                <td>' . $item['city'] . '</td>
                                                <td>' . $item['zip'] . '</td>
                                                <td>' . $item['country'] . '</td>
                                            </tr>';
                                    
                                    $i++;
                                } 
                            }
                            $html .= '</table>  
                            ' . ($i > 5 ? '<div class="clear"></div><div class="btn btn-primary control-pobocky display-' . $data['slug'] . '" data-value="' . $data['slug'] . '">' . __('Zobrazit pobočky','zasilkovna') . '</div><div class="clear"></div>' : '') . '
                            <div class="clear"></div>
                            <a class="button btn btn-success" href="' . admin_url() . 'admin.php?page=zasilkovna&form=' . $stat . '&pobocky=' . $key . '">' . __('Načíst pobočky','zasilkovna') . '</a> 
                            <div class="clear"></div>';
                        }
                    }
                }



                $html .= '</form>';
                echo $html;

            }


        ?>

        </div>
    </div>
</div>     
  
<div class="clear"></div>
</div>  

