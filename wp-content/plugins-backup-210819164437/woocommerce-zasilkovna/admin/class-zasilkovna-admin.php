<?php
/**
 * @package   Toret Zásilkovna
 * @author    toret.cz
 * @license   GPL-2.0+
 * @link      http://toret.cz
 * @copyright 2016 Toret.cz
 */



 
class Toret_Zasilkovna_Admin {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = null;

	/**
	 * Initialize the plugin by loading admin scripts & styles and adding a
	 * settings page and menu.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		$plugin = Toret_Zasilkovna::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
		//add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

		// Add the options page and menu item.
		add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

    	/**
    	 *  Output fix
    	 */              
    	add_action('admin_init', array( $this, 'output_buffer' ) );

		add_action('admin_init', array( $this, 'send_ticket' ) );
		
		add_filter( 'woocommerce_product_data_tabs', array( $this , 'add_zasilkovna_product_data_tab') , 99 , 1 );
		add_action( 'woocommerce_product_data_panels', array( $this , 'add_zasilkovna_product_data_fields' ));
		add_action( 'woocommerce_process_product_meta', array( $this , 'woocommerce_zasilkovna_fields_save' ));

    	add_filter( 'manage_edit-shop_order_columns', array( $this, 'barcode_column' ), 99999 );
    	add_action( 'manage_shop_order_posts_custom_column', array( $this, 'barcode_column_display' ), 10, 2 );

        // Add an action link pointing to the options page.
        add_filter( 'plugin_row_meta' , array( $this, 'add_action_links' ), 10, 2 );

        add_action( 'add_meta_boxes', array( $this, 'metabox' ) );
    
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_styles() {

		wp_enqueue_style( $this->plugin_slug .'-admin-styles', WOOZASILKOVNAURL . 'assets/css/admin.css', array(), Toret_Zasilkovna::VERSION );

	}

	/**
	 * Register and enqueue admin-specific JavaScript.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_scripts() {

		//wp_enqueue_script( $this->plugin_slug . '-admin-script', plugins_url( 'assets/js/admin.js', __FILE__ ), array( 'jquery' ), Toret_Zasilkovna::VERSION );

	}

    /**
     * Add settings action link to the plugins page.
     *
     * @since    1.0.0
     */
    public function add_action_links( $meta, $file ) {

        if( $file == 'woocommerce-zasilkovna/woocommerce-zasilkovna.php' ){
            $meta[] = '<a href="' . admin_url( 'admin.php?page=zasilkovna' ) . '">' . __( 'Nastavení', 'zasilkovna' ) . '</a>';
            $meta[] = '<a href="https://documentation.toret.cz/zasilkovna/" target="_blank">' . __( 'Dokumentace', 'zasilkovna' ) . '</a>';
            $meta[] = '<a href="https://toret.cz/podpora/" target="_blank">' . __( 'Podpora', 'zasilkovna' ) . '</a>';
        }

        return $meta;

    }


	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

	if (!defined('TORETMENU')) {
     
     	add_menu_page(
			__( 'Toret plugins', $this->plugin_slug ),
			__( 'Toret plugins', $this->plugin_slug ),
			'manage_options',
			'toret-plugins',
			array( $this, 'display_toret_plugins_admin_page' ),
            WOOZASILKOVNAURL.'assets/images/ikona-toret.svg'
		);
     
     	define( 'TORETMENU', true );
  	}
  
  
  	
        add_submenu_page(
            'toret-plugins',
            __( 'Zásilkovna', 'zasilkovna' ),
            __( 'Zásilkovna', 'zasilkovna' ),
            'manage_options',
            'zasilkovna',
            array( $this, 'control_xml' )
        );

        add_submenu_page(
            'toret-plugins',
            __( 'Zásilkovna log', 'zasilkovna' ),
            __( 'Zásilkovna log', 'zasilkovna' ),
            'manage_woocommerce',
            'zasilkovna-log',
            array( $this, 'display_plugin_log_page' )
        );

	}

    /**
	 * Render the settings page for all plugins
	 *
	 * @since    1.0.0
	 */
	public function display_toret_plugins_admin_page() {
		include_once( 'views/toret.php' );
	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function control_xml() {
		include_once( 'views/control-xml.php' );
	}

    /**
     * Render the settings page for this plugin.
     *
     *
     */
    public function display_plugin_log_page() {
        include_once( 'views/log.php' );
    }

	 
  
  	/**
	 * Headers allready sent fix
	 * 
	 * @since    1.0.0        
	 */
	public function output_buffer() {
		ob_start();
	}


	/**
   	 *
  	 *
  	 */        
  	public function barcode_column($columns) {
    	$new_columns = array();
    	foreach($columns as $key => $item){
      		$new_columns[$key] = $item;
      		if($key == 'cb'){
         		$new_columns['zasilkovna'] = __( 'Barcode', $this->plugin_slug );
      		}
    	}    
    	$new_columns['zasilkovna_send'] = __( 'Zásilkovna', $this->plugin_slug );
    	return $new_columns;
  	}

  	/**
  	 * Display send button and barcode
  	 *
  	 */
  	public function barcode_column_display($column_name, $post_id){
    	global $post, $wp;

      $url_args = array();

      foreach( $_GET as $key => $item ){
        if( $key == 'zasilkovna_ticket_id' || $key == 'zasilkovna_ticket_id_assistent' || $key == 'zasilkovna_id_objednavky_assistent' || $key == 'zasilkovna_ticket_id' ){ continue; }
        $url_args[$key] = $item;
      }

      $location = add_query_arg( $url_args, admin_url() . 'edit.php' ); 

      	if ( $column_name == 'zasilkovna' ) { 
        	$field = get_post_meta( $post_id, 'zasilkovna_barcode', true );
        	echo '<a href="https://www.zasilkovna.cz/vyhledavani?det='.$field.'" target="_blank">'.$field.'</a>';
      	}

      	if ( $column_name == 'zasilkovna_send' ) { 

            if( !empty( $_GET['paged'] ) ){ $paged = '&paged='.$_GET['paged']; }else{ $paged = ''; }
      		
            $zasilkovna_id = get_post_meta( $post_id, 'zasilkovna_id_pobocky', true );
  			
            if( !empty( $zasilkovna_id ) ){
      			$field = get_post_meta( $post_id, 'zasilkovna_barcode', true );
      			
            if( empty( $field ) || apply_filters('zasilkovna_allow_multiple_submissions', false)){
        			echo '<a href="'.$location.'&zasilkovna_id_objednavky='.$post_id.'" class="button" style="padding: 2px 4px 1px 5px;"><span class="dashicons dashicons-external"></span></a>';
            }
        		
        		$id_zasilky = get_post_meta( $post_id, 'zasilkovna_id_zasilky', true );
        		
        		if( !empty( $id_zasilky ) ){
        			echo '<a href="'.$location.'&zasilkovna_ticket_id='.$id_zasilky.'" class="button" style="padding: 2px 4px 1px 5px;"><span class="dashicons dashicons-arrow-down-alt"></span></a>';

                    $zasilkovna_option    = get_option( 'zasilkovna_option');
                    if(!empty($zasilkovna_option['asistent']) && $zasilkovna_option['asistent'] == 'ok'){
                        $id_zasilky_assistent = get_post_meta( $post_id, 'zasilkovna_id_zasilky_assistent', true );
                        
                        if( !empty( $id_zasilky_assistent ) ){
                            echo '<a href="'.$location.'&zasilkovna_ticket_id_assistent='.$id_zasilky_assistent.'" class="button" style="padding: 2px 4px 1px 5px;margin-left:10px;"><span class="dashicons dashicons-arrow-down-alt2"></span></a>';
                        }else{
                            echo '<a href="'.$location.'&zasilkovna_id_objednavky_assistent='.$post_id.'" class="button" style="padding: 2px 4px 1px 5px;margin-left:10px;"><span class="dashicons dashicons-admin-users"></span></a>';
                        }

                    }                    
        		}
      		}
      	}

  	}

	
  	/**
  	 * Send ticket or download packet
  	 *
  	 */
  	public function send_ticket(){

  		if( !empty( $_GET['zasilkovna_id_objednavky'] ) ){
  			Zasilkovna_Ticket::send_ticket( $_GET['zasilkovna_id_objednavky'] ); 
  		}
  		if( !empty( $_GET['zasilkovna_ticket_id'] ) ){
  			
  			$zasilkovna_option = get_option( 'zasilkovna_option');
  			$apiPassword = $zasilkovna_option['api_password'];
                                                   
            $gw = new SoapClient("http://www.zasilkovna.cz/api/soap.wsdl");
  			try {

                $packetId = $_GET['zasilkovna_ticket_id'];
                $format = "A7 on A7";
                $offset = 0;

      			$packet = $gw->packetLabelPdf($apiPassword, $packetId, $format, $offset);

                header('Content-type: application/pdf');
                header('Content-Disposition: attachment; filename=ticket-'.$_GET['zasilkovna_ticket_id'].'.pdf');

                echo $packet;

 			}
  			catch(SoapFault $e) {
      			// TODO: process error
  				var_dump( $e );
  			}
  			die();
  		}
        if( !empty( $_GET['zasilkovna_id_objednavky_assistent'] ) ){
            Zasilkovna_Ticket_Claim::send_ticket( $_GET['zasilkovna_id_objednavky_assistent'] ); 
        }
        if( !empty( $_GET['zasilkovna_ticket_id_assistent'] ) ){
            
            $zasilkovna_option = get_option( 'zasilkovna_option');
            $apiPassword = $zasilkovna_option['api_password'];
                                                   
            $gw = new SoapClient("http://www.zasilkovna.cz/api/soap.wsdl");
            try {

                $packetId = $_GET['zasilkovna_ticket_id_assistent'];
                $format = "A7 on A7";
                $offset = 0;

                $packet = $gw->packetLabelPdf($apiPassword, $packetId, $format, $offset);

                header('Content-type: application/pdf');
                header('Content-Disposition: attachment; filename=ticket-'.$_GET['zasilkovna_ticket_id_assistent'].'.pdf');

                echo $packet;

            }
            catch(SoapFault $e) {
                // TODO: process error
                var_dump( $e );
            }
            die();
        }

  	}

  	/**
  	 * Get form 
  	 *
  	 */
  	public function get_form(){

  		if( !empty( $_GET['form'] ) ){
  			$form = esc_attr( $_GET['form'] );
  			$forms = array(
  				'main-setting' => 'main',
  				'shipping-setting' => 'shipping_methods',
			  );
			if(($form == 'main-setting')||($form == 'shipping-setting')){
				return $forms[$form];
			}else{
				return $form;
			}

  		}else{
  			return 'main';
  		}

  	}

  	/**
  	 * Get form 
  	 *
  	 */
  	public function get_active( $value ){

  		if( !empty( $_GET['form'] ) && $_GET['form'] == $value ){ 
  			echo 'active'; 
  		}

  	}

  	/**
  	 * Save services prices
  	 * 
  	 */
  	public function save_service_prices(){

  		if( isset( $_POST['services_price'] ) ){ 
			$zasilkovna_prices = get_option( 'zasilkovna_prices' );
				
			if( empty( $zasilkovna_prices ) ){
				$zasilkovna_prices = array();
			}

			if( isset( $_POST['services_price_kurzy'] ) && $_POST['services_price_kurzy'] == 'ok' ){
				$fields = array('kurz-euro', 'kurz-forint', 'kurz-zloty', 'kurz-lei');
			}else{

				$komplet_data = Zasilkovna_Helper::komplet_data();
				$zasilkovna_kde = Zasilkovna_Helper::zasilkovna_kde();
				$fields = array();

				foreach($komplet_data as $key => $data){
					if(strtolower($data['stat']) == $_POST['services_price_country']){
						$hmotnosti = (!empty($data['hmotnosti']) ? $data['hmotnosti'] : array(1,2,5,10));
						if(in_array(strtolower ($data['stat']), $zasilkovna_kde)){
							$fields[] = 'zasilkovna-' . strtolower($data['stat']) . '-5kg';
							$fields[] = 'zasilkovna-' . strtolower($data['stat']) . '-10kg';
							$fields[] = 'zasilkovna-' . strtolower($data['stat']) . '-20kg';
							$fields[] = 'zasilkovna-' . strtolower($data['stat']) . '-30kg';
							$fields[] = 'zasilkovna-' . strtolower($data['stat']) . '-dobirka';
							$fields[] = 'zasilkovna-' . strtolower($data['stat']) . '-free';
						}
						foreach($hmotnosti as $hmotnost){
							$fields[] = $data['slug'] . '-' . sanitize_title($hmotnost) . 'kg';
							$fields[] = $data['slug'] . '-dobirka';
							$fields[] = $data['slug'] . '-free';
						}
					}
				}
			
		
		}

		foreach( $fields as $field ){

			if( !empty( $_POST[$field] ) ){  
				$zasilkovna_prices[$field]  = $_POST[$field]; 
			}else{
				unset( $zasilkovna_prices[$field] );
			}
		}

		update_option( 'zasilkovna_prices', $zasilkovna_prices );
			
    
		}

  	}

  	/**
  	 * Save main setting
  	 * 
  	 */
  	public function save_setting(){

  		if( isset( $_POST['update'] ) ){
  
			woo_zasilkovna_control_licence( $_POST['zas_licence'] ); 
  
  			$zasilkovna_option = array();
  
  			if(!empty($_POST['api_key'])){
    			$zasilkovna_option['api_key'] = sanitize_text_field($_POST['api_key']);
  			}
  			if(!empty($_POST['api_password'])){
    			$zasilkovna_option['api_password'] = sanitize_text_field($_POST['api_password']);
  			}
  			if(!empty($_POST['nazev_eshopu'])){
    			$zasilkovna_option['nazev_eshopu'] = sanitize_text_field($_POST['nazev_eshopu']);
  			}
  			if(!empty($_POST['povolene_staty'])){  
    			$zasilkovna_option['povolene_staty'] = $_POST['povolene_staty'];
  			}
  			if(!empty($_POST['cz_pobocky']) && $_POST['cz_pobocky'] == 'cz'){  
    			$zasilkovna_option['cz_pobocky'] = 'cz';
  			}
  			if(!empty($_POST['sk_pobocky']) && $_POST['sk_pobocky'] == 'sk'){  
    			$zasilkovna_option['sk_pobocky'] = 'sk';
  			}
  			if(!empty($_POST['hu_pobocky']) && $_POST['hu_pobocky'] == 'hu'){  
    			$zasilkovna_option['hu_pobocky'] = 'hu';
  			}
  			if(!empty($_POST['pl_pobocky']) && $_POST['pl_pobocky'] == 'pl'){  
    			$zasilkovna_option['pl_pobocky'] = 'pl';
  			}
  			if(!empty($_POST['ro_pobocky']) && $_POST['ro_pobocky'] == 'ro'){  
    			$zasilkovna_option['ro_pobocky'] = 'ro';
  			}
  			if(!empty($_POST['odeslani_zasilky'])){  
    			$zasilkovna_option['odeslani_zasilky'] = sanitize_text_field($_POST['odeslani_zasilky']);
  			}
  			if(!empty($_POST['doprava_zdarma'])){  
    			$zasilkovna_option['doprava_zdarma'] = sanitize_text_field($_POST['doprava_zdarma']);
  			}
  			if(!empty($_POST['icon_url'])){  
    			$zasilkovna_option['icon_url'] = sanitize_text_field($_POST['icon_url']);
  			}
  			if(!empty($_POST['icon_url_sk'])){  
    			$zasilkovna_option['icon_url_sk'] = sanitize_text_field($_POST['icon_url_sk']);
  			}
  			if(!empty($_POST['icon_url_hu'])){  
    			$zasilkovna_option['icon_url_hu'] = sanitize_text_field($_POST['icon_url_hu']);
  			}
  			if(!empty($_POST['icon_url_pl'])){  
    			$zasilkovna_option['icon_url_pl'] = sanitize_text_field($_POST['icon_url_pl']);
  			}
  			if(!empty($_POST['icon_url_ro'])){  
    			$zasilkovna_option['icon_url_ro'] = sanitize_text_field($_POST['icon_url_ro']);
  			}
  			if(!empty($_POST['no_send'])){  
    			$zasilkovna_option['no_send'] = sanitize_text_field($_POST['no_send']);
  			}
            if(!empty($_POST['error_email']) && $_POST['error_email'] == 'email'){  
                $zasilkovna_option['error_email'] = 'email';
            }
            if(!empty($_POST['asistent']) && $_POST['asistent'] == 'ok'){  
                $zasilkovna_option['asistent'] = 'ok';
            }
            if(!empty($_POST['max_weight']) && $_POST['max_weight'] == 'ok'){  
                $zasilkovna_option['max_weight'] = 'ok';
            }
  
  			update_option( 'zasilkovna_option', $zasilkovna_option );
  
		} 

  	}

  	/**
     * Metabox for order detail 
     *
     * 
     */
    public function metabox() {
        
        global $post;

        $order = wc_get_order( $post->ID );
        if( !$order ){ return; }

        include('includes/metabox.php');
        add_meta_box( 'zasilkovna_log', __('Zásilkovna Log','zasilkovna'), 'order_zasilkovna_log_meta_box', 'shop_order', 'side', 'high' );
        
	}
	
	public function add_zasilkovna_product_data_tab( $product_data_tabs ) {
		$product_data_tabs['zasilkovna'] = array(
			'label' => __( 'Zásilkovna', 'my_text_domain' ),
			'target' => 'zasilkovna_product_data',
		);
		return $product_data_tabs;
	} 
	
	public function add_zasilkovna_product_data_fields() {
		global $woocommerce, $post;
		?>
		<div id="zasilkovna_product_data" class="panel woocommerce_options_panel">
			<div class="options_group">
				<?php
				woocommerce_wp_checkbox( array( 
					'id'            => '_zasilkovna_vek', 
					'label'         => __( 'Ověření věku', $this->plugin_slug ),
					'description'   => __( 'Zaškrtněte pokud je pro tento produkt požadováno ověření věku.', $this->plugin_slug ),
					'default'       => '0',
					'desc_tip'      => false,
				) );
				?>
			</div>
			<p><b>Zaškrtněte dopravní metody Zásilkovny, u kterých nechcete, aby byly pro tento produkt dostupné.</b></p>
			<div class="options_group toret-zasilkovna-produkt-admin">
			<?php
			woocommerce_wp_checkbox( array( 
				'id'            => '_zasilkovna_vypnuti', 
				'label'         => __( 'Zásikovna', $this->plugin_slug ),
				'description'   => '',
				'default'       => '0',
				'desc_tip'      => false,
			) );
			$zasilkovna_services = get_option('zasilkovna_services');
			foreach( Zasilkovna_Helper::komplet_data() as $key => $service ){
				if(!empty($zasilkovna_services['service-active-'.$key])){
					woocommerce_wp_checkbox( array( 
						'id'            => '_' . $key . '_vypnuti', 
						'label'         => __( (!empty($zasilkovna_services['service-label-'.$key]) ? $zasilkovna_services['service-label-'.$key] : $service['nazev']) , $this->plugin_slug ),
						'description'   => '',
						'default'       => '0',
						'desc_tip'      => false,
					) );	
				}
			}
			?>
		</div>
		</div>
		<?php
	}

	public function woocommerce_zasilkovna_fields_save( $post_id ){
		$woo_checkbox = isset( $_POST['_zasilkovna_vek'] ) ? 'yes' : 'no';
		update_post_meta( $post_id, '_zasilkovna_vek', $woo_checkbox );
		$woo_checkbox = isset( $_POST['_zasilkovna_vypnuti'] ) ? 'yes' : 'no';
		update_post_meta( $post_id, '_zasilkovna_vypnuti', $woo_checkbox );
		foreach( Zasilkovna_Helper::komplet_data() as $key => $service ){
			$woo_checkbox = isset( $_POST['_' . $key . '_vypnuti'] ) ? 'yes' : 'no';
			update_post_meta( $post_id, '_' . $key . '_vypnuti', $woo_checkbox );
		}
	}

}//Class end
