=== WooCommerce Zasilkovna ===
Requires at least: 4.0
Tested up to: 5.3.2
Stable tag: 3.2.8
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Version: 3.2.8

WooCommerce integrační plugin pro napojení na služby Zásilkovny. 

== Description ==
WordPress plugin WooCommerce Zásilkovna implementuje tuto službu do e-shopu. Zákazník si v pokladně může pohodlně vybrat pobočku, kam chce objednávku poslat. WooCommerce Zásilkovna načítá pobočky a objednávky automaticky odesílá systému Zásilkovny.

Nákupem produktu získáte jednu unikátní licenci, která může být aktivována na jedné WordPress instalaci.Licence není časově omezená a umožňuje automatické aktualizace pluginu.

== Instalace ==
* Plugin je možné nainstalovat pomocí FTP, nebo nahráním Zip souboru v administraci

= Minimální požadavky =
* WordPress 4.0 nebo vyšší
* PHP version 5.2.4 nebo vyšší
* MySQL version 5.0 nebo vyšší

== Changelog ==
<a href="https://toret.cz/produkt/woocommerce-zasilkovna/" target="_blank">Changelog</a>

= 3.2.8 =
* Doplnění informačních textů

= 3.2.7 =
* Změna zaokrouhlení dobírky

= 3.2.6 = 
* Doplnění informačních textů

= 3.2.5 =
* Přidání překladů SK a PL

= 3.2.4 =
* Přidání překladů HU a EN
* Zlepšení kompatibility s WPML

= 3.2.3 =
* zlepšení kompatibility s WPML

= 3.2.2 =
* aktulizace možností doručení
* oprava překladu
* přidány filtry $html = apply_filters( 'zasilkovna_vystup_html', $html, $zasilkovna_mista[$zasilkovna_id] ); a $html = apply_filters( 'zasilkovna_vystup_html_email', $html, $zasilkovna_mista[$zasilkovna_id] );

= 3.2.1 =
* změna váhy doručení Bratislava

= 3.2.0 =
* změna kontroly licence
* oprava váhy v zásilkovně
* zobrazení "Zdarma" při ceně 0

= 3.1.0 =
* přidání možnosti vypnout dopravu v závislosti na produktu

= 3.0.8 =
* oprava tlačítka vybrat místo vyzvednutí

= 3.0.7 =
* oprava zobrazení dopravců podle váhy produktu

= 3.0.6 =
* oprava překlepu

= 3.0.5 =
* oprava aktivace dopravců

= 3.0.4 =
* přidáno ověření věku

= 3.0.3 =
* Oprava Call to a member function get() on null

= 3.0.2 =
* Přesunutí upozornění na aktualizaci 3.X

= 3.0.1 =
* Oprava zobrazování dopravců

= 3.0.0 =
* Přidání dalších zemí a dopravců
* Změna zadávání nových zemí a dopravců
* Přidána kontrola zda dopravce přijímá dobírku

= 2.0.18 =
* Zlepšení kompatibility s více stránkovou pokladnou

= 2.0.17 =
* Oprava ukládání cen

= 2.0.16 =
* Úprava textů v administraci

= 2.0.15 =
*Přidání filtru "dobirka_order_status"

= 2.0.14 =
*Drobná úprava textu

= 2.0.13 =
*Přidání filtru na úpravu dopravy zdarma 'zasilkovana_free_shipping_filter'

= 2.0.12 =
*Přidání filtru na nastavení daňové třídy u dobírky 'zasilkovna_taxclass_dobirka'

= 2.0.11 = 
* Přidání filtru na úpravu váhy 'zasilkovna_weigh_koef'

= 2.0.10 =
* Úprava filtru pro opakované odeslání

= 2.0.9 =
* Upravy informacnich textu v pluginu

= 2.0.8 =
* Změna získání použité platební metody pro data ticketu
* Funkce zasilkovna_get_cod_value nově přijímá 4 parametry - $s_method, $price, $country, $order
* Nový filter pro hodnotu dobírky v ticketu - apply_filters( 'zasilkovna_dobirka_shipping_value', $cod, $price, $country, $order );

= 2.0.7 =
* Přidání filtru na value při odeslání zásilky $price = apply_filters( 'zasilkovna_ticket_value', $price, $order, $zasilkovna_option, $zasilkovna_id );

= 2.0.6 =
* rozšíření inicializace widgetu o definici vybrané země
* nastavení typeof u kontroly undefined
* úprava v platbě na účet - kompatibilita s WooCommerce 3+
* oprava indefined index form v administraci

= 2.0.5 =
* oprava deaktivace konverze měn
* úprava kontroly virtuálního produktu v košíku

= 2.0.4 =
* úprava kontroly dostupnosti platební metody PNU

= 2.0.3 =
* Přidána PLN měna pro Polsko
* Doplněna možnost deaktivace konverze měn - používejte pouze v případě, že máte v Zásilkovně povolenou konverzi, nebo máte účty v příslušných měnách, viz. - https://www.zasilkovna.cz/eshopy/konverze-men

= 2.0.2 =
* Upraveny ulr tlačítek v administraci, aby napřesměrovávaly na první stranu výpisu objednávek

= 2.0.1 =
* Odstranění chybového hlášení, pokud je log prázdný

= 2.0.0 =
* Nový widget pro výběr poboček
* Doplněny dopravy pro stávající země
* Přidány dopravy a nové země
* Přidána možnost nastavit dopravu zdarma pro většinu doprav
* Rozšířeny filtry pro stávající a nové dopravy, viz. dokumentace https://documentation.toret.cz/zasilkovna/pouziti-filtru/
* Úprava a refaktoring stávajícího kódu
* Odstranění souboru notify_log.txt
* Přidáno interní logování komunikace se Zásilkovnou
* Přidán metabox pro výpis logu pro jednotlivou objednávku
* Přidána stránka s kompletním výpisem logu
* Vytvořen videonávod pro aktuální verzi pluginu https://www.youtube.com/watch?v=3J6NEVblFsg
* Rozšířená stávající dokumentace https://documentation.toret.cz/zasilkovna/
* Soubor pro aktualizaci poboček byl doplněn o parametry, umožňující aktualizovat pobočky Paczkomatů a Nova Postha

= 1.6.4 =
* Nový filtr cesko_nejlevnejsi_doruceni_shipping_cost

= 1.6.3 =
* Doplněna kontrola existence WooCommerce i pro multisite
* Upraveno vkládání souboru s kontrolou licence

= 1.6.2 =
* Doplněna podpora vlastních stavů objednávek v nastavení dobírky a platby na účet
* Upraveny zavržené metody v platbě na účet
* Doplněny odkazy na dokumentaci a nastavení ve výpisu pluginů

= 1.6.1 =
* Odstraněny nepotřebné filtry zasilkovna_cz_cost, zasilkovna_sk_cost a zasilkovna_hu_cost
* Pro zásilkovnu je nyní dostupný filtr zasilkovna_shipping_cost, který je použit pro všechny země
* Filtry z minulé aktualizace byly přemístěny tak, aby je bylo možné použít i pro "vypnutí" konkrétní dopravy. Více v dokumentaci

= 1.6.0 =
* Přidány filtry pro každnou platební metodu, umožňující ovlivnit výslednou cenu
* zasilkovna_cz_cost - Zásilkovna CZ
* zasilkovna_sk_cost - Zásilkovna SK
* zasilkovna_hu_cost - Zásilkovna HU
* zasilkovna_cp_cz_cost - Česká Pošta CZ
* zasilkovna_intime_cz_cost - In Time CZ
* zasilkovna_dpd_cz_cost - DPD CZ
* zasilkovna_express_brno_cz_cost - Expresní doručení Brno
* zasilkovna_express_praha_cz_cost - Expresní doručení Praha
* zasilkovna_express_ostrava_cz_cost - Expresní doručení Ostrava
* zasilkovna_austria_at_cost - Rakouská Pošta
* zasilkovna_germany_de_cost - Německá Pošta
* zasilkovna_hungary_hu_cost - Maďarská Pošta
* zasilkovna_dpd_hu_cost - DPD HU
* zasilkovna_transoflex_hu_cost - Transoflex HU
* zasilkovna_poland_pl_cost - Polská Pošta
* zasilkovna_dpd_pl_cost - DPD PL
* zasilkovna_slovensko_na_adresu_cost - Slovensko Doručení na adresu
* zasilkovna_slovenska_posta_cost - Slovenská Pošta
* zasilkovna_expres_bratislava_cost - Expresní doručení Bratislava
* zasilkovna_slovensko_kuryr_cost - Slovensko Kurýr
* zasilkovna_fan_ro_cost - Rumunsko FAN
* zasilkovna_dpd_ro_cost - DPD RO
* zasilkovna_dpd_bl_cost - DPD BL

= 1.5.9 =
* Přidán reklamační asistent do administrace objedávek

= 1.5.8 =
* Doplněna kompatibilita s WooCommerce 3.4.0

= 1.5.7 =
* Id objednávky předávané do Zásilkovny změněnno na $order->get_order_number(); - comaptibilita s WOSN pluginem
* Přidán filtr zasilkovna_order_number pro možnost ovlivnit id objednávky předané do Zásilkovny

= 1.5.6 =
* Opraveno ukládání hodnot kurzů měn

= 1.5.5 =
* Oprava chyby s nulovou cenou pro CZ Zásilkovnu
* Oprava neukládání prázdných polí v nastavení

= 1.5.4 =
* Barcode v administraci změněn na odkaz vedoucí na sledování zásilky na Zásilkovně
* Přidána nová třída pro načtení poboček Zásilkovna
* Přidány pobočky zásilkovny pro Polsko, Maďarsko a Rumunsko
* Upraven vzhled administrace, pro lepší orientaci v zadávání cen doprav
* Tlačítko pro manuální odeslání do Zásilkovny změněno na ikonu
* Přidáno tlačítko pro stažení štítku zásilky
* Přidáno nemazání vybrané pobočky Zásilkovny, při změně platební metody
* Přidána možnost uložení překladu do složky languages/zasilkovna, ten pak nebude přepisován aktualizací
* Doplněna třída s notice pro kontrolu existence WooCommerce, aktivní licence, cURL a Soap
* Přidána třída pro generování fee pro dobírku
* Přidán email administrátorovi, pokud dojde k selhání vytvoření zásilky
* Pro vyzvedutí zásilky na pobočce Zásilkvny je nyní možné nastavit cenu, od které bude zdarma ( nezávislé na nastavení Dopravy Zdarma )

= 1.5.3 =
* Přidána kontrola existence indexu vybrané dopravní metody v metodě zasilkovna_select_option

= 1.5.2 =
* Odstranění funkce zasilkovna_wc_remote_gateway, která skrývala původní bankovní převod a COD
* Úprava inicializace výchozích hodnot u PNU platební metody

= 1.5.1 =
* V metodě set_fee_by_dobirka_free_shipping byla změněna kontrolovaná hodnota pole na show_cod

= 1.5.0 =
* Doplněna kontrola existence dopravy pro objednávku

= 1.4.9 =
* Úprava html kódu a řetězce pro překlad v metodě zasilkovna_customer_email_info

= 1.4.8 =
* Úprava kódu tabulky pro zobrazení vybrané pobočky a odkau ke sledování na děkovné stránce

= 1.4.7 =
* Úprava kontroly vyřazeného produktu v helper třídě

= 1.4.6 =
* Změna způsobu načítání xml souboru s pobočkami - nově curl

= 1.4.5 =
* Oprava získání statusu objednávky pro verze 2.6+

= 1.4.4 =
* Zaktualizovány třídy pro kompatibilitu s WooCommerce 3.0.+

= 1.4.3 =
* Opraven undefined index zasilkovna-cr-dobirka

= 1.4.2 =
* Přidán výpis pobočky Zásilkovny v detailu objednávky v administraci

= 1.4.1 =
* Přidána no js kontrola vybrané pobočky na pokladně
* Přidána kompatibilita kontroly doručovací adresy pro WooCommerce 3.0.+

= 1.4.0 =
* Přidána kontrola aktivní WooCommerce
* Dobírka upravena pro kompatibilitu s WooCommerce 3.0.+
* Platba na účet upravena pro kompatibilitu s WooCommerce 3.0.+
* Přidána funkce toret_get_customer_country() pro zjištění zadané země zákazníka, s kompatibilitou pro WooCommerce 3.0.+
* Doplněny třídy pro kompatibilitu s WooCommerce 2.6 až 3.0+
* Doplněna kompatibilita pro WooCommerce 3.0.+


= 1.3.9 =
* Oprava undefined old_rate na pokladně
* Doplněno nastavení výchozích měn, pro různé země, viz. dokumentace https://zasilkovna.toret.cz/nastaveni-men-zasilek/
* Přidána registrace stringů pro WPML, pro názvy doprav. Po každé změně, se musí v překladu stringů ve WPML, položky upravit

= 1.3.8 =
* Opraveno volání metody __get v dobírce
* Odstraněn výpis pole v administraci
* Přidáno nastavení do úpravy chování dopravy zdarma - skryje dopravu zdarma a zanechá ceny beze změny. Důvod - možnost kombinovat s nastavením pluginu Woo Doprava

= 1.3.7 =
* Do dobírky byl doplněn filtr woo_doprava_is_virtual_product_in_cart
* Platba na účet byla sjednocena s pluginem Woo Doprava pro vzájemnou kompatibilitu
* Pro platbu na účet, lze nastavit stav objednávky při vytvoření
* Přidána mezera za dvojtečku v informaci o pobočce v emailu
* Oprava zobrazení ikony Zásilkovny v pokladně
* Nastavení ikony Dopravní metody bylo přesunuto z nastavení dopravy, do nastavení pluginu
* Možnost vypnutí odesílání zásilek do systému Zásilkovny
* Upraven výpis pobočky Zásilkovny na děkovné stránce a v mém účtu
* Ošetřeno zobrazování tlačítek pro odeslání do Zásilkovny ve výpisu objednávek, pokud není doprava Zásilkovna

= 1.3.6 =
* Oprava příplatku za dobírku pro Doručení na adresu SK

= 1.3.5 =
* Možnost nastavení chování dopravy zdarma - výchozí/vše zdarma

= 1.3.4 =
* Aktualizace překladových souborů a načítání jazykové domény

= 1.3.3 =
* Možnost nastavení stavu objednávky, po zaplacení na dobírku

= 1.3.2 =
* Doplněno method_title pro zasilkovna_shipping class

= 1.3.1 =
* Oprava načítání názvu metody dobírce

= 1.3.0 =
* Oprava ukládání názvu metody pro zobrazení na pokladně

= 1.2.9 =
* Změna generování zásilek
* Úprava přepočítávání měn
* Kompatibilita s pluginy WooCommerce Currency Switcher a WooCommerce Multilingual
* Informace o důvodu neodeslání zásilky do systému Zásilkovny v detailu objednávky (administrace)
* Zobrazování čísla zásilky (Barcode) ve výisu objednávek
* Zobrazování odkazu na sledování zásilky Track and Trace
* Možnost manuálního odeslání objednávky do Zásilkovny, po opravě dat objednávky
* Kompletní refactoring kódu a přidání nových tříd
* Odstranění drobných chyb a jejich výpisů
* Přidán odkaz na sledování zásilek do klientského emailu a detailu objednávky v účtu zákazníka

= 1.2.8 =
* Změna odesílání dat z Soap na REST

= 1.2.7 =
* Upravena detekce dopravy zdarma, pro kompatibilitu s WooCommerce 2.6+

= 1.2.6 =
* Upravena dopravní metoda, pro kompatibilitu s WooCommerce 2.6
* Přidána kompatibilita s Shipping Zones

= 1.2.5 =
* Doplněny další možnosti doprav

= 1.2.4 =
* Přidána možnost, vypnout DPH, pro příplatek za dobírku

= 1.2.3 =
* Přidána defaultní položka do výběru poboček
* Vložena kontrola výběru pobočky na pokladně.

= 1.2.2 =
* Přidán notifikační log
* Přidáno SK logo pro slovenskou Zásilkovnu
* Seznam poboček Zásilkovny byl rozdělen na Slovenskou a Českou část
* Přidána funkce zasilkovna_get_cod_value
* Přidána funkce zasilkovna_log
* Zprovozněno přidání adresy pobočky Zásilkovny do emailu.

= 1.2.1 =
* Oprava detekce země a měny, pokud je zásilka potvrzena z administrace eshopu

= 1.2.0 =
* Doplnění nových druhů doprav a zemí, které Zásilkovna nově podporuje
* Úprava vzhledu administrace

= 1.1.2 =
* Oprava drobných syntax chyb

= 1.1.1 =
* Přidána možnost výběru momentu odeslání zásilky do Zásilkovny
* Přidán jazykový po soubor pro překlad


= 1.1.0 =
* Kompletní přepsání struktury pluginu
* Přidání licenčního klíče
* Přidání automatické aktualizace

= 1.0.0 =
* Vydání pluginu