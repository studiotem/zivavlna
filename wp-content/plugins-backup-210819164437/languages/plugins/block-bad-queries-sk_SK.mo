��            )   �      �     �  $   �     �  b   �     2     D  :   \  C   �     �  #   �            
   (     3     I     Y     l     �  %   �  m   �     '  J   :     �     �     �     �  .   �     �  J       a  '   n  
   �  h   �     
       6   4  ;   k     �  )   �     �     �  
   	     	     ,	     E	  $   [	  
   �	  ,   �	  _   �	     
  I   1
  !   {
     �
  #   �
     �
  .   �
                                              
                                                                                    	             BBQ Firewall BBQ Pro: Advanced WordPress Firewall BBQ Version BBQ is a super fast firewall that automatically protects WordPress against malicious URL requests. Block Bad Queries Block Bad Queries (BBQ) Click here to rate and review this plugin at WordPress.org Free and Pro versions of BBQ cannot be activated at the same time.  Get BBQ Pro &raquo; Get BBQ Pro for advanced protection Go&nbsp;Pro Homepage Jeff Starr Please return to the  Plugin Homepage Plugin Information Rate this plugin&nbsp;&raquo; Settings Thanks for using the free version of  The free version is completely plug-&amp;-play, protecting your site automatically with no settings required. Upgrade to BBQ Pro Upgrade your site security with advanced protection and complete control.  Visit the BBQ plugin page Warning: WordPress Admin Area and try again. https://perishablepress.com/block-bad-queries/ https://plugin-planet.com/ PO-Revision-Date: 2019-05-14 14:40:16+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: GlotPress/2.4.0-alpha
Language: sk
Project-Id-Version: Plugins - BBQ: Block Bad Queries - Stable (latest release)
 BBQ Firewall BBQ Pro: pokročilí WordPress Firewall BBQ verzia BBQ je super rýchly firewall, ktorý automaticky chráni WordPress proti škodlivým URL požiadavkám. Block Bad Queries Block Bad Queries (BBQ) Kliknite sem a hodnoťte tento plugin na WordPress.org Nie je možné aktivovať súčasne Free a Pro verzia BBQ . Získať BBQ Pro &raquo; Získať BBQ Pro pre rozšírenú ochranu Prejdi na &nbsp;Pro Hlavná stránka Jeff Starr Prosím vráte sa Hlavná stránka pluginu Informácie o plugine Ohodnoťte tento plugin&nbsp;&raquo; Nastavenia Ďakujeme za používanie bezplatnej verzie  Bezplatná verzia je úplne plug-&amp;-play, chráni vaše stránky automaticky bez nastavenia. Aktualizovať na BBQ Pro Vylepšiť zabezpečenie webu s pokročilou ochranou a úplnou kontrolou. Navštívte stránku BBQ pluginu. Upozronenie: Administrátorská čast WordPressu a skús znova. https://perishablepress.com/block-bad-queries/ https://plugin-planet.com/ 