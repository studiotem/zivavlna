# Translation of Plugins - Classic Editor - Stable (latest release) in Slovak
# This file is distributed under the same license as the Plugins - Classic Editor - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2020-09-18 07:18:26+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n >= 2 && n <= 4) ? 1 : 2);\n"
"X-Generator: GlotPress/3.0.0-alpha.2\n"
"Language: sk\n"
"Project-Id-Version: Plugins - Classic Editor - Stable (latest release)\n"

#: classic-editor.php:822
msgctxt "Editor Name"
msgid "Edit (classic editor)"
msgstr "Upraviť (klasický editor)"

#: classic-editor.php:815
msgctxt "Editor Name"
msgid "Edit (block editor)"
msgstr "Upraviť (editor blokov)"

#: classic-editor.php:489
msgid "Change settings"
msgstr "Zmeniť nastavenia"

#: classic-editor.php:476
msgid "Default editor for all sites"
msgstr "Predvolený editor pre všetky stránky "

#: classic-editor.php:472
msgid "Editor Settings"
msgstr "Nastavenia editoru"

#: classic-editor.php:456
msgid "Default Editor"
msgstr "Predvolený editor"

#: classic-editor.php:856
msgctxt "Editor Name"
msgid "block editor"
msgstr "blokový editor"

#: classic-editor.php:853
msgctxt "Editor Name"
msgid "classic editor"
msgstr "Klasický editor"

#. translators: %s: post title
#: classic-editor.php:817
msgid "Edit &#8220;%s&#8221; in the block editor"
msgstr "Upraviť &#8222;%s&#8220; v klasickom editore"

#: classic-editor.php:657
msgid "Switch to block editor"
msgstr "Zmeniť na editor blokov"

#: classic-editor.php:681
msgid "Switch to classic editor"
msgstr "Zmeniť na klasický editor"

#: classic-editor.php:493
msgid "By default the block editor is replaced with the classic editor and users cannot switch editors."
msgstr "V predvolenom nastavení je editor blokov nahradený klasickým editorom a používatelia nemôžu meniť editory."

#: classic-editor.php:492
msgid "Allow site admins to change settings"
msgstr "Povoliť administrátorom webovej stránky meniť nastavenia"

#: classic-editor.php:638
msgid "Editor"
msgstr "Editor"

#: classic-editor.php:431
msgid "No"
msgstr "Nie"

#: classic-editor.php:427
msgid "Yes"
msgstr "Áno"

#: classic-editor.php:354
msgid "Allow users to switch editors"
msgstr "Povoliť používateľom prepnúť editory"

#: classic-editor.php:353
msgid "Default editor for all users"
msgstr "Predvolený editor pre všetkých používateľov"

#. Author URI of the plugin
msgid "https://github.com/WordPress/classic-editor/"
msgstr "https://github.com/WordPress/classic-editor/"

#. Plugin URI of the plugin
msgid "https://wordpress.org/plugins/classic-editor/"
msgstr "https://sk.wordpress.org/plugins/classic-editor/"

#. Author of the plugin
msgid "WordPress Contributors"
msgstr "WordPress prispievatelia"

#. Description of the plugin
msgid "Enables the WordPress classic editor and the old-style Edit Post screen with TinyMCE, Meta Boxes, etc. Supports the older plugins that extend this screen."
msgstr "Povoľuje klasický editor WordPress a starú obrazovku pre úpravu článku s TinyMCE editorom, vlastnými poľami a ďalšími. Podporuje tiež rozšírenia, ktoré túto pridávajú funkcie tejto obrazovky. "

#. Plugin Name of the plugin
msgid "Classic Editor"
msgstr "Klasický editor"

#. translators: %s: post title
#: classic-editor.php:824
msgid "Edit &#8220;%s&#8221; in the classic editor"
msgstr "Upraviť &#8220;%s&#8221; v klasickom editore"

#: classic-editor.php:700
msgid "Settings"
msgstr "Nastavenia"