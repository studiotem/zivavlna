<?php
if (!function_exists('na_shortcode_info_teams')) {
    function na_shortcode_info_teams($atts,$output)
    {
        $atts = shortcode_atts(

            array(
                'title' => '',
                'block_content' => '',
                'css' => '',
                'items' => '',
            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'info-teams' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_info_teams', 'na_shortcode_info_teams');

add_action('vc_before_init', 'na_info_teams_integrate_vc');

if (!function_exists('na_info_teams_integrate_vc')) {
    function na_info_teams_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Info Teams', 'nano'),
                'base' => 'na_info_teams',
                'icon' => 'icon-wpb-information-white',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Block About Teams', 'nano'),
                'params' => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Title','nano'),
                        "param_name" => "title",
                        'admin_label' => true,
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__('Description','nano'),
                        "param_name" => "block_content",
                    ),
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Team Settings', 'nano' ),
                        'param_name' => 'items',
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("upload an image. min size :270x300", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'	=> esc_html__('Image', 'nano' ),

                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Name','nano'),
                                "param_name" => "title_box",
                                'admin_label' => true,
                            ),
                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__('Position','nano'),
                                "param_name" => "content_box",
                            ),
                        ),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}