<?php
if (!function_exists('na_shortcode_intro_images')) {
    function na_shortcode_intro_images($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'title_box'     => '',
                'image_box'     => '',
                'content_box'   => '',
                'link_box'      => '#',
                'css' => '',
                'items' => '',
            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'intro-images' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_intro_images', 'na_shortcode_intro_images');

add_action('vc_before_init', 'na_intro_images_integrate_vc');

if (!function_exists('na_intro_images_integrate_vc')) {
    function na_intro_images_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Slider Images', 'nano'),
                'base' => 'na_intro_images',
                'icon' => 'vc_icon-vc-hoverbox',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Image Intro ', 'nano'),
                'params' => array(

                    array(
                        "type" => "attach_image",
                        "description" => esc_html__("upload an image.", 'nano'),
                        "param_name" => "image_box",
                        "value" => '',
                        'heading'	=> esc_html__('Image', 'nano' ),

                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__('Title','nano'),
                        "param_name" => "title_box",
                        'admin_label' => true,
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__('Description','nano'),
                        "param_name" => "content_box",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__("Link Url", 'nano'),
                        "param_name" => "link_box",
                        "value" => '#',
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}