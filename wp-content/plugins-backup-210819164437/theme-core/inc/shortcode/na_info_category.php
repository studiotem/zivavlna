<?php

if (!function_exists('na_shortcode_info_category')) {
    function na_shortcode_info_category($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'image_f'     => '',
                'des_f'       => 'NanoAgency Theme 2018',
                'title_box'     => 'Designed for men, Just for you.',
                'content_box'   => '',
                'link'          => '',
                'image_t'       => '',
                'des_t'         => 'All New OVER Collections 2018',
                'box_layouts'   => 'style-1',
                'css' => '',

            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'info-category' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_info_category', 'na_shortcode_info_category');

add_action('vc_before_init', 'na_info_category_integrate_vc');

if (!function_exists('na_info_category_integrate_vc')) {
    function na_info_category_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Info Category', 'nano'),
                'base' => 'na_info_category',
                'icon' => 'icon-infoCategory',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Introduction to your special category ', 'nano'),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__('Highlights photos', 'nano'),
                        'value' => '',
                        'description' => esc_html__( 'Select images from media library. Default to use 570x700 size.', 'nano' ),
                        'param_name' => 'image_f',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Description for highlights photos ', 'nano'),
                        'value' => esc_html('NanoAgency Theme 2018'),
                        'param_name' => 'des_f',
                    ),

                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => esc_html('Designed for men, Just for you.'),
                        'param_name' => 'title_box',
                    ),

                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__('Description','nano'),
                        "param_name" => "content_box",
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => esc_html__( 'URL (Link)', 'js_composer' ),
                        'param_name' => 'link',
                        'description' => esc_html__( 'Add link to button (Important: adding link automatically adds button).', 'nano' ),
                    ),

                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__('Description photos', 'nano'),
                        'value' => '',
                        'description' => esc_html__( 'Select images from media library.Default to use 600x600 size.', 'nano' ),
                        'param_name' => 'image_t',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Description for Highlights photos', 'nano'),
                        'value' => esc_html('All New OVER Collections 2018'),
                        'param_name' => 'des_t',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Layout Styles', 'nano'),
                        'value' => array(
                            esc_html__('Style 1', 'nano') => 'style-1',
                            esc_html__('Style 2', 'nano') => 'style-2',
                        ),
                        'std' => 'style-1',
                        'param_name' => 'box_layouts',
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}