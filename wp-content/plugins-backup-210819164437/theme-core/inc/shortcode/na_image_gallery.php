<?php

if (!function_exists('na_shortcode_image_gallery')) {
    function na_shortcode_image_gallery($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'icon' => '',
                'title' => 'Block Gallery',
                'content_box' => '',
                'images' => '',
                'img_size' => '950x670',
                'link' => '',
                'css' => '',

            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'image-gallery' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_image_gallery', 'na_shortcode_image_gallery');

add_action('vc_before_init', 'na_image_gallery_integrate_vc');

if (!function_exists('na_image_gallery_integrate_vc')) {
    function na_image_gallery_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Image Gallery', 'nano'),
                'base' => 'na_image_gallery',
                'icon' => 'icon-imageGallery',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Introduction text and Slider box', 'nano'),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__('Icon', 'nano'),
                        'value' => '',
                        'description' => esc_html__( 'Select images from media library.', 'nano' ),
                        'param_name' => 'icon',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => esc_html('Block Image Gallery'),
                        'param_name' => 'title',
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__('Description','nano'),
                        "param_name" => "content_box",
                    ),
                    array(
                        'type' => 'attach_images',
                        'heading' => esc_html__('Image Gallery', 'nano'),
                        'value' => '',
                        'description' => esc_html__( 'Select images from media library.', 'nano' ),
                        'param_name' => 'images',
                        'group' => 'Gallery Group',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Image size', 'nano' ),
                        'param_name' => 'img_size',
                        'value' => '950x670',
                        'description' => esc_html__( 'Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Default to use "950x670" size.', 'nano' ),
                        'group' => 'Gallery Group',
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => esc_html__( 'URL (Link)', 'js_composer' ),
                        'param_name' => 'link',
                        'description' => esc_html__( 'Add link to button (Important: adding link automatically adds button).', 'nano' ),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),

                )
            )
        );
    }
}