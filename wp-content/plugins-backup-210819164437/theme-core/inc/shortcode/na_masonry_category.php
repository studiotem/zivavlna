<?php
if (!function_exists('na_shortcode_masonry_category')) {
    function na_shortcode_masonry_category($atts,$output)
    {
        $atts = shortcode_atts(

            array(
                'css' => '',
                'items' => '',
            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'masonry-category' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_masonry_category', 'na_shortcode_masonry_category');

add_action('vc_before_init', 'na_masonry_category_integrate_vc');

if (!function_exists('na_masonry_category_integrate_vc')) {
    function na_masonry_category_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Masonry Categories', 'nano'),
                'base' => 'na_masonry_category',
                'icon' => 'icon-wpb-application-icon-large',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Box with layout is masonry Category', 'nano'),
                'params' => array(
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Masonry Categories Layout Settings', 'nano' ),
                        'param_name' => 'items',
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("upload an image.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'	=> esc_html__('Category Image', 'nano' ),

                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Title','nano'),
                                "param_name" => "title_box",
                                'admin_label' => true,
                            ),

                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__('Description','nano'),
                                "param_name" => "content_box",
                            ),

                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__( 'URL (Link)', 'nano' ),
                                'param_name' => 'link_box',
                            ),

                        ),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}