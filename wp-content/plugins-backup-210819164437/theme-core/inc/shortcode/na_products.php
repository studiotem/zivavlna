<?php

/* ================================================================================== */
/*      Products Shortcode
/* ================================================================================== */
if (!function_exists('na_shortcode_products')) {
    function na_shortcode_products($atts, $content) {
        $atts = shortcode_atts(array(
            'box_title'             => 'Block Products',
            'style_title'           => 'center',
            'post_type' 			=> 'product',
            'number'		        => 8,
            'meta_query'            => '',
            'tax_query'             => array(),
            'list_type'             => 'latest',
            'box_layouts'		    => 'grid',
            'ignore_sticky_posts'	=> 1,
            'show'					=> '',
            'category'              =>'',
            'box_content'           =>'',
            'link'                  =>'',
            'padding_bottom_module' => '0px',
        ), $atts);

        ob_start();
        nano_template_part('shortcode', 'products' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_products', 'na_shortcode_products');

add_action('vc_before_init', 'na_products_integrate_vc');

if (!function_exists('na_products_integrate_vc')) {
    function na_products_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Products', 'nano'),
                'base' => 'na_products',
                'icon' => 'icon-products',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Block products .', 'nano'),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => 'Block Products',
                        'param_name' => 'box_title',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Style Title', 'nano'),
                        'param_name' => 'style_title',
                        'std' => 'center',
                        'value' => array(
                            esc_html__('Center', 'nano') => 'center',
                            esc_html__('Left', 'nano') => 'left',
                        ),

                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__('Position','nano'),
                        "param_name" => "box_content",
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Show product type', 'nano'),
                        'param_name' => 'list_type',
                        'value' => array(
                            esc_html__('Latest Products', 'nano') => 'latest',
                            esc_html__('Featured Products', 'nano') => 'featured',
                            esc_html__('Best Selling Products', 'nano') => 'best-selling',
                            esc_html__('TopRated Products', 'nano') => 'toprate',
                            esc_html__('Special Products', 'nano') => 'onsale'
                        ),
                        'std' => 'latest',
                    ),
                    array(
                        'type' => 'nano_product_categories',
                        'heading' => esc_html__( 'Category','nano' ),
                        'param_name' => 'category',
                        'description' => esc_html__( 'Product category list','nano'),
                    ),
                    array(
                        'type' => 'nano_image_radio',
                        'heading' => esc_html__('Layout Product', 'nano'),
                        'value' => array(
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-grid.jpg', 'nano')        => 'grid',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/cat-duel.jpg', 'nano')        => 'duel',
                        ),
                        'width' => '100px',
                        'height' => '70px',
                        'param_name' => 'box_layouts',
                        'std' => 'grid',
                        'description' => esc_html__('Select layout type', 'nano'),
                        'group' => __( 'Layout options', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Number of Products show on the block', 'nano'),
                        'value' => 8,
                        'param_name' => 'number',
                        'group' => __( 'Layout options', 'nano' ),
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => esc_html__( 'URL (Link)', 'js_composer' ),
                        'param_name' => 'link',
                        'description' => esc_html__( 'When you add this link, then change the product layout title and do not display Load More Button', 'nano' ),
                        'group' => __( 'Layout options', 'nano' ),
                    ),
                )
            )
        );
    }
}