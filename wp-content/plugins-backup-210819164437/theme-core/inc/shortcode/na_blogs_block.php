<?php
if (!function_exists('nano_shortcode_block_posts')) {
    function nano_shortcode_block_posts($atts)
    {
        $atts = shortcode_atts(array(
            'title'             => '',
            'type_post'         => 'no',
            'category_name'     => '',
            'layout_section'    => 'layout_grid',
            'layout_post'       => 'grid',
            'content_box'       => '',
            'number_post'       => 8,
            'show_post'         => 3,
            'el_class'          => ''
        ), $atts);
        ob_start();
        nano_template_part('shortcode', 'blog-block', array('atts' => $atts));
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}
add_shortcode('block_posts', 'nano_shortcode_block_posts');

add_action('vc_before_init', 'nano_block_posts_integrate_vc');

if (!function_exists('nano_block_posts_integrate_vc')) {
    function nano_block_posts_integrate_vc()
    {
        vc_map(array(
            'name' => esc_html__('NA:Posts', 'nano'),
            'base' => 'block_posts',
            'description' => esc_html__('Show Block Posts.', 'nano'),
            'category' => esc_html__('NA', 'nano'),
            'icon' => 'icon-boxBlog',
            "params" => array(
                array(
                    "type" => "textfield",
                    "heading" => esc_html__("Title", 'nano'),
                    "param_name" => "title",
                    'value' => 'Block Posts',
                    "admin_label" => true
                ),
                array(
                    "type" => "textarea",
                    "class" => "",
                    "heading" => esc_html__('Description','nano'),
                    "param_name" => "content_box",
                ),
                array(
                    "type" => "dropdown",
                    "heading" => esc_html__("Type Post", 'nano'),
                    "param_name" => "type_post",
                    "value" => array(
                        esc_html__('News ', 'nano' )        => 'news',
                        esc_html__('Featured', 'nano' )     => 'featured',
                    ),
                    'std' => 'views',
                    "description" => esc_html__("The criteria you want to show",'nano')
                ),
                array(
                    "type" => "nano_post_categories",
                    "heading" => __("Category IDs", 'nano'),
                    "description" => __("Select category", 'nano'),
                    "param_name" => "category_name",
                    "admin_label" => true
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __('Block Layout Of the post', 'nano'),
                    'value' => array(
                        esc_html__('Grid', 'nano') => 'layout_grid',
                        esc_html__('Carousel', 'nano') => 'layout_carouse',
                    ),
                    'std' => 'layout_grid',
                    'param_name' => 'layout_section',
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Posts Count", 'nano'),
                    "param_name" => "number_post",
                    "value" => '6'
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Posts show in a row", 'nano'),
                    "param_name" => "show_post",
                    "value" => '3'
                ),
                array(
                    'type' => 'nano_image_radio',
                    'heading' => esc_html__('Layout a post', 'nano'),
                    'value' => array(
                        esc_html__(NANO_PLUGIN_URL.'assets/images/box-list.jpg', 'nano') => 'list',
                        esc_html__(NANO_PLUGIN_URL.'assets/images/box-grid.jpg', 'nano') => 'grid',
                        esc_html__(NANO_PLUGIN_URL.'assets/images/box-tran.jpg', 'nano') => 'tran',
                    ),
                    'width' => '100px',
                    'height' => '70px',
                    'param_name' => 'layout_post',
                    'std' => 'tran',
                )
            )
        ));
    }
}
