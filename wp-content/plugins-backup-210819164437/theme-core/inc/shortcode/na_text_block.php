<?php
if (!function_exists('na_shortcode_text_block')) {
    function na_shortcode_text_block($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'image' => '',
                'title' => 'Stunning || Fashion',
                'content_box' => '',
                'css' => '',
                'link' => '',

            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'text-block' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_text_block', 'na_shortcode_text_block');

add_action('vc_before_init', 'na_text_block_integrate_vc');

if (!function_exists('na_text_block_integrate_vc')) {
    function na_text_block_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Text Block', 'nano'),
                'base' => 'na_text_block',
                'icon' => 'icon-wpb-ui-custom_heading',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Image Banner', 'nano'),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__('Image', 'nano'),
                        'value' => '',
                        'param_name' => 'image',
                    ),
                    array(
                        'type' => 'textarea',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => esc_html('Text Block'),
                        'param_name' => 'title',
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__('Description','nano'),
                        "param_name" => "content_box",
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}