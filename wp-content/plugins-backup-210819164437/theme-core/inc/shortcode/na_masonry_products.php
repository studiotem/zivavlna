<?php

/* ================================================================================== */
/*      Products Shortcode
/* ================================================================================== */
if (!function_exists('na_shortcode_masonry_products')) {
    function na_shortcode_masonry_products($atts, $content) {
        $atts = shortcode_atts(array(
            'image_box'             => '',
            'link'                  => '',
            'post_type' 			=> 'product',
            'posts_per_page'        =>  5,
            'meta_query'            => '',
            'tax_query'             => array(),
            'list_type'             => 'latest',
            'ignore_sticky_posts'	=> 1,
            'show'					=> '',
            'style'                 =>'default',
            'category'              =>'',
            'box_layouts'           =>'na-left',

        ), $atts);

        ob_start();
        nano_template_part('shortcode', 'masonry-products' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_masonry_products', 'na_shortcode_masonry_products');

add_action('vc_before_init', 'na_masonry_products_integrate_vc');

if (!function_exists('na_masonry_products_integrate_vc')) {
    function na_masonry_products_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: masonry Products', 'nano'),
                'base' => 'na_masonry_products',
                'icon' => 'vc_icon-vc-media-grid',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Block masonry Products .', 'nano'),
                'params' => array(
                    array(
                        "type" => "attach_image",
                        "description" => esc_html__("upload an image.", 'nano'),
                        "param_name" => "image_box",
                        "value" => '',
                        'heading'	=> esc_html__('Banner Image', 'nano' ),

                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'URL (Link)', 'js_composer' ),
                        'param_name' => 'link',
                        'description' => esc_html__( 'Add link to Image', 'nano' ),
                    ),

                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Show product type', 'nano'),
                        'param_name' => 'list_type',
                        'value' => array(
                            esc_html__('Latest Products', 'nano') => 'latest',
                            esc_html__('Featured Products', 'nano') => 'featured',
                            esc_html__('Best Selling Products', 'nano') => 'best-selling',
                            esc_html__('TopRated Products', 'nano') => 'toprate',
                            esc_html__('Special Products', 'nano') => 'onsale'
                        ),
                        'std' => 'latest',
                        'group' => __( 'Product options', 'nano' ),
                    ),
                    array(
                        'type' => 'nano_product_categories',
                        'heading' => esc_html__( 'Category','nano' ),
                        'param_name' => 'category',
                        'description' => esc_html__( 'Product category list','nano'),
                        'group' => __( 'Product options', 'nano' ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Layout Styles', 'nano'),
                        'value' => array(
                            esc_html__('Banner - Left', 'nano') => 'na-left',
                            esc_html__('Banner - Right', 'nano') => 'na-right',
                            esc_html__('Banner - Center', 'nano') => 'na-center',
                        ),
                        'std' => 'na-left',
                        'param_name' => 'box_layouts',
                    ),

                )
            )
        );
    }
}