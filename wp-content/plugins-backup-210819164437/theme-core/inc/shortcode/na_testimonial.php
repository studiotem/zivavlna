<?php
if (!function_exists('na_shortcode_testimonial')) {
    function na_shortcode_testimonial($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'title' => '',
                'css' => '',
                'items' => '',
            ), $atts);
        ob_start();
            nano_template_part('shortcode', 'testimonial' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_testimonial', 'na_shortcode_testimonial');

add_action('vc_before_init', 'na_testimonial_integrate_vc');

if (!function_exists('na_testimonial_integrate_vc')) {
    function na_testimonial_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Testimonial', 'nano'),
                'base' => 'na_testimonial',
                'icon' => 'icon-testimonial',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show customer comments', 'nano'),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => esc_html('Text Block'),
                        'param_name' => 'title',
                    ),
                    
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Customer comments', 'nano' ),
                        'param_name' => 'items',
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("upload an image.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'	=> esc_html__('Customer Image', 'nano' ),

                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Name','nano'),
                                "param_name" => "title_box",
                                'admin_label' => true,
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__('Position', 'nano'),
                                'value' => esc_html('Designer'),
                                'param_name' => 'position_box',
                            ),
                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__('Comments','nano'),
                                "param_name" => "content_box",
                            ),

                        ),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}