<?php
/**
 * The default template for displaying content
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$idran= random_int(0,99);

//category
if(isset($atts['category']) && empty($atts['category'])){
    $args = array(
        'post_type'      => 'product',
        'post_status'    => 'publish',
        'posts_per_page' => -1,
    );
    $query_shop = new WP_Query($args);
}
else{
    $terms=explode(',', $atts['category']);
    $args= array(
        array(
            'taxonomy'          => 'product_cat',
            'field'             => 'slug',
            'terms'             => $terms,
            'post_status'       => 'publish'
        )

    );
    $atts['tax_query']= $args;

}

//list tabs
$types = explode(',',$atts['list_type']);
foreach ($types as $type) {
    $list_types[$type] = get_TabTitle($type);
}
switch ($atts['list_type']) {
    case 'best-selling':
        $atts['meta_key'] = 'total_sales';
        $atts['order']    = 'DESC';
        $atts['orderby']  = 'meta_value_num';
        $meta_query = WC()->query->get_meta_query();
        $atts['tax_query']= $args;
        $atts['post__in'] = '';
        $atts['meta_query'] = $meta_query;
        break;

    case 'toprate':
        $atts['meta_key'] = '_wc_average_rating';
        $atts['orderby']  = 'meta_value_num';
        $meta_query = WC()->query->get_meta_query();
        $atts['tax_query']= $args;
        $atts['post__in'] = '';
        $atts['meta_query'] = $meta_query;
        break;

    case 'latest':
        $meta_query = WC()->query->visibility_meta_query();
        $meta_query = WC()->query->stock_status_meta_query();
        $atts['meta_query'] = $meta_query;
        break;

    case 'featured':
        $atts['meta_key'] = '';
        $atts['orderby']  = '';
        $atts['post__in'] = '';
        $meta_query = array_merge( $atts['tax_query'], WC()->query->get_tax_query() );
        $meta_query[]= array(
            'taxonomy'         => 'product_visibility',
            'terms'            => 'featured',
            'field'            => 'name',
            'operator'         => 'IN',
            'include_children' => false,
        );
        $atts['tax_query'] = $meta_query;
        break;

    case 'onsale':
        $product_ids_on_sale = wc_get_product_ids_on_sale();
        $product_ids_on_sale[]  = 0;
        $atts['meta_key'] = '';
        $atts['orderby']  = '';
        $atts['post__in'] = $product_ids_on_sale;
        $atts['tax_query']= $args;
        $meta_query = WC()->query->get_meta_query();
        $atts['meta_query'] = $meta_query;
        break;
}


$query_shop = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $atts ) );
$query_shop->query( $atts );
$img = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_box'], 'thumb_size' => '330x670' ) );
?>

<div class="block-masonry-products block <?php echo esc_attr($atts['box_layouts']);?>">
    <div class="widgetcontent clearfix" id="block-products<?php echo $idran; ?>">

            <div class="masonry-product">
                <div class="rows">
                    <ul class="products-blocks affect-isotope-cats clearfix" data-number="2">

                        <li class="grid-sizer"></li>

                        <?php if(isset($atts['image_box']) && $atts['box_layouts']=='na-left'){?>
                        <li class="grid-banner col-item ">
                            <?php if ($atts['image_box'] && $atts['link']) {?>
                                <a class="link-banner nano-infobanner" href="<?php echo esc_url($atts['link']);?>" target="_blank>"> <?php echo $img['thumbnail']; ?></a>
                            <?php }  ?>
                        </li>
                        <?php }?>

                        <?php
                        $na=1;
                        while ( $query_shop->have_posts() ){
                            $query_shop->the_post();
                            global $product; ?>

                            <?php if(isset($atts['image_box']) && $na==3 && ($atts['box_layouts']=='na-center')){?>
                                <li class="grid-banner col-item">
                                    <?php if ($atts['link']) {?>
                                        <a class="link-banner nano-infobanner" href="<?php echo esc_url($atts['link']);?>" target="_blank>"> <?php echo $img['thumbnail']; ?></a>
                                    <?php }  ?>
                                </li>
                            <?php }?>
                            <?php if(isset($atts['image_box']) && $na==4 && ($atts['box_layouts']=='na-right')){?>
                                <li class="grid-banner col-item">
                                    <?php if ($atts['link']) {?>
                                        <a class="link-banner nano-infobanner" href="<?php echo esc_url($atts['link']);?>" target="_blank>"> <?php echo $img['thumbnail']; ?></a>
                                    <?php }  ?>
                                </li>
                            <?php }?>
                            <li <?php post_class('col-item'); ?>>
                                <?php wc_get_template_part( 'layouts/content-product-trans-large'); ?>
                            </li>
                       <?php $na++; }  wp_reset_postdata();?>

                    </ul>
                </div>
            </div>

   </div>
</div>



