<?php
$class_col ='col-md-3 col-sm-3';
$class_i =0;
$class_col =$atts['column'];
$number=4;
if($class_col       == "col-md-12 col-sm-12"){
    $number=1;
}
elseif($class_col   == 'col-md-6 col-sm-6'){
    $number=2;
}
elseif($class_col   == 'col-md-4 col-sm-6'){
    $number=3;
}
elseif($class_col   == 'col-md-3 col-sm-6'){
    $number=4;
}
elseif($class_col   == 'col-md-2 col-sm-4'){
    $number=6;
}
else{
    $number=3;
}

$query_shop = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $atts ) );
$query_shop->query( $atts );?>

<div class="products_shortcode_wrap product-taxonomy <?php esc_attr( $atts['element_custom_class']);?> ">
    <div class="products row clearfix">
        <?php while ( $query_shop->have_posts() ){
            $query_shop->the_post();

            global $product;
            $class_i ++;

            if($class_i==1){?>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12"><?php wc_get_template_part( 'content', 'product-inner'); ?></div>

            <?php }
            if($class_i==2){?>
                                <div class="col-md-6 col-sm-6 col-xs-12"><?php wc_get_template_part( 'content', 'product-inner'); ?></div>
                            </div>
                        </div>
            <?php }
            if($class_i==3){?>
                        <div class="col-md-12 layout-large">
                            <?php wc_get_template_part( 'content', 'product-inner'); ?>
                        </div>
                    </div>
                </div>
            <?php }
            if($class_i==4){?>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                        <div class="col-md-12 layout-large">
                            <?php wc_get_template_part( 'content', 'product-inner'); ?>
                        </div>
            <?php }
            if($class_i==5){?>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12"><?php wc_get_template_part( 'content', 'product-inner'); ?></div>
            <?php }
            if($class_i==6){?>
                                <div class="col-md-6 col-sm-6 col-xs-12"><?php wc_get_template_part( 'content', 'product-inner'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
            ?>

        <?php
        }
        wp_reset_postdata();?>
    </div>
</div>