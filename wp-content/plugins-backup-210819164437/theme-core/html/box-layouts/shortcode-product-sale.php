<?php
global $product;

$productID = get_the_ID();
$woo_product_style  = get_theme_mod('na_woo_product_style','default');
$date_sale = get_post_meta( $productID, '_sale_price_dates_to', true );

wp_enqueue_script( 'countdown');
?>
<div class="product-block product inner-product-content <?php echo esc_attr($woo_product_style);?>">
    <figure class="caption-image product-image">
        <?php woocommerce_show_product_loop_sale_flash(); ?>

        <a href="<?php the_permalink(); ?>" class="product-image">
            <?php
            /**
             * woocommerce_before_shop_loop_item_title hook
             *
             * @hooked woocommerce_show_product_loop_sale_flash - 10
             * @hooked woocommerce_template_loop_product_thumbnail - 10
             */
            do_action( 'woocommerce_before_shop_loop_item_title' );
            ?>
        </a>
        <div class="button-groups clearfix">
            <?php do_action('na_add_yith' );?>
        </div>
        <?php if($date_sale > time()):?>
            <div class="deal-countdown clearfix " data-countdown="countdown"
                 data-date="<?php echo date('m',$date_sale).'-'.date('d',$date_sale).'-'.date('Y',$date_sale).'-'. date('H',$date_sale) . '-' . date('i',$date_sale) . '-' .  date('s',$date_sale) ; ?>">
            </div>
        <?php endif; ?>
    </figure>
    <div class="caption-product">
        <div class="caption">
            <?php woocommerce_template_loop_rating(); ?>

            <h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <?php
            /**
             * woocommerce_after_shop_loop_item_title hook
             *
             * @hooked woocommerce_template_loop_rating - 5
             * @hooked woocommerce_template_loop_price - 10
             */
            remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_rating',5);
            remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_price',10);
            do_action( 'woocommerce_after_shop_loop_item_title' );
            ?>
            <div class="price-review clearfix">
                <div class="list-price">
                    <?php woocommerce_template_loop_price(); ?>
                </div>

                <div class="product-review">
                    <?php
                    if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
                        $review_count = $product->get_review_count().__(' Reviews','nano');
                        echo esc_html($review_count);
                    }
                    ?>
                </div>
                <div class="product-add-review">
                    <a href="<?php the_permalink(); ?>/#tab-reviews"><?php esc_html__('Add your review','nano') ?></a>
                </div>
                <div class="grid-price">
                    <?php woocommerce_template_loop_price(); ?>
                </div>
            </div>

            <div class="description-product">
                <?php
                woocommerce_template_single_excerpt();
                ?>
            </div>
        </div>
        <div class="ground-addcart">
            <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
        </div>
    </div>


</div>