<?php
global $query_shop;
$query_shop = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $atts ) );
$query_shop->query( $atts );
$na_count = 1;
?>

<div class="products_shortcode_wrap items-duel type-tabShop main-content">

    <ul class="products-block affect-isotope product-cat-duel  col2 row clearfix" data-pages="<?php echo esc_attr($query_shop->max_num_pages);?>">
        <?php while ( $query_shop->have_posts() ){

            $query_shop->the_post();

            if($na_count == 1 || $na_count == 2 || (($na_count - 1)%4)==0 || (($na_count - 2)%4)==0 ) {
                $padding='duel-right';
            }else{
                $padding='duel-left';
            }
            if($na_count == 1 || $na_count == 2 || ((($na_count - 1)%4)==0 || (($na_count - 2)%4)==0) ) {
                $padding_top='duel-top';
            }else{
                $padding_top='duel-bottom';
            }

            ?>
            <li <?php post_class('col-item duel-'.$na_count); ?>>
                <?php wc_get_template_part( 'layouts/content-product-scattered');?>
            </li>


        <?php $na_count++; }
            wp_reset_postdata();

        ?>

    </ul>
    <?php
    if ( empty($atts['link']) ) {
        nano_pagination(3, $query_shop);
    } else{
        $link = trim( $atts['link'] );
        $link = ( '||' === $link ) ? '' : $link;
        $link = vc_build_link( $link );
        if ( strlen( $link['url'] ) > 0 ) {?>
            <a class="btn-readmore" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
        <?php }
    }?>

</div>
