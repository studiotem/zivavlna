<?php

$class_col ='col-md-3 col-sm-3';

$class_col = str_replace('column', 'products-',$atts['column']);

$query_shop = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $atts ) );
$query_shop->query( $atts );?>

<div class="products_shortcode_wrap product-list sidebar woocommerce <?php esc_attr( $atts['element_custom_class']);?> ">
    <div class="products clearfix">
    <?php while ( $query_shop->have_posts() ){
        $query_shop->the_post();
        global $product;
        ?>
        <div class="<?php echo esc_attr($class_col); ?>">
            <?php wc_get_template_part( 'content', 'product-inner'); ?>
        </div>
    <?php
    }
    wp_reset_postdata();?>
</div>
</div>
