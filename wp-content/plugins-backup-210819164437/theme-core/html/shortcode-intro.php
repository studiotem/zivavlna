<?php
$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
?>
<div class="nano-intro block <?php echo esc_attr($css_class); ?> clearfix">

        <?php if($atts['title']) { ?>
            <div class="content-box">
                <div class="border-mask"></div>
                <div class="content">
                    <?php
                    $html = '';
                    if($atts['title']){
                        $html .= '<h3 class="title-text">'. esc_html($atts['title']) .'</h3>';
                    }
                    if($atts['content_box']){
                        $html .= '<p class="content-text">'. wpb_js_remove_wpautop($atts['content_box']).'</p>';
                    }
                    $link = trim( $atts['link'] );
                    $link = ( '||' === $link ) ? '' : $link;
                    $link = vc_build_link( $link );
                    if ( strlen( $link['url'] ) > 0 ) {
                        $html .= '<a class="btn btn-link" href="'.esc_url($link['url']).'" title="'.esc_attr($link['title']).'" target="'. esc_attr($link['target']).'" rel="'. esc_attr($link['rel']).'">'.esc_html($link['title']).'</a>';
                    }
                    echo $html;
                    ?>
                </div>
            </div>
        <?php } ?>

        <?php if($atts['image']) { ?>
            <?php
            $img         = wp_get_attachment_image_src( $atts['image'], 'full' );
            $style_css ='';

            if($img){
                $bg_image          ="background-image:url('$img[0]')";
                $style_css            ='style = '.$bg_image.'';
            }
            ?>
            <div class="image-box bg-cover bg-lg" <?php echo esc_attr($style_css);?>> </div>
        <?php } ?>

</div>