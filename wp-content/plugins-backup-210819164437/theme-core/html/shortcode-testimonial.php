<?php

$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items              = (array) vc_param_group_parse_atts($atts['items']);
?>
<div class="block nano-testimonial <?php echo esc_attr($css_class);?>" data-rtl="true">
    <?php if ( $atts['title'] ) {?>
        <h3 class="block-title">
            <?php echo esc_html( $atts['title'] ); ?>
        </h3>
    <?php }?>
    <div class="block-content items-testimonial items-carousel clearfix">
            <?php

            foreach ( $items as $item ) {?>

                <div class="testimonial-box clearfix">

                    <?php if(isset($item['image_box'])) { ?>
                        <div class="image-box">
                            <?php echo wp_get_attachment_image($item['image_box'],array(100,100),true); ?>
                        </div>
                    <?php } ?>

                    <div class="box-content clearfix">

                        <?php  if(isset($item['content_box'])){?>
                            <div class="des-box"> <?php echo esc_html($item['content_box']);?></div>
                        <?php }?>
                        <div class="box-customer clearfix">
                            <?php if(($item['title_box'])){?>
                                <div class="name-box"><?php echo esc_html($item['title_box']); ?></div>
                            <?php }?>
                            <?php if(isset($item['position_box'])){?>
                                <div class="position-box"><?php echo esc_html($item['position_box']); ?></div>
                            <?php }?>
                        </div>

                    </div>
                </div>

            <?php }
            ?>
    </div>
</div>