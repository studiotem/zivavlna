<?php 
$na 		= 0;
$css_class  = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items		= (array) vc_param_group_parse_atts($atts['items']);
?>
<div class="block block-service na-service">
    <div class="block-content items-service clearfix">
        <?php foreach ( $items as $item ) { 
            $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => 'full')); ?>
			<div class="row">
				<?php if($na%2){ ?>
					<div class="col-md-6"></div> 
				<?php } ?>
				<div class="col-md-6">
	                <div class="service-box col-item clearfix">
	                    <div class="service-box-inner">
	                        <?php if($item['image_box']) { ?>
	                            <div class="entry-image"> 
									<?php if(isset($item['link_box'])) { ?><a href="<?php echo esc_url($item['link_box']); ?>"><?php } ?>
	                                    <?php echo $img['thumbnail']; ?> 
									<?php if(isset($item['link_box'])) { ?></a><?php } ?>
	                            </div>
	                        <?php } ?>
	                        <div class="box-content clearfix">
	                            <div class="entry-content-inner">
	                                <?php if($item['title_box']){
	                                    //echo '<h4 class="title-box"> ';
	                                    if(isset($item['link_box'])) {
	                                    	echo '<a href="'.$item['link_box'].'">';
	                                    }
	                                    echo nl2br($item['title_box']);
	                                    if(isset($item['link_box'])) {
	                                    	echo '</a>';
	                                    }
	                                    //echo '</h4>';
	                                } ?>
	                            </div>
	                        </div>
	                    </div>
	                </div>
				</div> 
				<?php if($na%2){ ?>
				
				<?php }else{ ?>
					<div class="col-md-6"></div> 
				<?php } ?> 
				<?php $na++; ?>
			</div>
        <?php } ?>
    </div>
</div>