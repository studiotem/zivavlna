<?php
?>
<div class="nano-menu block clearfix">

        <div class="content-box">
            <div class="content">
                <?php
                $html = '';
                if($atts['title']){
                    $html .= '<h3 class="title-text">'. esc_html($atts['title']) .'</h3>';
                }
                if($atts['nav_menu']){
                    wp_nav_menu( array(
                        'menu'        =>$atts['nav_menu'],
                        'menu_class'     => 'nav navbar-nav na-menu mega-menu',
                        'container_id'   => 'vertical-primary',
                        'walker'         => new Sok_Menu_Maker_Walker(),
                    ) );
                }
                echo $html;
                ?>
            </div>
        </div>

</div>