<?php
$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
?>
<div class="nano-textBlock block <?php echo esc_attr($css_class); ?> clearfix">
        <div class="image-box">
            <?php if($atts['image']) { ?>
                <?php
                $img = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image'], 'thumb_size' => 'full' ) );
                echo $img['thumbnail'];
                ?>
            <?php } ?>
        </div>
        <div class="content-box">
            <?php if($atts['title']){?>
                <h3 class="block-title"><?php echo ent2ncr($atts['title']); ?></h3>
            <?php }?>
            <div class="content">
                <?php
                if($atts['content_box']){?>
                    <p class="content-text"><?php  echo ent2ncr($atts['content_box']); ?> </p>
               <?php  } ?>
            </div>
        </div>


</div>