<?php

$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$images_ids         = empty( $atts['images'] ) ? array() : explode( ',', trim( $atts['images'] ) );
?>
<div class="widget na-imageGallery <?php echo esc_attr($css_class); ?>">
    <div class="entry-content clearfix">
        <div class="entry-content-inner clearfix">
            <?php if($atts['icon']) { ?>
                    <div class="icon">
                        <?php echo wp_get_attachment_image($atts['icon'], 'full'); ?>
                    </div>
            <?php } ?>
            <?php if ( $atts['title'] ) {?>
                <h3 class="title-block title-box">
                    <?php echo esc_html( $atts['title'] ); ?>
                </h3>
            <?php }?>
            <div class="content-box clearfix">
                <?php
                if(isset($atts['content_box'])){?>
                    <div  class="des"><?php echo esc_html($atts['content_box']);?> </div>
                <?php }

                $link = trim( $atts['link'] );
                $link = ( '||' === $link ) ? '' : $link;
                $link = vc_build_link( $link );
                if ( strlen( $link['url'] ) > 0 ) {?>
                    <a class="btn btn-link" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                <?php }
                ?>
            </div>
        </div>
    </div>
    <div class="entry-image slider_horizontal clearfix">
        <ul class="gallery-main">
            <?php
            $output='';
            foreach ( $images_ids as $image ) {
                $img = wpb_getImageBySize( array( 'attach_id' => (int) $image, 'thumb_size' => $atts['img_size'] ) );
                $output .= ( $img ? '<li>' . $img['thumbnail'] . '</li>' : '<li><img width="950" height="670" test="' . $image . '" src="' . vc_asset_url( 'vc/blank.gif' ) . '" class="attachment-thumbnail" alt="" title="" /></li>' );
            }
            echo $output;
            ?>
        </ul>
        <ul class="gallery-nav">
            <?php
            $output='';
            foreach ( $images_ids as $image ) {
                $img = wpb_getImageBySize( array( 'attach_id' => (int) $image, 'thumb_size' => '365x220' ) );
                $output .= ( $img ? '<li>' . $img['thumbnail'] . '</li>' : '<li><img width="365" height="220" test="' . $image . '" src="' . vc_asset_url( 'vc/blank.gif' ) . '" class="attachment-thumbnail" alt="" title="" /></li>' );
            }
            echo $output;
            ?>
        </ul>
    </div>
</div>
