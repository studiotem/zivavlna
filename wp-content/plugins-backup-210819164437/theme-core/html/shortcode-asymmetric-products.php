<?php
/**
 * The default template for displaying content
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$idran= random_int(0,99);

//category
if(isset($atts['category']) && empty($atts['category'])){
    $args = array(
        'post_type'      => 'product',
        'post_status'    => 'publish',
        'posts_per_page' => -1,
    );
    $query_shop = new WP_Query($args);
}
else{
    $terms=explode(',', $atts['category']);
    $args= array(
        array(
            'taxonomy'          => 'product_cat',
            'field'             => 'slug',
            'terms'             => $terms,
            'post_status'       => 'publish'
        )

    );
    $atts['tax_query']= $args;

}

//list tabs
$types = explode(',',$atts['list_type']);
foreach ($types as $type) {
    $list_types[$type] = get_TabTitle($type);
}
switch ($atts['list_type']) {
    case 'best-selling':
        $atts['meta_key'] = 'total_sales';
        $atts['order']    = 'DESC';
        $atts['orderby']  = 'meta_value_num';
        $meta_query = WC()->query->get_meta_query();
        $atts['tax_query']= $args;
        $atts['post__in'] = '';
        $atts['meta_query'] = $meta_query;
        break;

    case 'toprate':
        $atts['meta_key'] = '_wc_average_rating';
        $atts['orderby']  = 'meta_value_num';
        $meta_query = WC()->query->get_meta_query();
        $atts['tax_query']= $args;
        $atts['post__in'] = '';
        $atts['meta_query'] = $meta_query;
        break;

    case 'latest':
        $meta_query = WC()->query->visibility_meta_query();
        $meta_query = WC()->query->stock_status_meta_query();
        $atts['meta_query'] = $meta_query;
        break;

    case 'featured':
        $atts['meta_key'] = '';
        $atts['orderby']  = '';
        $atts['post__in'] = '';
        $meta_query = array_merge( $atts['tax_query'], WC()->query->get_tax_query() );
        $meta_query[]= array(
            'taxonomy'         => 'product_visibility',
            'terms'            => 'featured',
            'field'            => 'name',
            'operator'         => 'IN',
            'include_children' => false,
        );
        $atts['tax_query'] = $meta_query;
        break;

    case 'onsale':
        $product_ids_on_sale = wc_get_product_ids_on_sale();
        $product_ids_on_sale[]  = 0;
        $atts['meta_key'] = '';
        $atts['orderby']  = '';
        $atts['post__in'] = $product_ids_on_sale;
        $atts['tax_query']= $args;
        $meta_query = WC()->query->get_meta_query();
        $atts['meta_query'] = $meta_query;
        break;
}


$query_shop = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $atts ) );
$query_shop->query( $atts );
?>

<div class="block-asymmetric-products block <?php echo esc_attr($atts['box_layouts']);?>">
    <div class="widgetcontent clearfix" id="block-products<?php echo $idran; ?>">
        <div class="row">
                <?php if($atts['box_layouts']=='style-2') :?>
                    <div class="col-md-8 col-sm-12">
                        <div class="asymmetric-product">
                            <div class="row">
                                <ul class="products-blocks items-scattered clearfix" data-number="2">
                                    <?php while ( $query_shop->have_posts() ){
                                        $query_shop->the_post();
                                        global $product; ?>
                                        <li <?php post_class('col-item'); ?>>
                                            <?php wc_get_template_part( 'layouts/content-product-grid'); ?>
                                        </li>
                                        <?php
                                    }
                                    wp_reset_postdata();?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="asymmetric-title">
                            <?php if ( $atts['title-block'] ) {?>
                                <h3 class="block-title clearfix">
                                    <?php echo esc_html( $atts['title-block'] ); ?>
                                </h3>
                            <?php }?>
                            <?php if ( $atts['content-block'] ) {?>
                                <div class="block-content clearfix">
                                    <?php echo esc_html( $atts['content-block'] ); ?>
                                </div>
                            <?php }?>
                            <?php
                            $link = trim( $atts['link'] );
                            $link = ( '||' === $link ) ? '' : $link;
                            $link = vc_build_link( $link );
                            if ( strlen( $link['url'] ) > 0 ) {?>
                            <a class="btn btn-link" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                            <?php }  ?>

                        </div>
                    </div>
                <?php else:?>
                    <div class="col-md-4 col-sm-12">
                        <div class="asymmetric-title">
                            <?php if ( $atts['title-block'] ) {?>
                                <h3 class="block-title clearfix">
                                    <?php echo esc_html( $atts['title-block'] ); ?>
                                </h3>
                            <?php }?>
                            <?php if ( $atts['content-block'] ) {?>
                                <div class="block-content clearfix">
                                    <?php echo esc_html( $atts['content-block'] ); ?>
                                </div>
                            <?php }?>
                            <?php
                            $link = trim( $atts['link'] );
                            $link = ( '||' === $link ) ? '' : $link;
                            $link = vc_build_link( $link );
                            if ( strlen( $link['url'] ) > 0 ) {?>
                                <a class="btn btn-link" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                            <?php }  ?>

                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <div class="asymmetric-product">
                            <div class="row">
                                <ul class="products-blocks items-scattered clearfix" data-number="2">
                                    <?php while ( $query_shop->have_posts() ){
                                        $query_shop->the_post();
                                        global $product; ?>
                                        <li <?php post_class('col-item'); ?>>
                                            <?php wc_get_template_part( 'layouts/content-product-grid'); ?>
                                        </li>
                                        <?php
                                    }
                                    wp_reset_postdata();?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
        </div>
   </div>
</div>



