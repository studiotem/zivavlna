<?php


$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );

?>
<div class="block-intro-images">
    <div class="block-content box-intro-images clearfix">

            <?php
            $image_link         = wp_get_attachment_url($atts['image_box']);
            $background_image   ="background-image:url('$image_link')";
            $style_css          ='style ="'.$background_image.'"';
            ?>

            <div class="intro-images-box clearfix">
                <?php if($atts['image_box']) { ?>
                    <div class="box-image bg-img-fixed" <?php echo ($style_css);?>>
                    </div>
                <?php } ?>
                <div class="box-content clearfix">
                    <div class="box-content-inner">
                        <?php
                        if($atts['title_box']){?>
                            <h3 class="title-box">
                                <?php if($atts['link_box']) { ?>
                                    <a href="<?php echo esc_url($atts['link_box']); ?>">
                                        <?php echo ent2ncr($atts['title_box']); ?>
                                    </a>
                                <?php } else{
                                        echo ent2ncr($atts['title_box']);
                                } ?>
                            </h3>
                        <?php }?>

                        <?php if($atts['content_box']){?>
                            <div class="content-box">
                                    <?php echo ent2ncr($atts['content_box']); ?>
                            </div>
                        <?php }?>

                    </div>
                </div>
            </div>
    </div>

</div>