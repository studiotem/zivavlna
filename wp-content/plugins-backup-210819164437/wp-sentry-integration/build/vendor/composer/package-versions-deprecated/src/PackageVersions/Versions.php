<?php

declare (strict_types=1);
namespace WPSentry\ScopedVendor\PackageVersions;

use WPSentry\ScopedVendor\Composer\InstalledVersions;
use OutOfBoundsException;
/**
 * This class is generated by composer/package-versions-deprecated, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 *
 * @deprecated in favor of the Composer\InstalledVersions class provided by Composer 2. Require composer-runtime-api:^2 to ensure it is present.
 */
final class Versions
{
    /**
     * @deprecated please use {@see \Composer\InstalledVersions::getRootPackage()} instead. The
     *             equivalent expression for this constant's contents is
     *             `\Composer\InstalledVersions::getRootPackage()['name']`.
     *             This constant will be removed in version 2.0.0.
     */
    const ROOT_PACKAGE_NAME = 'stayallive/wp-sentry';
    /**
     * Array of all available composer packages.
     * Dont read this array from your calling code, but use the \PackageVersions\Versions::getVersion() method instead.
     *
     * @var array<string, string>
     * @internal
     */
    const VERSIONS = array('clue/stream-filter' => 'v1.4.1@5a58cc30a8bd6a4eb8f856adf61dd3e013f53f71', 'composer/installers' => 'v1.9.0@b93bcf0fa1fccb0b7d176b0967d969691cd74cca', 'composer/package-versions-deprecated' => '1.10.99@dd51b4443d58b34b6d9344cf4c288e621c9a826f', 'guzzlehttp/promises' => 'v1.3.1@a59da6cf61d80060647ff4d3eb2c03a2bc694646', 'guzzlehttp/psr7' => '1.6.1@239400de7a173fe9901b9ac7c06497751f00727a', 'http-interop/http-factory-guzzle' => '1.0.0@34861658efb9899a6618cef03de46e2a52c80fc0', 'jean85/pretty-package-versions' => '1.5.0@e9f4324e88b8664be386d90cf60fbc202e1f7fc9', 'paragonie/random_compat' => 'v9.99.99@84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95', 'php-http/client-common' => '2.3.0@e37e46c610c87519753135fb893111798c69076a', 'php-http/curl-client' => '2.1.0@9e79355af46d72e10da50be20b66f74b26143441', 'php-http/discovery' => '1.9.1@64a18cc891957e05d91910b3c717d6bd11fbede9', 'php-http/httplug' => '2.2.0@191a0a1b41ed026b717421931f8d3bd2514ffbf9', 'php-http/message' => '1.8.0@ce8f43ac1e294b54aabf5808515c3554a19c1e1c', 'php-http/message-factory' => 'v1.0.2@a478cb11f66a6ac48d8954216cfed9aa06a501a1', 'php-http/promise' => '1.1.0@4c4c1f9b7289a2ec57cde7f1e9762a5789506f88', 'psr/http-client' => '1.0.1@2dfb5f6c5eff0e91e20e913f8c5452ed95b86621', 'psr/http-factory' => '1.0.1@12ac7fcd07e5b077433f5f2bee95b3a771bf61be', 'psr/http-message' => '1.0.1@f6561bf28d520154e4b0ec72be95418abe6d9363', 'psr/log' => '1.1.3@0f73288fd15629204f9d42b7055f72dacbe811fc', 'ralouphie/getallheaders' => '3.0.3@120b605dfeb996808c31b6477290a714d356e822', 'sentry/sentry' => '2.4.2@b3b4f4a08b184c3f22b208f357e8720ef42938b0', 'symfony/options-resolver' => 'v3.4.43@0edf31d2e34f4adb72dd4d2e4a8aa21f84b943e5', 'symfony/polyfill-php80' => 'v1.18.0@d87d5766cbf48d72388a9f6b85f280c8ad51f981', 'symfony/polyfill-uuid' => 'v1.18.0@da48e2cccd323e48c16c26481bf5800f6ab1c49d', 'stayallive/wp-sentry' => 'v3.10.0@06cb79c13953008359346c89772d35bcef52e1e0');
    private function __construct()
    {
        \class_exists(\WPSentry\ScopedVendor\Composer\InstalledVersions::class);
    }
    /**
     * @throws OutOfBoundsException If a version cannot be located.
     *
     * @psalm-param key-of<self::VERSIONS> $packageName
     * @psalm-pure
     *
     * @psalm-suppress ImpureMethodCall we know that {@see InstalledVersions} interaction does not
     *                                  cause any side effects here.
     */
    public static function getVersion(string $packageName) : string
    {
        if (\class_exists(\WPSentry\ScopedVendor\Composer\InstalledVersions::class, \false)) {
            return \WPSentry\ScopedVendor\Composer\InstalledVersions::getPrettyVersion($packageName) . '@' . \WPSentry\ScopedVendor\Composer\InstalledVersions::getReference($packageName);
        }
        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }
        throw new \OutOfBoundsException('Required package "' . $packageName . '" is not installed: check your ./vendor/composer/installed.json and/or ./composer.lock files');
    }
}
