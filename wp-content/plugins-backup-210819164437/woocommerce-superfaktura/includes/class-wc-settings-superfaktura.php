<?php
class WC_Settings_SuperFaktura extends WC_Settings_Page {

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since 1.8.0
	 */
	public function __construct() {
		$this->id	= 'superfaktura';
		$this->label = __( 'SuperFaktúra', 'wc-superfaktura' );

		add_filter( 'woocommerce_settings_tabs_array', [$this, 'add_settings_page'], 20 );
		add_action( 'woocommerce_settings_' . $this->id, [$this, 'output']);
		add_action( 'woocommerce_settings_save_' . $this->id, [$this, 'save']);
		add_action( 'woocommerce_sections_' . $this->id, [$this, 'output_sections']);
	}

	private function get_or_create_default_secret_key() {
		$secret_key = get_option('woocommerce_sf_sync_secret_key', false);
		if ( $secret_key === false ) {
			$secret_key = $this->create_default_secret_key();
			update_option('woocommerce_sf_sync_secret_key', $secret_key);
		}

		return $secret_key;
	}



	private function create_default_secret_key() {
		return WC_SecretKeyHelper::generate_secret_key();
	}



	/**
	 * Create sections.
	 *
	 * @since 1.8.0
	 * @return array
	 */
	public function get_sections() {

		$sections = [
			'' => __( 'Authorization', 'wc-superfaktura' ),
			'invoice' => __( 'Invoice', 'wc-superfaktura' ),
			'invoice_creation' => __( 'Invoice Creation', 'wc-superfaktura' ),
			'integration' => __( 'Integration', 'wc-superfaktura' ),
			'payment' => __( 'Payment', 'wc-superfaktura' ),
			'shipping' => __( 'Shipping', 'wc-superfaktura' ),
			'accounting' => __( 'Accounting', 'wc-superfaktura' ),
			'api_log' => __( 'API log', 'wc-superfaktura' ),
			'help' => __( 'Help', 'wc-superfaktura' )
		];

		return apply_filters( 'woocommerce_get_sections_' . $this->id, $sections );
	}



	/**
	 * Create settings.
	 *
	 * @since 1.8.0
	 * @param string $current_section Optional. Defaults to empty string.
	 * @return array Array of settings
	 */
	public function get_settings( $current_section = '' ) {
		$wc_gateways = WC()->payment_gateways();
		$gateways = $wc_gateways->payment_gateways;

		$settings = [];
		switch ( $current_section ) {

			case 'invoice':
				$settings = [
					[
						'title' => __('Invoice Options', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_sf_invoice_title2'
					],
					[
						'title' => __('Proforma Invoice Sequence ID', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_proforma_invoice_sequence_id',
						'desc' => '',
						'type' => 'text',
					],
					[
						'title' => __('Invoice Sequence ID', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_sequence_id',
						'desc' => '',
						'type' => 'text',
					],
					[
						'title' => __('Credit Note Sequence ID', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_cancel_sequence_id',
						'desc' => '',
						'type' => 'text',
					],
					[
						'title' => __('Custom invoice numbering', 'wc-superfaktura'),
						'desc' => __('Use custom invoice numbering (this is a deprecated option, please use sequence IDs above instead)', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_custom_num',
						'default' => 'no',
						'type' => 'checkbox',
					],
					[
						'title' => __('Proforma Invoice Nr.', 'wc-superfaktura'),
						'desc' => sprintf(__('Available Tags: %s', 'wc-superfaktura'), '[YEAR], [YEAR_SHORT], [MONTH], [DAY], [COUNT], [ORDER_NUMBER]'),
						'id' => 'woocommerce_sf_invoice_proforma_id',
						'default' => 'ZAL[YEAR][MONTH][COUNT]',
						'type' => 'text',
						'class' => 'custom-invoice-numbering-item',
					],
					[
						'title' => __('Invoice Nr.', 'wc-superfaktura'),
						'desc' => sprintf(__('Available Tags: %s', 'wc-superfaktura'), '[YEAR], [YEAR_SHORT], [MONTH], [DAY], [COUNT], [ORDER_NUMBER]'),
						'id' => 'woocommerce_sf_invoice_regular_id',
						'default' => '[YEAR][MONTH][COUNT]',
						'type' => 'text',
						'class' => 'custom-invoice-numbering-item',
					],
					[
						'title' => __('Credit Note Nr.', 'wc-superfaktura'),
						'desc' => sprintf(__('Available Tags: %s', 'wc-superfaktura'), '[YEAR], [YEAR_SHORT], [MONTH], [DAY], [COUNT], [ORDER_NUMBER]'),
						'id' => 'woocommerce_sf_invoice_cancel_id',
						'default' => '[YEAR][MONTH][COUNT]',
						'type' => 'text',
						'class' => 'custom-invoice-numbering-item',
					],
					[
						'title' => __('Current Proforma Invoice Number for [COUNT]', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_proforma_count',
						'default' => '1',
						'type' => 'number',
						'class' => 'wi-small',
						'custom_attributes' => [
							'min' => 1,
							'step' => 1
						],
						'class' => 'custom-invoice-numbering-item',
					],
					[
						'title' => __('Current Invoice Number for [COUNT]', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_regular_count',
						'default' => '1',
						'type' => 'number',
						'class' => 'wi-small',
						'custom_attributes' => [
							'min' => 1,
							'step' => 1
						],
						'class' => 'custom-invoice-numbering-item',
					],
					[
						'title' => __('Current Credit Note Number for [COUNT]', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_cancel_count',
						'default' => '1',
						'type' => 'number',
						'class' => 'wi-small',
						'custom_attributes' => [
							'min' => 1,
							'step' => 1
						],
						'class' => 'custom-invoice-numbering-item',
					],
					[
						'title' => __('Number of digits for [COUNT]', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_count_decimals',
						'default' => '4',
						'type' => 'number',
						'class' => 'wi-small',
						'custom_attributes' => [
							'min' => 1,
							'step' => 1
						],
						'class' => 'custom-invoice-numbering-item',
					],
					[
						'title' => __('Delivery name', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_delivery_name',
						'default' => 'no',
						'type' => 'checkbox',
						'desc' => __('Use format <em>CompanyName - FirstName LastName</em>', 'wc-superfaktura')
					],
					[
						'title' => __( 'Invoice language', 'wc-superfaktura' ),
						'id' => 'woocommerce_sf_invoice_language',
						'default' => 'endpoint',
						'type' => 'select',
						'class' => 'wc-enhanced-select',
						'options' => [
							'endpoint' => __( 'Default endpoint language', 'wc-superfaktura' ),
							'locale' => __( 'Site locale (fallback to endpoint)', 'wc-superfaktura' ),
							'wpml' => __( 'WPML', 'wc-superfaktura' ),
							'slo' => __( 'Slovak', 'wc-superfaktura' ),
							'cze' => __( 'Czech', 'wc-superfaktura' ),
							'eng' => __( 'Εnglish', 'wc-superfaktura' ),
							'deu' => __( 'German', 'wc-superfaktura' ),
							'rus' => __( 'Russian', 'wc-superfaktura' ),
							'ukr' => __( 'Ukrainian', 'wc-superfaktura' ),
							'hun' => __( 'Hungarian', 'wc-superfaktura' ),
							'pol' => __( 'Polish', 'wc-superfaktura' ),
							'rom' => __( 'Romanian', 'wc-superfaktura' ),
							'hrv' => __( 'Croatian', 'wc-superfaktura' ),
							'slv' => __( 'Slovenian', 'wc-superfaktura' ),
						],
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_wi_invoice_title2'
					],
					[
						'title' => __('Invoice Comments', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_sf_invoice_title8'
					],
					[
						'title' => __('Allow custom comments', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_comments',
						'default' => 'yes',
						'type' => 'checkbox',
						'desc' => __('Override default comments options in SuperFaktúra. Adds custom comment, order comment and tax liability if needed.', 'wc-superfaktura')
					],
					/* 2017/09/25 presunutie cisla objednavky do z invoice.comment do invoice.order_no
					array(
						'title' => __('Order number', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_order_number_visibility',
						'type' => 'checkbox',
						'desc' => __( 'Display an order number if comments are enabled.', 'wc-superfaktura' ),
						'default' => 'yes',
					),
					*/
					[
						'title' => __('Comment', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_comment',
						'class' => 'input-text wide-input',
						'css'   => 'width:100%; height: 75px;',
						//'default' => '',
						'type' => 'textarea',
					],
					[
						'title' => __('Order note', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_comment_add_order_note',
						'default' => 'no',
						'type' => 'checkbox',
						'desc' => __('Add order note from customer to comment.', 'wc-superfaktura')
					],
					//Prenesená daňová povinnosť
					[
						'title' => __('Tax Liability', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_tax_liability',
						'class' => 'input-text wide-input',
						'default' => 'Dodanie tovaru je oslobodené od dane. Dodanie služby podlieha preneseniu daňovej povinnosti.',
						'type' => 'textarea',
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_wi_invoice_title8'
					],
					[
						'title' => __('Additional Invoice Fields', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_wi_invoice_title3'
					],
					[
						'title' => __('Variable symbol', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_variable_symbol',
						'type' => 'radio',
						'default' => 'invoice_nr',
						'options' => [
							'invoice_nr' => __( 'Use invoice number', 'wc-superfaktura' ),
							'order_nr' => __( 'Use order number', 'wc-superfaktura' ),
						],
					],
					[
						'title' => __('Add field ID #', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_checkout_id',
						'default' => 'yes',
						'type' => 'checkbox',
						'desc' => ''
					],
					[
						'title' => __('Add field VAT #', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_checkout_vat',
						'default' => 'yes',
						'type' => 'checkbox',
						'desc' => ''
					],
					[
						'title' => __('Add field TAX ID #', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_checkout_tax',
						'default' => 'yes',
						'type' => 'checkbox',
						'desc' => ''
					],
					[
						'title' => __('Checkout fields required', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_checkout_required',
						'default' => 'no',
						'type' => 'checkbox'
					],
				   [
						'title' => __('PAY by square', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_bysquare',
						'type' => 'checkbox',
						'desc' => __('Display a QR code', 'wc-superfaktura'),
						'default' => 'yes'
				   ],
					[
						'title' => __('Issued by', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_issued_by',
						'type' => 'text',
					],
					[
						'title' => __('Issued by Phone', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_issued_phone',
						'type' => 'text',
					],
					[
						'title' => __('Issued by Web', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_issued_web',
						'type' => 'text',
					],
					[
						'title' => __('Issued by Email', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_issued_email',
						'type' => 'text',
					],
					[
						'title' => __('Created Date', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_created_date_as_order',
						'type' => 'checkbox',
						'desc' => __('Use order date instead of current date', 'wc-superfaktura'),
						'default' => 'no'
					],
					[
						'title' => __('Delivery Date', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_delivery_date_value',
						'type' => 'select',
						'desc' => __('Select date to be used as delivery date', 'wc-superfaktura'),
						'default' => 'invoice_created',
						'options' => [
							'invoice_created' => __('Invoice creation date', 'wc-superfaktura'),
							'order_paid' => __('Order payment date', 'wc-superfaktura'),
							'order_created' => __('Order creation date', 'wc-superfaktura'),
							'none' => __('Do not display', 'wc-superfaktura'),
						]
					],
					[
						'title' => __('Product Description', 'wc-superfaktura'),
						'desc' => sprintf(__('Available Tags: %s', 'wc-superfaktura'), '[ATTRIBUTES], [NON_VARIATIONS_ATTRIBUTES], [VARIATION], [SHORT_DESCR], [SKU]'),
						'id' => 'woocommerce_sf_product_description',
						'css' => 'width:50%; height: 75px;',
						'default' => '[ATTRIBUTES]' . ('yes' === get_option('woocommerce_sf_product_description_visibility', 'yes') ? "\n[SHORT_DESCR]" : ''),
						'type' => 'textarea',
					],
					[
						'title' => __('Discount', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_product_description_show_discount',
						'type' => 'checkbox',
						'desc' => __('Show product discount in description', 'wc-superfaktura'),
						'default' => 'yes'
					],
					[
						'title' => __('Coupon Invoice Items', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_coupon_invoice_items',
						'type' => 'radio',
						'desc' => '',
						'default' => 'total',
						'options' => [
							'total' => __('Total', 'wc-superfaktura'),
							'per_item' => __('Per Item', 'wc-superfaktura')
						]
					],
					[
						'title' => __('Coupon Description', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_product_description_show_coupon_code',
						'type' => 'checkbox',
						'desc' => __('Show coupon code in description', 'wc-superfaktura'),
						'default' => 'yes'
					],
					[
						'title' => __('Discount Name', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_discount_name',
						'default' => 'Zľava',
						'desc' => '',
						'type' => 'text',
					],
					[
						'title' => __('Shipping Item Name', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_shipping_item_name',
						'default' => 'Poštovné',
						'desc' => '',
						'type' => 'text',
					],
					[
						'title' => __( 'Free Shipping Name', 'wc-superfaktura' ),
						'id' => 'woocommerce_sf_free_shipping_name',
						'default' => '',
						'desc' => '<br>' . __( 'By default, in case of free shipping, the invoice does not contain shipping item; to force the item to appear, fill in its name in this field', 'wc-superfaktura' ),
						'type' => 'text',
					],
					[
						'title' => __('Refunded items', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_product_subtract_refunded_qty',
						'type' => 'checkbox',
						'desc' => __('Subtract refunded items quantity on invoice', 'wc-superfaktura'),
						'default' => 'no'
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_wi_invoice_title3'
					],
				];

				$settings = apply_filters( 'superfaktura_invoice_settings', $settings );
				break;



			case 'invoice_creation':
				$wc_get_order_statuses = $this->get_order_statuses();

				$shop_order_status = ['0' => __('Don\'t generate', 'wc-superfaktura')];
				$shop_order_status = array_merge( $shop_order_status, $wc_get_order_statuses );

				$settings[] = [
					'title' => __('Invoice Creation', 'wc-superfaktura'),
					'type' => 'title',
					'desc' => __('Select when you would like to create an invoice for each payment gateway.', 'wc-superfaktura'),
					'id' => 'woocommerce_wi_invoice_creation1'
				];

				foreach($gateways as $gateway)
				{
					$settings[] = [
						'title' => $gateway->title,
						'id' => 'woocommerce_sf_invoice_regular_' . $gateway->id,
						'default' => 0,
						'type' => 'select',
						'class' => 'wc-enhanced-select',
						'options' => $shop_order_status
					];

					$settings[] = [
						'title' => '',
						'id' => 'woocommerce_sf_invoice_regular_' . $gateway->id . '_set_as_paid',
						'default' => 'no',
						'type' => 'checkbox',
						'desc' => __('Create as paid', 'wc-superfaktura')
					];
				}

				$settings[] = [
					'type' => 'sectionend',
					'id' => 'woocommerce_wi_invoice_creation1'
				];



				$settings[] = [
					'type' => 'title',
					'id' => 'woocommerce_wi_invoice_creation2'
				];

				$settings[] = [
					'title' => __('Zero value invoices', 'wc-superfaktura'),
					//  the fake payment ID makes it look like one of the payment gateways above
					'id' => 'woocommerce_sf_invoice_regular_' . WC_SuperFaktura::$ZERO_VALUE_ORDER_FAKE_PAYMENT_METHOD_ID,
					'default' => '',
					'type' => 'select',
					'class' => 'wc-enhanced-select',
					'options' => $shop_order_status,
					'desc' => __('Allow zero value invoices to be generated when order changes to the selected status.', 'wc-superfaktura'),
				];

				$settings[] = [
					'title' => __('Invoice for orders without processing', 'wc-superfaktura'),
					'id' => 'woocommerce_sf_invoice_regular_processing_skipped_fix',
					'default' => 'no',
					'type' => 'checkbox',
					'desc' => sprintf(
						__('Allow invoice creation in the "%1$s" state for orders that do not need processing. Only applies if the invoice was supposed to be created in "%2$s" state.', 'wc-superfaktura'),
						$wc_get_order_statuses['completed'],
						$wc_get_order_statuses['processing']
					)
				];

				$settings[] = [
					'title' => __('Manual Invoice Creation', 'wc-superfaktura'),
					'id' => 'woocommerce_sf_invoice_regular_manual',
					'default' => 'no',
					'type' => 'checkbox',
					'desc' => __('Allow manual invoice creation', 'wc-superfaktura')
				];

				$settings[] = [
					'title' => __('Client Data', 'wc-superfaktura'),
					'id' => 'woocommerce_sf_invoice_update_addressbook',
					'default' => 'no',
					'type' => 'checkbox',
					'desc' => __('Update client data in SuperFaktura', 'wc-superfaktura')
				];



				// set default values
				$default_options = ['completed'];
				// backward compatibility with previous options woocommerce_sf_invoice_regular_processing_set_as_paid and woocommerce_sf_invoice_regular_dont_set_as_paid
				if (get_option('woocommerce_sf_invoice_regular_processing_set_as_paid', 'no') == 'yes') {
					$default_options[] = 'processing';
				}
				if (get_option('woocommerce_sf_invoice_regular_dont_set_as_paid', 'no') == 'yes') {
					array_shift($default_options); // remove 'completed'
				}

				$settings[] = [
					'title' => __('Set invoice as paid in these order statuses', 'wc-superfaktura'),
					'id' => 'woocommerce_sf_invoice_set_as_paid_statuses',
					'default' => $default_options,
					'type' => 'multiselect',
					'class' => 'wc-enhanced-select',
					'options' => $wc_get_order_statuses
				];

				/* :TODO: remove
				$settings[] = [
					'title' => __('Invoice Paid Status ("Processing")', 'wc-superfaktura'),
					'id' => 'woocommerce_sf_invoice_regular_processing_set_as_paid',
					'default' => 'no',
					'type' => 'checkbox',
					'desc' => __('Set invoice as paid for order status "Processing"', 'wc-superfaktura')
				];

				$settings[] = [
					'title' => __('Invoice Paid Status ("Completed")', 'wc-superfaktura'),
					'id' => 'woocommerce_sf_invoice_regular_dont_set_as_paid',
					'default' => 'no',
					'type' => 'checkbox',
					'desc' => __('Do not set invoice as paid for order status "Completed"', 'wc-superfaktura')
				];
				*/



				$settings[] = [
					'type' => 'sectionend',
					'id' => 'woocommerce_wi_invoice_creation2'
				];



				$settings[] = [
					'title' => __('Proforma Invoice Creation', 'wc-superfaktura'),
					'type' => 'title',
					'desc' => __('Select when you would like to create a proforma invoice for each payment gateway.', 'wc-superfaktura'),
					'id' => 'woocommerce_wi_invoice_creation3'
				];

				foreach($gateways as $gateway)
				{
					$settings[] = [
						'title' => $gateway->title,
						'id' => 'woocommerce_sf_invoice_proforma_'.$gateway->id,
						'default' => 0,
						'type' => 'select',
						'class' => 'wc-enhanced-select',
						'options' => $shop_order_status
					];
				}

				$settings[] = [
					'title' => __('Manual Proforma Invoice Creation', 'wc-superfaktura'),
					'id' => 'woocommerce_sf_invoice_proforma_manual',
					'default' => 'no',
					'type' => 'checkbox',
					'desc' => __('Allow manual proforma invoice creation', 'wc-superfaktura')
				];

				$settings[] = [
					'type' => 'sectionend',
					'id' => 'woocommerce_wi_invoice_creation3'
				];

				$settings = apply_filters( 'superfaktura_invoice_creation_settings', $settings );
				break;



			case 'integration':
				$settings = [
					[
						'title' => __('Checkout', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_sf_invoice_title12'
					],
					[
						'title' => __('Billing fields', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_add_company_billing_fields',
						'default' => 'yes',
						'type' => 'checkbox',
						'desc' => __('Add company billing fields to checkout', 'wc-superfaktura')
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_wi_invoice_title12'
					],
					[
						'title' => __('Order received', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_sf_invoice_title11'
					],
					[
						'title' => __('Invoice link', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_order_received_invoice_link',
						'default' => 'yes',
						'type' => 'checkbox',
						'desc' => __('Add invoice link to order received screen', 'wc-superfaktura')
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_wi_invoice_title11'
					],
					[
						'title' => __('Emails', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_sf_invoice_title10'
					],
					[
						'title' => __('Billing details', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_email_billing_details',
						'default' => 'no',
						'type' => 'checkbox',
						'desc' => __('Add customer billing details (ID #, VAT #, TAX ID #) to WooCommerce emails', 'wc-superfaktura')
					],
					[
						'title' => __('Online payment link', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_email_payment_link',
						'default' => 'yes',
						'type' => 'checkbox',
						'desc' => __('Add online payment link to WooCommerce emails', 'wc-superfaktura')
					],
					[
						'title' => __('Invoice link', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_email_invoice_link',
						'default' => 'yes',
						'type' => 'checkbox',
						'desc' => __('Add invoice link to WooCommerce emails', 'wc-superfaktura')
					],
					[
						'title' => __('Invoice PDF attachment', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_pdf_attachment',
						'default' => 'no',
						'type' => 'checkbox',
						'desc' => __('Attach invoice PDF to WooCommerce emails', 'wc-superfaktura')
					],
					[
						'title' => __('Completed orders', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_completed_email_skip_invoice',
						'default' => 'no',
						'type' => 'checkbox',
						'desc' => __('Don\'t add invoice to WooCommerce emails for completed orders', 'wc-superfaktura')
					],
					[
						'title' => __('Cash on delivery orders', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_cod_email_skip_invoice',
						'default' => 'no',
						'type' => 'checkbox',
						'desc' => __('Don\'t add invoice to WooCommerce emails for cash on delivery orders', 'wc-superfaktura')
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_wi_invoice_title10'
					],
					[
						'title' => __('Admin', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_sf_invoice_title13'
					],
					[
						'title' => __('Order list', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_download_button_actions',
						'type' => 'checkbox',
						'desc' => __('Show invoice action button in order list', 'wc-superfaktura'),
						'default' => 'no',
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_sf_invoice_title13'
					],
					[
						'title' => __('Automatic pairing', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '
							<p><strong>' . __('Default', 'wc-superfaktura') . '</strong>:
							' . sprintf(
								__('To automatically update order status when payment is paired to an invoice in SuperFaktura, fill in callback URL and Secret Key in <a target="_blank" href="%s">SuperFaktura settings</a>.<br>Callback URL: <strong>%s</strong>', 'wc-superfaktura'),
								('cz' == get_option('woocommerce_sf_lang', 'sk')) ? 'https://moje.superfaktura.cz/users/edit_profile/settings#tab-bmails' : 'https://moja.superfaktura.sk/users/edit_profile/settings#tab-bmails',
								site_url('/') . '?callback=wc_sf_order_paid'
							) . '</p>
							<p><strong>' . __('Multiple eshops per single company', 'wc-superfaktura') . '</strong>:
							' . __('Plugin will send Callback URL to SuperFaktura automatically for each document issued.', 'wc-superfaktura') . '</p>
						',
						'id' => 'woocommerce_sf_invoice_title9'
					],
					[
						'title' => __('Automatic pairing type', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_sync_type',
						'type' => 'radio',
						'default' => 'single',
						'options' => [
							'single' => __('Default', 'wc-superfaktura'),
							'multi' => __('Multiple eshops per single company', 'wc-superfaktura')
						],
					],
					[
						'title' => __('Secret Key', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_sync_secret_key',
						'desc' => '<button id="createSecretKey" type="button" class="button" onclick="wc_sf_create_secret_key()">' . __('Regenerate', 'wc-superfaktura') . '</button>',
						'class' => 'input-text regular-input',
						'type' => 'text',
						'default' => $this->get_or_create_default_secret_key()
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_sf_invoice_title9'
					]
				];

				$settings = apply_filters( 'superfaktura_integration_settings', $settings );
				break;



			case 'payment':
				$settings[] = [
					'title' => __('Payment Methods', 'wc-superfaktura'),
					'type' => 'title',
					'desc' => __('Map Woocommerce payment methods to ones in SuperFaktura', 'wc-superfaktura'),
					'id' => 'woocommerce_wi_invoice_title6'
				];

				$gateway_mapping = [
					'0' => __('Don\'t use', 'wc-superfaktura'),
					'transfer' => __('Transfer', 'wc-superfaktura'),
					'cash' => __('Cash', 'wc-superfaktura'),
					'paypal' => __('Paypal', 'wc-superfaktura'),
					'trustpay' => __('Trustpay', 'wc-superfaktura'),
					'besteron' => __('Besteron', 'wc-superfaktura'),
					'barion' => __('Barion', 'wc-superfaktura'),
					'credit' => __('Credit card', 'wc-superfaktura'),
					'debit' => __('Debit card', 'wc-superfaktura'),
					'cod' => __('Cash on delivery', 'wc-superfaktura'),
					'accreditation' => __('Mutual credit', 'wc-superfaktura'),
					'gopay' => __('GoPay', 'wc-superfaktura'),
					'viamo' => __('Viamo', 'wc-superfaktura'),
					'postal_order' => __('Postal money order', 'wc-superfaktura'),
					'other' => __('Other', 'wc-superfaktura'),
				];

				$gateway_mapping = apply_filters('sf_gateway_mapping', $gateway_mapping);

				foreach($gateways as $gateway)
				{
					$settings[] = [
						'title' => $gateway->title,
						'id' => 'woocommerce_sf_gateway_'.$gateway->id,
						'default' => 0,
						'type' => 'select',
						'class' => 'wc-enhanced-select',
						'options' => $gateway_mapping
					];
				}

				$settings[] = [
					'type' => 'sectionend',
					'id' => 'woocommerce_wi_invoice_title6'
				];



				// cash registers

				$settings[] = [
					'title' => __('Cash Registers', 'wc-superfaktura'),
					'type' => 'title',
					'desc' => __('Map Woocommerce payment methods to cash registers in SuperFaktura', 'wc-superfaktura'),
					'id' => 'woocommerce_wi_invoice_title7'
				];

				foreach($gateways as $gateway)
				{
					$settings[] = [
						'title' => $gateway->title,
						'id' => 'woocommerce_sf_cash_register_'.$gateway->id,
						'desc' => 'Cash register ID',
						'type' => 'text',
					];
				}

				$settings[] = [
					'type' => 'sectionend',
					'id' => 'woocommerce_wi_invoice_title7'
				];

				$settings = apply_filters( 'superfaktura_payment_settings', $settings );
				break;



			case 'shipping':
				$shipping_mapping = [
					'0'			=> __('Don\'t use', 'wc-superfaktura'),
					'mail'		 => __('By mail', 'wc-superfaktura'),
					'courier'	  => __('By courier', 'wc-superfaktura'),
					'personal'	 => __('Personal pickup', 'wc-superfaktura'),
					'haulage'	  => __('Freight', 'wc-superfaktura'),
					'pickup_point' => __('Pickup point', 'wc-superfaktura'),
				];

				if ( class_exists( 'WC_Shipping_Zones') ) {
					$zones = WC_Shipping_Zones::get_zones();

					// rest of the world zone
					$rest = new WC_Shipping_Zone( 0 );
					$zones[0] = $rest->get_data();
					$zones[0]['formatted_zone_location'] = $rest->get_formatted_location();
					$zones[0]['shipping_methods'] = $rest->get_shipping_methods();

					foreach ( $zones as $id => $zone ) {
						$settings[] = [
							'title' => __( 'Shipping Methods', 'wc-superfaktura' ) . ': ' . $zone['formatted_zone_location'],
							'type' => 'title',
							'id' => 'woocommerce_wi_invoice_title_zone_' . $id,
						];

						foreach ( $zone['shipping_methods'] as $method ) {
							if ( 'no' === $method->enabled ) {
								continue;
							}
							$legacy = get_option( 'woocommerce_sf_shipping_' . $method->id );
							$settings[] = [
								'title' => $method->title,
								'id' => 'woocommerce_sf_shipping_' . $method->id . ':' . $method->instance_id,
								'default' => empty( $legacy ) ? 0 : $legacy,
								'type' => 'select',
								'class' => 'wc-enhanced-select',
								'options' => $shipping_mapping,
							];
						}

						$settings[] = [
							'type' => 'sectionend',
							'id' => 'woocommerce_wi_invoice_title_zone_' . $id,
						];
					}
				}
				else {
					$wc_shipping = WC()->shipping();
					$shippings = $wc_shipping->get_shipping_methods();

					if ( $shippings )
					{
						$settings[] = [
							'title' => __('Shipping Methods', 'wc-superfaktura'),
							'type' => 'title',
							'desc' => 'Map Woocommerce shipping methods to ones in SuperFaktúra.sk',
							'id' => 'woocommerce_wi_invoice_title7'
						];

						foreach($shippings as $shipping)
						{
							if ( $shipping->enabled == 'no' )
								continue;

							$settings[] = [
								'title' => $shipping->title,
								'id' => 'woocommerce_sf_shipping_'.$shipping->id,
								'default' => 0,
								'type' => 'select',
								'class' => 'wc-enhanced-select',
								'options' => $shipping_mapping
							];
						}

						//array_shift( $shipping_mapping );

						// $settings[] = array(
						//	 'title' => __('Delivery Date', 'wc-superfaktura'),
						//	 'id' => 'woocommerce_sf_delivery_date_visibility',
						//	 'type' => 'multiselect',
						//	 'desc' => 'Display a delivery date only for selected shipping methods.',
						//	 'default' => array_flip( $shipping_mapping ),
						//	 'options' => $shipping_mapping
						// );

						$settings[] = [
							'type' => 'sectionend',
							'id' => 'woocommerce_wi_invoice_title7'
						];
					}
				}

				$settings = apply_filters( 'superfaktura_shipping_settings', $settings );
				break;



			case 'accounting':
				$item_type_options = [
					'0'			=> __('Don\'t use', 'wc-superfaktura'),
					'item'		=> __('Item', 'wc-superfaktura'),
					'service'	=> __('Service', 'wc-superfaktura'),
				];

				$settings = [
					[
						'title' => __('Product', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_sf_invoice_title21'
					],
					[
						'title' => __('Item Type', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_item_type_product',
						'default' => 0,
						'type' => 'select',
						'class' => 'wc-enhanced-select',
						'options' => $item_type_options
					],
					[
						'title' => __('Analytics Account', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_analytics_account_product',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'title' => __('Synthetic Account', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_synthetic_account_product',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'title' => __('Preconfidence', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_preconfidence_product',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_sf_invoice_title21'
					],



					[
						'title' => __('Fees', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_sf_invoice_title22'
					],
					[
						'title' => __('Item Type', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_item_type_fees',
						'default' => 0,
						'type' => 'select',
						'class' => 'wc-enhanced-select',
						'options' => $item_type_options
					],
					[
						'title' => __('Analytics Account', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_analytics_account_fees',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'title' => __('Synthetic Account', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_synthetic_account_fees',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'title' => __('Preconfidence', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_preconfidence_fees',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_sf_invoice_title22'
					],



					[
						'title' => __('Shipping', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '',
						'id' => 'woocommerce_sf_invoice_title23'
					],
					[
						'title' => __('Item Type', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_item_type_shipping',
						'default' => 0,
						'type' => 'select',
						'class' => 'wc-enhanced-select',
						'options' => $item_type_options
					],
					[
						'title' => __('Analytics Account', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_analytics_account_shipping',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'title' => __('Synthetic Account', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_synthetic_account_shipping',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'title' => __('Preconfidence', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_preconfidence_shipping',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_sf_invoice_title23'
					],
				];

				$settings = apply_filters( 'superfaktura_accounting_settings', $settings);
				break;



			case 'api_log':

				global $wpdb;
				$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}wc_sf_log ORDER BY time DESC LIMIT 200", ARRAY_A);

				$table = '
					<table class="wc-sf-api-log">
						<thead>
							<tr>
								<th>Order ID</th>
								<th>Document Type</th>
								<th>Request Type</th>
								<th>Response Status</th>
								<th>Response Message</th>
								<th>Time</th>
							</tr>
						</thead>
						<tbody>
				';

				if ($results) {
					foreach ($results as $index => $row) {
						$table .= '
							<tr class="' . ((0 == $index % 2) ? 'odd' : '') . ' ' . (($row['response_status']) ? ' error' : '') . '">
								<td>' . $row['order_id'] . '</td>
								<td>' . $row['document_type'] . '</td>
								<td>' . $row['request_type'] . '</td>
								<td>' . $row['response_status'] . '</td>
								<td>' . $row['response_message'] . '</td>
								<td>' . $row['time'] . '</td>
							</tr>
						';
					}
				}

				$table .= '
						</tbody>
					</table>
				';

				$settings = [
					[
						'title' => __('API log', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => $table,
						'id' => 'woocommerce_sf_invoice_title98'
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_sf_invoice_title98'
					]
				];

				$settings = apply_filters( 'superfaktura_api_log_settings', $settings);
				break;



			case 'help':
				$settings = [
					[
						'title' => __('Help', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => '
							<p>' . __('For more information about the plugin and its settings check the articles on SuperFaktura blog.', 'wc-superfaktura') . '</p>

							<h3>SuperFaktura.sk</strong></h3>
							<ul style="padding-left: 1em; list-style-type: disc;">
								<li><a href="https://www.superfaktura.sk/blog/superfaktura-a-woocommerce-diel-1-instalacia-a-autorizacia/">Diel 1. – Inštalácia a autorizácia</a></li>
								<li><a href="https://www.superfaktura.sk/blog/superfaktura-a-woocommerce-diel-2-vytvaranie-faktur/">Diel 2. – Vytváranie faktúr</a></li>
								<li><a href="https://www.superfaktura.sk/blog/superfaktura-a-woocommerce-diel-3-cislovanie-dokladov/">Diel 3. – Číslovanie dokladov</a></li>
								<li><a href="https://www.superfaktura.sk/blog/superfaktura-a-woocommerce-diel-4-pokrocile-nastavenia/">Diel 4. – Pokročilé nastavenia</a></li>
								<li><a href="https://www.superfaktura.sk/blog/superfaktura-a-woocommerce-diel-5-platby-doprava-pokladne/">Diel 5. – Platby, doprava, pokladne</a></li>
								<li><a href="https://www.superfaktura.sk/blog/superfaktura-a-woocommerce-faq/">FAQ</a></li>
							</ul>

							<h3>SuperFaktura.cz</h3>
							<ul style="padding-left: 1em; list-style-type: disc;">
								<li><a href="https://www.superfaktura.cz/blog/superfaktura-a-woocommerce-dil-1-instalace-a-autorizace/">Díl 1. – Instalace a autorizace</a></li>
								<li><a href="https://www.superfaktura.cz/blog/superfaktura-a-woocommerce-dil-2-vytvareni-faktur/">Díl 2. – Vytváření faktur</a></li>
								<li><a href="https://www.superfaktura.cz/blog/superfaktura-a-woocommerce-dil-3-cislovani-dokladu/">Díl 3. – Číslování dokladů</a></li>
								<li><a href="https://www.superfaktura.cz/blog/superfaktura-a-woocommerce-dil-4-pokrocila-nastaveni/">Díl 4. – Pokročilá nastavení</a></li>
								<li><a href="https://www.superfaktura.cz/blog/superfaktura-a-woocommerce-dil-5-platby-doprava-pokladny-a-eet/">Díl 5. – Platby, doprava, pokladny a EET</a></li>
								<li><a href="https://www.superfaktura.cz/blog/superfaktura-a-woocommerce-faq/">FAQ</a></li>
							</ul>

							<hr>
							<p>'  . __( 'Do you have a technical issue with the plugin? Contact us at <a href="mailto:superfaktura@2day.sk">superfaktura@2day.sk</a>', 'wc-superfaktura' ) . '</p>
						',
						'id' => 'woocommerce_sf_invoice_title99'
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_sf_invoice_title99'
					]
				];

				$settings = apply_filters( 'superfaktura_help_settings', $settings);
				break;



			case '':
			default:
				$settings = [
					[
						'title' => __('Authorization', 'wc-superfaktura'),
						'type' => 'title',
						'desc' => __('You can find your API access credentials in your SuperFaktura account at <a href="https://moja.superfaktura.sk/api_access">Tools &gt; API</a>', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_invoice_title1'
					],
					[
						'title' => __('Version', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_lang',
						'type' => 'radio',
						'desc' => '',
						'default' => 'sk',
						'options' => ['sk' => 'SuperFaktura.sk', 'cz' => 'SuperFaktura.cz', 'at' => 'SuperFaktura.at']
					],
					[
						'title' => __('API Email', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_email',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'title' => __('API Key', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_apikey',
						'desc' => '',
						'class' => 'input-text regular-input',
						'type' => 'text',
					],
					[
						'title' => __('Company ID', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_company_id',
						'desc' => '
							<a class="button wc-sf-api-test" href="">' . __('Test API connection', 'wc-superfaktura') . '</a>
							<span class="wc-sf-api-test-loading"><img src="' . plugins_url('../images/rolling.gif', __FILE__) . '" width="25" height="25" alt=""></span>
							<span class="wc-sf-api-test-ok"><img src="' . plugins_url('../images/ok.png', __FILE__) . '" width="32" height="32" alt=""></span>
							<span class="wc-sf-api-test-fail"><img src="' . plugins_url('../images/fail.png', __FILE__) . '" width="32" height="32" alt=""></span>
							<span class="wc-sf-api-test-fail-message" style="color: #e15b64;"></span>
						',
						'type' => 'text',
					],
					[
						'title' => __('Logo ID', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_logo_id',
						'desc' => '',
						'type' => 'text',
					],
					[
						'title' => __('Bank Account ID', 'wc-superfaktura'),
						'id' => 'woocommerce_sf_bank_account_id',
						'desc' => '',
						'type' => 'text',
					],
					[
						'type' => 'sectionend',
						'id' => 'woocommerce_wi_invoice_title1'
					],
				];

				$settings = apply_filters( 'superfaktura_authorization_settings', $settings);
				break;
		}

		return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings, $current_section );

	}

	/**
	 * :TODO:
	 *
	 * @since 1.8.0
	 */
	function get_order_statuses()
	{
		if ( function_exists( 'wc_order_status_manager_get_order_status_posts' ) ) // plugin WooCommerce Order Status Manager
		{
			$wc_order_statuses = array_reduce(
				wc_order_status_manager_get_order_status_posts(),
				function($result, $item)
				{
					$result[$item->post_name] = $item->post_title;
					return $result;
				},
				[]
			);

			return $wc_order_statuses;
		}

		if ( function_exists( 'wc_get_order_statuses' ) )
		{
			$wc_get_order_statuses = wc_get_order_statuses();

			return $this->alter_wc_statuses( $wc_get_order_statuses );
		}

		$order_status_terms = get_terms('shop_order_status','hide_empty=0');

		$shop_order_statuses = [];
		if ( ! is_wp_error( $order_status_terms ) )
		{
			foreach ( $order_status_terms as $term )
			{
				$shop_order_statuses[$term->slug] = $term->name;
			}
		}

		return $shop_order_statuses;
	}

	/**
	 * :TODO:
	 *
	 * @since 1.8.0
	 * @param array $array
	 * @return array
	 */
	function alter_wc_statuses( $array )
	{
		$new_array = [];
		foreach ( $array as $key => $value )
		{
			$new_array[substr($key,3)] = $value;
		}

		return $new_array;
	}



	/**
	 * Output the settings.
	 *
	 * @since 1.8.0
	 */
	public function output() {
		WC_SuperFaktura::maybe_migrate_invoice_settings();

		global $current_section;
		$settings = $this->get_settings( $current_section );
		WC_Admin_Settings::output_fields( $settings );

		if ('invoice' == $current_section) {
			$this->get_country_specific_settings();
		}

		?>
		<script>
		function wc_sf_generate_secret_key() {
			<?php $nonce = wp_create_nonce( 'wc_sf' ); ?>
			jQuery.ajax({
				type: "post",
				url: "admin-ajax.php",
				data: {
					action: 'wc_sf_generate_secret_key',
					_ajax_nonce: '<?php echo $nonce; ?>'
				},
				success: function(response) {
					if (response) {
						document.getElementById('woocommerce_sf_sync_secret_key').value = response;
					}
				}
			});
		}

		var createSecretKey = document.getElementById('createSecretKey');
		if (createSecretKey) {
			createSecretKey.addEventListener('click', wc_sf_generate_secret_key);
		}
		</script>
		<?php
	}


	/**
 	 * Save settings.
 	 *
 	 * @since 1.8.0
	 */
	public function save() {
		global $current_section;
		$settings = $this->get_settings( $current_section );
		WC_Admin_Settings::save_fields( $settings );

		if (isset($_POST['woocommerce_sf_country_settings'])) {
			update_option('woocommerce_sf_country_settings', sanitize_text_field(stripslashes($_POST['woocommerce_sf_country_settings'])));
		}
	}



	/**
	 *
	 * Country specific settings
	 *
	 * @since 1.8.20
	 *
	 */
	private function get_country_specific_settings() {
		$countries = new WC_Countries();
		$country_options = $countries->__get('countries');
		$default_country = get_option('woocommerce_default_country');

		$country_settings = ['template'];
		$country_settings_db = json_decode(get_option('woocommerce_sf_country_settings', false), true);
		if (is_array($country_settings_db)) {
			$country_settings = array_merge(['template'], $country_settings_db);
		}
		?>

		<h2><?php _e('Countries', 'wc-superfaktura'); ?></h2>
		<div id="woocommerce_sf_invoice_title9-description"><p><?php _e('Override invoice settings for specific countries based on customer billing address.', 'wc-superfaktura'); ?></p></div>

		<input type="hidden" name="woocommerce_sf_country_settings">

		<table class="wc_input_table widefat">
			<thead>
				<tr>
					<th><?php _e('Country', 'wc-superfaktura'); ?></th>
					<th><?php _e('VAT #', 'wc-superfaktura'); ?></th>
					<th><?php _e('TAX ID #', 'wc-superfaktura'); ?></th>
					<th><?php _e('Bank Account ID', 'wc-superfaktura'); ?></th>
					<th><?php _e('Proforma Invoice Sequence ID', 'wc-superfaktura'); ?></th>
					<th><?php _e('Invoice Sequence ID', 'wc-superfaktura'); ?></th>
					<th><?php _e('Credit Note Sequence ID', 'wc-superfaktura'); ?></th>
					<th></th>
				</tr>
			</thead>

			<tfoot>
				<tr>
					<th colspan="8">
						<a href="#" class="button sf-add-country-settings"><?php _e('Add country', 'wc-superfaktura'); ?></a>
					</th>
				</tr>
			</tfoot>

			<tbody id="sf-countries">

				<?php foreach ($country_settings as $country): ?>

					<tr <?php echo ('template' == $country) ? 'data-name="template" style="display: none;"' : ''; ?>>
						<td style="padding: 5px 10px;">
							<select name="_country_country" class="_wc-enhanced-select">
								<?php foreach ($country_options as $value => $text): ?>
									<option value="<?php echo $value; ?>" <?php echo (('template' == $country && $default_country == $value) || ('template' != $country && $country['country'] == $value)) ? 'selected="selected"' : ''; ?>><?php echo $text; ?></option>
								<?php endforeach; ?>
							</select>
						</td>

						<td><input type="text" name="_country_vat" placeholder="<?php _e('VAT #', 'wc-superfaktura'); ?>" value="<?php echo ('template' != $country) ? $country['vat_id'] : ''; ?>" style="padding: 12px 10px !important;"></td>
						<td><input type="text" name="_country_tax" placeholder="<?php _e('TAX ID #', 'wc-superfaktura'); ?>" value="<?php echo ('template' != $country) ? $country['tax_id'] : ''; ?>" style="padding: 12px 10px !important;"></td>
						<td><input type="text" name="_country_bank_account_id" placeholder="<?php _e('Bank Account ID', 'wc-superfaktura'); ?>" value="<?php echo ('template' != $country) ? $country['bank_account_id'] : ''; ?>" style="padding: 12px 10px !important;"></td>
						<td><input type="text" name="_country_proforma_invoice_sequence_id" placeholder="<?php _e('Proforma Invoice Sequence ID', 'wc-superfaktura'); ?>" value="<?php echo ('template' != $country) ? $country['proforma_sequence_id'] : ''; ?>" style="padding: 12px 10px !important;"></td>
						<td><input type="text" name="_country_invoice_sequence_id" placeholder="<?php _e('Invoice Sequence ID', 'wc-superfaktura'); ?>" value="<?php echo ('template' != $country) ? $country['invoice_sequence_id'] : ''; ?>" style="padding: 12px 10px !important;"></td>
						<td><input type="text" name="_country_cancel_sequence_id" placeholder="<?php _e('Credit Note Sequence ID', 'wc-superfaktura'); ?>" value="<?php echo ('template' != $country) ? $country['cancel_sequence_id'] : ''; ?>" style="padding: 12px 10px !important;"></td>
						<td><a href="#" class="sf-delete-country-settings" style="display: block; padding: 12px 10px; white-space: nowrap;"><span class="dashicons dashicons-dismiss"></span><?php _e('Delete', 'wc-superfaktura'); ?></a></td>
					</tr>

				<?php endforeach; ?>

			</tbody>
		</table>

		<?php
	}
}
