�J;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"2032";s:11:"post_author";s:1:"2";s:9:"post_date";s:19:"2020-01-30 21:24:44";s:13:"post_date_gmt";s:19:"2020-01-30 20:24:44";s:12:"post_content";s:13307:"Je novembrové ráno, na tráve sa ešte drží námraza a pri Štrkoveckom jazere v Bratislave sa ozývajú zvuky vtákov. Stretávame sa s <em>Jánom Gúghom</em> z organizácie <em><a href="http://vtaky.sk/">SOS/BirdLife Slovensko</a></em>, ktorý nám ihneď predstavuje osadenstvo vtákov pri jazere.

<img class="aligncenter wp-image-2034 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_01.jpg" alt="" width="1024" height="683" />

Vidíme tu kačicu divú, ktorá na Štrkovci i hniezdi a patrí medzi najrozšírenejšie vtáky pri vodných plochách v meste. Ďalej lysku čiernu, ktorá tu zimuje, ale nehniezdni. A tiež čajku smejivú, tá je príkladom toho, ako sa donedávna divý vták dokázal adaptovať na mestské prostredie a v súčasnosti už taktiež patrí medzi najrozšírenejšie vtáky v mestách pri vodných plochách počas zimného obdobia.

Obchádzame jazero a na zábradlí vidíme sedieť čajku malú. Tá je u nás menej častá ako čajka smejivá, a preto nás stretnutie s ňou potešilo.

Zdalo by sa, že na pozorovanie vtákov nie je najvhodnejší čas, pretože väčšina vtáctva už odletela do teplejších krajov, ale podľa Jána má každé ročné obdobie svoje čaro a to zimné je bohaté práve na prílety severských vodných vtákov.

Neďaleko nás práve špacírujú dve kavky. Tie patria ku krkavcovitým vtákom a ich výzor pripomína vranu alebo havrana, akurát v menšom vydaní. „Kavky sú veľmi milé a zaujímavé krkavcovité vtáky, ktoré sa u nás vyskytujú celoročne a stavajú si tu i hniezda. V zime prilietajú ďalšie zo severu spoločne s havranmi, aby tu prezimovali,“ hovorí o kavkách Ján.

<img class="aligncenter wp-image-2035 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_06.jpg" alt="" width="1024" height="683" />

<img class="aligncenter wp-image-2040 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_012.jpg" alt="" width="1024" height="683" />
<h4><b>INTELIGENTNÉ VTÁKY</b></h4>
Krkavcovité vtáky – havrany, vrany, straky či kavky – patria všeobecne medzi veľmi inteligentné vtáky. Zároveň sú nesmierne dôležité pre ostatné vtáky. Napríklad príchod sov do mesta majú na svedomí práve títo operenci. Sovy si totiž nerobia vlastné hniezda, ale využívajú tie vranie či stračie, takže prirodzene nasledujú svojich hostiteľov. Osud havranov a ďalších krkavcovitých druhov má teda vplyv aj na život sov a sokolov.

Populácia havranov za posledné desaťročia prudko poklesla a takmer 80 % celej populácie sa nachádza vo východných krajinách –<span class="Apple-converted-space">  </span>najmä v Rusku a na Ukrajine. Na Slovensko prilietajú zimovať práve tieto severské populácie havranov vo veľkom počte. Prirodzeným biotopom havrana sú poľné lesy, stromoradia, rôzne parky či aleje. V slovenskej prírode ohrozuje havrana najmä poľovnícka činnosť – známe sú prípady masového ničenia havraních hniezd z údajného záškodníctva.

Dôvodov, prečo sa havrani, ale aj ostatné vtáky presúvajú do mesta, je viacero. „Zmenilo sa predovšetkým ich prírodné prostredie a nenachádzajú tam dostatok potravy a bezpečné hniezdiská. Naopak, v mestskom prostredí sa cítia bezepčenejšie, majú tu omnoho menej predátorov a viac potravy. Ďalším dôležitým faktorom synantropizácie (sťahovania vtákov do mesta) je to, že v mestách sa nepoľuje. Tie vtáky to vedia a i preto sa cítia v meste bezpečnejšie,“ objasňuje nám situáciu Ján.

<img class="aligncenter wp-image-2037 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_08.jpg" alt="" width="1024" height="683" />

<img class="aligncenter wp-image-2038 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_09.jpg" alt="" width="1024" height="683" />

Vo všeobecnosti by sa dalo povedať, že historicky vtákov ubúda, nielen v prírode, ale i v mestách. „Proces urbanizácie u nás už prebehol, mestá sú čoraz zastavanejšie, miznú zelené plochy a prostredie v horách a nížinách bolo tak zásadne pretvorené, že niektoré vtáčie druhy regionálne úplne vymizli. Môžem spomenúť napríklad brehára čiernochvostého, ktorý bol pred 30 – 40 rokmi jeden z najrozšírenejších vtákov, zdržiaval sa na zaplavených lúkach, pasienkoch a mokradiach. Dnes ho u nás už nenájdete.<span class="Apple-converted-space">  </span>Ďalej napríklad krakľa belasá, ktorá bola rozšíreným druhom a ktorá u nás vyhynula v roku 2010 – 2011, kedy tu hniezdil posledný pár na Podunajskej nížine. Odvtedy hniezdenie kraklí nebolo zaznamenané,“ rozpráva o miznúcej populácii zaujímavých vtáčích druhov Ján.

<img class="aligncenter wp-image-2039 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_011.jpg" alt="" width="1024" height="683" />

<img class="aligncenter wp-image-2254 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_013.jpg" alt="" width="1024" height="683" />

V mestskom prostredí je však aj zopár druhov, ktoré, naopak, pribúdajú. Príkladom je holub hrivnák. Ide o vzrastovo väčšieho vtáka ako klasický holub a počas roka je bežne k videniu i v Bratislave. Patrí však medzi sťahovavé vtáky a na zimu odlieta do západnej Európy. Populácia hrivnákov z roka na rok rastie aj na Slovensku.
<h4><b>NÁSTRAHY MESTSKEJ DIVOČINY</b></h4>
„V mestách sa vyskytuje viacero druhov vtákov, ktoré sú výrazne ohrozované ľudskou činnosťou. Najvýraznejším faktorom na úmrtie vtákov v celosvetovom meradle je výstavba presklených budov. Robila sa štúdia, ktorá dokázala, že sú to naozaj desiatky až stovky miliónov vtákov na celom svete, ktoré takto uhynú. U nás má táto príčina smrti vtákov taktiež zvyšujúcu sa tendenciu. V niektorých krajinách začali túto situáciu už aktívne riešiť,“ vysvetľuje Ján.

„V Nemecku je možné zakúpiť sklá, ktoré majú zabudovaný UV vzor. Človek ho voľným okom nevidí, ale vtáky vnímajú širšie svetelné spektrum, takže vzor evidujú ako žiarivý objekt a dokážu sa mu vyhnúť,“ pokračuje Ján. „To sklo sa dá, samozrejme, dovážať i k nám, no u nás na niečo také zatiaľ nie je vôľa a v zákonoch nenájdete nič, čo by pomáhalo týmto úmrtiam predchádzať.“

<img class="aligncenter wp-image-2046 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_02.jpg" alt="" width="1024" height="683" />

Použiť sa však dajú nálepky siluet vtákov, alebo hocijaké iné vzory, ktoré sa lepia celoplošne na sklá a zabraňujú nárazom zviditeľnením nebezpečných presklených objektov. Nedokážu však zabrániť úmrtiam vtákov počas noci, pokiaľ nejde o UV nálepky. Na Slovensku evidujeme taktiež trend výstavby presklených autobusových zastávok, ktoré sú napriek svojej malej rozlohe pomerne nebezpečné a spôsobujú úhyny vtáctva. Tu sa dá spraviť jednoduché opatrenie oblepením takýchto plôch nálepkami. „Mnohé vtáky migrujú cez mesto aj v noci. Často pri týchto budovách nachádzame vzácne lesné druhy, ktoré uhynú pri náraze v noci,“ hovorí Ján o problémoch nočných letcov.

„Ďalším podstatným problémom je trend zateplovania budov, ktorý ohrozuje viaceré druhy vtákov, napríklad dáždovníky obyčajné, ktoré hniezdia najmä na panelových domoch. Počas zateplovania sú likvidované obrovské počty vtákov, pretože sa im zadebnia prístupy k hniezdam. Okrem dáždovníkov sú to aj vrabce, alebo netopiere. Ubúda aj belorítok, lebo ich hniezda na nových fasádach dobre nedržia a nenachádzajú pre ne dostatok vhodného stavebného materiálu. Ich hniezda sú taktiež vo veľkom zhadzované počas prípravy na zateplovanie a rekonštrukciu,“ uvádza Ján.

Pomôcť pri problematike dáždovníkov môžu alternatívne riešenia v podobe vtáčich búdok. Kde je vôla, je možné všetko.

<img class="aligncenter wp-image-2041 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_05.jpg" alt="" width="1024" height="683" />
<h4><b>PREDSUDKY VERZUS SKUTOČNOSŤ</b></h4>
Medzi najrozšírenejšie mestské vtáky na celom európskom kontinente patrí jednoznačne holub, tzv. ferálny mestský holub.

„Informácia o vzniku a existencii populácii mestských holubov nie je úplne podchytená, ale je možné, že pravdepodobne vznikli z divo žijúcich holubov skalných a do veľkej mieri boli tieto populácie dotované aj chovanými poštovými holubmi, ktoré uleteli a nevrátili sa k majiteľovi. Koľkokrát môžeme ešte dnes vidieť na pouličnom holubovi obrúčku na jeho nohe, ktorá nám prezradí jeho pôvod. Počas vojny boli holuby veľmi dôležitý komunikačný nástroj, nosili stovky listov,“ vysvetľuje Ján.

„Vzťah ľudí k holubom je zmiešaný. Niekotrí ich nenávidia, iní ich majú radi. Aj tu, bohužiaľ, fungujú predsudky, umelo vyvolaná panika a strach, ktoré pramenia z neinformovanosti. Holuby sú vnímané ako lietajúce potkany, ktorí šíria nákazu, pritom na Slovensku nebol nikdy potvrdený prípad, žeby sa niekto nakazil od mestského holuba. Môže, samozrejme, vzniknúť problém, napríklad na povalách, kde žijú veľké kolónie už desaťročia a hromadí sa tam ich trus a sú medzi nimi aj mŕtve vtáky, ale v zásade ani v takýchto prípadoch nebola zaznamenaná nákaza. Známe sú skôr prípady, kedy sa nakazili chovatelia domácich holubov alebo hydiny, ktoré chovali v nevhodných podmienkach,“ reaguje na mýty o holuboch v meste Ján.

<img class="aligncenter wp-image-2042 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_04.jpg" alt="" width="1024" height="683" />

Ak teda uvidíte našuchoreného holuba na vašom balkóne, nemajte strach, nenaznačuje to automaticky žiadnu chorobu, holub iba odpočíva a relaxuje.

V meste môžeme ešte pozorovať sýkorky veľké (bielolíce) alebo sýkorky belasé, drozdy čvikotavé, ktoré sa nachádzajú vo vyššie položených mestách, no niekoľko párov tento rok hniezdilo aj v Bratislave, ďalej napríklad drozdy čierne.

Spevavé vtáky ako drozdy či sýkorky ovplyvňuje svetelný smog i hluk mesta. Spievajú aj v noci, pretože nočné osvetlenie vplýva aj na ich biorytmy, a zároveň je dokázané, že vtáky v meste spievajú hlasnejšie ako v prírodnom prostredí. Adaptovali sa na mestský hluk a snažia sa ho prekričať.

<img class="aligncenter wp-image-2043 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_03.jpg" alt="" width="1024" height="683" />
<h4><b>UNIKÁTNY VTÁČÍ ŽIVOT V NAŠICH KONČINÁCH</b></h4>
Jedno je úplne isté, ak sa pozrieme na Bratislavu a jej okolie – Malé Karpaty, Dunaj, povodie Moravy – žije tu množstvo vzácnych druhov vtákov.

„Dá sa povedať, že Bratislava je v rámci<span class="Apple-converted-space">  </span>stredoeurópskych metropol unikátna, vystkytuje sa tu naozaj veľa zaujímavých druhov, môžem spomenúť napríklad dropa veľkého, ktorý patrí medzi kriticky ohrozené vtáky nielen u nás, a vyskytuje sa na bratislavských poliach. Alebo vzácny muchárik malý, ktorý žije v Malých Karpatoch. Ďalej vzácne druhy môžeme nájsť aj v panelových domoch, napríklad z juhovýchodného Slovenska je známe hniezdenie výrika či kuvika priamo na panelákoch. V tomto ale nezaostáva ani Bratislava, aj tu sme v prirodzených stromových dutinách zaznamenali hniezdenie vzácnych výrikov. Cez Bratislavu počas migrácie prelietajú také vtáčie druhy, ktoré by ste bežne v meste vôbec neočakávali. Celkovo sa v<span class="Apple-converted-space">  </span>Bratislave a jej okolí vyskytuje 149 hniezdiacich vtáčích druhov. Dokazuje to náš projekt, na ktorom sme pracovali<span class="Apple-converted-space">  </span>10 rokov a jeho výstupom bude Altas vtákov Bratislavy, “ uzatvára našu vtáčiu prechádzku Ján Gúgh.

<img class="aligncenter wp-image-2044 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_014.jpg" alt="" width="1024" height="683" />

Organizácia SOS/BirdLife Slovensko dlhodobo mapuje problematiku vtáctva na Slovensku. Edukuje verejnosť a snaží sa o ochranu nielen vzácnych vtáčich druhov, ale celkovej populácie vtákov na Slovensku. Viac o jej činnosti sa dozviete na stránke <em><a href="http://vtaky.sk/">vtaky.sk</a></em> a pravidelné novinky zo života vtákov môžete sledovať na ich <em><a href="https://www.facebook.com/SOSBirdLife-Slovensko-246238118725735/?fref=ts">facebooku</a></em>. Organizácii držíme palce, jej členovia robia kus vzácnej práce, pretože nie je nič krajšie, ako keď sa započúvate do vtáčieho spevu aj v rušnom meste.

<img class="aligncenter wp-image-2045 size-full" src="https://egres.local/wp-content/uploads/2020/01/vtaky_v_meste_016.jpg" alt="" width="1024" height="683" />

<em>Text Zuzana Mitošinková, fotografie Jana Makroczy</em>";s:10:"post_title";s:41:"Život v meste bez vtákov by bol smutný";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:38:"zivot-v-meste-bez-vtakov-by-bol-smutny";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-05-21 14:35:36";s:17:"post_modified_gmt";s:19:"2021-05-21 12:35:36";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:27:"https://egres.local/?p=2032";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}