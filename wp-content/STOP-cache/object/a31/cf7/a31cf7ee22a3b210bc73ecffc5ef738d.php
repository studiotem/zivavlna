�L;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"8152";s:11:"post_author";s:4:"2494";s:9:"post_date";s:19:"2020-05-14 22:13:19";s:13:"post_date_gmt";s:19:"2020-05-14 20:13:19";s:12:"post_content";s:2162:"<h4>Charakteristika</h4>
Detektívny náučny príbeh je o vode z hĺbky našich bukových lesov až po tajomné jaskyne. Vodník Vincent sa vydá hľadať stratenú knihu múdrosti Vodovedu. Všetci vodníci sa z nej učia, ako chrániť vodu, a teraz zrazu zmizla. Vincent prejde celý vodný svet, sprevádza ho sestra Ester a pani učiteľka Minerálka Búrlivá. Postupne sa dozvedá všetko o vode. Ako vzniká prameň, rieka, jazero. Aké živočíchy v nich žijú. Prečo sú dôležité ľadovce.

Knihu sme zložili ako taký menší experiment. Príbeh sa v nej strieda s vyučovaním o vode pútavou formou. Nech to nie je len kratochvíľa, ale hlboký ponor pre deti do vodného sveta. Čarovné žiarivé ilustrácie vytvorila Lucia Žatkuliaková.

Prečo voda? Lebo je taká dôležitá pre život a ľudia o nej vedia asi málo. Inak by ju toľko neznečisťovali. Európa už prišla o väčšinu vzácnych mokradí. Do riek prúdi množstvo odpadkov. Sú spútané a nedokážu sa vylievať. Je čas učiť naše deti lepšiemu porozumeniu k životnému prostrediu, než sme mali my.

Knižku odporúčame pre deti od 6 rokov a pre večne mladých dospelých.
<h4>Materiály, veľkosť a iné</h4>
<ul>
 	<li>Formát: 210 x 280 mm</li>
 	<li>Väzba: pevná, šitá</li>
 	<li>Počet strán: 80</li>
 	<li>Papier Fedrigoni: Arcoprint Milk White 150 g, Tintoretto Ceylon Crystal salt 140 g</li>
 	<li>Tlač: 4 Pantone farby, ofset</li>
 	<li>Výroba: vytlačené v Českej republike</li>
 	<li>Rok vydania: 2020</li>
 	<li>ISBN: 978-80-99924-01-8</li>
</ul>
<h4><img class="alignnone size-full wp-image-14164" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-recyklovatelne.png" alt="" width="100" height="100" /><img class="alignnone size-full wp-image-14165" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-fsc.png" alt="" width="100" height="100" /></h4>
Viac o značkách a možnostiach reckylácie našich produktov nájdete na stránke <a href="https://egres.internal.studiotem.com/recyklacny-program/" target="_blank" rel="noopener noreferrer"><strong>recyklačný program</strong></a>.";s:10:"post_title";s:7:"Vincent";s:12:"post_excerpt";s:439:"Príbeh o vode z hĺbky našich bukových lesov až po tajomné jaskyne. Vodník Vincent sa vydá hľadať stratenú knihu múdrosti Vodovedu. Všetci vodníci sa z nej učia, ako chrániť vodu, a teraz zrazu zmizla. Sprevádza ho sestra Ester a pani učiteľka Minerálka Búrlivá. Postupne sa dozvedá všetko o vode. Ako vzniká prameň, rieka, jazero. Aké živočíchy v nich žijú. Prečo sú dôležité ľadovce. Prečo prší.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:7:"vincent";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-09-09 14:46:22";s:17:"post_modified_gmt";s:19:"2021-09-09 12:46:22";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:54:"https://www.zivavlna.sk/?post_type=product&#038;p=8152";s:10:"menu_order";s:2:"14";s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}