M;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"9056";s:11:"post_author";s:4:"2494";s:9:"post_date";s:19:"2020-06-07 18:14:56";s:13:"post_date_gmt";s:19:"2020-06-07 16:14:56";s:12:"post_content";s:1082:"<h4>Charakteristika</h4>
Utierka na utieranie alebo zakrývanie potravín do každej domácnosti. Utierka je vyrobená z kombinácie ľanu a bavlny a je potlačená ekologickou farbou. Odporúčame prať na 40 °C a žehliť na nepotlačenej časti.
<h4>Materiály, veľkosť a iné</h4>
<ul>
 	<li>Formát: 50 x 70 cm</li>
 	<li>Materiál: 50 % ľan, 50 % bavlna, (drobný štítok 100 % viskóza)</li>
 	<li>Tlač: jednofarebná eko Pantone farba na vodnej báze</li>
 	<li>Výroba: vyrobené vo Švédsku</li>
</ul>
<h4><img class="alignnone size-full wp-image-14164" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-recyklovatelne.png" alt="" width="100" height="100" /><img class="alignnone size-full wp-image-14189" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-vegan.png" alt="" width="100" height="100" /></h4>
Viac o značkách a možnostiach reckylácie našich produktov nájdete na stránke <a href="https://www.zivavlna.sk/recyklacny-program/" target="_blank" rel="noopener noreferrer"><strong>recyklačný program</strong></a>.";s:10:"post_title";s:16:"Utierka Záhrada";s:12:"post_excerpt";s:77:"Ľanovo-bavlnená utierka so záhradkárskou ilustráciou od Alice Raticovej.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:15:"utierka-zahrada";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-09-02 22:54:17";s:17:"post_modified_gmt";s:19:"2021-09-02 20:54:17";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:54:"https://www.zivavlna.sk/?post_type=product&#038;p=9056";s:10:"menu_order";s:2:"26";s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}