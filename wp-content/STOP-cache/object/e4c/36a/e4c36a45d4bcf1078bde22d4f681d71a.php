qO;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:5:"14438";s:11:"post_author";s:1:"2";s:9:"post_date";s:19:"2021-06-25 13:50:56";s:13:"post_date_gmt";s:19:"2021-06-25 11:50:56";s:12:"post_content";s:9832:"<span style="font-weight: 400;">Pre nás sú to hrdinovia a hrdinky. Azylové farmy pre zachránené „hospodárske“ zvieratá menia vnímanie ľudí. Aj baran alebo kohút dokáže byť rovnaký kamoš a maznoš ako psík alebo mačka. Nikto nie je lepší zvierací influencer ako samotné zviera. S jeho radosťou, hravosťou aj smútkom.</span><span style="font-weight: 400;">
</span><span style="font-weight: 400;">
</span><span style="font-weight: 400;">Prevádzkovatelia takýchto azylov často obetujú svoj vlastný čas, pohodlie aj financie, aby pomohli tam, kde sa takmer nikto iný nestará. Sú prvým a dôležitým krokom k súcitnejšiemu svetu a je dôležité, aby sme o nich viac hovorili a podporovali ich.</span><span style="font-weight: 400;">
</span>

<img class="aligncenter wp-image-14439 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-01.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-01-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-01-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-01.jpg 1000w" alt="" width="1000" height="667" />

<img class="aligncenter wp-image-14440 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-02.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-02-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-02-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-02.jpg 1000w" alt="" width="1000" height="667" />

<span style="font-weight: 400;">Na Slovensku to ide s azylmi pomalšie, ale v susednom Česku je ich už niekoľko (na záver nájdete zoznam azylových fariem aj s odkazmi na ich weby či sociálne siete). Pri nedávnej ceste do Prahy sme navštívili napríklad <a href="https://www.facebook.com/ZachraneneZvierata/" target="_blank" rel="noopener">Skrytý azyl</a>. Zachránili vyhodených či nechcených zakrslých králičkov. Kačky pohodené v smetnom koši s ich mŕtvymi druhmi. Alebo napríklad prasničku Boženku z domáceho chovu, kde ju zneužívali na neustále množenie. Pri Prahe konečne našli bezpečie a spokojný domov.</span><span style="font-weight: 400;">
</span><span style="font-weight: 400;">
</span><span style="font-weight: 400;">Prasnička je teraz k ľuďom nedôverčivá a ukrýva sa, no nie je sa čo čudovať, keď jej brali všetky deti. Ale boli sme vybavení cuketou, tak na chvíľu vyšla von najesť sa a vykúpať sa vo vode a blate, ako to má rada. Občas sa vám o nohu obtrie kocúr alebo priblíži zvedavá sliepočka. A na pamlsku vylezie z nory aj králička Uška.</span><span style="font-weight: 400;">
</span>

<img class="aligncenter wp-image-14441 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-03.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-03-300x224.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-03-768x572.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-03.jpg 1000w" alt="" width="1000" height="745" />

<img class="aligncenter wp-image-14443 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-05.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-05-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-05-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-05.jpg 1000w" alt="" width="1000" height="667" />

<img class="aligncenter wp-image-14616 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/kiwi.jpg" alt="" width="1000" height="667" />

<img class="aligncenter wp-image-14446 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-08.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-08-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-08-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-08.jpg 1000w" alt="" width="1000" height="667" />

<img class="aligncenter wp-image-14447 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-09.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-09-300x224.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-09-768x572.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-09.jpg 1000w" alt="" width="1000" height="745" />

<img class="aligncenter wp-image-14448 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-010.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-010-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-010-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-010.jpg 1000w" alt="" width="1000" height="667" />

<span style="font-weight: 400;">Žiaľ, moriak Johan je bez jednej nohy. Bol vyšľachtený na to, aby čo najrýchlejšie nabral najviac váhy. Brojlerovým zvieratám, ako sa zvykne hovoriť moriakom aj sliepkam, ktoré sa chovajú na mäso, často zlyháva srdiečko a lámu sa im kosti, lebo ich telo taký rýchly a neprirodzený nápor rastu nezvláda. Nepočíta sa ani s tým, že by sa tieto bytosti dožili nejakého veku. Starostlivosť o ne preto býva veľmi náročná. Johanovi vyrábajú vozíček s kolieskami, aby mohol normálne dožiť, ako je to bežné aj v zahraničných azyloch.</span>

<span style="font-weight: 400;"><img class="aligncenter wp-image-14449 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-011.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-011-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-011-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-011.jpg 1000w" alt="" width="1000" height="667" /></span>

<span style="font-weight: 400;"><img class="aligncenter wp-image-14450 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-012.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-012-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-012-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-012.jpg 1000w" alt="" width="1000" height="667" /></span>

<span style="font-weight: 400;">Čo s tým môžeme ako bežní ľudia urobiť? Nepodporovať živočíšny priemysel. Uprednostňovať rastlinnú stravu a šíriť osvetu okolo seba. Pomáhať azylovým farmám, či už finančne alebo fyzickou silou, každému ako je najlepšie.</span>

<img class="aligncenter wp-image-14452 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-014.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-014-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-014-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-014.jpg 1000w" alt="" width="1000" height="667" />

<img class="aligncenter wp-image-14451 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-013.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-013-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-013-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/skryty-azyl-013.jpg 1000w" alt="" width="1000" height="667" />

<strong><em>Zoznam azylových fariem na Slovensku a v Českej republike:</em></strong>

<a href="https://www.instagram.com/home_regained/" target="_blank" rel="noopener">Home Regained</a>, SR

<a href="https://www.facebook.com/azyl.jakubov/" target="_blank" rel="noopener">Azyl pre presiatka a králiky</a>, SR

<a href="https://www.instagram.com/zvierata_vychodu/" target="_blank" rel="noopener">Zvieratá východu</a>, SR

<a href="https://www.instagram.com/skrytyazyl/" target="_blank" rel="noopener">Skrytý azyl</a>, ČR

<a href="https://www.facebook.com/farmajarikhanda/" target="_blank" rel="noopener">Azylová farma Jarikhanda</a>, ČR

<a href="https://domovprozvirata.cz" target="_blank" rel="noopener">Domov pro zvířatá</a>, ČR

<a href="https://www.statekradosti.cz" target="_blank" rel="noopener">Statek radosti</a>, ČR

<a href="https://farmanadeje.cz" target="_blank" rel="noopener">Farma Naděje</a>, ČR

<a href="http://www.kozipelisek.cz" target="_blank" rel="noopener">Kozí pelíšek</a>, ČR

<a href="https://www.azylponto.cz" target="_blank" rel="noopener">Azyl Ponto</a>, ČR

<a href="https://www.instagram.com/zivot_na_louce/" target="_blank" rel="noopener">Život na louce</a>, ČR

<a href="https://www.stastnyzverinec.cz" target="_blank" rel="noopener">Šťastný zvěřinec</a>, ČR

<a href="https://www.facebook.com/babetcinahajenka" target="_blank" rel="noopener">Babetčina hájenka</a>, ČR

<a href="https://www.utulektibet.cz" target="_blank" rel="noopener">Útulek Tibet</a>, ČR

<a href="https://www.zivotbezkrutosti.cz" target="_blank" rel="noopener">Život bez krutosti</a>, ČR";s:10:"post_title";s:55:"Hrdinovia zvieracích azylových fariem + zoznam azylov";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:51:"hrdinovia-zvieracich-azylovych-fariem-zoznam-azylov";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-08-23 21:30:25";s:17:"post_modified_gmt";s:19:"2021-08-23 19:30:25";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:32:"https://www.zivavlna.sk/?p=14438";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}