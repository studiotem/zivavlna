�L;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"1777";s:11:"post_author";s:1:"2";s:9:"post_date";s:19:"2020-01-29 20:47:16";s:13:"post_date_gmt";s:19:"2020-01-29 19:47:16";s:12:"post_content";s:1634:"<h4>Charakteristika</h4>
Túto knižku inšpirovalo detstvo. Pamätáte si prázdniny u starých rodičov? Oberanie čerešní, zaváranie uhoriek, zbieranie mandeliniek. Kríky plné ríbezlí. Príbeh je o malej Hanke, ktorá sa učí rozoznávať ovocie a zeleninu, pripravovať z nich jedlá a stolovať ako malá veľká slečna. Knižka je vytlačená kombináciou štyroch Pantone farieb, ktoré sme naschvál tak trochu rozpasovali. Akoby ju vyfarbovali deti.

Ku knižke vznikla v spolupráci so značkou detského oblečenia Mile veľmi chutná a podarená kolekcia. Nájdete ju na <a href="https://mile.sk/sk/v-zahrade/" target="_blank" rel="noopener noreferrer"><em>stránke Mile</em></a>.
<h4>Materiály, veľkosť a iné</h4>
<ul>
 	<li>Formát: 240 x 180 mm</li>
 	<li>Väzba: pevná, šitá</li>
 	<li>Počet strán: 32</li>
 	<li>Papier: BioTop Next 200 g, Colorplan Natural 100 g</li>
 	<li>Tlač: 4 Pantone farby, ofset</li>
 	<li>Výroba: vytlačené v Českej republike</li>
 	<li>Rok vydania: 2019</li>
 	<li>ISBN: 978-80-99924-01-8</li>
</ul>
<h4><img class="alignnone size-full wp-image-14164" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-recyklovatelne.png" alt="" width="100" height="100" /><img class="alignnone size-full wp-image-14165" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-fsc.png" alt="" width="100" height="100" /></h4>
Viac o značkách a možnostiach reckylácie našich produktov nájdete na stránke <a href="https://www.zivavlna.sk/recyklacny-program/" target="_blank" rel="noopener noreferrer"><strong>recyklačný program</strong></a>.";s:10:"post_title";s:10:"V záhrade";s:12:"post_excerpt";s:105:"Žiarivá, hravá a tak trochu šiši knižka o ovocí a zelenine z našich záhrad pre najmladšie deti.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:9:"v-zahrade";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-09-07 09:38:47";s:17:"post_modified_gmt";s:19:"2021-09-07 07:38:47";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:50:"https://egres.local/?post_type=product&#038;p=1777";s:10:"menu_order";s:2:"30";s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}