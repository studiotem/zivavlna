M;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:5:"13832";s:11:"post_author";s:4:"2494";s:9:"post_date";s:19:"2021-05-21 16:16:07";s:13:"post_date_gmt";s:19:"2021-05-21 14:16:07";s:12:"post_content";s:1456:"<h4>Charakteristika</h4>
Grafika vychádza z našej najvnovšej knižky o vodníkovi Vincentovi. Na ilustrácii sú živočíchy, ktoré žijú v našich vodách alebo pri vode: vydra riečna, rak riečny, šťuka severná, hadovka lesklá, struh potočný, salamandra škvrnitá, podustva severná, hlavátka podunajská, hlaváč pásoplutvý, volavka a vodnár potočný.

Každý kus grafiky je očíslovaný a podpísaný. Posielame zabalené na tvrdom kartóne.
<h4>Materiály, veľkosť a iné</h4>
<ul>
 	<li>Formát: 31 x 44 cm</li>
 	<li>Papier: Freelife Vellum 170 g</li>
 	<li>Tlač: trojfarebná risografia</li>
 	<li>Náklad tlače: 110 ks</li>
 	<li>Výroba: vytlačené v Českej republike</li>
</ul>
<h4><img class="alignnone size-full wp-image-14164" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-recyklovatelne.png" alt="" width="100" height="100" /><img class="alignnone size-full wp-image-14165" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-fsc.png" alt="" width="100" height="100" /><img class="alignnone size-full wp-image-14044" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/Frp4nshQ-znacka-kompostovatelne.png" alt="" width="100" height="100" /></h4>
Viac o značkách a možnostiach reckylácie našich produktov nájdete na stránke <a href="https://www.zivavlna.sk/recyklacny-program/" target="_blank" rel="noopener noreferrer"><strong>recyklačný program</strong></a>.";s:10:"post_title";s:38:"Živočíchy našich vôd / risografia";s:12:"post_excerpt";s:130:"Život pri vode a v jej hlbinách je plný života. Zoznámte sa s vybranými živočíchmi našich vôd. Trojfarebná risografia.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:31:"zivocichy-nasich-vod-risografia";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-07-03 23:49:57";s:17:"post_modified_gmt";s:19:"2021-07-03 21:49:57";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:55:"https://www.zivavlna.sk/?post_type=product&#038;p=13832";s:10:"menu_order";s:2:"19";s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}