qO;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:5:"13770";s:11:"post_author";s:4:"2494";s:9:"post_date";s:19:"2021-05-21 12:06:11";s:13:"post_date_gmt";s:19:"2021-05-21 10:06:11";s:12:"post_content";s:3812:"Medvedí cesnak <em>(Allium ursinum)</em> patrí k prvým jarným bylinám, ktoré môžeme nájsť najmä vo vlhších lesoch. Ide o liečivú bylinu, ktorá sa už po stáročia využívala nielen ako chutný doplnok v strave, ale aj pri liečbe ochorení.

<img class="aligncenter wp-image-14389 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/medvedi-cesnak-1.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/medvedi-cesnak-1-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/medvedi-cesnak-1-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/medvedi-cesnak-1.jpg 1000w" alt="" width="1000" height="667" />

K pozitívnym účinkom medvedieho cesnaku patria napríklad:
<ul>
 	<li>detoxikačné vlastnosti,</li>
 	<li>pomáha znižovať vysoký krvný tlak,</li>
 	<li>posiľňuje imunitu,</li>
 	<li>pomáha pri žalúdočných ťažkostiach (zápcha či plynnatosť).</li>
</ul>
Nezabúdajte, že medvedí cesnak, tak ako aj všetky ostatné chránené i nechránené byliny, je zakázané trhať v chránených úzamiach, v okolí prírodných pamiatok a v prírodných rezerváciach s tretím a vyšším stupňom ochrany.

<img class="aligncenter wp-image-14390 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/medvedi-cesnak-2.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/medvedi-cesnak-2-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/medvedi-cesnak-2-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/medvedi-cesnak-2.jpg 1000w" alt="" width="1000" height="667" />

Z medvedieho cesnaku sa zvykne pripravovať najmä pesto, alebo sa len tak voľne použije v šalátoch. Dá sa z neho spraviť napríklad aj chutná polievka, ktorá chutí jemne ako špenátová. Páči sa, tu je náš podomácky vytvorený jednoduchý recept/postup.

<strong>Potrebujeme:</strong>

2 stredne veľké batáty, 1/2 chlebového vrecka medvedieho cesnaku, 1 menšiu šalotku, rastlinné maslo, soľ, čierne korenie, 2 lyžičky lahôdkového droždia, 1/2 lyžičky mletej rasce, 1/2 lyžicky mletej rímskej rasce (kumín), 2 lyžičky sušenej vegeto zeleniny (zdravá verzia vegety, kúpite v bezopalových obchodoch), 100 ml rastlinnej smotany.

<strong>Postup: </strong>

Najemno nakrájanú šalotku opražíme na troške masla so štipkou soli. Násladne pridáme na kocky nakrájané batáty (tie polievku zjemnia, keďže sú to sladké zemiaky), vodou opláchnutý medvedí cesnak (nemusíte krájať) a zalejeme vodou (alebo zeleninovým vývarom ak máte) do polovice až trištvrtiny obsahu hrnca (nakoľko cesnak zmäkne, aby nebola polievka príliš vodnatá). Necháme ohriať do tepla a pridáme soľ a korenie podľa chuti a vyššie spomínané koreniny. Po zovretí varíme cca 20-30 minút. Po odstavení rozmixujeme do krémova, pridáme kocku rastlinného masla a smotanu. Podávať môžete s reďkovkovými klíčkami.

<img class="aligncenter wp-image-14397 size-full" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/polievka-z-medvedieho-cesnaku.jpg" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://d3i9l7sj72swdx.cloudfront.net/egres-online/polievka-z-medvedieho-cesnaku-300x200.jpg 300w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/polievka-z-medvedieho-cesnaku-768x512.jpg 768w, https://d3i9l7sj72swdx.cloudfront.net/egres-online/polievka-z-medvedieho-cesnaku.jpg 1000w" alt="" width="1000" height="667" />

<em><strong>Pozn.</strong> Medvedí cesnak trháme samozrejme bez cibuliek, len lístky. Cesnak na fotke sa nám nechtiac podaril vytiahnuť aj s cibuľkou a prišlo nám to fotogenické.</em>";s:10:"post_title";s:39:"Krémová polievka z medvedieho cesnaku";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:37:"kremova-polievka-z-medvedieho-cesnaku";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-05-22 21:57:53";s:17:"post_modified_gmt";s:19:"2021-05-22 19:57:53";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:32:"https://www.zivavlna.sk/?p=13770";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}