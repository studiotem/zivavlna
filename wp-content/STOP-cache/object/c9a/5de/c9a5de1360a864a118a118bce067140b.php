�J;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"1989";s:11:"post_author";s:1:"2";s:9:"post_date";s:19:"2020-01-30 20:31:03";s:13:"post_date_gmt";s:19:"2020-01-30 19:31:03";s:12:"post_content";s:11168:"Na jar sme našli na Kollárovom námestí v Bratislave poranenú mladú vranu. Pomohli sme jej a vybavili sme prevoz do záchrannej stanice na Záhorí. Pár dní potom sme objavili pri ceste mláďa straky. Po telefonáte s ornitológom sme zistili, že pokiaľ nie je zranená, máme ju nechať na mieste, lebo rodičia si ju strážia a kŕmia. Tak sme ju aspoň odniesli pod kríky, aby ju nezrazilo auto.

Nielen naše príbehy, ale aj podobné skúsenosti našich priateľov so zranenými zvieratami nás priviedli k tomu, aby sme spravili aspoň malú osvetu k tejto téme. Tento článok vznikol aj vďaka návšteve záchrannej stanice <em><a href="https://www.facebook.com/Z%C3%A1chrann%C3%A1-stanica-v-Borinke-N%C3%A1vrat-do-divo%C4%8Diny-Into-the-Wild-1703457206571584/?fref=ts">Návrat do divočiny v Borinke</a>.</em>

<img class="aligncenter wp-image-1990 size-full" src="https://egres.local/wp-content/uploads/2020/01/zachrana_zvierat_stanica_01.jpg" alt="" width="1500" height="1000" />
<h4><b>CHRÁNENÉ VOĽNE ŽIJÚCE ZVIERATÁ</b></h4>
Čo zvieratko, to špecifický tvor, ktorý si vyžaduje špeciálnu starostlivosť. Tobôž voľne žijúce zvieratá, o ktorých vie bežný človek pomerne málo. Väčšina ľudí dokonca nevie ani to, že takmer všetky divo žijúce zvieratá sú zákonom chránené. Nevhodná manipulácia, zlá strava alebo čokoľvek iné, čo poskytnete nájdenému zvieraťu hoci aj z dobrej vôle, môže jeho stav ešte zhoršiť. Preto je lepšie kontaktovať odborníkov, ktorí vás usmernia, ako správne pomôcť.

<img class="aligncenter wp-image-1997 size-full" src="https://egres.local/wp-content/uploads/2020/01/Zachrana_zvierat_grafika_01.png" alt="" width="1600" height="1600" />

Ak nájdete zviera a neviete, koho máte kontaktovať, najlepšie je ihneď volať <b>112</b>, kde vám poskytnú kontakty na ochranárov v danej lokalite alebo blízke záchranné stanice. Iné je, pokiaľ ide o poľovnú zver, tu je nutné kontaktovať políciu (158), ktorá zase upovedomí poľovníkov daného revíru, keďže poľovná zver je, bohužiaľ, podľa ich presvedčenia ich majetkom.

<img class="aligncenter wp-image-1991 size-full" src="https://egres.local/wp-content/uploads/2020/01/zachrana_zvierat_stanica_02.jpg" alt="" width="1500" height="1000" />
<h4><b>NA NÁVŠTEVE V ZÁCHRANNEJ STANICI</b></h4>
Na Slovensku máme zbruba 27 rehabilitačných a chovných staníc pre zranené divo žijúce zvieratá (zdroj: Štátna ochrana prírody SR – kompletný online zoznam všetkých staníc <a href="https://www.google.com/maps/d/viewer?mid=1yNXE5LFa5R0gkTI0SSR3ftX0YKw&amp;ll=48.807546284208094%2C19.539268533203085&amp;z=8"><i>nájdete tu</i></a>). Väčšina z nich spadá pod Štátnu ochranu prírody, no štyri z nich sú súkromné – <em><a href="https://www.facebook.com/zachrannastanicazazriva/?fref=ts">Stanica a Eko centrum v Zázrivej</a></em>, stanica <em><a href="http://spolocnosthavran.webnode.sk/">Havran v Ratnovciach</a></em> pri Piešťanoch, a stanice v <em><a href="https://www.facebook.com/ZachrannaStanicaBrezova/?fref=ts">Brezovej pod Bradlom</a></em> a <em><a href="https://www.facebook.com/Z%C3%A1chrann%C3%A1-stanica-v-Borinke-N%C3%A1vrat-do-divo%C4%8Diny-Into-the-Wild-1703457206571584/?fref=ts">Borinke</a></em>. Práve poslednú spomínanú stanicu, ktorá pôsobí viac-menej pre územie Bratislavského kraja, sme boli navštíviť.

„Pôsobíme už dva roky, i keď nápad založiť stanicu prišiel oveľa skôr a minimálne rok nám trvalo vybaviť všetky papiere a povolenia na prevádzkovanie stanice,“ začína rozhovor zoológ a veterinár Ján Jamriška, ktorý stanicu vedie.

<img class="aligncenter wp-image-1992 size-full" src="https://egres.local/wp-content/uploads/2020/01/zachrana_zvierat_stanica_03.jpg" alt="" width="1500" height="1000" />

Na otázku, ako sa dostal k založeniu záchrannej stanice, odpovedá s úsmevom: „Vyštudoval som zoólogiu a veterinárnu medicínu a vždy som chcel pracovať s inými ako spoločenskými zvieratami. Špecializoval som sa na exotické a voľne žijúce zvieratá. Absolvoval som niekoľko stáži na rôznych pracoviskách, pri ktorých sa vykryštalizovali moje záujmy. Pôvodne ma zaujímala aj medicína zoologických zvierat. Počas pobytu na Durrel Wildlife Conservation Trust som si však uvedomil, že ma najviac priťahuje 'conservation medicine', teda záchrana zvierat. Aj preto sa každý rok snažím pomáhať v rámci podobných záchranných programov vo svete.“
<h4><b>KOMPLIKOVANÁ ZÁCHRANA ZVIERAT</b></h4>
Stúpame do kopca, kde sú voliéry s dravcami a holubmi hrivnákmi. „Stanica je stále v procese budovania, pretože ja i kolegovia chodíme do práce, takže aktivitám v stanici venujem takmer všetok svoj voľný čas. Ročne nám stanicou prejdú stovky zvierat, čo je náročné na vybavenie, financie a i čas,“ vysvetľuje Ján.

„Väčšina našich pacientov sú menšie zvieratá: ježkovia, veveričky alebo rôzne druhy vtákov, dravcov či sov. Buď sa k nám dostávajú mláďatá, ktoré prišli o rodičov, alebo jedince, ktoré hlavne ľudskou činnosťou prišli k nejakému zraneniu. Snažíme sa im pomôcť a prinavrátiť ich do prírody a prirodzeného prostredia. Rovnako sa snažíme spolupracovať s ostatnými záchrannými stanicami a získavať zdroje na naše fungovanie a projekty. Do budúcnosti by sme chceli vylepšiť vybavenie na intenzívnu starostlivosť a edukáciu hlavne pre deti. Bohužiaľ, zatiaľ nemáme priestor na väčšie zvieratá, ako sú líšky či srny. Zároveň záchrana jedincov poľovnej zveri je o čosi zložitejšia.“

<img class="aligncenter wp-image-1993 size-full" src="https://egres.local/wp-content/uploads/2020/01/zachrana_zvierat_stanica_04.jpg" alt="" width="1500" height="1000" />

Existuje rad kategorizácie zveri, napríklad či ide o poľovnú chránenú alebo poľovnú nechránenú zver. Na to, aby stanica mohla vziať takúto zver, potrebuje povolenie od poľovníkov. Povolenie môže byť aj ústne, ale vo vzťahu k úradom nikdy neviete, je lepšie mať všetko na papieri, takže nastáva opäť byrokracia, ktorá môže spomaliť záchranu a znížiť šancu na pomoc zvieraťu.

Zdá sa, že situácia so záchranou poľovných zvierat je naozaj komplikovaná a jediným možným riešením by bola zmena zákona, ktorý by upravil to, aby pri poľovnej zveri nebol poľovník nad ochranárom a zverolekárom. Zákon by mal ísť v prospech chráneneho zraneného zvieraťa, aby sa mu, či už v štátnej alebo súkromnej stanici, dostalo čo najrýchlejšej pomoci.

„Stáva sa, že mi zavolá človek, ktorý napríklad našiel zranené zvieratko. No vzhľadom na fakt, že sme pracujúci ľudia, nevieme byť k dispozícii 24 hodín denne 7 dní v týždni. Nie je to naschváľ a je mi ľúto, že nemôžeme pomôcť každému zvieratku. Niekedy je to aj otázka kapacity, ktorú nevieme nafúknuť, alebo nedokážeme poskytnúť niektorým druhom podmienky, ktoré by potrebovali a to sú potom dôvody, kedy nemôžeme jedinca prijať.“

<img class="aligncenter wp-image-1994 size-full" src="https://egres.local/wp-content/uploads/2020/01/zachrana_zvierat_stanica_05.jpg" alt="" width="1500" height="1000" />
<h4><b>VOĽNE ŽIJÚCE ZVIERATÁ UBÚDAJÚ</b></h4>
Práca so zranenými zvieratami alebo nájdenými sirotami mláďať vtákov, dravcov či ježkov je náročná. Prevádzkovať stanicu predstavuje často obetu na úkor osobného času, lebo nikdy neviete, kedy a aký pacient vám pribudne. Veľa zvierat je zranených vďaka ľudskej neohľaduplnosti, ktorej by sa možno pri väčšej všímavosti dalo predísť.

Stačí sa pozrieť na slovenské cesty a diaľnice, ktoré sú každý rok (hlavne počas jari a leta) cintorínom pre množstvo živočíchov. Chýbajú migračné portály pre zvieratá, ktoré sú napríklad v Rakúsku, Maďarsku alebo Slovinsku úplne bežné. Problém zrážok s autom predstavuje jednu z najčastejších príčin zranení či úmrtí voľne žijúcich zvierat na Slovensku.

Ďalší, už globálnejší problém je ničenie prirodzených biotopov mnohých druhov zvierat. Na Slovensku tento problém predstavuje najmä nadmerná ťažba dreva. Ak nebudú podmienky pre život danej populácie dostatočne chránené, postupne daný druh vymizne. Ukážkovým príkladom môže byť situácia s kriticky ohrozeným hlucháňom hôrnym.

<img class="aligncenter wp-image-1995 size-full" src="https://egres.local/wp-content/uploads/2020/01/zachrana_zvierat_stanica_07.jpg" alt="" width="1500" height="1000" />

<img class="aligncenter wp-image-1996 size-full" src="https://egres.local/wp-content/uploads/2020/01/zachrana_zvierat_stanica_08.jpg" alt="" width="1500" height="1000" />

Podľa Svetového fondu na ochranu prírody (WWF) ubudlo za posledných 40 rokov (1970 – 2012) na planéte 58 % populácie zvierat a do roku 2020 by sa ich počet mal znížiť o ďalších 9 %. Vedci z WWF zistili, že príčinami je najmä ničenie oblastí pre voľne žijúce zvieratá, poľovnícto, pytliactvo a znečisťovanie prostredia. Celú štúdiu z roku 2016 si môžete prečítať v nasledujúcom <a href="https://www.wwf.org.uk/sites/default/files/2016-10/LPR_2016_full%20report_spread%20low%20res.pdf"><i>odkaze</i></a>.

Veľký pokles počtu voľne žijúcich zvierat je spolu s klimatickou zmenou jedným z najviditeľnejších znakov novej geologickej periódy, v ktorej ľudia dominujú planéte: „Už viac nie sme malý svet na veľkej planéte. Teraz sme veľký svet na malej planéte, kde sme dosiahli bod nasýtenia,” povedal v úvode k štúdii profesor Johan Rockström, výkonný riaditeľ Stockholm Resilience Centre.

Človek prehliada, že je plne závislý od tých istých podmienok, o ktoré oberá živočíchy. „Bohatstvo a rozmanitosť života na Zemi je základom pre život podporujúce systémy. Život tvorí život, toto je rovnica, ktorej sme súčasťou aj my. Stratiť biodiverzitu a prirodzené podmienky pre život znamená dospieť ku kolapsu,“ cituje šéfa WWF Marca Lambertiniho britský denník The Guardian.

<img class="aligncenter wp-image-1998 size-full" src="https://egres.local/wp-content/uploads/2020/01/Zachrana_zvierat_grafika_02.png" alt="" width="1600" height="1600" />

Pomôcť môžete aj vy – buďte ohľaduplnejší na cestách, dávajte väčší pozor na miestach, ktoré nie sú nijako zabezpečené a radšej spomaľte. Všímajte si viac svoje okolie, na jar skúste vyvesiť vtáčiu búdku na hniezdenie, v zime počas mrazov kŕmidlo a v lete dajte do záhrady, ku kríku či na parapet misku s vodou. Ďakujeme.

<em>Text Zuzana Mitošinková, fotografie Jana Míková, grafiky Silvia Vargová</em>

<i>Použité zdroje: </i><a href="https://www.theguardian.com/environment/2016/oct/27/world-on-track-to-lose-two-thirds-of-wild-animals-by-2020-major-report-warns"><i>The Guardian.com</i></a><i>, </i><a href="https://www.worldwildlife.org/"><i>Worldwildlife.org</i></a>";s:10:"post_title";s:86:"Ako pomôcť zraneným divo žijúcim zvieratám. Navštívili sme záchrannú stanicu";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:75:"ako-pomoct-zranenym-divo-zijucim-zvieratam-navstivili-sme-zachrannu-stanicu";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-05-21 14:35:15";s:17:"post_modified_gmt";s:19:"2021-05-21 12:35:15";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:27:"https://egres.local/?p=1989";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}