�O;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1105;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-12-19 12:36:38";s:13:"post_date_gmt";s:19:"2019-12-19 11:36:38";s:12:"post_content";s:30574:"<h3><strong>Všeobecné ustanovenia</strong></h3>
<h4><strong>Prevádzkovateľ</strong></h4>
<span style="font-weight: 400;">Prevádzkovateľom obchodu (e-shopu) na stránke www.zivavlna.sk je občianske združenie Egreš, so sídlom: Hálova 20, 851 01 Bratislava, IČO: 42362571, DIČ: 2024141636. </span>
<h5><strong>Kontaktné údaje prevádzkovateľa obchodu (predávajúceho)</strong></h5>
<span style="font-weight: 400;">názov: <strong>Živávlna / Egreš, o. z.</strong></span>

<span style="font-weight: 400;">adresa prevádzkarne (adresa, na ktorej môže kupujúci uplatniť právo na odstúpenie od zmluvy, reklamáciu, vrátiť zakúpený tovar, podať sťažnosť alebo iný podnet): </span>

<strong>Nová Cvernovka, </strong><strong>Račianska 78, </strong><strong>831 02 Bratislava, </strong><span style="font-weight: 400;">tel. číslo: <strong>+421 944 761 869</strong>, </span><span style="font-weight: 400;">e-mail: <strong>eshop@zivavlna.sk</strong>, <strong>info@zivavlna.sk.</strong></span>

<span style="font-weight: 400;">Orgánom dozoru je Slovenská obchodná inšpekcia (SOI) Inšpektorát SOI pre Bratislavský kraj Prievozská 32, P.O. Box 5, 820 07 Bratislava 27. Odbor výkonu dozoru, tel. č. 02/58 27 21 72, 02/58 27 21 04, fax č. 02/58 27 21 70.</span>
<h4><strong>Kupujúci</strong></h4>
<span style="font-weight: 400;">Kupujúcim je akákoľvek osoba, ktorá uzatvorí zmluvu s predávajúcim, a to odoslaním elektronickej objednávky prostredníctvom e-shopu na stránke www.zivavlna.sk a následnou úhradou celej ceny tovaru na základe objednávky. Kupujúci je spotrebiteľom, ak ide o fyzickú osobu, ktorá pri uzatváraní a plnení zmluvy nekoná v rámci predmetu svojej podnikateľskej činnosti, zamestnania alebo povolania. Ak kupujúci uvedie v rámci uzatvárania zmluvy (pri registrácii, objednávke a pod.) svoje identifikačné číslo (IČO) a/alebo DIČ, má sa za to, že v právnom vzťahu s predávajúcim ide o kupujúceho – podnikateľa.</span>

<span style="font-weight: 400;">V prípade uzatvorenia zmluvy s kupujúcim, ktorý je spotrebiteľom, sa zmluvný vzťah medzi kupujúcim a predávajúcim popri týchto Obchodných podmienkach riadi predovšetkým príslušnými ustanoveniami Občianskeho zákonníka (najmä § 52 až 54, všeobecné ustanovenia o kúpnej zmluve, ustanovenia § 613 až 627), zákona č. 102/2014 Z. z. o ochrane spotrebiteľa pri predaji tovaru alebo poskytovaní služieb na základe zmluvy uzavretej na diaľku alebo zmluvy uzavretej mimo prevádzkových priestorov predávajúceho v znení neskorších predpisov, a zákona č. 250/2007 Z. z. o ochrane spotrebiteľa v znení neskorších predpisov.</span>

<span style="font-weight: 400;">V prípade uzatvorenia zmluvy s kupujúcim, ktorý nie je spotrebiteľom, sa zmluvný vzťah medzi kupujúcim a predávajúcim ako obchodno-záväzkový vzťah popri týchto Obchodných podmienkach riadi predovšetkým príslušnými ustanoveniami Obchodného zákonníka (najmä všeobecné ustanovenia o kúpnej zmluve – § 409 a nasl.). Na kupujúceho – podnikateľa sa nevzťahujú práva, ktoré má spotrebiteľ, v zmysle týchto Obchodných podmienok a príslušných právnych predpisov.</span>

<span style="font-weight: 400;">Účelom týchto Obchodných podmienok je jasne a zrozumiteľne informovať kupujúceho o zmluvných podmienkach vo vzťahu k predaju tovaru predávajúcim resp. kúpe tovaru kupujúcim,  poskytnúť mu všetky potrebné informácie pred uzavretím zmluvného vzťahu a zároveň ho poučiť o práve na odstúpenie od zmluvy (v prípade kupujúceho – spotrebiteľa) a o reklamačnom poriadku.</span>
<h4><strong>Objednávka tovaru</strong></h4>
<span style="font-weight: 400;">Odoslaním elektronickej objednávky prostredníctvom e-shopu na stránke www.zivavlna.sk a následnou platbou celej ceny tovaru na základe objednávky vzniká medzi kupujúcim (fyzickou alebo právnickou osobou) a predávajúcim (prevádzkovateľom) zmluvný vzťah v zmysle týchto Obchodných podmienok.</span>

<span style="font-weight: 400;">Elektronickou objednávkou sa rozumie riadne vyplnený a odoslaný elektronický objednávkový formulár obsahujúci informácie o kupujúcom, o objednanom tovare a o konečnej celkovej cene tovaru (vrátane ceny dopravy a prípadných iných nákladov a poplatkov, ktoré uhrádza kupujúci v súvislosti s dodaním tovaru).</span>

<span style="font-weight: 400;">Predávajúci potvrdzuje prijatie záväznej objednávky zaslaním potvrdzujúceho e-mailu na adresu uvedenú kupujúcim.</span>

<span style="font-weight: 400;">Ako potvrdenie o uzavretí zmluvy slúži faktúra vygenerovaná po úhrade ceny tovaru na základe objednávky. Pri osobnom prevzatí slúži ako potvrdenie pokladničný doklad vygenerovaný registračnou pokladnicou.</span>
<h3><strong>Platba</strong></h3>
<h4><strong>Ceny tovaru, možnosti platby</strong></h4>
<span style="font-weight: 400;">Všetky ceny produktov v e-shope sú uvedené bez DPH. K cene tovaru sa pripočíta cena za dopravu podľa veľkosti objednávky a krajiny doručenia. Minimálna hodnota objednávky je <strong>5 Eur</strong> (bez ceny dopravy).</span>

<span style="font-weight: 400;">Kupujúci akceptuje platbu za objednaný tovar nasledujúcimi spôsobmi:</span>

<em><span style="font-weight: 400;">— online platobnou kartou prostredníctvom platobnej brány TrustPay,</span></em>

<em><span style="font-weight: 400;">— online bankovým prevodom prostredníctvom platobnej brány TrustPay,</span></em>

<em><span style="font-weight: 400;">— platba v hotovosti alebo kartou pri osobnom prevzatí tovaru.</span></em>

<span style="font-weight: 400;">Tovar expedujeme najneskôr do 5 až 8 pracovných dní od potvrdzujúceho e-mailu o vybavenej objednávke, ktorý Vám príde na Váš e-mail, keď sa nám platba pripíše na účet.</span>

<span style="font-weight: 400;">Nie je možná platba na dobierku.</span>
<h3><strong>Dodanie tovaru</strong></h3>
<h4><strong>Osobný odber</strong></h4>
<span style="font-weight: 400;">Osobný odber je možný len po prijatí potvrdzujúceho e-mailu o vybavení objednávky. Tovar si môžete vyzdvihnúť a uhradiť v našom ateliéri v budove Novej Cvernovky na Račianskej 78 v Bratislave, v hlavnej budove školy vpravo na prízemí.</span>

<span style="font-weight: 400;">Osobný odber v Novej Cvernovke:</span>

<em><span style="font-weight: 400;">pondelok: 12.00 — 18.00</span></em>

<em><span style="font-weight: 400;">utorok: zatvorené</span></em>

<em><span style="font-weight: 400;">streda: 12.00 — 18.00</span></em>

<em><span style="font-weight: 400;">štvrtok: zatvorené</span></em>

<em><span style="font-weight: 400;">piatok: zatvorené</span></em>

<span style="font-weight: 400;">Lehota na rezerváciu prevzatia tovaru pri osobnom odbere je <strong>30 kalendárnych dní</strong> od potvrdzujúceho e-mailu o vybavenej objednávke. V prípade nevyzdvihnutia tovaru v tejto lehote bude objednávka automaticky zrušená a refundovaná. V prípade, že potrebujete lehotu predĺžiť kontaktujte nás na e-mail eshop@zivavlna.sk. Ak by ste si želali zaslať tovar dodatočne prepravnou spoločnosťou, k cene prepravy bude prirátaný <strong>administratívny poplatok plus 2 €</strong>.</span>

V súvislosti s bezpečnostnými opatreniami Covid-19 až vás prosíme o dodržiavanie nasledujúcich opatrení:

<em><span style="font-weight: 400;">—</span> vstup do budovy Novej Cvernovky len s ochranným prekrytím nosa a úst,</em>

<em><span style="font-weight: 400;">—</span> použitie dezinfekčného gélu na ruky.</em>
<h4><strong>Zasielanie tovaru</strong></h4>
<span style="font-weight: 400;">Tovar zasielame:</span>

<em><span style="font-weight: 400;">— prostredníctvom kuriérskej spoločnosti Slovak Parcel Service, s. r. o., </span></em>

<em><span style="font-weight: 400;">— prostredníctvom Zásielkovne na výdajne miesto podľa výberu (platí pre Českú republiku, Maďarsko a Poľsko),</span></em>

<em><span style="font-weight: 400;">— prostredníctvom Slovenskej pošty a. s. (platí pre ostatné krajiny EÚ).</span></em>

<span style="font-weight: 400;">Konečná cena dopravy tovaru je uvedená v záväznej objednávke, ktorú potvrdzujete pred úhradou celkovej platby za objednávku. Cena za prepravu je stanovená podľa aktuálneho cenníka prepravných spoločností a podľa ceny za obalový materiál.</span>

<span style="font-weight: 400;">Nezodpovedáme za prípadné oneskorené dodanie tovaru alebo poškodenie tovaru, ak je zavinené spoločnosťou zabezpečujúcou doručenie tovaru.</span>
<h4><strong>Dodacia lehota</strong></h4>
<span style="font-weight: 400;">Tovar expedujeme do 5 až 8 pracovných dní od potvrdzujúceho e-mailu o vybavenej objednávke, ktorý Vám príde na Váš e-mail, keď sa nám platba pripíše na účet. Doručenie tovaru v rámci Slovenskej republiky trvá zvyčajne 1 až 3 pracovné dni. Doručenie tovaru v rámci Českej republiky tráva max. 1 týždeň, v rámci Európy trvá zvyčajne max. 2 týždne.</span>

<span style="font-weight: 400;">Ak vám tovar nebol doručený, napíšte nám e-mail na eshop@zivavlna.sk.</span>

<span style="font-weight: 400;">Vyhradzujeme si právo na predĺženie dodacej doby v prípade, ak sa vyskytnú nepredvídateľné okolnosti. O takomto predĺžení dodacej lehoty vás budeme bezodkladne informovať.</span>

V prípade, že kupujúci neuviedol v objednávkovom formulári všetky potrebné informácie na doručenie zásielky, napríklad popisné číslo ulice, bude vyzvaný na ich doplnenie elektronickou formou. Ak údaje nedoplní do 10 kalendárnych dní od vyzvania, objednávka bude zrušená a refundovaná.

<span style="font-weight: 400;">Ak vám nedodáme objednaný tovar najneskôr do 30 dní po uzavretí zmluvy (úhrade ceny tovaru) a nedodáme ho ani v dodatočne poskytnutej primeranej lehote, máte právo od zmluvy odstúpiť. V prípade doručovania tovaru mimo Slovenskej republiky sa uvedená lehota primerane predlžuje, a to o 30 dní pri doručovaní v rámci Európy.</span>
<h4><strong>Žiadosť o neskoršie dodanie</strong></h4>
<span style="font-weight: 400;">Ak si želáte dodať tovar neskôr, informujte nás o tom v poznámke k objednávke alebo bezodkladne e-mailom.</span>
<h3><strong>Vrátenie peňazí</strong></h3>
<h4><strong>Storno objednávky</strong></h4>
<span style="font-weight: 400;">Ak ste uhradili objednávku, ešte nebola expedovaná a rozhodli ste sa ju zrušiť, bezodkladne nás kontaktujte na eshop@zivavlna.sk. Peniaze vám vrátime do 14 pracovných dní na účet, z ktorého ste vykonali platbu.</span>

<span style="font-weight: 400;">Storno objednávky je samozrejme bez poplatku.</span>
<h4><strong>Odstúpenie od zmluvy, vrátenie tovaru</strong></h4>
<span style="font-weight: 400;">Do 14 dní od prevzatia tovaru máte právo odstúpiť od zmluvy bez uvedenia dôvodu. Avšak takéto právo na odstúpenie sa nevzťahuje na predaj zvukových záznamov, obrazových záznamov, zvukovoobrazových záznamov, kníh alebo počítačového softvéru predávaných v ochrannom obale (ak bol obal rozbalený) a predaj kníh nedodávaných v ochrannom obale.</span>

<span style="font-weight: 400;">Pri uplatnení práva na odstúpenie od zmluvy doručte oznámenie o odstúpení od zmluvy v elektronickej forme na e-mailovú adresu eshop@zivavlna.sk. Na tento účel je možné použiť <a href="https://d3i9l7sj72swdx.cloudfront.net/egres-online/zivavlna-egres-vzor-na-odstupenie-od-zmluvy.docx">živávlna_egreš_vzor-na-odstupenie-od-zmluvy</a> na odstúpenie od zmluvy, ktorý je zverejnený na stránke www.zivavlna.sk. V prípade, ak nepoužijete tento formulár, uveďte pri odstúpení od zmluvy v ňom obsiahnuté údaje.</span>

<span style="font-weight: 400;">Kompletný, nepoškodený, nepoužitý tovar v pôvodnom, neporušenom ochrannom obale odošlite spolu s dokladom (doklad spolu so vzorom na odstúpenie od zmluvy stačí poslať elektronicky na adresu eshop@zivavlna.sk) o kúpe najneskôr do 14 dní odo dňa uplatnenia práva na odstúpenie od zmluvy na adresu:</span>

<em><span style="font-weight: 400;">Živávlna (Egreš, o. z.), </span><span style="font-weight: 400;">Nová Cvernovka, </span><span style="font-weight: 400;">Račianska 78, </span><span style="font-weight: 400;">831 02 Bratislava</span></em>

<span style="font-weight: 400;">V súvislosti so škodou vzniknutou na vrátenom tovare zodpovedáte iba za akékoľvek zníženie hodnoty tovaru v dôsledku zaobchádzania s ním iným spôsobom, než aký je potrebný na zistenie povahy, vlastností a funkčnosti tovaru. V prípade vrátenia tovaru, ktorý je použitý alebo poškodený alebo neúplný alebo hodnota predmetného tovaru je znížená v dôsledku takého konania, ktoré je nad rámec zaobchádzania potrebného na zistenie vlastností a funkčnosti tovaru, vzniká nám nárok na náhradu škody v rozsahu zníženia hodnoty tovaru, pričom ako kupujúci ste o tejto skutočnosti upovedomený. Ako predávajúci sme oprávnení vykonať jednostranné započítanie vlastnej pohľadávky na uvedenú náhradu škody oproti pohľadávke kupujúceho na vrátenie platieb z titulu odstúpenia od zmluvy.</span>

<span style="font-weight: 400;">Kúpnu cenu tovaru vrátane ceny dopravy (pri dodaní tovaru kupujúcemu) vám vrátime do 14 dní od doručenia oznámenia o odstúpení, na účet, z ktorého ste vykonali platbu, avšak zároveň nie sme povinní vrátiť uhradené platby pred tým, než nám bude tovar riadne vrátený alebo než nám preukážete zaslanie tovaru na našu adresu. Poštové prípadne iné náklady na vrátenie tovaru hradíte vy ako kupujúci. Vrátenie tovaru formou dobierky neakceptujeme a zásielku neprevezmeme.</span>
<h4><strong>Neprevzatie tovaru</strong></h4>
<span style="font-weight: 400;">Ak si neprevezmete tovar v rámci 15-dňovej lehoty na rezerváciu prevzatia tovaru pri osobnom odbere, sme oprávnení odstúpiť od kúpnej zmluvy. Na nutnosť prevzatia tovaru do konca tejto lehoty vás vopred upozorníme e-mailom odoslaným na adresu zadanú v objednávke. </span>

<span style="font-weight: 400;">V prípade, ak je tovar zasielaný Slovenskou poštou, Zásielkovôu resp. inou spoločnosťou zabezpečujúcou doručenie tovaru, a zásielka s tovarom sa nám vráti späť ako neprevzatá alebo nedoručená, upozorníme vás na túto skutočnosť e-mailom odoslaným na adresu zadanú v objednávke s tým, že tovar vám bude k dispozícii na dodatočné prevzatie prostredníctvom osobného odberu, a to po dobu 10 dní od zaslania vyššie uvedeného upozornenia. Ak si neprevezmete tovar ani v tejto dodatočnej 10-dňovej lehote, sme oprávnení odstúpiť od kúpnej zmluvy.</span>

<span style="font-weight: 400;">Odstúpenie od zmluvy po uplynutí lehoty na prevzatie tovaru vám oznámime e-mailom odoslaným na adresu zadanú v objednávke. Kúpnu cenu tovaru vám vrátime do 14 dní od odoslania oznámenia o odstúpení, na účet, z ktorého ste vykonali platbu. V prípade neprevzatého tovaru, ktorý bol predtým zasielaný Slovenskou poštou, Zásielkovňou, resp. inou spoločnosťou zabezpečujúcou doručenie tovaru, nie sme povinní takýmto spôsobom vrátiť náklady na balenie tovaru a zaslanie, ktoré boli v rámci záväznej objednávky prirátané ku kúpnej cene tovaru. </span>

<span style="font-weight: 400;">Po odstúpení od zmluvy sme oprávnení vami neprevzatý tovar predať tretej osobe alebo s ním inak voľne nakladať.</span>
<h4><strong>Nedostupnosť tovaru</strong></h4>
<span style="font-weight: 400;">Sme oprávnení odstúpiť od kúpnej zmluvy až do okamihu odoslania tovaru, a to najmä z dôvodu vzniknutej nedostupnosti objednaného tovaru. O prípadnom odstúpení vás bezodkladne informujeme e-mailom odoslaným na adresu zadanú v objednávke. Pred odstúpením sme oprávnení ponúknuť vám ako kupujúcim náhradné plnenie, alternatívny tovar. Máte právo náhradné plnenie odmietnuť.</span>

<span style="font-weight: 400;">Ak ste už uhradili kúpnu cenu, vrátime vám ju na váš účet do 14 dní odo dňa oznámenia nedostupnosti tovaru, respektíve odo dňa odmietnutia náhradného plnenia.</span>
<h3><strong>Reklamačný poriadok</strong></h3>
<h4><strong>Zodpovednosť za vady</strong></h4>
<span style="font-weight: 400;">Predávajúci zodpovedá za vady, ktoré má predaný tovar pri prevzatí kupujúcim, a tiež za vady, ktoré sa vyskytnú po prevzatí veci v záručnej dobe (záruka). Pri veciach predávaných za nižšiu cenu nezodpovedá za vadu, pre ktorú bola dojednaná nižšia cena. nejde Predávajúci nezodpovedá však za vady, ktoré boli zapríčinené prepravnou spoločnosťou pri preprave tovvaru od kupujúceho k objednávateľovi.</span>

<span style="font-weight: 400;">Za vadu a relevantný dôvod tovaru sa nepovažuje poškodenie alebo opotrebovanie tovaru používaním resp. inou manipuláciou, napríklad zošúchanie farieb knihy či grafiky spôsobené dotýkaním sa vytlačenej plochy, alebo napríklad znehodnotenie dlhodobým vystavením priamym slnečným lúčom.</span>
<h4><strong>Záručná doba</strong></h4>
<span style="font-weight: 400;">Na všetok tovar platí zákonom stanovená záručná doba 24 mesiacov, ktorá začína plynúť dňom prevzatia tovaru. Ako záručný list slúži faktúra vygenerovaná po úhrade ceny tovaru.</span>

<span style="font-weight: 400;">Práva zo zodpovednosti za vady tovaru zaniknú, ak sa neuplatnili v záručnej dobe.</span>

<span style="font-weight: 400;">V prípade uzatvorenia zmluvy s kupujúcim, ktorý nie je spotrebiteľom, sa záruka za akosť tovaru neposkytuje, t. j. neplatí vyššie uvedená záručná doba, pričom zodpovednosť predávajúceho za vady tovaru sa riadi ustanoveniami § 422 a nasl. Obchodného zákonníka. Popri nárokoch ustanovených v § 436 a nasl. Obchodného zákonníka nemá kupujúci nárok na náhradu škody prípadne iné nároky.</span>
<h4><strong>Postup pri uplatnení reklamácie</strong></h4>
<span style="font-weight: 400;">O žiadosti o reklamáciu tovaru nás informujte e-mailom na eshop@egres.online, pričom uveďte najmä označenie kupujúceho, číslo objednávky, označenie tovaru, dôvod reklamácie (dostatočný popis vád), korešpondenčné údaje a číslo účtu, z ktorého ste uhradili kúpnu sumu.</span>

<span style="font-weight: 400;">Nepoškodený tovar (okrem reklamovanej vady) odošlite spolu s dokladom o kúpe na adresu:</span>

<em><span style="font-weight: 400;">Živávlna (Egreš, o. z.), </span><span style="font-weight: 400;">Nová Cvernovka, </span><span style="font-weight: 400;">Račianska 78, </span><span style="font-weight: 400;">831 02 </span><span style="font-weight: 400;">Bratislava</span></em>

<span style="font-weight: 400;">Poštové prípadne iné náklady na vrátenie tovaru pri uplatnení reklamácie hradí kupujúci. Vrátenie tovaru formou dobierky neakceptujeme a zásielku neprevezmeme.</span>

<span style="font-weight: 400;">V prípade odôvodnenej reklamácie vád náklady na vrátenie tovaru pri reklamácii, ako aj náklady na prípadné následné dodanie náhradného tovaru a všetky kupujúcim účelne vynaložené náklady v súvislosti s predmetnou reklamáciou, znáša predávajúci. </span>

<span style="font-weight: 400;">Predávajúci poskytne kupujúcemu náhradu účelne vynaložených nákladov na vrátenie tovaru a prípadných ďalších účelne vynaložených nákladov súvisiacich s predmetnou reklamáciou bez zbytočného odkladu po ich preukázaní predávajúcemu zo strany kupujúceho, v rámci vybavenia reklamácie. </span>

<span style="font-weight: 400;">O prijatí žiadosti o reklamáciu tovaru vás budeme informovať do 7 pracovných dní.</span>
<h4><strong>Práva kupujúceho</strong></h4>
<span style="font-weight: 400;">Ak ide o vadu, ktorú možno odstrániť, má kupujúci právo, aby bola bezplatne, včas a riadne odstránená. Predávajúci je povinný vadu bez zbytočného odkladu odstrániť. Kupujúci môže namiesto odstránenia vady požadovať výmenu veci, alebo ak sa vada týka len súčasti veci, výmenu súčasti, ak tým predávajúcemu nevzniknú neprimerané náklady vzhľadom na cenu tovaru alebo závažnosť vady. Predávajúci môže vždy namiesto odstránenia vady vymeniť vadný tovar za bezvadný, ak to kupujúcemu nespôsobí závažné ťažkosti.</span>

<span style="font-weight: 400;">Ak ide o vadu, ktorú nemožno odstrániť a ktorá bráni tomu, aby sa tovar mohol riadne užívať ako tovar bez vady, má kupujúci právo na výmenu tovaru alebo má právo od zmluvy odstúpiť. Tie isté práva prislúchajú kupujúcemu, ak ide síce o odstrániteľné vady, ak však kupujúci nemôže pre opätovné vyskytnutie sa vady po oprave alebo pre väčší počet vád tovar riadne užívať. Ak ide o iné neodstrániteľné vady, má kupujúci právo na primeranú zľavu z ceny tovaru.</span>

<span style="font-weight: 400;">Na základe rozhodnutia kupujúceho, ktoré z práv kupujúci uplatňuje, je predávajúci povinný určiť spôsob vybavenia reklamácie ihneď, v zložitých prípadoch najneskôr do 3 pracovných dní odo dňa uplatnenia reklamácie, v odôvodnených prípadoch, najmä ak sa vyžaduje zložité technické zhodnotenie stavu tovaru, najneskôr do 30 dní odo dňa uplatnenia reklamácie. Po určení spôsobu vybavenia reklamácie sa reklamácia vybaví ihneď, v odôvodnených prípadoch možno reklamáciu vybaviť aj neskôr; vybavenie reklamácie však nesmie trvať dlhšie ako 30 dní odo dňa uplatnenia reklamácie. Po uplynutí lehoty na vybavenie reklamácie má kupujúci právo od zmluvy odstúpiť alebo má právo na výmenu tovaru za nový tovar.</span>

<span style="font-weight: 400;">Vybavením reklamácie sa rozumie ukončenie reklamačného konania odovzdaním opraveného tovaru, výmenou tovaru, vrátením kúpnej ceny tovaru, vyplatením primeranej zľavy z ceny tovaru, s následnou výzvou na prevzatie plnenia podľa uvedeného, prípadne odôvodnené zamietnutie reklamácie. Predávajúci je povinný o vybavení reklamácie vydať písomný doklad najneskôr do 30 dní odo dňa uplatnenia reklamácie.</span>

<span style="font-weight: 400;">Ak kupujúci reklamáciu tovaru uplatnil počas prvých 12 mesiacov od kúpy, môže predávajúci vybaviť reklamáciu zamietnutím len na základe odborného posúdenia; bez ohľadu na výsledok odborného posúdenia nemožno od kupujúceho vyžadovať úhradu nákladov na odborné posúdenie ani iné náklady súvisiace s odborným posúdením. Predávajúci je povinný poskytnúť kupujúcemu kópiu odborného posúdenia odôvodňujúceho zamietnutie reklamácie najneskôr do 14 dní odo dňa vybavenia reklamácie.</span>

<span style="font-weight: 400;">Ak kupujúci reklamáciu tovaru uplatnil po 12 mesiacoch od kúpy a predávajúci ju zamietol, predávajúci je povinný v doklade o vybavení reklamácie uviesť, komu môže kupujúci zaslať tovar na odborné posúdenie. Ak je tovar zaslaný na odborné posúdenie určenej osobe, náklady odborného posúdenia, ako aj všetky ostatné s tým súvisiace účelne vynaložené náklady znáša predávajúci bez ohľadu na výsledok odborného posúdenia. Ak kupujúci odborným posúdením preukáže zodpovednosť predávajúceho za vadu, môže reklamáciu uplatniť znova; počas vykonávania odborného posúdenia záručná doba neplynie. </span>

<span style="font-weight: 400;">Predávajúci je povinný kupujúcemu uhradiť do 14 dní odo dňa znova uplatnenia reklamácie všetky náklady vynaložené na odborné posúdenie, ako aj všetky s tým súvisiace účelne vynaložené náklady. Znova uplatnenú reklamáciu nemožno zamietnuť.</span>
<h3><strong>Alternatívne riešenie sporov</strong></h3>
<h4><strong>Európska komisia</strong></h4>
<span style="font-weight: 400;">Ak je kupujúci spotrebiteľ, t. j. fyzická osoba nekonajúca v rámci predmetu svojej podnikateľskej činnosti, zamestnania alebo povolania, svoje práva a nároky súvisiace s kúpou tovaru si môže voči predávajúcemu uplatňovať aj v rámci alternatívneho riešenia sporu (on-line), v zmysle možností a podmienok takéhoto riešenia sporu uvedených v nasledujúcich ustanoveniach Obchodných podmienok. Obdobne môžu byť prostredníctvom alternatívneho riešenia sporu uplatnené aj nároky predávajúceho voči kupujúcemu. On-line riešenie sporu zabezpečuje Európska komisia a slovenské kontaktné miesto alternatívneho riešenia sporov prostredníctvom európskej on-line platformy, ktorá sa nachádza na <a href="https://ec.europa.eu/consumers/odr/main/">internetovej stránke Európskej komisie</a>.</span>

<span style="font-weight: 400;">Spotrebiteľ má právo obrátiť sa na predávajúceho so žiadosťou o nápravu (e-mailom na eshop@zivavlna.sk), ak nie je spokojný so spôsobom, ktorým predávajúci vybavil jeho reklamáciu, alebo ak sa domnieva, že predávajúci porušil jeho práva. Ak predávajúci odpovie na túto žiadosť zamietavo, alebo na ňu neodpovie do 30 dní od jej odoslania, spotrebiteľ má právo podať návrh na začatie alternatívneho riešenia sporu subjektu alternatívneho riešenia sporov podľa zákona č. 391/2015 Z. z. o alternatívnom riešení spotrebiteľských sporov. Subjektmi alternatívneho riešenia sporov sú orgány a oprávnené právnické osoby podľa § 3 zákona č. 391/2015 Z. z. Návrh môže spotrebiteľ podať spôsobom určeným v § 12 zákona č. 391/2015 Z. z.</span>

<span style="font-weight: 400;">Spotrebiteľ môže podať vyššie uvedenú sťažnosť aj prostredníctvom on-line platformy alternatívneho riešenia sporov, ktorá je dostupná na internetovej <a href="https://ec.europa.eu/consumers/odr/main/">stránke Európskej komisie</a>. Takáto sťažnosť bude vybavená do 90 dní.</span>

<span style="font-weight: 400;">Alternatívne riešenie sporov sa netýka sporov, kde hodnota sporu neprevyšuje sumu 20 EUR. Subjekt alternatívneho riešenia sporov môže od spotrebiteľa požadovať úhradu poplatku za začatie alternatívneho riešenia sporu maximálne do výšky 5 Eur bez DPH. Zoznam všetkých subjektov alternatívneho riešenia sporov zverejňuje Ministerstvo hospodárstva SR.</span>

<span style="font-weight: 400;">Podrobnejšie informácie o alternatívnom riešení sporov sú uvedené v zákone č. 391/2015 Z. z. o alternatívnom riešení spotrebiteľských sporov, v Nariadení Európskeho parlamentu a rady EÚ č. 524/2013 a v zákone č. 102/2014 Z. z. o ochrane spotrebiteľa pri predaji tovaru na diaľku alebo poskytovaní služieb na základe zmluvy uzavretej na diaľku.</span>
<h3><strong>Ostatné ustanovenia</strong></h3>
<h4><strong>Kontakt, osobné údaje, autorské práva</strong></h4>
<span style="font-weight: 400;">Ak nevyplýva niečo iné z týchto Obchodných podmienok, pri všetkých úkonoch a komunikácii zmluvných strán v súvislosti s plnením zmluvy sa použijú (s príslušnými právnymi účinkami) prostriedky diaľkovej komunikácie, a to najmä webové sídlo predávajúceho (www.zivavlna.sk), elektronická pošta a telefón, pričom sú relevantné kontaktné údaje predávajúceho uvedené v týchto Obchodných podmienok resp. na stránke www.zivavlna.sk a kontaktné údaje kupujúceho, ktoré uviedol pri uzatváraní zmluvy (pri registrácii resp. objednávke).</span>

<span style="font-weight: 400;">Osobné údaje, ktoré kupujúci poskytne predávajúcemu v rámci uzatvárania zmluvy (pri registrácii, objednávke a pod.), sa predávajúci zaväzuje používať iba na účel riadneho plnenia zmluvy s kupujúcim a spracuje ich v súlade so zákonom č. 18/2018 Z. z. o ochrane osobných údajov v znení neskorších predpisov. Podrobné informácie o spracúvaní osobných údajov predávajúcim sú uvedené v osobitnom dokumente, ktorý je uverejnený na stránke </span><span style="font-weight: 400;">www.</span><span style="font-weight: 400;">zivavlna.sk</span><span style="font-weight: 400;">.</span>

<span style="font-weight: 400;">Kúpou tovaru od predávajúceho nevznikajú žiadne práva na používanie autorských diel, ochranných známok či iných práv duševného vlastníctva predávajúceho prípadne tretích osôb, ak nie je v konkrétnom prípade osobitnou zmluvou dohodnuté inak.</span>

<span style="font-weight: 400;">Informácia o dĺžke trvania zmluvy a o minimálnej dĺžke trvania záväzkov spotrebiteľa: Zmluva medzi predávajúcim a kupujúcim má charakter spotrebiteľskej kúpnej zmluvy, ktorej predmetom je kúpa tovaru, pričom najčastejší spôsob zániku zmluvy je splnením záväzkov oboch zmluvných strán alebo môže zmluva zaniknúť všeobecnými spôsobmi zániku záväzku, najmä odstúpením od zmluvy. Nejde o zmluvu, pri ktorej sa automaticky predlžuje jej platnosť.</span>

<span style="font-weight: 400;">Predávajúci nepristúpil k žiadnym kódexom správania, ktorými by bol viazaný v súvislosti s predmetom zmluvy s kupujúcim.</span>

<span style="font-weight: 400;">Vstupom do zmluvného vzťahu s predávajúcim na základe príslušného úkonu – odoslaním elektronickej objednávky prostredníctvom e-shopu na stránke www.zivavlna.sk (s následnou platbou ceny tovaru) – kupujúci potvrdzuje, že sa riadne oboznámil s obsahom Obchodných podmienok, vrátane reklamačného poriadku, a plne ich akceptuje v platnom a účinnom znení (ku dňu odoslania objednávky kupujúcim).</span>

<span style="font-weight: 400;">Tieto Obchodné podmienky nadobúdajú platnosť a účinnosť dňa </span><span style="font-weight: 400;"><strong>25. 5. 2021</strong>. Vyhradzujeme si právo Obchodné podmienky kedykoľvek zmeniť alebo doplniť, obzvlášť v prípade zmien relevantných právnych predpisov upravujúcich práva a povinnosti predávajúceho a kupujúceho v súvislosti s kúpou tovaru, pričom takéto zmeny nadobúdajú účinnosť zverejnením na stránke</span> <strong>www.zivavlna.sk</strong><span style="font-weight: 400;">.</span>";s:10:"post_title";s:19:"Obchodné podmienky";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:18:"obchodne-podmienky";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-05-25 10:26:17";s:17:"post_modified_gmt";s:19:"2021-05-25 08:26:17";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:33:"https://egres.local/?page_id=1105";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}