M;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:5:"13129";s:11:"post_author";s:4:"2494";s:9:"post_date";s:19:"2020-12-01 12:16:23";s:13:"post_date_gmt";s:19:"2020-12-01 11:16:23";s:12:"post_content";s:1514:"<h4>Charakteristika</h4>
Grafika je inšpirovaná polárnymi krajinami a jej typickými obyvateľmi tučniakmi, ktorých zbožňujeme. Ilustrácia od Bianky Török je vytlačená risografom na modrý papier, s použitím modrej a zlatej farby. O krásu a divokosť polárnych oblastí môžeme však prísť v dôsledku klimatických zmien. Všetci vieme, že ľadovce sa veľmi rýchlo roztápajú. Ale stále to neberieme dosť vážne, aby sme podľa toho aj konali.

Každý kus grafiky je očíslovaný a podpísaný.
<h4>Materiály, veľkosť a iné</h4>
<ul>
 	<li>Formát: 31 x 44 cm</li>
 	<li>Papier: Colorplan Cool Blue 170 g</li>
 	<li>Tlač: dvojfarebná risografia</li>
 	<li>Náklad tlače: 90 ks</li>
 	<li>Výroba: vytlačené na Slovensku v Risomate</li>
</ul>
<h4><img class="alignnone size-full wp-image-14164" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-recyklovatelne.png" alt="" width="100" height="100" /> <img class="alignnone size-medium wp-image-14165" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-fsc.png" alt="" width="100" height="100" /><img class="alignnone size-full wp-image-14190" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/znacka-kompostovatelne.png" alt="" width="100" height="100" /></h4>
Viac o značkách a možnostiach reckylácie našich produktov nájdete na stránke <a href="https://www.zivavlna.sk/recyklacny-program/" target="_blank" rel="noopener noreferrer"><strong>recyklačný program</strong></a>.";s:10:"post_title";s:22:"Tučniaci / risografia";s:12:"post_excerpt";s:127:"Dvojfarebná risografika polárnej zimy s tučniakmi. Časť z predaja pôjde na pomoc azylovej farme pre zvieratá Jarikhanda.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:19:"tucniaci-risografia";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-07-03 23:50:41";s:17:"post_modified_gmt";s:19:"2021-07-03 21:50:41";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:55:"https://www.zivavlna.sk/?post_type=product&#038;p=13129";s:10:"menu_order";s:2:"18";s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}