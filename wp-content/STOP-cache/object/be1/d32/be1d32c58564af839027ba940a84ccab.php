qO;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:3;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-12-19 10:17:15";s:13:"post_date_gmt";s:19:"2019-12-19 09:17:15";s:12:"post_content";s:13519:"<h3><strong>Osobné údaje</strong></h3>
<h4><strong>Úvodné zhrnutie</strong></h4>
<span style="font-weight: 400;">Prevádzkovateľ e-shopu na stránke www.egres.online, občianske združenie Egreš, Hálova 20, 851 01 Bratislava, IČO: 42362571, DIČ: 2024141636, spracúva osobné údaje poskytnuté kupujúcim výlučne za účelom riadneho uzatvorenia kúpnej zmluvy, najmä potvrdenia podmienok kúpnej zmluvy, spracovania elektronickej objednávky, realizácie dodávky, zúčtovania platieb a nevyhnutnej komunikácie medzi zmluvnými stranami v rámci plnenia zmluvy, vrátane prípadného odstúpenia od zmluvy alebo reklamácie tovaru, po dobu vyplývajúcu z týchto podmienok. Osobné údaje nebudú zverejňované a ani nebude dochádzať k prenosu do ďalších krajín.</span>
<h4><strong>I. Právna úprava ochrany osobných údajov</strong></h4>
<span style="font-weight: 400;">Zákon č. 18/2018 Z. z. o ochrane osobných údajov (ďalej len „Zákon“) a súvisiace právne predpisy Slovenskej republiky (napr. vyhlášky Úradu na ochranu osobných údajov Slovenskej republiky)</span>

<span style="font-weight: 400;">Nariadenie Európskeho parlamentu a Rady (EÚ) 2016/679 z 27. apríla 2016 o ochrane fyzických osôb pri spracúvaní osobných údajov a o voľnom pohybe takýchto údajov, ktorým sa zrušuje smernica 95/46/ES (všeobecné nariadenie o ochrane údajov – „GDPR“).</span>
<h4><strong>II. Identifikačné údaje prevádzkovateľa</strong></h4>
<span style="font-weight: 400;">Prevádzkovateľom je občianske združenie Egreš, Hálova 20, 851 01 Bratislava, IČO: 42362571, DIČ: 2024141636 (ďalej len „prevádzkovateľ“).</span>
<h5><strong>Kontaktné údaje prevádzkovateľa obchodu (predávajúceho) </strong></h5>
<span style="font-weight: 400;">názov: Egreš, o. z.</span>

<span style="font-weight: 400;">adresa prevádzkarne (adresa, na ktorej môže dotknutá osoba uplatniť svoje práva týkajúce sa spracúvania osobných údajov): </span>

<span style="font-weight: 400;">Nová Cvernovka, Račianska 78, 831 02 Bratislava, </span><span style="font-weight: 400;">tel. číslo: +421 944 761 869, </span><span style="font-weight: 400;">e-mail: eshop@egres.online, info@egres.online, </span><span style="font-weight: 400;">zástupca prevádzkovateľa: nie je určený, </span><span style="font-weight: 400;">zodpovedná osoba: Zuzana Mitošinková.</span>

<span style="font-weight: 400;">Orgánom dozoru nad ochranou osobných údajov: Úrad na ochranu osobných údajov Slovenskej republiky, Hraničná 12, 820 07 Bratislava 27; tel. číslo: +421 2 3231 3214; e-mail: statny.dozor@pdp.gov.sk,</span><a href="http://www.dataprotection.gov.sk/"> <span style="font-weight: 400;">www.dataprotection.gov.sk</span></a><span style="font-weight: 400;">.</span>
<h4><strong>III. Rozsah spracúvania osobných údajov</strong></h4>
<span style="font-weight: 400;">Meno, priezvisko, fakturačná adresa, dodacia adresa (na doručenie tovaru), telefónne číslo, e-mailová adresa, číslo účtu (na evidovanie prijatých platieb a prípadné vrátenie platieb).</span>
<h4><strong>IV. Účel spracúvania osobných údajov</strong></h4>
<ol>
 	<li><span style="font-weight: 400;"> Registrácia kupujúceho (dotknutej osoby) v rámci e-shopu na stránke www.egres.online a prístup do e-shopu, evidovanie údajov o kupujúcom pre účely kúpy tovaru od predávajúceho, jeho objednávkach, stave vybavenia objednávok.</span></li>
 	<li><span style="font-weight: 400;"> Plnenie kúpnej zmluvy medzi prevádzkovateľom ako predávajúcim a dotknutou osobou ako kupujúcim – prijatie, spracovanie a vybavenie elektronickej objednávky, vyhotovenie faktúry, dodanie tovaru, vybavenie prípadného odstúpenia od zmluvy kupujúcim, prijatie a vybavenie prípadnej reklamácie tovaru, evidovanie prijatých platieb a prípadné vrátenie platieb kupujúcemu, prípadné plnenie ďalších zmluvných povinností, plnenie súvisiacich zákonných povinností (účtovných, daňových a pod.).</span></li>
</ol>
<h4><strong>V. Právny základ spracúvania osobných údajov</strong></h4>
<ol>
 	<li><span style="font-weight: 400;"> Podľa § 13 ods. 1 písm. a) Zákona – dotknutá osoba vyjadrila výslovný súhlas so spracúvaním svojich osobných údajov na účel uvedený v ods. IV bod 1 vyššie, a to pri registrácii kupujúceho v rámci e-shopu na stránke www.egres.online.</span></li>
 	<li><span style="font-weight: 400;"> Podľa  § 13 ods. 1 písm. b) resp. § 13 ods. 1 písm. f) Zákona – spracúvanie osobných údajov je nevyhnutné na riadne plnenie kúpnej zmluvy medzi prevádzkovateľom ako predávajúcim a dotknutou osobou ako kupujúcim, resp. v súvislosti s takouto zmluvou a existenciou vzájomných nárokov z takejto zmluvy, na účel uvedený v ods. IV bod 2 vyššie.</span></li>
</ol>
<h4><strong>VI. Oprávnené záujmy prevádzkovateľa alebo tretej strany, ak sa osobné údaje spracúvajú podľa § 13 ods. 1 písm. f) Zákona</strong></h4>
<span style="font-weight: 400;">Pre prípad uvedený v ods. V bod 2 vyššie je oprávneným záujmom prevádzkovateľa uplatnenie prípadných nárokov z kúpnej zmluvy voči dotknutej osobe prípadne oprávnená obrana pred uplatnenými nárokmi zo strany kupujúceho, príslušnými právnymi prostriedkami.</span>
<h4><strong>VII. Príjemcovia osobných údajov</strong></h4>
<span style="font-weight: 400;">Tretími stranami, ktorým sa poskytnú niektoré osobné údaje kupujúceho, sú subdodávatelia prevádzkovateľa, pričom toto poskytnutie osobných údajov je nevyhnutné pre zabezpečenie čiastkových úkonov pri riadnom plnení kúpnej zmluvy medzi prevádzkovateľom a dotknutou osobou. </span>

<span style="font-weight: 400;">Takýmito príjemcami osobných údajov sú:</span>
<ol>
 	<li><span style="font-weight: 400;"> Websupport, s.r.o., IČO: 36 421 928 (poskytovateľ hostingu e-shopu).</span></li>
 	<li><span style="font-weight: 400;"> Bubblestorm Management (Proprietary) Limited – WooCommerce, reg. č. 2010/006103/07 (systém e-shopu).</span></li>
 	<li><span style="font-weight: 400;"> TrustPay, a. s., IČO: 36865800 (platobné procesy – spracovanie platby za tovar cez platobnú bránu).</span></li>
 	<li><span style="font-weight: 400;"> Slovenská pošta, a.s., IČO: 36631124 (doručovacia služba) – príjemca v rozsahu meno, priezvisko, adresa na doručenie tovaru, e-mailová adresa, telefónne číslo.</span></li>
 	<li><span style="font-weight: 400;"> DPD (Direcet Parcel Distribution SK), s. r. o., IČO: 35834498 (alternatívna doručovacia služba) – príjemca v rozsahu meno, priezvisko, adresa na doručenie tovaru, e-mailová adresa, telefónne číslo.</span></li>
 	<li><span style="font-weight: 400;"> Zásielkovňa s. r. o., IČO: 48136999 (alternatívna doručovacia služba) – príjemca v rozsahu meno, priezvisko, adresa na doručenie tovaru, e-mailová adresa, telefónne číslo.</span></li>
 	<li><span style="font-weight: 400;"> osoby poskytujúce účtovné služby prevádzkovateľovi (plnenie zákonných účtovných a daňových povinností na strane prevádzkovateľa, spojených s predajom tovaru) – príjemca v rozsahu meno, priezvisko, adresa.</span></li>
</ol>
<span style="font-weight: 400;">Príjemcovi v tretej krajine alebo medzinárodnej organizácii žiadne osobné údaje nebudú poskytnuté.</span>
<h4><strong>VIII. Prenos osobných údajov do tretej krajiny alebo medzinárodnej organizácií</strong></h4>
<span style="font-weight: 400;">Prenos obchodných údajov do tretej krajiny alebo medzinárodnej organizácií sa nerealizuje ani nie ja zamýšľaný.</span>
<h4><strong>IX. Doba uchovávania osobných údajov</strong></h4>
<span style="font-weight: 400;">Osobné údaje spracúvané na účel uvedený v ods. IV bod 1 vyššie: Po dobu trvania registrácie kupujúceho v rámci e-shopu na stránke www.eshop.online; dotknutá osoba je kedykoľvek oprávnená zrušiť registráciu, čím zanikne predmetný účel spracúvania a skončí sa doba uchovávania osobných údajov z tohto titulu.</span>

<span style="font-weight: 400;">Osobné údaje spracúvané na účel uvedený v ods. IV bod 2 vyššie: Po dobu plnenia kúpnej zmluvy medzi prevádzkovateľom ako predávajúcim a dotknutou osobou ako kupujúcim, a následne počas trvania záručnej doby, prípadne aj počas uplatnenia nárokov zo zmluvy voči kupujúcemu resp. zo strany kupujúceho (min. počas trvania príslušnej premlčacej doby, ktorá sa uplatňuje vo vzťahu k daným nárokom).</span>

<span style="font-weight: 400;">Po uplynutí doby uchovávania osobných údajov prevádzkovateľ je povinný osobné údaje kupujúceho vymazať.</span>

<span style="font-weight: 400;">Bez ohľadu na vyššie uvedené, účtovné doklady je prevádzkovateľ povinný uchovať po dobu min. 10 rokov.</span>
<h4><strong>X. Poučenie o právach dotknutej osoby </strong></h4>
<span style="font-weight: 400;">Za podmienok stanovených v Zákone má dotknutá osoba v súvislosti so spracúvaním svojich osobných údajov:</span>
<ol>
 	<li><span style="font-weight: 400;"> právo požadovať od prevádzkovateľa prístup k osobným údajom týkajúcich sa dotknutej osoby ako aj ďalšie informácie podľa § 21 Zákona;</span></li>
 	<li><span style="font-weight: 400;"> právo na opravu nesprávnych osobných údajov resp. právo na doplnenie neúplných osobných údajov (podľa § 22 Zákona);</span></li>
 	<li><span style="font-weight: 400;"> právo na vymazanie osobných údajov (podľa § 23 Zákona);</span></li>
 	<li><span style="font-weight: 400;"> právo na obmedzenie spracúvania osobných údajov (podľa § 24 Zákona);</span></li>
 	<li><span style="font-weight: 400;"> právo namietať spracúvanie osobných údajov (podľa § 27 Zákona);</span></li>
 	<li><span style="font-weight: 400;"> právo na prenosnosť osobných údajov, t.j. právo získať osobné údaje, ktoré sa jej týkajú a ktoré poskytla prevádzkovateľovi, v štruktúrovanom, bežne používanom a strojovo čitateľnom formáte, a právo preniesť tieto osobné údaje ďalšiemu prevádzkovateľovi (podľa § 26 Zákona);</span></li>
 	<li><span style="font-weight: 400;"> právo kedykoľvek svoj súhlas so spracúvaním svojich osobných údajov odvolať, a to rovnakým spôsobom, akým súhlas udelila (podľa § 14 ods. 3 Zákona);</span></li>
 	<li><span style="font-weight: 400;"> právo podať návrh na začatie konania o ochrane osobných údajov podľa § 100 Zákona na Úrade na ochranu osobných údajov Slovenskej republiky –  za účelom zistenia, či došlo k porušeniu práv dotknutej osoby pri spracúvaní jej osobných údajov alebo došlo k porušeniu Zákona alebo osobitného predpisu v oblasti ochrany osobných údajov, a v prípade zistenia nedostatkov, ak je to dôvodné a účelné, uloženia opatrení na nápravu, prípadne pokutu za porušenie tohto zákona alebo osobitného predpisu pre oblasť ochrany osobných údajov.</span></li>
</ol>
<span style="font-weight: 400;">Svoje práva môže dotknutá osoba uplatniť písomne na adrese prevádzkovateľa alebo na e-mailovej adrese: eshop@egres.online, kde taktiež môžu byť dotknutej osobe poskytnuté prípadne potrebné bližšie informácie o právach dotknutej osoby.</span>
<h4><strong>XI. Ďalšie informácie dotknutej osobe podľa § 19 Zákona</strong></h4>
<span style="font-weight: 400;">Poskytovanie osobných údajov prevádzkovateľovi je dobrovoľné, avšak ide o požiadavku, ktorá je potrebná na uzavretie kúpnej zmluvy medzi prevádzkovateľom ako predávajúcim a dotknutou osobou ako kupujúcim. Následkom neposkytnutia osobných údajov je:</span>
<ol>
 	<li><span style="font-weight: 400;"> v súvislosti s účelom uvedeným v ods. IV bod 1 vyššie: nemožnosť registrovať kupujúceho v rámci e-shopu na stránke www.egres.online;</span></li>
 	<li><span style="font-weight: 400;"> v súvislosti s účelom uvedeným v ods. IV bod 2 vyššie: nemožnosť uzavrieť a riadne plniť kúpnu zmluvu medzi prevádzkovateľom ako predávajúcim a dotknutou osobou ako kupujúcim.</span></li>
</ol>
<span style="font-weight: 400;">Automatizované individuálne rozhodovanie vrátane profilovania podľa § 28 ods. 1 a 4 Zákona nie je uplatnené.</span>
<h4><strong>XII. Zabezpečenie osobných údajov</strong></h4>
<span style="font-weight: 400;">Prevádzkovateľ vyhlasuje, že prijal všetky vhodné technické a organizačné opatrenia k zabezpečeniu osobných údajov dotknutej osoby.</span>

<span style="font-weight: 400;">Prevádzkovateľ prijal technické opatrenia k zabezpečeniu dátových úložísk osobných údajov, najmä zabezpečenie prístupu k počítačom heslom, používaním antivírového programu a pravidelnou údržbou počítačov.</span>
<h4><strong>XIII. Záverečné ustanovenia</strong></h4>
<span style="font-weight: 400;">Odoslaním elektronickej objednávky v e-shope na stránke www.egres.online potvrdzuje dotknutá osoba – kupujúci, že bol oboznámený s podmienkami ochrany osobných údajov a že ich v celom rozsahu prijíma, v platnom a účinnom znení (ku dňu potvrdenia zo strany dotknutej osoby).</span>

<span style="font-weight: 400;">Tieto podmienky ochrany osobných údajov nadobúdajú platnosť a účinnosť dňa 1. 2. 2020.</span>";s:10:"post_title";s:17:"Ochrana súkromia";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:16:"ochrana-sukromia";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-05-22 09:44:56";s:17:"post_modified_gmt";s:19:"2021-05-22 07:44:56";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:30:"https://egres.local/?page_id=3";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}