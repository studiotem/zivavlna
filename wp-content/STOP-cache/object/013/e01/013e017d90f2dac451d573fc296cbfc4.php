M;a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:5:"13816";s:11:"post_author";s:4:"2494";s:9:"post_date";s:19:"2021-05-21 16:16:09";s:13:"post_date_gmt";s:19:"2021-05-21 14:16:09";s:12:"post_content";s:1791:"<h4>Charakteristika</h4>
Živávlna x Baggu! Taška je vyrobená kombináciou ripstop nylónu a recyklovaného nylónu pochádzajúceho z nylónových priadzí (napríklad silónové cievky a podobne). Máme otestované, že oproti klasickým bavlneným plátenkám taška z nylónu vydrží oveľa dlhšie, navyše je nepremokavá a pokiaľ sa uplatňuje princíp cirkulárnej ekonomiky, šetria sa aj prírodné zdroje. Tašku môžete prať v práčke alebo ručne vo vlažnej vode. Nežehliť. Nosnosť až do 20 kíl.

Spoločnosť Baggu vyrába v etických podmienkach v Číne a je v priamom kontakte so svojou dielňou, ktorú každý rok navštevuje. Viac o etických a udržateľných princípoch spoločnosti Baggu môžete nájsť na ich <a href="https://baggu.com/pages/sustainability" target="_blank" rel="noopener">webstránke</a>. Od roku 2020 Baggu vylúčila zo svojej výroby akékoľvek komponenty živočišného charakteru a všetky produkty sú úplne vegánske. Je pre nás dôležité spolupracovať s dodávateľmi, ktorí zdielajú podobnú filozofiu.
<h4>Materiály, veľkosť a iné</h4>
<ul>
 	<li>Veľkosť: 38 x 40 cm (bez ramien)</li>
 	<li>Nosnosť: do 20 kg</li>
 	<li>Materiál: 60 % ripstop nylón, 40 % recyklovaný nylón</li>
 	<li>Potlač: jednofarebná Pantone Warm grey</li>
 	<li>Výroba: v Číne v etických podmienkach</li>
</ul>
<h4></h4>
<img class="alignnone size-full wp-image-14040" src="https://d3i9l7sj72swdx.cloudfront.net/egres-online/2pbCyScn-znacka-vegan.png" alt="" width="100" height="100" />

Viac o značkách a možnostiach reckylácie našich produktov nájdete na stránke <a href="https://www.zivavlna.sk/recyklacny-program/" target="_blank" rel="noopener noreferrer"><strong>recyklačný program</strong></a>.";s:10:"post_title";s:25:"Zelená taška Živávlna";s:12:"post_excerpt";s:122:"Taška na nákupy je vyrobená aj z recycklovaného nylónu. Potlačená je Pantone farbou. Pre živúvlnu vyrobilo Baggu.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:14:"taska-zivavlna";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-09-09 14:46:22";s:17:"post_modified_gmt";s:19:"2021-09-09 12:46:22";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:55:"https://www.zivavlna.sk/?post_type=product&#038;p=13816";s:10:"menu_order";s:1:"3";s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}