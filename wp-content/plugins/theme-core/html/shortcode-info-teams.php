<?php

$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items              = (array) vc_param_group_parse_atts($atts['items']);
?>
<div class="block nano-infoTeams <?php echo esc_attr($css_class); ?> clearfix">
        <?php if ( $atts['title'] ) {?>
            <h3 class="block-title clearfix">
                <?php echo htmlspecialchars_decode( $atts['title'] ); ?>
            </h3>
        <?php }?>
        <?php if ( isset($atts['block_content']) ) {?>
            <div class="block-des">
                <?php echo esc_html( $atts['block_content'] ); ?>
            </div>
        <?php }?>
        <div class="block-content clearfix">
            <?php foreach ( $items as $item ) {?>
                <?php $img_size         = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => '500x600' ) );
                ?>
                <div class="box-team-list">
                    <div class="box-image clearfix">
                        <?php if( isset($item['image_box'])) { ?>
                            <div class="image">
                                <?php echo $img_size['thumbnail'];?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="box-content clearfix">
                    <?php if (  isset($item['title_box'] )) {?>
                        <h5 class="title-box">
                            <?php echo esc_html( $item['title_box'] ); ?>
                        </h5>
                    <?php }?>
                    <?php if ( isset($item['content_box']) ) {?>
                        <div class="des-box">
                                <?php echo esc_html( $item['content_box'] ); ?>
                        </div>
                    <?php }?>

                    </div>
                </div>
            <?php }?>
        </div>
</div>
