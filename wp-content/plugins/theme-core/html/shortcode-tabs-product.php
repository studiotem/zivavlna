<?php
/**
 * The default template for displaying content
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$idran= random_int(0,99);
//category
if(isset($atts['category']) && empty($atts['category'])){
    $args = array(
        'post_type'     => 'product',
        'post_status'   => 'publish',
        'tax_query'     => array(
            array(
                'taxonomy'  => 'product_cat'
            )
        )
    );
    $query_shop = new WP_Query($args);
}
else{
    $terms=explode(',', $atts['category']);
    $args= array(
        array(
            'taxonomy'          => 'product_cat',
            'field'             => 'slug',
            'terms'             => $terms,
            'posts_per_page'    => -1,
            'post_status'       => 'publish',
        )

    );
    $atts['tax_query']= $args;
}
$atts['posts_per_page']   = ($atts['number'] > 0) ? $atts['number'] : get_option('posts_per_page');
$atts['paged']            = ((nano_get_query_var('paged')) ? nano_get_query_var('paged') : 1);

//list tabs
$types = explode(',',$atts['list_type']);
foreach ($types as $type) {
    $list_types[$type] = get_TabTitle($type);
}

?>
<div data-example-id="togglable-tabs" role="tabpanel" class="product-tabs block">
            <div class="widgettitle  clearfix">
                <?php if ( $atts['box_title'] ) {?>
                    <h3 class="tabs-title">
                        <?php echo htmlspecialchars_decode( $atts['box_title'] ); ?>
                    </h3>
                <?php }?>
                <ul role="tablist" class="nav nav-tabs nav-product-tabs">
                    <?php
                    $count=0;
                    foreach ($list_types as $key =>$list) {
                        ?>
                        <li class="<?php echo ($count==0)?'active':''; ?>" role="presentation">
                            <a class="tabs-title-product" aria-expanded="true" data-toggle="tab" role="tab" href="#<?php echo  $key; ?><?php echo $idran; ?>">
                                <?php echo $list['title']; ?>
                            </a>
                        </li>
                        <?php
                        $count++;
                    }
                    ?>
                </ul>
            </div>
            <div class="tab-content widgetcontent clearfix" id="tabs_product<?php echo $idran; ?>">
                <?php $count=0; ?>
                <?php foreach ($list_types as $key => $list) {
                    switch ($key) {
                        case 'best-selling':
                            $atts['meta_key'] = 'total_sales';
                            $atts['order']    = 'DESC';
                            $atts['orderby']  = 'meta_value_num';
                            $meta_query = WC()->query->get_meta_query();
                            $atts['tax_query']= $args;
                            $atts['post__in'] = '';
                            $atts['meta_query'] = $meta_query;
                            break;

                        case 'toprate':
                            $atts['meta_key'] = '_wc_average_rating';
                            $atts['orderby']  = 'meta_value_num';
                            $meta_query = WC()->query->get_meta_query();
                            $atts['tax_query']= $args;
                            $atts['post__in'] = '';
                            $atts['meta_query'] = $meta_query;
                            break;

                        case 'latest':
                            $meta_query = WC()->query->visibility_meta_query();
                            $meta_query = WC()->query->stock_status_meta_query();
                            $atts['meta_query'] = $meta_query;
                            break;

                        case 'featured':
                            $atts['meta_key'] = '';
                            $atts['orderby']  = '';
                            $atts['post__in'] = '';
                            $meta_query = array_merge( $atts['tax_query'], WC()->query->get_tax_query() );
                            $meta_query[]= array(
                                'taxonomy'         => 'product_visibility',
                                'terms'            => 'featured',
                                'field'            => 'name',
                                'operator'         => 'IN',
                                'include_children' => false,
                            );
                            $atts['tax_query'] = $meta_query;
                            break;

                        case 'onsale':
                            $product_ids_on_sale = wc_get_product_ids_on_sale();
                            $product_ids_on_sale[]  = 0;
                            $atts['meta_key'] = '';
                            $atts['orderby']  = '';
                            $atts['post__in'] = $product_ids_on_sale;
                            $atts['tax_query']= $args;
                            $meta_query = WC()->query->get_meta_query();
                            $atts['meta_query'] = $meta_query;
                            break;
                    }
                    ?>
                    <div  id="<?php echo  $key; ?><?php echo $idran; ?>" class="tab-pane <?php echo ($count==0)?' active':''; ?>" role="tabpanel">
                        <?php
                            $output = nano_template_part('box-layouts/shortcode', 'product-grid', array('atts' => $atts));
                        ?>
                    </div>
                    <?php $count++;
                    ?>
                <?php } ?>
            </div>
</div>



