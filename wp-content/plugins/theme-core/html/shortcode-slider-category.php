<?php

$na=1;
$add_class='';
$btn_link ='#';
$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items              = (array) vc_param_group_parse_atts($atts['items']);


?>
<div class="widget block-slider nano-slider-category">
    <div class="widgetcontent article-carousel clearfix" data-number="2" data-dots="true" data-arrows="false" data-table="2" data-rtl="false" data-mobile="1" data-mobilemin="1">
        <?php
            foreach ( $items as $item ) {?>
            <div class="slider-box col-item <?php echo esc_attr($add_class);?> clearfix">
                <?php if($item['image_box']) {
                    $image_link         = wp_get_attachment_url($item['image_box']);
                    $background_image   ="background-image:url('$image_link')";
                    $style_css          ='style ='.$background_image.'';
                    ?>
                    <div class="image-box clearfix " <?php echo esc_attr($style_css);?>>
                    </div>
                <?php } ?>

                <div class="content-box clearfix">

                    <div class="entry-content-inner">
                        <?php  if ( !empty($item['link'])) {
                            $link = trim( $item['link'] );
                            $link = ( '||' === $link ) ? '' : $link;
                            $link = vc_build_link( $link );
                            if ( strlen( $link['url'] ) > 0 ) {
                                $btn_link=$link['url'];
                            }?>
                        <?php }?>

                        <?php if($item['title_box']){?>
                            <h3 class="title-box">
                                <a class="btn-box" href="<?php echo esc_url($btn_link); ?>">
                                    <?php echo esc_html($item['title_box']); ?>
                                </a>
                            </h3>
                        <?php } ?>
                       <?php  if ( !empty($item['link'])) {
                            if ( strlen( $link['url'] ) > 0 ) {?>
                                <a class="btn btn-next" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>">
                                    <?php echo esc_html($link['title']);?>
                                    <i class="ion-ios-arrow-thin-right"></i>
                                </a>
                            <?php }
                        }?>

                    </div>
                </div>

            </div>
            <?php $na++; }
        ?>
    </div>

</div>