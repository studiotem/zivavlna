<?php
global $query_shop;
$na=1;
$class='';
$productsTypes='scattered';

$query_shop = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $atts ) );
$query_shop->query( $atts );

?>

<div class="products_shortcode_wrap items-scattered type-tabShop main-content">
    <ul class="products-block tab-isotopes row clearfix" data-col="2" data-pages="<?php echo esc_attr($query_shop->max_num_pages);?>">
        <?php while ( $query_shop->have_posts() ){
            $query_shop->the_post();
                if($na == 1 || ($na-1)%4==0 ):
                    $class='sm-center';
                    $productsTypes='scattered-sm';
                elseif ($na == 2 || ($na-2)%4==0):
                    $class='lg-right';
                    $productsTypes='scattered';
                elseif ($na == 3 || ($na-3)%4==0):
                    $class='lg-left';
                    $productsTypes='scattered';
                elseif ($na == 4 || ($na-4)%4==0):
                    $class='sm-right';
                    $productsTypes='scattered-sm';
                else:
                    $class='lg-center';
                    $productsTypes='scattered';
                endif;
                ?>
                <li <?php post_class($na.' col-item '.$class); ?>>
                    <?php wc_get_template_part( 'layouts/content-product-'.$productsTypes); ?>
                </li>

        <?php  $na++; }
            wp_reset_postdata();
        ?>
    </ul>
    <a href="<?php echo esc_url($atts['link']); ?>">
        <?php if($atts['btn_name']){?>
            <span  class="btn-loadmore"><?php echo esc_html($atts['btn_name']);?></span>
        <?php }?>
    </a>
</div>