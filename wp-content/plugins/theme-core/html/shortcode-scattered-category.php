<?php

$na=1;$add_class='';$image_size='sok-scattered-thumbnail-size';
$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items              = (array) vc_param_group_parse_atts($atts['items']);
?>
<div class="widget widget-scattered-category na-scattered-category">
    <div class="widgettitle  clearfix">
        <?php if ( $atts['title'] ) {?>
            <h3 class="title-block">
                <?php echo esc_html( $atts['title'] ); ?>
            </h3>
        <?php }?>
    </div>
    <div class="widgetcontent items-scattered clearfix">
            <?php

            foreach ( $items as $item ) {?>
                <?php
                    if( $na==1 || (($na-1)%6==0) ){
                        $add_class='ver-left';
                        $image_size='sok-scattered-thumbnail-size';
                    }
                    elseif ( $na==2 || (($na-2)%6==0) ){
                        $add_class='squ-right';
                        $image_size='sok-scattered-square';
                    }
                    elseif ( $na==4 || (($na-2)%6==0) ){
                        $add_class='squ-left';
                        $image_size='sok-scattered-square';
                    }
                    elseif ( $na==5 || (($na-2)%6==0) ){
                        $add_class='ver-right';
                        $image_size='sok-scattered-thumbnail-size';
                    }
                    else{
                        $add_class='hoz-center';
                        $image_size='sok-scattered-horizontal';
                    }
                ?>


                <div class="scattered-box col-item <?php echo esc_attr($add_class);?> clearfix">
                    <?php if($item['image_box']) { ?>
                        <div class="entry-image">
                            <a href="<?php echo esc_url($item['link_box']); ?>">
                                <?php echo wp_get_attachment_image($item['image_box'], $image_size); ?>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="entry-content clearfix">
                        <div class="border-mask"></div>
                        <div class="entry-content-inner">
                            <?php
                            $html = '';
                            if($item['title_box']){
                                $html .= '<h3 class="title-box">'. esc_html($item['title_box']) .'</h3>';
                            }
                            if(isset($item['content_box'])){
                                $html .= '<div  class="des-box">'. esc_html($item['content_box']) .'</div>';
                            }
                            if($item['btn_box']){
                                $html .= '<a class="btn-box" href="'.esc_html($item['link_box']).'">'.esc_html($item['btn_box']).'</a>';
                            }
                            echo $html;
                            ?>
                        </div>
                    </div>
                </div>

            <?php $na++; }
            ?>
    </div>
</div>