<?php

$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$img_size_f         = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_f'], 'thumb_size' => '570x700' ) );
$img_size_t         = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_t'], 'thumb_size' => '470x470' ) );

?>
<div class="block na-infoCategory box <?php echo esc_attr($css_class); ?> clearfix">
        <?php if ($atts['box_layouts'] && $atts['box_layouts']=='style-2'):
            $img_size_f         = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_f'], 'thumb_size' => '470x470' ) );
            $img_size_t         = wpb_getImageBySize( array( 'attach_id' => (int) $atts['image_t'], 'thumb_size' => '570x315' ) );
            ?>
            <div class="entry-image style-2 clearfix">
                <?php if ( $atts['des_f'] ) {?>
                    <span class="des_f">
                        <?php echo esc_html( $atts['des_f'] ); ?>
                    </span>
                <?php }?>
                <?php if($atts['image_f']) { ?>
                    <div class="image_f">
                        <?php echo $img_size_f['thumbnail'];?>
                    </div>
                <?php } ?>

            </div>
            <div class="entry-content style-2 clearfix">
                <div class="image-box clearfix">
                    <?php if($atts['image_t']) { ?>
                        <div class="image_t">
                            <?php echo $img_size_t['thumbnail'];?>
                        </div>
                    <?php } ?>
                    <?php if ( $atts['des_f'] ) {?>
                        <span class="des_t">
                            <?php echo esc_html( $atts['des_t'] ); ?>
                        </span>
                    <?php }?>
                </div>
                <div class="content-box clearfix">
                    <?php if ( $atts['title_box'] ) {?>
                        <h3 class="title-block title-box">
                            <?php echo esc_html( $atts['title_box'] ); ?>
                        </h3>
                    <?php }?>
                    <?php
                    if(isset($atts['content_box'])){?>
                        <div  class="des-box"><?php echo esc_html($atts['content_box']);?> </div>
                    <?php }

                    $link = trim( $atts['link'] );
                    $link = ( '||' === $link ) ? '' : $link;
                    $link = vc_build_link( $link );
                    if ( strlen( $link['url'] ) > 0 ) {?>
                        <a class="btn btn-link" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                    <?php }
                    ?>
                </div>

            </div>
        <?php else:?>
            <div class="entry-image clearfix">
                <?php if($atts['image_f']) { ?>
                        <div class="image_f">
                           <?php echo $img_size_f['thumbnail'];?>
                        </div>
                <?php } ?>
                <?php if ( $atts['des_f'] ) {?>
                    <span class="des_f">
                        <?php echo esc_html( $atts['des_f'] ); ?>
                    </span>
                <?php }?>
            </div>
            <div class="entry-content clearfix">

                <div class="content-box clearfix">
                    <?php if ( $atts['title_box'] ) {?>
                        <h3 class="title-block title-box">
                            <?php echo esc_html( $atts['title_box'] ); ?>
                        </h3>
                    <?php }?>
                    <?php
                    if(isset($atts['content_box'])){?>
                        <div  class="des-box"><?php echo esc_html($atts['content_box']);?> </div>
                    <?php }

                    $link = trim( $atts['link'] );
                    $link = ( '||' === $link ) ? '' : $link;
                    $link = vc_build_link( $link );
                    if ( strlen( $link['url'] ) > 0 ) {?>
                        <a class="btn btn-link" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?></a>
                    <?php }
                    ?>
                </div>
                <div class="image-box clearfix">
                    <?php if($atts['image_t']) { ?>
                        <div class="image_t">
                            <?php echo $img_size_t['thumbnail'];?>
                        </div>
                    <?php } ?>
                    <?php if ( $atts['des_f'] ) {?>
                        <span class="des_t">
                            <?php echo esc_html( $atts['des_t'] ); ?>
                        </span>
                    <?php }?>
                </div>
            </div>
        <?php endif;?>
</div>