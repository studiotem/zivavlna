<?php
$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );

    function generate_transient_key( $usernames_or_hashtags ) {

        $id = $usernames_or_hashtags;

        $transient_key = md5( 'sok_instagram_widget_' . $id );

        return $transient_key;

    }

    function get_follow_link( $usernames_or_hashtags ) {

        $usernames_hashtags_array = explode( ',', $usernames_or_hashtags );
        $number_of_username_hashtag = count( $usernames_hashtags_array );

        if ( $number_of_username_hashtag !== 1 ) return '';

        return  get_instagram_url( $usernames_or_hashtags );
    }

    function limit_images_number( $photos, $limit = 1 ) {
        if ( empty( $photos ) || is_wp_error( $photos ) ) {
            return array();
        }
        return array_slice( $photos, 0, $limit );
    }
    function get_instagram_url( $searched_term ) {

        $searched_term = trim( strtolower( $searched_term ) );

        switch ( substr( $searched_term, 0, 1 ) ) {
            case '#':
                return $url = 'https://instagram.com/explore/tags/' . str_replace( '#', '', $searched_term );
                break;

            default:
                return $url = 'https://instagram.com/' . str_replace( '@', '', $searched_term );
                break;
        }
    }

    function parse_instagram_images( $images ) {

        $pretty_images = array();

        foreach ( $images as $image ) {

            $pretty_images[] = array(
                'caption'    => isset( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'] ) ? $image['node']['edge_media_to_caption']['edges'][0]['node']['text'] : '',
                'link'       => trailingslashit( 'https://instagram.com/p/' . $image['node']['shortcode'] ),
                'time'     	 => $image['node']['taken_at_timestamp'],
                'comments'   => $image['node']['edge_media_to_comment']['count'],
                'likes'   	 => $image['node']['edge_liked_by']['count'],
                'thumbnail'   => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][0]['src'] ), //150
                'small'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][1]['src'] ), //240
                'medium'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][2]['src'] ), //320
                'large'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][3]['src'] ), //480
                'original'    => preg_replace( '/^https?\:/i', '', $image['node']['display_url'] ),
            );

        }

        return $pretty_images;
    }

    function get_instagram_data( $url )
    {

        $request = wp_remote_get($url);

        if (is_wp_error($request) || empty($request)) {
            return new WP_Error('invalid_response', esc_html__('Unable to communicate with Instagram.', 'sok'));
        }

        $body = wp_remote_retrieve_body($request);
        $shared = explode('window._sharedData = ', $body);
        $json = explode(';</script>', $shared[0]);
        $data = json_decode($json[0], true);

        if (empty($data)) {
            return new WP_Error('bad_json', esc_html__('Instagram has returned empty data. Please check your username/hashtag.','sok'));
        }

        if (isset($data['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'])) {
            $images = $data['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'];
        } elseif (isset($data['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'])) {
            $images = $data['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'];
        } else {
            return new WP_Error('bad_json_2', esc_html__('Instagram has returned invalid data.', 'sok'));
        }

        $images = parse_instagram_images($images);

        if (empty($images)) {
            return new WP_Error('no_images', esc_html__('Images not found. This may be a temporary problem. Please try again soon.', 'sok'));
        }

        return $images;
    }

    function get_photos( $usernames_or_hashtags ) {

        if ( empty( $usernames_or_hashtags ) ) {
            return false;
        }

        $transient_key = generate_transient_key( $usernames_or_hashtags );

        $cached = get_transient( $transient_key );

        if ( !empty( $cached ) ) {
            return $cached;
        }

        $usernames_or_hashtags = explode( ',', $usernames_or_hashtags );

        $images = array();

        foreach ( $usernames_or_hashtags as  $username_or_hashtag ) {

            $username_or_hashtag = trim( $username_or_hashtag );

            $url = get_instagram_url( $username_or_hashtag );
            $data = get_instagram_data( $url );

            if ( is_wp_error( $data )) {
                return $data;
            }

            $images[] = $data;
        }

        $images =  array_reduce( $images, 'array_merge', array() );

        usort( $images, function ( $a, $b ) {
            if ( $a['time'] == $b['time'] ) return 0;
            return ( $a['time'] < $b['time'] ) ? 1 : -1;
        } );

        set_transient( $transient_key, $images, DAY_IN_SECONDS );

        return $images;
    }
    $photos =get_photos( $atts['username'] );
    $photos =limit_images_number( $photos, $atts['photos_number'] );
    $follow_link =get_follow_link( $atts['username'] );
//    $size=$photo['original'];
//    $size=$photo['large'];
//    $size=$photo['medium'];
//    $size=$photo['thumbnail'];
//    $size=$photo['small'];


?>
<div class="nano-instagram block <?php echo esc_attr($css_class); ?> clearfix">
       <?php if ( $atts['title-block'] ) {?>
                <h3 class="block-title">
                    <?php echo htmlspecialchars_decode( $atts['title-block'] ); ?>
                </h3>
       <?php }?>
       <div class="block-content">
          <?php if ( !empty($photos) ): ?>
           <div class="instagram-row">
               <?php foreach ( $photos as $photo ): ?>
                   <div class="item-instagram col-<?php echo esc_attr($atts['columns']);?> space-<?php echo esc_attr($atts['photo_space']);?>">
                       <a href="<?php echo esc_attr( $photo['link'] ); ?>" title="<?php echo esc_attr( $photo['caption'] ); ?>" target="_blank" rel="nofollow">
                           <img src="<?php echo esc_attr( $photo['medium'] ); ?>" alt="<?php echo esc_attr( $photo['caption'] ); ?>">
                       </a>
                   </div>
               <?php endforeach; ?>
           </div>
           <?php endif;?>
       </div>

</div>