<?php

$n=1;
$na=1;
$add_class='';
$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items              = (array) vc_param_group_parse_atts($atts['items']);

$bg_css=$color_css=$color='';
if(isset($atts['bg_box']) && !empty($atts['bg_box'])){
    $color      = esc_attr($atts['bg_box']);
    $style_bg_css  = "background-color:".$color;
    $bg_css      = 'style='.  $style_bg_css .'';

    $style_color_css  = "color:".$color;
    $color_css      = 'style='.  $style_color_css .'';
}

?>
<div class="widget block-slider nano-showcase">

        <div class="widgetcontent clearfix" data-number="3" data-dotes="false" data-arrows="false" data-table="2" data-rtl="false" data-mobile="2" data-mobilemin="2">
            <div class="content-left" <?php echo esc_attr($bg_css);?>>
                    <div class="border-mask bottom"></div>
                    <div class="gallery-top">
                            <div class="swiper-wrapper">
                                <?php
                                    foreach ( $items as $item ) {
                                        $link = trim( $item['link'] );
                                        $link = ( '||' === $link ) ? '' : $link;
                                        $link = vc_build_link( $link );
                                     ?>

                                    <div class="swiper-slide col-item clearfix">
                                        <?php if($item['image_box']) { ?>
                                            <div class="image-box clearfix">

                                                <div class="entry-image slider-img" data-slide="<?php echo esc_attr($na);?>">
                                                    <a href="<?php echo esc_url($link['url']); ?>">
                                                        <?php
                                                        $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => array(454,630) ) );
                                                        echo $img['thumbnail'];
                                                        ?>
                                                    </a>
                                                </div>

                                                <?php if($item['title_box']){?>
                                                    <h3 class="title-box"><a class="btn-box" href="<?php echo esc_url($link['url']);?>"><?php echo esc_html($item['title_box']); ?></a></h3>
                                                <?php }?>
                                                <div class="content-box">
                                                    <?php if ( strlen( $link['url'] ) > 0 ) {?>
                                                        <a class="btn btn-next" href="<?php echo esc_url($link['url']);?>" title="<?php echo esc_attr($link['title']);?>" target="<?php echo esc_attr($link['target']);?>" rel="<?php echo esc_attr($link['rel']);?>"><?php echo esc_html($link['title']);?><i class="ion-ios-arrow-thin-right"></i></a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $na++; } ?>
                            </div>
                        <div class="border-mask center"></div>
                </div>
            </div>
            <div class="content-right">
                <div class="border-mask center-right"></div>
                <div class="gallery-thumbs">
                    <div class="swiper-wrapper">
                        <style>
                            .cat-menu h2:hover {
                                -webkit-text-fill-color: <?php echo esc_attr($color);?>;
                            }
                            .nano-showcase .cat-menu h2:hover::after {
                                -webkit-text-fill-color: <?php echo esc_attr($color);?>;
                            }
                            .nano-showcase .swiper-slide-thumb-active .cat-menu h2::after{
                                -webkit-text-fill-color:  <?php echo esc_attr($color);?>;
                            }
                        </style>
                        <?php
                        foreach ( $items as $item ) {?>
                            <div class="swiper-slide clearfix">
                                <?php  if($item['title_cat']){?>
                                    <div class="cat-menu" data-slide="<?php echo esc_attr($n);?>">

                                        <h2 data-title="<?php echo  esc_html($item['title_cat']) ?>" style="-webkit-text-stroke: 1px <?php echo esc_attr($color);?>; ">
                                            <?php echo  esc_html($item['title_cat']) ?>
                                        </h2>
                                    </div>
                                    <?php  if(isset($item['sub_box'])){?>
                                        <span class="sub_box" <?php echo esc_attr($color_css);?>><?php echo  esc_html($item['sub_box']) ?></span>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <?php   $n++; } ?>
                    </div>
                </div>
            </div>
        </div>

</div>