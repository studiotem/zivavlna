<?php

$na=1;
$image_size=array(434,336);
$add_class='small';
$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$items              = (array) vc_param_group_parse_atts($atts['items']);
?>
<div class="block-masonry-category na-masonry-category">
    <div class="block-content affect-isotope items-masonry clearfix">
            <div class="grid-sizer"></div>
            <?php

            foreach ( $items as $item ) {?>
                <?php
                    if ( $na==2 ){
                        $image_size=array(580,630);
                        $add_class='lager';
                    }
                    elseif ( $na==3 || $na==4 ){
                        $image_size=array(285,630);
                        $add_class='small';
                    }
                    elseif ( $na==5 ){
                        $image_size=array(875,336);
                        $add_class='medium';
                    }
                    else{
                        $image_size=array(285,336);
                        $add_class='small';
                    }
                    $img = wpb_getImageBySize( array( 'attach_id' => (int) $item['image_box'], 'thumb_size' => $image_size ) );

                ?>


                <div class="<?php echo $add_class;?> masonry-box col-item clearfix">
                    <?php if($item['image_box']) { ?>
                        <div class="entry-image">
                            <a href="<?php echo esc_url($item['link_box']); ?>">
                                <?php echo $img['thumbnail']; ?>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="box-content clearfix">
                        <div class="border-mask"></div>
                        <div class="entry-content-inner">
                            <?php
                            $html = '';
                            if($item['title_box']){
                                $html .= '<h3 class="title-box"> <a href="'.$item['link_box'].'">'. esc_html($item['title_box']) .'</a></h3>';
                            }
                            if(isset($item['content_box'])){
                                $html .= '<div  class="des-box">'. esc_html($item['content_box']) .'</div>';
                            }
                            echo $html;
                            ?>
                        </div>
                    </div>
                </div>

            <?php $na++; }
            ?>
    </div>
</div>