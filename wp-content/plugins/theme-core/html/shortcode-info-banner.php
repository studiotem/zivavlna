<?php

$css_class          = vc_shortcode_custom_css_class( $atts['css'], ' ' );
$animation_classes  = getCSSAnimation( $atts['animation']);

?>
<div class="nano-infobanner <?php echo esc_attr($css_class); ?> <?php echo esc_attr($atts['position_text']);?>">
    <div class="image-hover-inner">
        <?php if($atts['image']) { ?>
            <a href="<?php echo esc_url($atts['link']); ?>">
                <?php echo wp_get_attachment_image($atts['image'], 'full'); ?>
            </a>
        <?php } ?>
        <?php if($atts['title'] || $atts['sub_title'] || $atts['text_link']) { ?>
            <div class="image-content-hover <?php echo esc_attr($animation_classes);?>">
                <div class="border-mask"></div>
                <div class="content">
                    <?php
                        $html = '';
                        if($atts['title']){
                            $html .= '<h3 class="title-banner">'. esc_html($atts['title']) .'</h3>';
                        }
                        if($atts['text_link']){
                            $html .= '<a class="btn-banner" href="'.esc_html($atts['link']).'">'.esc_html($atts['text_link']).'</a>';
                        }
                        echo $html;
                    ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
