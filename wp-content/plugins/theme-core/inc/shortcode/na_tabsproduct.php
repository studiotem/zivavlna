<?php

/* ================================================================================== */
/*      Products Shortcode
/* ================================================================================== */

if (!function_exists('na_shortcode_tabsproduct')) {
    function na_shortcode_tabsproduct($atts, $content) {
        $atts = shortcode_atts(array(
            'box_title'             => '',
            'post_type' 			=> 'product',
            'column' 				=> '4',
            'number'		        => 8,
            'meta_query'            => array(),
            'tax_query'             => array(),
            'list_type'             => 'latest,featured,toprate',
            'products_types'		=> 'grid',
            'ignore_sticky_posts'	=> 1,
            'show'					=> '',
            'style'                 =>'default',
            'category'              =>'',
            'padding_bottom_module' => '0px',
        ), $atts);

        ob_start();
        nano_template_part('shortcode', 'tabs-product' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_tabsproduct', 'na_shortcode_tabsproduct');

add_action('vc_before_init', 'na_tabsproduct_integrate_vc');

if (!function_exists('na_tabsproduct_integrate_vc')) {
    function na_tabsproduct_integrate_vc()
    {
        $show_tab = array(
            array('latest', esc_html__('Latest Products', 'nano')),
            array('featured', esc_html__('Featured Products', 'nano' )),
            array('best-selling', esc_html__('Best Selling Products', 'nano' )),
            array('toprate', esc_html__('TopRated Products', 'nano' )),
            array('onsale', esc_html__('Special Products', 'nano' ))
        );
        vc_map(
            array(
                'name' => esc_html__('NA: Tabs Products', 'nano'),
                'base' => 'na_tabsproduct',
                'icon' => 'icon-tabsProduct',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show multiple tabs products .', 'nano'),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => '',
                        'param_name' => 'box_title',
                    ),
                    array(
                        'type' => 'sorted_list',
                        'heading' => esc_html__('Show Tab', 'nano'),
                        'param_name' => 'list_type',
                        'description' => esc_html__('Control teasers look. Enable blocks and place them in desired order.', 'nano'),
                        'value' => 'latest,featured,toprate',
                        'options' => $show_tab
                    ),
                    array(
                        'type' => 'nano_product_categories',
                        'heading' => esc_html__( 'Category','nano' ),
                        'param_name' => 'category',
                        'description' => esc_html__( 'Product category list','nano'),
                    ),
                    array(
                        'type' => 'nano_image_radio',
                        'heading' => esc_html__('Layout product', 'nano'),
                        'value' => array(
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-tran.jpg', 'nano')        => 'trans',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-grid.jpg', 'nano')        => 'grid',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-gallery.jpg', 'nano')     => 'gallery',
                        ),
                        'width' => '100px',
                        'height' => '70px',
                        'param_name' => 'products_types',
                        'std' => 'grid',
                        'description' => esc_html__('Select layout type', 'nano'),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Products per row', 'nano'),
                        'value' => array(
                            esc_html__('2', 'nano') => '2',
                            esc_html__('3', 'nano') => '3',
                            esc_html__('4', 'nano') => '4',
                            esc_html__('5', 'nano') => '5',
                            esc_html__('6', 'nano') => '6'
                        ),
                        'std' => '4',
                        'param_name' => 'column',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Number of Products show on the Tabs', 'nano'),
                        'value' => 8,
                        'param_name' => 'number',
                    ),
                )
            )
        );
    }
}