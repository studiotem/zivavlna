<?php

/* ================================================================================== */
/*      Products Shortcode
/* ================================================================================== */
if (!function_exists('na_shortcode_asymmetric_products')) {
    function na_shortcode_asymmetric_products($atts, $content) {
        $atts = shortcode_atts(array(
            'title-block'           => 'Fashion Collection 2018',
            'content-block'         => '',
            'link'                  => '',
            'post_type' 			=> 'product',
            'posts_per_page'        =>  2,
            'meta_query'            => '',
            'tax_query'             => array(),
            'list_type'             => 'latest',
            'ignore_sticky_posts'	=> 1,
            'show'					=> '',
            'style'                 =>'default',
            'category'              =>'',
            'box_layouts'           =>'style-1',

        ), $atts);

        ob_start();
        nano_template_part('shortcode', 'asymmetric-products' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_asymmetric_products', 'na_shortcode_asymmetric_products');

add_action('vc_before_init', 'na_asymmetric_products_integrate_vc');

if (!function_exists('na_asymmetric_products_integrate_vc')) {
    function na_asymmetric_products_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Asymmetric Products', 'nano'),
                'base' => 'na_asymmetric_products',
                'icon' => 'vc_icon-vc-masonry-grid',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Block Asymmetric Products .', 'nano'),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => 'Fashion Collection 2018',
                        'param_name' => 'title-block',
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__('Description','nano'),
                        "param_name" => "content-block",
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => esc_html__( 'URL (Link)', 'js_composer' ),
                        'param_name' => 'link',
                        'description' => esc_html__( 'Add link to button (Important: adding link automatically adds button).', 'nano' ),
                    ),

                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Show product type', 'nano'),
                        'param_name' => 'list_type',
                        'value' => array(
                            esc_html__('Latest Products', 'nano') => 'latest',
                            esc_html__('Featured Products', 'nano') => 'featured',
                            esc_html__('Best Selling Products', 'nano') => 'best-selling',
                            esc_html__('TopRated Products', 'nano') => 'toprate',
                            esc_html__('Special Products', 'nano') => 'onsale'
                        ),
                        'std' => 'latest',
                        'group' => __( 'Product options', 'nano' ),
                    ),
                    array(
                        'type' => 'nano_product_categories',
                        'heading' => esc_html__( 'Category','nano' ),
                        'param_name' => 'category',
                        'description' => esc_html__( 'Product category list','nano'),
                        'group' => __( 'Product options', 'nano' ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Layout Styles', 'nano'),
                        'value' => array(
                            esc_html__('Style 1', 'nano') => 'style-1',
                            esc_html__('Style 2', 'nano') => 'style-2',
                        ),
                        'std' => 'style-1',
                        'param_name' => 'box_layouts',
                    ),

                )
            )
        );
    }
}