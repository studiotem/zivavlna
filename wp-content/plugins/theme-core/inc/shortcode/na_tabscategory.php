<?php

/* ================================================================================== */
/*      Products Shortcode
/* ================================================================================== */

if (!function_exists('na_shortcode_tabs_category')) {
    function na_shortcode_tabs_category($atts, $content) {
        $outputs='';
        $atts = shortcode_atts(array(
            'box_title'             => '',
            'post_type' 			=> 'product',
            'column' 				=> '4',
            'number'		        => 8,
            'meta_query'            => '',
            'tax_query'             => array(),
            'products_types'		=> 'grid',
            'box_layouts'		    => 'grid',
            'title_align'			=> 'center',
            'paged' 				=> -1,
            'ignore_sticky_posts'	=> 1,
            'show'					=> '',
            'style'                 =>'default',
            'category'              =>'',
            'order_by'              =>'id',
            'link'                  =>'#',
            'btn_name'              =>'View All Products',
        ), $atts);

        //IF category empty
        if(isset($atts['category']) && empty($atts['category'])){
            $args = array(
                'post_type'     => 'product',
                'post_status'   => 'publish',
                'tax_query'     => array(
                    array(
                        'taxonomy'  => 'product_cat'
                    )
                )
            );
            $query_shop = new WP_Query($args);
            $atts['tax_query']  = $query_shop;
        }


        ob_start();
        nano_template_part('shortcode', 'tabs-category' , array('atts' => $atts));?>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_shortcode('na_tabs_category', 'na_shortcode_tabs_category');

add_action('vc_before_init', 'na_tabs_category_integrate_vc');

if (!function_exists('na_tabs_category_integrate_vc')) {
    function na_tabs_category_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Tabs Category Products', 'nano'),
                'base' => 'na_tabs_category',
                'icon' => 'icon-tabsCategory',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show multiple tabs Category Product.', 'nano'),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => '',
                        'param_name' => 'box_title',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Title align', 'nano'),
                        'value' => array(
                            esc_html__('Center', 'nano') => 'center',
                            esc_html__('Left', 'nano') => 'left',
                            esc_html__('Right', 'nano') => 'right',
                            esc_html__('Fixed Left', 'nano') => 'left-fixed',
                            esc_html__('Hidden', 'nano') => 'hidden'
                        ),
                        'param_name' => 'title_align',
                        'group' => esc_html__( 'Layouts', 'nano' ),
                    ),

                    array(
                        'type' => 'nano_product_categories',
                        'heading' => esc_html__( 'Category','nano' ),
                        'param_name' => 'category',
                        'description' => esc_html__( 'Product category list','nano'),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Block layout', 'nano'),
                        'value' => array(
                            esc_html__('Grid', 'nano') => 'grid',
                            esc_html__('Scattered', 'nano') => 'scattered',
//                            esc_html__('Masonry', 'nano') => 'masonry',
                        ),
                        'std' => 'grid',
                        'param_name' => 'box_layouts',
                        'group' => esc_html__( 'Layouts', 'nano' ),
                    ),
                    array(
                        'type' => 'nano_image_radio',
                        'heading' => esc_html__('Layout product', 'nano'),
                        'value' => array(
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-tran.jpg', 'nano')        => 'trans',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-grid.jpg', 'nano')        => 'grid',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/box-gallery.jpg', 'nano')     => 'gallery',
                        ),
                        'width' => '100px',
                        'height' => '70px',
                        'param_name' => 'products_types',
                        'std' => 'grid',
                        'description' => esc_html__('Select layout type', 'nano'),
                        'group' => esc_html__( 'Layouts', 'nano' ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Products per row', 'nano'),
                        'value' => array(
                            esc_html__('2', 'nano') => '2',
                            esc_html__('3', 'nano') => '3',
                            esc_html__('4', 'nano') => '4',
                            esc_html__('5', 'nano') => '5',
                            esc_html__('6', 'nano') => '6'
                        ),
                        'std' => '4',
                        'param_name' => 'column',
                        'group' => esc_html__( 'Layouts', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Number of Products show on the Tabs', 'nano'),
                        'value' => 8,
                        'param_name' => 'number',
                        'group' => esc_html__( 'Layouts', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Link go to Shop', 'nano'),
                        'value' => '#',
                        'param_name' => 'link',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Text Button', 'nano'),
                        'value' => 'View All Products',
                        'param_name' => 'btn_name',
                    ),
                )
            )
        );
    }
}