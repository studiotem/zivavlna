<?php
if (!function_exists('na_shortcode_slider_category')) {
    function na_shortcode_slider_category($atts,$output)
    {
        $atts = shortcode_atts(

            array(
                'css'           => '',
                'items'         => '',
            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'slider-category' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_slider_category', 'na_shortcode_slider_category');

add_action('vc_before_init', 'na_slider_category_integrate_vc');

if (!function_exists('na_slider_category_integrate_vc')) {
    function na_slider_category_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Slider Category', 'nano'),
                'base' => 'na_slider_category',
                'icon' => 'icon-wpb-slideshow',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Horizontal Slider with the categories', 'nano'),
                'params' => array(
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__(' Block Slider Settings', 'nano' ),
                        'param_name' => 'items',
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("upload an image.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'	=> esc_html__('Image', 'nano' ),

                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Title','nano'),
                                "param_name" => "title_box",
                                'admin_label' => true,
                            ),
                            array(
                                'type' => 'vc_link',
                                'heading' => esc_html__( 'URL (Link)', 'js_composer' ),
                                'param_name' => 'link',
                                'description' => esc_html__( 'Add link to button (Important: adding link automatically adds button).', 'nano' ),
                            ),

                        ),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}