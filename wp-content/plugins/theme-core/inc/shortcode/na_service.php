<?php
if (!function_exists('na_shortcode_service')) {
    function na_shortcode_service($atts,$output)
    {
        $atts = shortcode_atts(

            array(
                'css' => '',
                'items' => '',
            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'service' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_service', 'na_shortcode_service');

add_action('vc_before_init', 'na_service_integrate_vc');

if (!function_exists('na_service_integrate_vc')) {
    function na_service_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Service', 'nano'),
                'base' => 'na_service',
                'icon' => 'icon-wpb-vc_pie',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Box Service', 'nano'),
                'params' => array(
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__('Service Settings', 'nano' ),
                        'param_name' => 'items',
                        'params' => array(
                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("upload an icon.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'	=> esc_html__('icon', 'nano' ),

                            ),
                            array(
                                "type" => "textarea",
                                "class" => "",
                                "heading" => esc_html__('Title','nano'),
                                "param_name" => "title_box",
                                'admin_label' => true,
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__( 'URL (Link)', 'nano' ),
                                'param_name' => 'link_box',
                            ),

                        ),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}