<?php
if (!function_exists('na_shortcode_menu')) {
    function na_shortcode_menu($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'title' => '',
                'nav_menu' => '',

            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'menu' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_menu', 'na_shortcode_menu');

add_action('vc_before_init', 'na_menu_integrate_vc');

if (!function_exists('na_menu_integrate_vc')) {
    function na_menu_integrate_vc()
    {

        $custom_menus = array();
        if ( 'vc_edit_form' === vc_post_param( 'action' ) && vc_verify_admin_nonce() ) {
            $menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
            if ( is_array( $menus ) && ! empty( $menus ) ) {
                foreach ( $menus as $single_menu ) {
                    if ( is_object( $single_menu ) && isset( $single_menu->name, $single_menu->term_id ) ) {
                        $custom_menus[ $single_menu->name ] = $single_menu->term_id;
                    }
                }
            }
        }
        vc_map(
            array(
                'name' => esc_html__('NA: Menu Block', 'nano'),
                'base' => 'na_menu',
                'icon' => 'icon-menu',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Menu Custom', 'nano'),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Widget title', 'nano' ),
                        'param_name' => 'title',
                        'description' => __( 'What text use as a widget title. Leave blank to use default widget title.', 'nano' ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Menu', 'nano' ),
                        'param_name' => 'nav_menu',
                        'value' => $custom_menus,
                        'description' => empty( $custom_menus ) ? __( 'Custom menus not found. Please visit <b>Appearance > Menus</b> page to create new menu.', 'nano' ) : __( 'Select menu to display.', 'nano' ),
                        'admin_label' => true,
                        'save_always' => true,
                    ),
                )
            )
        );
    }
}