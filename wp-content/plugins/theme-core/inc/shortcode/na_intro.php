<?php
if (!function_exists('na_shortcode_intro')) {
    function na_shortcode_intro($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'image' => '',
                'title' => 'Stunning || Fashion',
                'content_box' => '',
                'css' => '',
                'link' => '',

            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'intro' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_intro', 'na_shortcode_intro');

add_action('vc_before_init', 'na_intro_integrate_vc');

if (!function_exists('na_intro_integrate_vc')) {
    function na_intro_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Intro Block', 'nano'),
                'base' => 'na_intro',
                'icon' => 'icon-scatteredCategory',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Image Banner', 'nano'),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__('Image', 'nano'),
                        'value' => '',
                        'param_name' => 'image',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => esc_html('Text Block'),
                        'param_name' => 'title',
                    ),
                    array(
                        "type" => "textarea",
                        "class" => "",
                        "heading" => esc_html__('Description','nano'),
                        "param_name" => "content_box",
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => esc_html__( 'URL (Link)', 'js_composer' ),
                        'param_name' => 'link',
                        'description' => esc_html__( 'Add link to button (Important: adding link automatically adds button).', 'nano' ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Extra class name', 'nano' ),
                        'param_name'  => 'el_class',
                        'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'nano' )
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}