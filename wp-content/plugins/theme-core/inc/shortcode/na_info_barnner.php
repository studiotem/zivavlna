<?php

if (!function_exists('na_shortcode_info_banner')) {
    function na_shortcode_info_banner($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'image' => '',
                'title' => 'Stunning || Fashion',
                'link' => 'http://',
                'text_link' => 'View More',
                'position_text' => 'center-center',
                'animation' => '',
                'css' => '',

            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'info-banner' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_info_banner', 'na_shortcode_info_banner');

add_action('vc_before_init', 'na_info_banner_integrate_vc');

if (!function_exists('na_info_banner_integrate_vc')) {
    function na_info_banner_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Info Banner', 'nano'),
                'base' => 'na_info_banner',
                'icon' => 'icon-infoBanner',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Image Banner', 'nano'),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__('Image', 'nano'),
                        'value' => '',
                        'param_name' => 'image',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => esc_html('Stunning || Fashion'),
                        'param_name' => 'title',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Link', 'nano'),
                        'value' => 'http://',
                        'param_name' => 'link',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Text Link', 'nano'),
                        'value' => esc_html('View More'),
                        'param_name' => 'text_link',
                    ),
                    array(
                        'type' => 'animation_style',
                        'heading' => __( 'Animation Text Block', 'nano' ),
                        'param_name' => 'animation',
                        'description' => __( 'Choose your animation style', 'nano' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Position Group',
                    ),
                    array(
                        'type' => 'nano_image_radio',
                        'heading' => esc_html__('Position Text ', 'nano'),
                        'value' => array(
                            esc_html__(NANO_PLUGIN_URL.'assets/images/banner/left-under.png', 'nano')        => 'left-under',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/banner/left-between.png', 'nano')        => 'left-between',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/banner/left-top.png', 'nano')        => 'left-top',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/banner/center-top.png', 'nano')        => 'center-top',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/banner/center-center.png', 'nano')        => 'center-center',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/banner/center-under.png', 'nano')        => 'center-under',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/banner/right-under.png', 'nano')        => 'right-under',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/banner/right-between.png', 'nano')        => 'right-center',
                            esc_html__(NANO_PLUGIN_URL.'assets/images/banner/right-top.png', 'nano')        => 'right-top',
                        ),
                        'width' => '100px',
                        'height' => '70px',
                        'param_name' => 'position_text',
                        'std' => 'center-center',
                        'description' => esc_html__('Select Position for Text', 'nano'),
                        'group' => __( 'Position Group', 'nano' ),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),

                )
            )
        );
    }
}