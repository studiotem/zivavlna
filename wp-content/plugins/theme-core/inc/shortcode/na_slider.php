<?php
if (!function_exists('na_shortcode_slider')) {
    function na_shortcode_slider($atts,$output)
    {
        $atts = shortcode_atts(

            array(
                'css'           => '',
                'items'         => '',
                'bg_box'        => '',
                'color_box'     => '',

            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'slider' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_slider', 'na_shortcode_slider');

add_action('vc_before_init', 'na_slider_integrate_vc');

if (!function_exists('na_slider_integrate_vc')) {
    function na_slider_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Slider Showcase', 'nano'),
                'base' => 'na_slider',
                'icon' => 'icon-wpb-vc-round-chart',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Slider Showcase with the scroll bar', 'nano'),
                'params' => array(
                    array(
                        'type' => 'param_group',
                        'heading' => esc_html__(' Block Slider Settings', 'nano' ),
                        'param_name' => 'items',
                        'params' => array(
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Title Category','nano'),
                                "param_name" => "title_cat",
                                'admin_label' => true,
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Sub Title','nano'),
                                "param_name" => "sub_box",
                                'admin_label' => true,
                            ),

                            array(
                                "type" => "attach_image",
                                "description" => esc_html__("upload an image.", 'nano'),
                                "param_name" => "image_box",
                                "value" => '',
                                'heading'	=> esc_html__('Image', 'nano' ),
                            ),
                            array(
                                "type" => "textfield",
                                "class" => "",
                                "heading" => esc_html__('Title Image','nano'),
                                "param_name" => "title_box",
                                'admin_label' => true,
                            ),

                            array(
                                'type' => 'vc_link',
                                'heading' => esc_html__( 'URL (Link)', 'js_composer' ),
                                'param_name' => 'link',
                                'description' => esc_html__( 'Add link to button (Important: adding link automatically adds button).', 'nano' ),
                            ),

                        ),
                    ),

                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__( 'Background color', 'nano' ),
                        'param_name' => 'bg_box',
                        'description' => esc_html__( 'Select custom background color.', 'nano' ),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__( 'Text color', 'nano' ),
                        'param_name' => 'color_box',
                        'description' => esc_html__( 'Select custom Text color.', 'nano' ),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}