<?php
if (!function_exists('na_shortcode_instagram')) {
    function na_shortcode_instagram($atts,$output)
    {
        $atts = shortcode_atts(
            array(
                'title-block'           => 'Instagram',
                'username'              => '@nano_sok',
                'photos_number'         => '8',
                'columns'               => '4',
                'photo_space'           => '5px',
                'link_text'             => 'Follow @ Nano Sok',
                'css'                   => '',

            ), $atts);

        ob_start();
            nano_template_part('shortcode', 'instagram' , array('atts' => $atts));?>
            <?php
            $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}

add_shortcode('na_instagram', 'na_shortcode_instagram');

add_action('vc_before_init', 'na_instagram_integrate_vc');

if (!function_exists('na_instagram_integrate_vc')) {
    function na_instagram_integrate_vc()
    {
        vc_map(
            array(
                'name' => esc_html__('NA: Instagram', 'nano'),
                'base' => 'na_instagram',
                'icon' => 'icon-wpb-ui-custom_heading',
                'category' => esc_html__('NA', 'nano'),
                'description' => esc_html__('Show Instagram Block ', 'nano'),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__('Title', 'nano'),
                        'value' => esc_html('Instagram'),
                        'param_name' => 'title-block',
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__('Instagram Username','nano'),
                        'description' => esc_html__( 'Multiple usernames and hastags are alowed.Example 1: @sok_nano.', 'nano' ),
                        "param_name" => "username",
                        'value' => esc_html('@nano_sok'),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Number of photos:', 'js_composer' ),
                        'param_name' => 'photos_number',
                        'description' => esc_html__( 'Add link to button (Important: adding link automatically adds button).', 'nano' ),
                        'value' => esc_html('8'),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Columns:', 'js_composer' ),
                        'param_name' => 'columns',
                        'value' => esc_html('4'),
                        'description' => esc_html__( 'Choose in how many columns you would like to display your photos.', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Photo spacing:', 'js_composer' ),
                        'param_name' => 'photo_space',
                        'value' => esc_html('5px'),
                        'description' => esc_html__( 'Specify a spacing between your photos.', 'nano' ),
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( '"Follow" link text:', 'js_composer' ),
                        'param_name' => 'link_text',
                        'value' => esc_html('Follow @ Nano Sok'),
                        'description' => esc_html__( 'Specify a text for your "follow" link, or leave empty if you do not want to display the "follow" link.', 'nano' ),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'Css', 'nano' ),
                        'param_name' => 'css',
                        'group' => __( 'Design options', 'nano' ),
                    ),
                )
            )
        );
    }
}