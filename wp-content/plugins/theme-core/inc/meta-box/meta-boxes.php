<?php
if (!class_exists('NA_Meta_Boxes')) {
    class NA_Meta_Boxes
    {
        public $meta_boxes;

        public function __construct()
        {
            $this->add_meta_box_options();
            add_action('admin_init', array($this, 'register'));
        }

        public static function &getInstance()
        {
            static $instance;
            if (!isset($instance)) {
                $instance = new NA_Meta_Boxes();
            }
            return $instance;
        }

        public function add_meta_box_options()
        {
            $meta_boxes = array();
            $custom_menus = array();
            $menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
            if ( is_array( $menus ) && ! empty( $menus ) ) {
                $custom_menus[''] = esc_html__('None','sok');
                foreach ( $menus as $single_menu ) {
                    if ( is_object( $single_menu ) && isset( $single_menu->name, $single_menu->term_id ) ) {
                        $custom_menus[ $single_menu->term_id ] = $single_menu->name;
                    }
                }
            }
            /* Page Options */
            $meta_boxes[] = array(
                'id'         => 'page_option',
                'title'      => __( 'Page Options', 'sok' ),
                'pages'      => array( 'page' ), // Post type
                'context'    => 'side',
                'priority'   => 'high',
                'show_names' => true, // Show field names on the left
                'fields'     => array(

                    array(
                        'name'       => esc_html__( 'Show Title', 'sok' ),
                        'id'         => 'sok_show_title',
                        'type'       => 'switch',
                        'style'      => 'rounded',
                        'on_label'   => 'Yes',
                        'std'        => 1,
                    ),
                    array(
                        'name'  => __('Image Breadcrumb', 'sok'),
                        'id'    => "sok_img_breadcrumb",
                        'type'  => 'image_advanced',
                        'max_file_uploads' => 1,
                    ),

                    array(
                        'name'       => __( 'Header Style', 'sok' ),
                        'id'         => 'layout_header',
                        'type'       => 'select',
                        'options'    => array(
                            'global'                    => esc_html__( 'Use Global', 'nano' ),
                            'simple'                    => esc_html__('Header Default', 'nano'),
                            'full'                      => esc_html__('Header Full Width', 'nano'),
                            'trans'                     => esc_html__('Header Transparent', 'nano'),
                            'center'                    => esc_html__('Header Center', 'nano'),
                        ),
                        'std'  => 'global',
                    ),
                    array(
                        'name'       => __( 'Menu Shop', 'sok' ),
                        'id'         => 'sok_shop_menu',
                        'type'       => 'select',
                        'options'    => $custom_menus,
                        'std'  => '',
                    ),
                    array(
                        'name'       => __( 'Footer Style', 'sok' ),
                        'id'         => 'layout_footer',
                        'type'       => 'select',
                        'options'    => array(
                            'global'                   => esc_html__('Use Global', 'nano'),
                            'footer-1'                   => esc_html__('Footer 1', 'nano'),
                            'footer-2'                   => esc_html__('Footer 2', 'nano'),
                            'footer-hidden'              => esc_html__('Hidden Footer', 'nano'),
                        ),
                        'std'  => 'footer-1',
                    ),
                    array(
                        'name'       => __( 'Padding Content', 'sok' ),
                        'id'         => 'padding_content',
                        'type'       => 'select',
                        'options'    => array(
                            'global'                            => esc_html__('Default', 'nano'),
                            'no-padding-top'                    => esc_html__('No Padding Top', 'nano'),
                            'no-padding-bottom'                 => esc_html__('No Padding Bottom', 'nano'),
                            'no-padding-content'                => esc_html__('No Padding', 'nano'),
                        ),
                        'std'  => 'footer-1',
                    ),
                )

            );
            /* Layout Box */
            /* Banner Meta Box */
            $meta_boxes[] = array(
                'id' => 'banner_meta_box',
                'title' => __('Banner Options', 'sok'),
                'pages' => array('banner'),
                'context' => 'normal',
                'fields' => array(
                    array(
                        'name' => __('Banner Url', 'sok'),
                        'desc' => __('When click into banner, it will be redirect to this url', 'sok'),
                        'id' => "na_banner_url",
                        'type' => 'text'
                    ),
                    array(
                        'name' => __('Banner class', 'sok'),
                        'desc' => __('', 'sok'),
                        'id' => "na_banner_class",
                        'type' => 'text'
                    ),
                )
            );
            /* Member Meta Box */
            $meta_boxes[] = array(
                'id' => 'member_meta_box',
                'title' => __('Member Options', 'sok'),
                'pages' => array('member'),
                'context' => 'normal',
                'fields' => array(
                    array(
                        'name' => __('Member Image', 'sok'),
                        'desc' => __('', 'sok'),
                        'id' => "na_member_image",
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1
                    ),
                    array(
                        'name' => __('Member Position', 'sok'),
                        'desc' => __('', 'sok'),
                        'id' => "na_member_position",
                        'type' => 'text',
                        'std' => '#'
                    ),
                    array(
                        'name' => __('Link Facebook', 'sok'),
                        'desc' => __('', 'sok'),
                        'id' => "na_member_fb",
                        'type' => 'text',
                        'std' => '#'
                    ),
                    array(
                        'name' => __('Link Twitter', 'sok'),
                        'desc' => __('', 'sok'),
                        'id' => "na_member_tw",
                        'type' => 'text',
                        'std' => '#'
                    ),
                    array(
                        'name' => __('Link Instagram', 'sok'),
                        'desc' => __('', 'sok'),
                        'id' => "na_member_isg",
                        'type' => 'text',
                        'std' => '#'
                    ),
                    array(
                        'name' => __('Link Goolge +', 'sok'),
                        'desc' => __('', 'sok'),
                        'id' => "na_member_gl",
                        'type' => 'text',
                        'std' => '#'
                    ),
                )
            );

            /* Testimonial Meta Box */
            $meta_boxes[] = array(
                'id' => 'testimonial_meta_box',
                'title' => __('Testimonial Options', 'sok'),
                'pages' => array('testimonial'),
                'context' => 'normal',
                'fields' => array(
                    array(
                        'name' => __('Image User', 'sok'),
                        'id' => "na_testimonial_image",
                        'type' => 'image_advanced'
                    ),
                    array(
                        'name' => __('Position', 'sok'),
                        'desc' => __('', 'sok'),
                        'id' => "na_testimonial_position",
                        'type' => 'text'
                    ),
                )
            );

            /* Product Meta Box */
            $meta_boxes[] = array(
                'id' => 'image_product_meta_box',
                'title' => __('Extra Attribute', 'sok'),
                'pages' => array( 'product' ),
                'context' => 'normal',
                'priority'   => 'low',
                'fields' => array(

                    // Image Attribute
                    array(
                        'name'  => __('Image For Attribute', 'sok'),
                        'desc'  => __('Upload a photo that for this product , default size: height=980px,width:246px', 'sok'),
                        'id'    => "image_att_product",
                        'type'  => 'image_advanced',
                        'max_file_uploads' => 1
                    ),
                    array(
                        'name' => __('Attribute Name', 'sok'),
                        'desc' => __('', 'sok'),
                        'id' => "name_att_product",
                        'type' => 'text',
                        'std' => 'Size Guide'
                    ),
                )
            );

            $this->meta_boxes = $meta_boxes;
        }

        public function register()
        {
            if (class_exists('RW_Meta_Box')) {
                foreach ($this->meta_boxes as $meta_box) {
                    new RW_Meta_Box($meta_box);
                }
            }
        }
    }
}

NA_Meta_Boxes::getInstance();
