<?php
/**
 * the file for using in Sok theme
 */
add_filter( 'soo_demo_packages', function() {
    $settings=array(
        array(
            'name'       => 'Sok Fashion',
            'preview'    => NANO_PLUGIN_URL.'inc/importer/data/main-home.jpg',
            'content'    => NANO_PLUGIN_URL.'inc/importer/data/demo-content.xml',
            'customizer' => NANO_PLUGIN_URL.'inc/importer/data/customizer.dat',
            'widgets'    => NANO_PLUGIN_URL.'inc/importer/data/widgets.wie',
            'sliders'    => NANO_PLUGIN_URL.'inc/importer/data/sliders.zip',
            'pages'      => array(
                'front_page' => 'Home Main',
                'blog'       => 'Blog',
                'shop'       => 'Sok Fashion',
                'cart'       => 'Cart',
                'checkout'   => 'Checkout',
                'my_account' => 'My Account',
            ),
            'menus'      => array(
                'primary_navigation'    => 'primary-navigation',
                'shop_navigation'	    => '',
            ),
            'options' => array(
                'shop_catalog_image_size' => array(
                    'width' => 600,
                    'height' => 700,
                    'crop' => '6:7',
                ),
                'shop_single_image_size' => array(
                    'width' => 1200,
                    'height' => 1678,
                    'crop' => '1200:1678',
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 180,
                    'height' => 210,
                    'crop' => '180:210',
                ),
            ),
        ),
    );
    return $settings;
} );
?>