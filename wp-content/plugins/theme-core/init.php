<?php
/**
 * Plugin Name: Nano Core Plugin
 * Plugin URI: http://www.nanoliberty.co
 * Description: This is not just a plugin, it is a package with many tools a website needed.
 * Author: Nanoliberty
 * Version: 1.0.1
 * Author URI: http://www.nanoliberty.co
 * Text Domain: nano
*/

require_once('core-config.php');
require_once('inc/helpers/vc.php');

/*  Include Meta Box ================================================================================================ */
require_once( 'inc/meta-box/meta-box-master/meta-box.php');
require_once( 'inc/meta-box/meta-boxes.php');

/*  Include Post Format ============================================================================================= */
require_once( 'inc/vafpress-post-formats/vp-post-formats-ui.php');

/*  Importer ======================================================================================================== */
require_once( 'inc/importer/sample.php');
require_once( 'inc/importer/wp-importer.php');

//Shortcode ======================================================================================================== */
require_once( 'inc/shortcode/na_blogs_block.php');
require_once( 'inc/shortcode/na_products.php');
require_once( 'inc/shortcode/na_info_barnner.php');
require_once( 'inc/shortcode/na_slider.php');
require_once( 'inc/shortcode/na_slider_category.php');
require_once( 'inc/shortcode/na_service.php');
require_once( 'inc/shortcode/na_text_block.php');
require_once( 'inc/shortcode/na_info_teams.php');
require_once( 'inc/shortcode/na_instagram.php');
require_once( 'inc/shortcode/na_intro_images.php');
require_once( 'inc/shortcode/na_intro.php');
require_once( 'inc/shortcode/na_menu.php');

/*  Include Widgets ================================================================================================= */
//require_once( 'inc/widgets/social.php');
//require_once( 'inc/widgets/search.php');
//require_once( 'inc/widgets/recent-post.php');
//require_once( 'inc/widgets/about.php');
//require_once( 'inc/widgets/contact-info.php');
//require_once( 'inc/widgets/instagram.php');
//if (in_array('woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ))) {
////    require_once( 'inc/widgets/widget-layered.php');
////    require_once( 'inc/widgets/woocommerce-price-filter.php');
////    require_once( 'inc/widgets/wcva_add_layered_navigation_widget.php');
//}

?>