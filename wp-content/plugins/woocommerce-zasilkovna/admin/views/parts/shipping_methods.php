<?php
/**
 * @package   WooCommerce Zásilkovna
 * @author    toret.cz
 * @license   GPL-2.0+
 * @link      http://toret.cz
 * @copyright 2018 Toret.cz
 */
 
?>

<form method="post" action="" class="setting-form">	
    <table class="table-bordered">
        <tr>
            <th><?php _e('Stát','zasilkovna'); ?></th>
            <th><?php _e('Dopravce','zasilkovna'); ?></th>
            <th><?php _e('Titulek služby','zasilkovna'); ?></th>
            <th><?php _e('Aktivní','zasilkovna'); ?></th>
        </tr>

        <?php 
        foreach( Zasilkovna_Helper::komplet_data() as $key => $service ){
        ?>
        <tr>
            <th><?php _e($service['stat'],'zasilkovna'); ?></th>
            <th><?php _e($service['nazev'],'zasilkovna'); ?></th>
            <?php 
                $html = '';
                if($service['deklarace'] > 0){
                    $html .= '<td colspan="2" class="zasilkovna-pozadavek">' . __('Dopravce nelze použít, protože zásilka musí mít specifickou celní deklaraci.','zasilkovna') . '</td>';
                }elseif($service['rozmery'] > 0){
                    $html .= '<td colspan="2" class="zasilkovna-pozadavek">' . __('Dopravce nelze použít, protože vyžaduje posílání informací o rozměrech balíku.','zasilkovna') . '</td>';
                }else{
                    $html .= '<td><input type="text" name="service-label-' . $key . '" value="' . (!empty($zasilkovna_services['service-label-'.$key]) ? $zasilkovna_services['service-label-'.$key] : $service['nazev']) . '"></td>
                    <td><input type="checkbox" name="service-active-' . $key . '" ' . (!empty($zasilkovna_services['service-active-'.$key]) ? 'checked="checked"' : '') . '></td>';
                }
                echo $html; 
            ?>
         </tr>
        <?php
        }
        ?>
   	</table>
    <div class="clear"></div>
   	<input type="hidden" name="services" value="ok" />
   	<input type="submit" class="button" <?php _e('Uložit','zasilkovna'); ?> />
</form>