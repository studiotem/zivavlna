<?php
/**
 * @package   WooCommerce Zásilkovna
 * @author    toret.cz
 * @license   GPL-2.0+
 * @link      http://toret.cz
 * @copyright 2018 Toret.cz
 */
 
$komplet_data = Zasilkovna_Helper::komplet_data();

$staty = array();
$staty_kont = array();
foreach($komplet_data as $key => $data){
    $staty[$data['stat']] = $data['statnazev'];
    $staty_kont[$data['stat']] = sanitize_title($data['statnazev']);
}
asort($staty_kont);
?>

<div class="notice-success updated toret-padding">
    <h3>Toret Zásilkovna 4</h3>
    <p>Kompletně jsme přepracovali a vylepšili plugin pro napojení na služby Zásilkovny. Bohužel na tuto novou verzi pluginu není možné aktualizovat automaticky a je potřeba plugin znovu nainstalovat a nastavit. Současný plugin budeme podporovat i nadále, ale není do něj možné přidat nové funkce.</p>
    <p>Plugin Toret Zásilkovna je zdarma dostupný pro všechny, kdo již plugin Zásilkovny vlastní a půjde v něm použít zakoupený licenční klíč. Plugin si můžete stáhnout v sekci <a target="_blank" href="https://toret.cz/muj-ucet/">Můj účet</a>.</p>
    <p><a target="_blank" href="https://toret.cz/toret-zasilkovna-4-0beta-a-prehled-novinek/">Přehled vylepšení</a> | <a target="_blank" href="https://documentation.toret.cz/zasilkovna/prechod-na-verzi-toret-zasilkovna-4/">Jak aktualizovat</a></p>
</div>

<?php if($lic == 'active'){ ?>
    <div class="notice-success updated toret-padding"><?php _e('Vaše licence je aktivní.', 'zasilkovna'); ?></div>
  <?php
    }else{
            echo $licence_info;
    }
  ?>
<form method="post" action="" class="setting-form">	
	<table class="table-bordered">
    

    
    	<tr>
      		<th><?php _e('Licenční klíč','zasilkovna'); ?></th>
      		<td>
        		<input type="text" name="zas_licence" value="<?php if(!empty($licence_key)){ echo $licence_key; } ?>">
   	            <input type="submit" class="button" value="<?php _e('Aktivovat','zasilkovna'); ?>" />
      		</td>
    	</tr>
    
    	<tr>
      		<th><?php _e('Klíč API','zasilkovna'); ?></th>
      		<td><input type="text" name="api_key" value="<?php if(!empty($zasilkovna_option['api_key'])){  echo $zasilkovna_option['api_key'];} ?>"></td>
    	</tr>
    
    	<tr>
      		<th><?php _e('Heslo API','zasilkovna'); ?></th>
      		<td><input type="text" name="api_password" value="<?php if(!empty($zasilkovna_option['api_password'])){  echo $zasilkovna_option['api_password'];} ?>"></td>
    	</tr>
    	<tr>
      		<th><?php _e('Název odesílatele','zasilkovna'); ?></th>
      		<td><input type="text" name="nazev_eshopu" value="<?php if(!empty($zasilkovna_option['nazev_eshopu'])){  echo $zasilkovna_option['nazev_eshopu'];} ?>"></td>
    	</tr>
    	<tr>
      		<th colspan="2"><?php _e('Nastavení států','zasilkovna'); ?></th>
        </tr>
        <tr>
            <th><?php _e('Zobrazit státy','zasilkovna'); ?></th>
            <td>
                <?php 
                    foreach($staty_kont as $key => $stat){
                        echo '<label for="' . $key . '"><input type="checkbox" name="povolene_staty[]" id="' . $key . '" value="' . $key . '" ';
                        if(!empty($zasilkovna_option['povolene_staty']) && in_array($key, $zasilkovna_option['povolene_staty'])){  echo 'checked="checked"'; } 
                        echo '/>' . $staty[$key] . '</label>';
                    }
                ?>
            </td>
        </tr>
    	<tr>
      		<th colspan="2"><?php _e('Základní nastavení Zásilkovny','zasilkovna'); ?></th>
        </tr>
    	<tr>
      		<th><?php _e('Povolit načítání CZ poboček','zasilkovna'); ?></th>
      		<td><input type="checkbox" name="cz_pobocky" value="cz" <?php if(!empty($zasilkovna_option['cz_pobocky']) && $zasilkovna_option['cz_pobocky'] == 'cz'){  echo 'checked="checked"'; } ?> /></td>
    	</tr>
    	<tr>
      		<th><?php _e('Povolit načítání SK poboček','zasilkovna'); ?></th>
      		<td><input type="checkbox" name="sk_pobocky" value="sk" <?php if(!empty($zasilkovna_option['sk_pobocky']) && $zasilkovna_option['sk_pobocky'] == 'sk'){  echo 'checked="checked"'; } ?> /></td>
    	</tr>
        <tr>
            <th><?php _e('Povolit načítání HU poboček','zasilkovna'); ?></th>
            <td><input type="checkbox" name="hu_pobocky" value="hu" <?php if(!empty($zasilkovna_option['hu_pobocky']) && $zasilkovna_option['hu_pobocky'] == 'hu'){  echo 'checked="checked"'; } ?> /></td>
        </tr>
        <tr>
            <th><?php _e('Povolit načítání PL poboček','zasilkovna'); ?></th>
            <td><input type="checkbox" name="pl_pobocky" value="pl" <?php if(!empty($zasilkovna_option['pl_pobocky']) && $zasilkovna_option['pl_pobocky'] == 'pl'){  echo 'checked="checked"'; } ?> /></td>
        </tr>
        <tr>
            <th><?php _e('Povolit načítání RO poboček','zasilkovna'); ?></th>
            <td><input type="checkbox" name="ro_pobocky" value="ro" <?php if(!empty($zasilkovna_option['ro_pobocky']) && $zasilkovna_option['ro_pobocky'] == 'ro'){  echo 'checked="checked"'; } ?> /></td>
        </tr>
    	<tr>
      		<th><?php _e('Vypnout odesílání do zásilkovny','zasilkovna'); ?></th>
      		<td><input type="checkbox" name="no_send" value="yes" <?php if(!empty($zasilkovna_option['no_send']) && $zasilkovna_option['no_send'] == 'yes'){  echo 'checked="checked"'; } ?> /></td>
    	</tr>
      	<tr>
      		<th><?php _e('Odeslání zásilky do systému Zásilkovny','zasilkovna'); ?></th>
      		<td>
        		<select name="odeslani_zasilky" id="odeslani_zasilky">
          			<option value="thankyou" <?php if(!empty($zasilkovna_option['odeslani_zasilky']) && $zasilkovna_option['odeslani_zasilky'] == 'thankyou'){  echo 'selected="selected"';} ?>><?php _e('Po vytvoření objednávky na děkovné stránce','zasilkovna'); ?></option>
          			<option value="processing" <?php if(!empty($zasilkovna_option['odeslani_zasilky']) && $zasilkovna_option['odeslani_zasilky'] == 'processing'){  echo 'selected="selected"';} ?>><?php _e('Při označení objednávky "Zpracovává se"','zasilkovna'); ?></option>
          			<option value="finished" <?php if(!empty($zasilkovna_option['odeslani_zasilky']) && $zasilkovna_option['odeslani_zasilky'] == 'finished'){  echo 'selected="selected"';} ?>><?php _e('Při označení objednávky "Dokončená"','zasilkovna'); ?></option>
        		</select>
      		</td>
    	</tr>
    	<tr>
      		<th><?php _e('Doprava zdarma','zasilkovna'); ?></th>
      		<td>
        		<select name="doprava_zdarma" id="doprava_zdarma">
          			<option value="all" <?php if(!empty($zasilkovna_option['doprava_zdarma']) && $zasilkovna_option['doprava_zdarma'] == 'all'){  echo 'selected="selected"';} ?>><?php _e('Zdarma budou všechny druhy dopravy','zasilkovna'); ?></option>
          			<option value="zasilkovna" <?php if(!empty($zasilkovna_option['doprava_zdarma']) && $zasilkovna_option['doprava_zdarma'] == 'zasilkovna'){  echo 'selected="selected"';} ?>><?php _e('Zdarma pouze dopravy Zásilkovny','zasilkovna'); ?></option>
          			<option value="default" <?php if(!empty($zasilkovna_option['doprava_zdarma']) && $zasilkovna_option['doprava_zdarma'] == 'default'){  echo 'selected="selected"';} ?>><?php _e('Doprava zdarma ve výběru dopravy','zasilkovna'); ?></option>
          			<option value="only" <?php if(!empty($zasilkovna_option['doprava_zdarma']) && $zasilkovna_option['doprava_zdarma'] == 'only'){  echo 'selected="selected"';} ?>><?php _e('Odstranit dopravu zdarma - ceny zústanou','zasilkovna'); ?></option>
       			</select>
      		</td>
    	</tr>
    	<tr>
      		<th><?php _e('URL ikony Zásilkovny v pokladně','zasilkovna'); ?></th>
      		<td><input type="text" name="icon_url" value="<?php if(!empty($zasilkovna_option['icon_url'])){  echo $zasilkovna_option['icon_url'];} ?>"></td>
    	</tr>
    	<tr>
      		<th><?php _e('URL ikony SK Zásilkovny v pokladně','zasilkovna'); ?></th>
      		<td><input type="text" name="icon_url_sk" value="<?php if(!empty($zasilkovna_option['icon_url_sk'])){  echo $zasilkovna_option['icon_url_sk'];} ?>"></td>
    	</tr>
        <tr>
            <th><?php _e('URL ikony HU Zásilkovny v pokladně','zasilkovna'); ?></th>
            <td><input type="text" name="icon_url_hu" value="<?php if(!empty($zasilkovna_option['icon_url_hu'])){  echo $zasilkovna_option['icon_url_hu'];} ?>"></td>
        </tr>
        <tr>
            <th><?php _e('URL ikony PL Zásilkovny v pokladně','zasilkovna'); ?></th>
            <td><input type="text" name="icon_url_pl" value="<?php if(!empty($zasilkovna_option['icon_url_pl'])){  echo $zasilkovna_option['icon_url_pl'];} ?>"></td>
        </tr>
        <tr>
            <th><?php _e('URL ikony RO Zásilkovny v pokladně','zasilkovna'); ?></th>
            <td><input type="text" name="icon_url_ro" value="<?php if(!empty($zasilkovna_option['icon_url_ro'])){  echo $zasilkovna_option['icon_url_ro'];} ?>"></td>
        </tr>
        <tr>
            <th><?php _e('Odesílat upozornění e-mailem o chybě vytvoření zásilky','zasilkovna'); ?></th>
            <td><input type="checkbox" name="error_email" value="email" <?php if(!empty($zasilkovna_option['error_email']) && $zasilkovna_option['error_email'] == 'email'){  echo 'checked="checked"'; } ?> /></td>
        </tr>
        <tr>
            <th><?php _e('Aktivovat reklamačního asistenta','zasilkovna'); ?></th>
            <td><input type="checkbox" name="asistent" value="ok" <?php if(!empty($zasilkovna_option['asistent']) && $zasilkovna_option['asistent'] == 'ok'){  echo 'checked="checked"'; } ?> /></td>
        </tr>
        <tr>
            <th><?php _e('Povolit pro Zásilkovnu navýšení hmotností na 20 a 30 kg','zasilkovna'); ?></th>
            <td><input type="checkbox" name="max_weight" value="ok" <?php if(!empty($zasilkovna_option['max_weight']) && $zasilkovna_option['max_weight'] == 'ok'){  echo 'checked="checked"'; } ?> /></td>
        </tr>
   	</table>
    <div class="clear"></div>
   	<input type="hidden" name="update" value="ok" />
    <input type="submit" class="button" value="<?php _e('Odeslat','zasilkovna'); ?>" />
</form>
<div class="clear"></div>
<form method="post" action="" class="setting-form">
    <table class="table-bordered">
        <tr>
            <th colspan="2" class="cena_top"><?php _e('Kurzy měn','zasilkovna'); ?></th>
        </tr>
        <tr>
            <th><?php _e('Položka','zasilkovna'); ?></th>
            <th><?php _e('Kurz','zasilkovna'); ?></th>
        </tr>
        <tr>
            <td><?php _e('Euro','zasilkovna'); ?></td>
            <td><input type="text" name="kurz-euro" value="<?php if(!empty($zasilkovna_prices['kurz-euro'])){  echo $zasilkovna_prices['kurz-euro']; } ?>"></td>
        </tr>
        <tr>
            <td><?php _e('Forint','zasilkovna'); ?></td>
            <td><input type="text" name="kurz-forint" value="<?php if(!empty($zasilkovna_prices['kurz-forint'])){  echo $zasilkovna_prices['kurz-forint']; } ?>"></td>
        </tr>
        <tr>
            <td><?php _e('Zlotý','zasilkovna'); ?></td>
            <td><input type="text" name="kurz-zloty" value="<?php if(!empty($zasilkovna_prices['kurz-zloty'])){  echo $zasilkovna_prices['kurz-zloty']; } ?>"></td>
        </tr>
        <tr>
            <td><?php _e('Lei','zasilkovna'); ?></td>
            <td><input type="text" name="kurz-lei" value="<?php if(!empty($zasilkovna_prices['kurz-lei'])){  echo $zasilkovna_prices['kurz-lei']; } ?>"></td>
        </tr>
    </table> 
    <div class="clear"></div>
    <input type="hidden" name="services_price" value="ok" />
    <input type="hidden" name="services_price_kurzy" value="ok" />
    <input type="submit" class="button" value="<?php _e('Odeslat','zasilkovna'); ?>" />
   
</form>
<div class="clear"></div>
<form method="post" action="" class="setting-form"> 
    <table class="table-bordered">
        <tr>
            <th colspan="2" class="cena_top"><?php _e('Nastavení měn a zemí','zasilkovna'); ?></th>
        </tr>
        <tr>
            <th><?php _e('Vypnout konverzi měn (dobírka se bude odesílat v měně objednávky)','zasilkovna'); ?></th>
            <td><input type="checkbox" name="country_currency_deactivate" value="ok" <?php if(!empty($country_currency['country_currency_deactivate']) && $country_currency['country_currency_deactivate'] == 'ok'){  echo 'checked="checked"'; } ?> /></td>
        </tr>
        <tr>
            <th><?php _e('Země','zasilkovna'); ?></th>
            <th><?php _e('Povolená měna','zasilkovna'); ?></th>
        </tr>
        <tr>
            <th><?php _e('Bulharsko','zasilkovna'); ?></th>
            <td>
                <input type="hidden" name="country_currency_bg" value="CZK" />CZK
            </td>
        </tr>
        <tr>
            <th><?php _e('Česká republika','zasilkovna'); ?></th>
            <td>
                <input type="hidden" name="country_currency_cz" value="CZK" />CZK
            </td>
        </tr>
        <tr>
            <th><?php _e('Maďarsko','zasilkovna'); ?></th>
            <td>
                <select name="country_currency_hu">
                    <option value="CZK" <?php if( !empty( $country_currency['country_currency_hu'] ) && $country_currency['country_currency_hu'] == 'CZK' ){ echo 'selected="selected"'; } ?>>CZK</option>
                    <option value="HUF" <?php if( !empty( $country_currency['country_currency_hu'] ) && $country_currency['country_currency_hu'] == 'HUF' ){ echo 'selected="selected"'; } ?>>HUF</option>
                </select>                
            </td>
        </tr>
        <tr>
            <th><?php _e('Německo','zasilkovna'); ?></th>
            <td>
                <select name="country_currency_de">
                    <option value="CZK" <?php if( !empty( $country_currency['country_currency_de'] ) && $country_currency['country_currency_de'] == 'CZK' ){ echo 'selected="selected"'; } ?>>CZK</option>
                    <option value="EUR" <?php if( !empty( $country_currency['country_currency_de'] ) && $country_currency['country_currency_de'] == 'EUR' ){ echo 'selected="selected"'; } ?>>EUR</option>
                </select>                
            </td>
        </tr>
        <tr>
            <th><?php _e('Polsko','zasilkovna'); ?></th>
            <td>
                <select name="country_currency_pl">
                    <option value="PLN" <?php if( !empty( $country_currency['country_currency_pl'] ) && $country_currency['country_currency_pl'] == 'PLN' ){ echo 'selected="selected"'; } ?>>PLN</option>
                    <option value="CZK" <?php if( !empty( $country_currency['country_currency_pl'] ) && $country_currency['country_currency_pl'] == 'CZK' ){ echo 'selected="selected"'; } ?>>CZK</option>
                    <option value="EUR" <?php if( !empty( $country_currency['country_currency_pl'] ) && $country_currency['country_currency_pl'] == 'EUR' ){ echo 'selected="selected"'; } ?>>EUR</option>
                </select>                
            </td>
        </tr>
        <tr>
            <th><?php _e('Rakousko','zasilkovna'); ?></th>
            <td>
                <select name="country_currency_at">
                    <option value="CZK" <?php if( !empty( $country_currency['country_currency_at'] ) && $country_currency['country_currency_at'] == 'CZK' ){ echo 'selected="selected"'; } ?>>CZK</option>
                    <option value="EUR" <?php if( !empty( $country_currency['country_currency_at'] ) && $country_currency['country_currency_at'] == 'EUR' ){ echo 'selected="selected"'; } ?>>EUR</option>
                </select> 
            </td>
        </tr>
        <tr>
            <th><?php _e('Rumunsko','zasilkovna'); ?></th>
            <td>
                <input type="hidden" name="country_currency_ru" value="CZK" />CZK
            </td>
        </tr>
        <tr>
            <th><?php _e('Slovensko','zasilkovna'); ?></th>
            <td>
                <select name="country_currency_sk">
                    <option value="CZK" <?php if( !empty( $country_currency['country_currency_sk'] ) && $country_currency['country_currency_sk'] == 'CZK' ){ echo 'selected="selected"'; } ?>>CZK</option>
                    <option value="EUR" <?php if( !empty( $country_currency['country_currency_sk'] ) && $country_currency['country_currency_sk'] == 'EUR' ){ echo 'selected="selected"'; } ?>>EUR</option>
                </select> 
            </td>
        </tr>
        
    </table>
    <div class="clear"></div>
    <input type="hidden" name="country_currency" value="ok" />
    <input type="submit" class="button" value="<?php _e('Odeslat','zasilkovna'); ?>" />
</form>
<div class="clear"></div>