<?php

    if( isset( $_GET['delete'] ) ){

        $log = Woo_Zasilkovna_Log::get_instance();
        $log->delete_logs();
        wp_redirect( admin_url() . 'admin.php?page=zasilkovna-log' );

    }

?>

<div class="wrap zasilkovna-wrap">

	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
    <div style="clear:both;"></div>  

    <div class="t-col-12 zasilkovna-main-wrap">
        <div class="toret-box box-info">
            <div class="box-header">
                <h3 class="box-title"><?php _e( 'Záznamy', 'zasilkovna' ); ?></h3>
            </div>
            <p><a href="<?php echo admin_url(); ?>admin.php?page=zasilkovna-log&delete=log" class="btn btn-info" style="margin-left:10px;"><?php _e( 'Smazat log', 'zasilkovna' ); ?></a></p>
            <div class="box-body">
                <?php 
                    
                    $log = Woo_Zasilkovna_Log::get_instance();
                    echo $log->render_table();

                ?>
                <div class="clear"></div>
                <?php echo $log->pagination(); ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>                 
    </div>
<div class="clear"></div>    
</div>
<div class="clear"></div>
