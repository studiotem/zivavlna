<?php 
/**
 * @package   Toret Zásilkovna
 * @author    toret.cz
 * @license   GPL-2.0+
 * @link      http://toret.cz
 * @copyright 2016 Toret.cz
 */

class Zasilkovna_Helper {

    /**
     * Get weight by weight unit
     * 
     * @since 1.1.9
     */ 
    public static function get_weight( $weight ){
        
        $weight = apply_filters( 'zasilkovna_weigh_koef', $weight );

        $weight_unit = get_option('woocommerce_weight_unit');
        if(!empty($weight_unit) && $weight_unit == 'g'){ 
    
            $weight = $weight * 0.001; 
    
        }
    
        return $weight;
    }


    /**
     * Multi currency handle
     *
     */  
    public static function currency_handle($fee){ 
  
        $active_plugins = get_option('active_plugins');
            if(in_array('woocommerce-currency-switcher/index.php', $active_plugins)){
    
                $fee = apply_filters('woocs_exchange_value', $fee);
    
            }elseif( in_array( 'sitepress-multilingual-cms/sitepress.php', $active_plugins ) ){
                // WPML WooCommerce Multicurrency fix
                global $woocommerce_wpml;                            
                if(!empty($woocommerce_wpml)){
                    if(property_exists($woocommerce_wpml,'multi_currency')){
                        $fee = $woocommerce_wpml->multi_currency->convert_price_amount($fee);
                    }
                } 
            }
    
        return $fee;  
    }


    /**
     *
     * Get current payment gateway
     *
     */
    
    public static function get_current_gateway(){
        
        $available_gateways = WC()->payment_gateways->get_available_payment_gateways();
        
        $current_gateway = null;
        
        $default_gateway = get_option( 'woocommerce_default_gateway' );
        if ( ! empty( $available_gateways ) ) {

           // Chosen Method
            if ( isset( WC()->session->chosen_payment_method ) && isset( $available_gateways[ WC()->session->chosen_payment_method ] ) ) {
                
                $current_gateway = $available_gateways[ WC()->session->chosen_payment_method ];
            
            } elseif ( isset( $available_gateways[ $default_gateway ] ) ) {
                $current_gateway = $available_gateways[ $default_gateway ];
            } else {
                $current_gateway = current( $available_gateways );
            }
        }

        if ( ! is_null( $current_gateway ) )
            return $current_gateway;
        else 
            return false;
    }


    /**
     *
     * Current payment gateway setting
     *
     */   
 
    public static function get_current_gateway_settings( ) {
        if ( $current_gateway = self::get_current_gateway() ) {
            $settings = $current_gateway->settings;
            return $settings;
        }
        return false;
    }

    /**
     * Is free shipping available
     *
     */   
    public static function free_shippping_available(){

        $all_packages = WC()->shipping->get_packages();
        $packages = $all_packages[0]['rates'];
    
        $is_available = false;
        foreach( $packages as $item ){
            if( $item->id == 'zasilkovna'  ){
                if( $item->cost == 0 ){ $is_available = true; }
            }
        }
    
        return $is_available;

    } 

    /**
     * Check if is disable product in cart
     *
     * return true or false
     */            
    public static function is_disable_product_in_cart() {
   
        $has_disable = false;
        if(!empty( WC()->session->cart->cart_contents)){
            $cart_data = WC()->session->cart->cart_contents;
        }else{
            $cart_data = WC()->session->cart;
        }
        if( !empty( $cart_data ) ){
            foreach($cart_data as $item){
        
                $product = wc_get_product( $item['variation_id'] ? $item['variation_id'] : $item['product_id'] );
                $product_id = Toret_Product_Compatibility::get_id( $product );
                $disable_product = get_post_meta( $product_id, 'disable_zasilkovna_shipping', true ); 
            
                if( !empty($disable_product) && $disable_product == 'yes' ){
                    $has_disable = true;
                } 
            } 
        }
        
        return $has_disable;
   
    }

    /**
     * Check if is disable product in cart
     *
     * return true or false
     */            
    public static function set_fee_by_dobirka_free_shipping( $fee, $shipping_total, $dobirka_option ){
   
        if( $shipping_total == 0 ){
        
            if( !empty( $dobirka_option['show_cod'] ) && $dobirka_option['show_cod'] == 'yes' ){
                return $fee;
            }else{
                return 0;
            }
        }else{
            return $fee;
        }
   
    }

    /**
     * Set shipping services
     *
     */
    public static function set_shipping_services(){
        $komplet_data = self::komplet_data();

        $services = array();

        foreach($komplet_data as $key => $data){
            $services[$key] = $data['preklad'];
        }

        return $services;

    }

    /**
     * Set shipping services
     *
     */
    public static function set_shipping_ids(){
        $komplet_data = self::komplet_data();

        $ids = array();

        foreach($komplet_data as $key => $data){
            $ids[$data['slug']] = $key;
        }

        return $ids;       

    }

    /**
     * Set shipping services ids for order
     *
     */
    public static function set_order_shipping_ids(){
        $komplet_data = self::komplet_data();

        $ids = array('zasilkovna>z-points');

        foreach($komplet_data as $key => $data){
            $ids[] = $data['prac'];
        }

        return $ids;       

    }

    /**
     * Set shipping services
     *
     */
    public static function set_services(){
        $komplet_data = self::komplet_data();

        $services = array_keys($komplet_data);
        return $services;

    }

    /**
     * Control isset shipping
     *
     */
    public static function isset_shipping( $option, $value ){

        if( isset( $option[$value] ) ){
            return $option[$value];
        }else{
            //return 0;
            return false;
        }

    }

    static public function get_customer_country(){

        $version = toret_check_wc_version();
        if( $version === false ){

            $country = WC()->customer->__get('country');   
                    
        }else{

            $shipping_country = WC()->customer->get_shipping_country();

            if( !empty( $shipping_country ) ){

                $country = WC()->customer->get_shipping_country();
            
            }else{

                $country = WC()->customer->get_billing_country();

            }

        }

        return $country;

    }

    static public function services_price_kurzy(){



        $fields = array(
            'kurz-euro',
            'kurz-forint',
            'kurz-zloty',
            'kurz-lei'
        );

        return $fields;

    }

    static public function generate_setting_table( $title, $args ){

        $zasilkovna_prices = get_option( 'zasilkovna_prices' );
    	if( empty( $zasilkovna_prices ) ){
        	$zasilkovna_prices = array();
    	}

        ?>
        <table class="table-bordered">
            <tr>   
                <th colspan="2" class="cena_top"><?php echo $title; ?></th>
            </tr>
            <tr>
                <th><?php _e('Položka','zasilkovna'); ?></th>
                <th><?php _e('Cena','zasilkovna'); ?></th>
            </tr>
            <?php 
            foreach( $args as $key => $label ){
            ?>
                <tr>
                    <td><?php _e( $label, 'zasilkovna' ); ?></td>
                    <td>
                        <input type="text" name="<?php echo $key; ?>" value="<?php if( !empty( $zasilkovna_prices[$key] ) ){  echo $zasilkovna_prices[$key]; } ?>">
                    </td>
                </tr>
                <?php
            } 
            ?>
        </table>
        <div class="clear"></div>
        <?php

    }

    static public function komplet_data(){

        $komplet = array(
            80 => array( 
                "stat" => "AT",
                "statnazev" => __('Rakousko', 'zasilkovna'),
                "nazev" => "Rakouská pošta",
                "preklad" => __("Rakouská pošta",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,30),
                "pobocky" => 0,
                "slug" => "austria-at",
                "prac" => "zasilkovna>austria-at"
            ),
            4832 => array( 
                "stat" => "BE",
                "statnazev" => __('Belgie', 'zasilkovna'),
                "nazev" => "Nizozemská pošta - Belgie",
                "preklad" => __("Nizozemská pošta - Belgie",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(0.5,1,2,5,10,20),
                "pobocky" => 0,
                "slug" => "belgie-nipost",
                "prac" => "zasilkovna>belgie-nipost"
            ),
            4015 => array( 
                "stat" => "BG",
                "statnazev" => __('Bulharsko', 'zasilkovna'),
                "nazev" => "Bulharský Speedy - doručení na adresu",
                "preklad" => __("Bulharský Speedy - doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,20),
                "pobocky" => 0,
                "slug" => "bulharsko-speedydna",
                "prac" => "zasilkovna>bulharsko-speedydna"
            ),
            4017 => array( 
                "stat" => "BG",
                "statnazev" => __('Bulharsko', 'zasilkovna'),
                "nazev" => "Bulharský Speedy - výdejní místo",
                "preklad" => __("Bulharský Speedy - výdejní místo",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,20),
                "pobocky" => 1,
                "slug" => "bulharsko-speedyvm",
                "prac" => "zasilkovna>bulharsko-speedyvm"
            ),
            106 => array( 
                "stat" => "CZ",
                "statnazev" => __('Česko', 'zasilkovna'),
                "nazev" => "CZ – Nejlevnější doručení na adresu",
                "preklad" => __("CZ – Nejlevnější doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(5,10),
                "pobocky" => 0,
                "slug" => "cesko-nejlevnejsi-doruceni",
                "prac" => "zasilkovna>cesko-nejlevnejsi-doruceni"
            ),
            13 => array( 
                "stat" => "CZ",
                "statnazev" => __('Česko', 'zasilkovna'),
                "nazev" => "Česká pošta",
                "preklad" => __("Česká pošta",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(2,3,4,5,10,30),
                "pobocky" => 0,
                "slug" => "ceska-posta-cz",
                "prac" => "zasilkovna>ceska-posta-cz"
            ),
         /*   633 => array( 
                "stat" => "CZ",
                "statnazev" => __('Česko', 'zasilkovna'),
                "nazev" => "Česká republika DPD",
                "preklad" => __("Česká republika DPD",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(5,10),
                "pobocky" => 0,
                "slug" => "dpd-cz",
                "prac" => "zasilkovna>dpd-cz"
            ),*/
     /*       153 => array( ////////////////////////////////////////////////////////////////
                "stat" => "CZ",
                "statnazev" => __('Česko', 'zasilkovna'),
                "nazev" => "Česká republika InTime",
                "preklad" => __("Česká republika InTime",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "pobocky" => 0,
                "slug" => "cesko-intime",
                "prac" => "zasilkovna>cesko-intime"
            ),*/
            136 => array( ///////////////////////////////////////////////////////////////
                "stat" => "CZ",
                "statnazev" => __('Česko', 'zasilkovna'),
                "nazev" => "Expresní doručení Brno",
                "preklad" => __("Expresní doručení Brno",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "pobocky" => 0,
                "slug" => "express-brno",
                "prac" => "zasilkovna>express-brno"
            ),
            134 => array( ///////////////////////////////////////////
                "stat" => "CZ",
                "statnazev" => __('Česko', 'zasilkovna'),
                "nazev" => "Expresní doručení Ostrava",
                "preklad" => __("Expresní doručení Ostrava",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "pobocky" => 0,
                "slug" => "express-ostrava",
                "prac" => "zasilkovna>express-ostrava"
            ),
            257 => array( 
                "stat" => "CZ",
                "statnazev" => __('Česko', 'zasilkovna'),
                "nazev" => "Expresní doručení Praha",
                "preklad" => __("Expresní doručení Praha",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "pobocky" => 0,
                "slug" => "express-praha",
                "prac" => "zasilkovna>express-praha"
            ),
            6373 => array( 
                "stat" => "DE",
                "statnazev" => __('Německo', 'zasilkovna'),
                "nazev" => "DE Hermes Direct Home",
                "preklad" => __("DE Hermes Direct Home",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "de-hermes-dh",
                "prac" => "zasilkovna>de-hermes-dh"
            ),
            6828 => array( 
                "stat" => "DE",
                "statnazev" => __('Německo', 'zasilkovna'),
                "nazev" => "DE Hermes Direct Pickup",
                "preklad" => __("DE Hermes Direct Pickup",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 1,
                "slug" => "de-hermes-dp",
                "prac" => "zasilkovna>de-hermes-dp"
            ),
            111 => array( 
                "stat" => "DE",
                "statnazev" => __('Německo', 'zasilkovna'),
                "nazev" => "Německá pošta",
                "preklad" => __("Německá pošta",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,20),
                "pobocky" => 0,
                "slug" => "germany-de",
                "prac" => "zasilkovna>germany-de"
            ),
            3946 => array( 
                "stat" => "DE",
                "statnazev" => __('Německo', 'zasilkovna'),
                "nazev" => "Německý Hermes",
                "preklad" => __("Německý Hermes",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "de-hermes",
                "prac" => "zasilkovna>de-hermes"
            ),
            4993 => array( 
                "stat" => "DK",
                "statnazev" => __('Dánsko', 'zasilkovna'),
                "nazev" => "Dánsko Post Nord - doručení na adresu",
                "preklad" => __("Dánsko Post Nord - doručení na adresu",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,20),
                "pobocky" => 0,
                "slug" => "dansko-postdna",
                "prac" => "zasilkovna>dansko-postdna"
            ),
            4994 => array( 
                "stat" => "DK",
                "statnazev" => __('Dánsko', 'zasilkovna'),
                "nazev" => "Dánsko Post Nord - výdejní místo",
                "preklad" => __("Dánsko Post Nord - výdejní místo",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,20),
                "pobocky" => 1,
                "slug" => "dansko-postvm",
                "prac" => "zasilkovna>dansko-postvm"
            ),
            5062 => array( 
                "stat" => "EE",
                "statnazev" => __('Estonsko', 'zasilkovna'),
                "nazev" => "Estonsko Omniva - výdejní automat",
                "preklad" => __("Estonsko Omniva - výdejní automat",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 1,
                "hmotnosti" => array(5,10,15,30),
                "pobocky" => 1,
                "slug" => "estonsko-omnivava",
                "prac" => "zasilkovna>estonsko-omnivava"
            ),
            5060 => array( 
                "stat" => "EE",
                "statnazev" => __('Estonsko', 'zasilkovna'),
                "nazev" => "Estonsko Omniva - doručení na adresu",
                "preklad" => __("Estonsko Omniva - doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "estonsko-omnivadna",
                "prac" => "zasilkovna>estonsko-omnivadna"
            ),
            5061 => array( 
                "stat" => "EE",
                "statnazev" => __('Estonsko', 'zasilkovna'),
                "nazev" => "Estonsko Omniva - výdejní místo",
                "preklad" => __("Estonsko Omniva - výdejní místo",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0, 
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 1,
                "slug" => "estonsko-omnivavm",
                "prac" => "zasilkovna>estonsko-omnivavm"
            ),
            4653 => array( 
                "stat" => "ES",
                "statnazev" => __('Španělsko', 'zasilkovna'),
                "nazev" => "Španělsko MRW - doručení na adresu",
                "preklad" => __("Španělsko MRW - doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15),
                "pobocky" => 0,
                "slug" => "spanelsko-mrwdna",
                "prac" => "zasilkovna>spanelsko-mrwdna"
            ),
      /*      4654 => array( 
                "stat" => "ES",
                "statnazev" => __('Španělsko', 'zasilkovna'),
                "nazev" => "Španělsko MRW - výdejní místo",
                "preklad" => __("Španělsko MRW - výdejní místo",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10),
                "pobocky" => 1,
                "slug" => "spanelsko-mrwvm",
                "prac" => "zasilkovna>spanelsko-mrwvm"
            ),*/
            4638 => array( 
                "stat" => "ES",
                "statnazev" => __('Španělsko', 'zasilkovna'),
                "nazev" => "Španělsko Correos",
                "preklad" => __("Španělsko Correos",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "spanelsko-correos",
                "prac" => "zasilkovna>spanelsko-correos"
            ),
            4830 => array( 
                "stat" => "FI",
                "statnazev" => __('Finsko', 'zasilkovna'),
                "nazev" => "Finsko Post Nord - doručení na adresu",
                "preklad" => __("Finsko Post Nord - doručení na adresu",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15),
                "pobocky" => 0,
                "slug" => "finsko-postdna",
                "prac" => "zasilkovna>finsko-postdna"
            ),
            4828 => array( 
                "stat" => "FI",
                "statnazev" => __('Finsko', 'zasilkovna'),
                "nazev" => "Finsko Post Nord - výdejní místo",
                "preklad" => __("Finsko Post Nord - výdejní místo",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15),
                "pobocky" => 1,
                "slug" => "finsko-postvm",
                "prac" => "zasilkovna>finsko-postvm"
            ),
            4309 => array( 
                "stat" => "FR",
                "statnazev" => __('Francie', 'zasilkovna'),
                "nazev" => "Francouzké Colissimo - doručení na adresu",
                "preklad" => __("Francouzké Colissimo - doručení na adresu",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(0.25,0.5,0.75,1,2,5,10,15),
                "pobocky" => 0,
                "slug" => "fr-colissimo",
                "prac" => "zasilkovna>fr-colissimo"
            ),
          /*  4307 => array( 
                "stat" => "FR",
                "statnazev" => __('Francie', 'zasilkovna'),
                "nazev" => "Francouzké Colissimo - výdejní místo",
                "preklad" => __("Francouzké Colissimo - výdejní místo",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(0.25,0.5,0.75,1,2,5,10),
                "pobocky" => 1,
                "slug" => "francie-colissimovm",
                "prac" => "zasilkovna>francie-colissimovm"
            ),*/
            3885 => array( 
                "stat" => "GB",
                "statnazev" => __('Británie', 'zasilkovna'),
                "nazev" => "Spojené království Hermes",
                "preklad" => __("Spojené království Hermes",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "gb-hermes",
                "prac" => "zasilkovna>gb-hermes"
            ),
            1120 => array( 
                "stat" => "GB",
                "statnazev" => __('Británie', 'zasilkovna'),
                "nazev" => "Spojené království Royal Mail",
                "preklad" => __("Spojené království Royal Mail",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "gb-royal-mail",
                "prac" => "zasilkovna>gb-royal-mail"
            ),
            4856 => array( 
                "stat" => "GB",
                "statnazev" => __('Británie', 'zasilkovna'),
                "nazev" => "Royal Mail 24",
                "preklad" => __("Royal Mail 24",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(0.5,1,2,5,10,20),
                "pobocky" => 0,
                "slug" => "britanie-rm24",
                "prac" => "zasilkovna>britanie-rm24"
            ),
            4857 => array( 
                "stat" => "GB",
                "statnazev" => __('Británie', 'zasilkovna'),
                "nazev" => "Royal Mail 48",
                "preklad" => __("Royal Mail 48",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(0.5,1,2,5,10,20),
                "pobocky" => 0,
                "slug" => "britanie-rm48",
                "prac" => "zasilkovna>britanie-rm48"
            ),
            4738 => array( 
                "stat" => "GR",
                "statnazev" => __('Řecko', 'zasilkovna'),
                "nazev" => "Řecko Speedy",
                "preklad" => __("Řecko Speedy",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15),
                "pobocky" => 0,
                "slug" => "recko-speedy",
                "prac" => "zasilkovna>recko-speedy"
            ),
            4634 => array( 
                "stat" => "HR",
                "statnazev" => __('Chorvatsko', 'zasilkovna'),
                "nazev" => "Chorvatská pošta - doručení na adresu",
                "preklad" => __("Chorvatská pošta - doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "chorvatsko-postdna",
                "prac" => "zasilkovna>chorvatsko-postdna"
            ),
            4635 => array( 
                "stat" => "HR",
                "statnazev" => __('Chorvatsko', 'zasilkovna'),
                "nazev" => "Chorvatská pošta - výdejní místo",
                "preklad" => __("Chorvatská pošta - výdejní místo",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15),
                "pobocky" => 1,
                "slug" => "chorvatsko-postvm",
                "prac" => "zasilkovna>chorvatsko-postvm"
            ),
            4646 => array( 
                "stat" => "HR",
                "statnazev" => __('Chorvatsko', 'zasilkovna'),
                "nazev" => "Chorvatsko DPD - doručení na adresu",
                "preklad" => __("Chorvatsko DPD - doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "chorvatsko-dpd-dna",
                "prac" => "zasilkovna>chorvatsko-dpd-dna"
            ),
            4159 => array( 
                "stat" => "HU",
                "statnazev" => __('Maďarsko', 'zasilkovna'),
                "nazev" => "HU - best delivery solution",
                "preklad" => __("HU - best delivery solution",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,3,4,5,10,30,50),
                "pobocky" => 0,
                "slug" => "hu-nejlevnejsi-doruceni",
                "prac" => "zasilkovna>hu-nejlevnejsi-doruceni"
            ),
            763 => array( 
                "stat" => "HU",
                "statnazev" => __('Maďarsko', 'zasilkovna'),
                "nazev" => "Maďarská pošta",
                "preklad" => __("Maďarská pošta",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,30),
                "pobocky" => 0,
                "slug" => "hungary-hu",
                "prac" => "zasilkovna>hungary-hu"
            ),
            805 => array( 
                "stat" => "HU",
                "statnazev" => __('Maďarsko', 'zasilkovna'),
                "nazev" => "Maďarsko DPD",
                "preklad" => __("Maďarsko DPD",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,30),
                "pobocky" => 0,
                "slug" => "dpd-hu",
                "prac" => "zasilkovna>dpd-hu"
            ),
            151 => array( 
                "stat" => "HU",
                "statnazev" => __('Maďarsko', 'zasilkovna'),
                "nazev" => "Maďarsko Express One",
                "preklad" => __("Maďarsko Express One",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30,50),
                "pobocky" => 0,
                "slug" => "transoflex-hu",
                "prac" => "zasilkovna>transoflex-hu"
            ),
            3294 => array(   
                "stat" => "CH",
                "statnazev" => __('Švýcarsko', 'zasilkovna'), 
                "nazev" => "Švýcarsko-Lichtenštejnská pošta",
                "preklad" => __("Švýcarsko-Lichtenštejnská pošta",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10),
                "pobocky" => 0,
                "slug" => "su-posta",
                "prac" => "zasilkovna>su-posta"
            ),
            3870 => array( 
                "stat" => "CH",
                "statnazev" => __('Švýcarsko', 'zasilkovna'),
                "nazev" => "Švýcarsko-Lichtenštejnská - prioritní",
                "preklad" => __("Švýcarsko-Lichtenštejnská - prioritní",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,30),
                "pobocky" => 0,
                "slug" => "svycarsko-prior",
                "prac" => "zasilkovna>svycarsko-prior"
            ),
            4524 => array( 
                "stat" => "IE",
                "statnazev" => __('Irsko', 'zasilkovna'),
                "nazev" => "Irský Hermes",
                "preklad" => __("Irský Hermes",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "irsko-hermes",
                "prac" => "zasilkovna>irsko-hermes"
            ),
            2726 => array( 
                "stat" => "IT",
                "statnazev" => __('Itálie', 'zasilkovna'),
                "nazev" => "Itálie GLS",
                "preklad" => __("Itálie GLS",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30,50,100),
                "pobocky" => 0,
                "slug" => "it-gls",
                "prac" => "zasilkovna>it-gls"
            ),
            5066 => array( 
                "stat" => "LT",
                "statnazev" => __('Litva', 'zasilkovna'),
                "nazev" => "Litva Omniva - výdejní automat",
                "preklad" => __("Litva Omniva - výdejní automat",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 1,
                "hmotnosti" => array(5,10,15,30),
                "pobocky" => 1,
                "slug" => "litva-omnivava",
                "prac" => "zasilkovna>litva-omnivava"
            ),
          /*  5065 => array( 
                "stat" => "LT",
                "statnazev" => __('Litva', 'zasilkovna'),
                "nazev" => "Litva Omniva - doručení na adresu",
                "preklad" => __("Litva Omniva - doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10),
                "pobocky" => 0,
                "slug" => "litva-omnivadna",
                "prac" => "zasilkovna>litva-omnivadna"
            ),*/
            4834 => array( 
                "stat" => "LU",
                "statnazev" => __('Lucembursko', 'zasilkovna'),
                "nazev" => "Lucembursko DPD",
                "preklad" => __("Lucembursko DPD",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(0.5,1,2,5,10),
                "pobocky" => 0,
                "slug" => "lucembursko-dpd",
                "prac" => "zasilkovna>lucembursko-dpd"
            ),
            5064 => array( 
                "stat" => "LV",
                "statnazev" => __('Lotyšsko', 'zasilkovna'),
                "nazev" => "Lotyšsko Omniva - výdejní automat",
                "preklad" => __("Lotyšsko Omniva - výdejní automat",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 1,
                "hmotnosti" => array(5,10,15,30),
                "pobocky" => 1,
                "slug" => "lotyssko-omnivava",
                "prac" => "zasilkovna>lotyssko-omnivava"
            ),
            5063 => array( 
                "stat" => "LV",
                "statnazev" => __('Lotyšsko', 'zasilkovna'),
                "nazev" => "Lotyšsko Omniva - doručení na adresu",
                "preklad" => __("Lotyšsko Omniva - doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "lotyssko-omnivadna",
                "prac" => "zasilkovna>lotyssko-omnivadna"
            ),
            4329 => array( 
                "stat" => "NL",
                "statnazev" => __('Nizozemsko', 'zasilkovna'),
                "nazev" => "Nizozemská pošta",
                "preklad" => __("Nizozemská pošta",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(0.5,1,2,5,10,15,20),
                "pobocky" => 0,
                "slug" => "nizozemsko-post",
                "prac" => "zasilkovna>nizozemsko-post"
            ),
            4162 => array( 
                "stat" => "PL",
                "statnazev" => __('Polsko', 'zasilkovna'),
                "nazev" => "PL - best delivery solution",
                "preklad" => __("PL - best delivery solution",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,3,4,5,10,30,50),
                "pobocky" => 0,
                "slug" => "pl-nejlevnejsi-doruceni",
                "prac" => "zasilkovna>pl-nejlevnejsi-doruceni"
            ),
            272 => array( 
                "stat" => "PL",
                "statnazev" => __('Polsko', 'zasilkovna'),
                "nazev" => "Polská pošta",
                "preklad" => __("Polská pošta",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,20),
                "pobocky" => 0,
                "slug" => "poland-pl",
                "prac" => "zasilkovna>poland-pl"
            ),
            1438 => array( 
                "stat" => "PL",
                "statnazev" => __('Polsko', 'zasilkovna'),
                "nazev" => "Polská pošta 24h",
                "preklad" => __("Polská pošta 24h",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,20),
                "pobocky" => 0,
                "slug" => "pl-mail-24",
                "prac" => "zasilkovna>pl-mail-24"
            ),
            1406 => array( 
                "stat" => "PL",
                "statnazev" => __('Polsko', 'zasilkovna'),
                "nazev" => "Polské DPD",
                "preklad" => __("Polské DPD",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,30),
                "pobocky" => 0,
                "slug" => "dpd-pl",
                "prac" => "zasilkovna>dpd-pl"
            ),
            3603 => array( 
                "stat" => "PL",
                "statnazev" => __('Polsko', 'zasilkovna'),
                "nazev" => "Polsko InPost",
                "preklad" => __("Polsko InPost",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,30),
                "pobocky" => 0,
                "slug" => "pl-inpost",
                "prac" => "zasilkovna>pl-inpost"
            ),
            3060 => array( 
                "stat" => "PL",
                "statnazev" => __('Polsko', 'zasilkovna'),
                "nazev" => "Polsko Paczkomaty",
                "preklad" => __("Polsko Paczkomaty",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 1,
                "hmotnosti" => array(5,10),
                "pobocky" => 1,
                "slug" => "pl-paczkomaty",
                "prac" => "zasilkovna>pl-paczkomaty"
            ),
            4655 => array( 
                "stat" => "PT",
                "statnazev" => __('Portugalsko', 'zasilkovna'),
                "nazev" => "Portugalsko MRW - doručení na adresu",
                "preklad" => __("Portugalsko MRW - doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15),
                "pobocky" => 0,
                "slug" => "portugalsko-mrwdna",
                "prac" => "zasilkovna>portugalsko-mrwdna"
            ),
          /*  4656 => array( 
                "stat" => "PT",
                "statnazev" => __('Portugalsko', 'zasilkovna'),
                "nazev" => "Portugalsko MRW - výdejní místo",
                "preklad" => __("Portugalsko MRW - výdejní místo",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10),
                "pobocky" => 1,
                "slug" => "portugalsko-mrwvm",
                "prac" => "zasilkovna>portugalsko-mrwvm"
            ),*/
            4161 => array( 
                "stat" => "RO",
                "statnazev" => __('Rumunsko', 'zasilkovna'),
                "nazev" => "RO - best delivery solution",
                "preklad" => __("RO - best delivery solution",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,3,4,5,10,30,50),
                "pobocky" => 0,
                "slug" => "ro-nejlevnejsi-doruceni",
                "prac" => "zasilkovna>ro-nejlevnejsi-doruceni"
            ),
            590 => array( 
                "stat" => "RO",
                "statnazev" => __('Rumunsko', 'zasilkovna'),
                "nazev" => "Rumunsko Cargus",
                "preklad" => __("Rumunsko Cargus",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "ro-cargus",
                "prac" => "zasilkovna>ro-cargus"
            ),
            836 => array( 
                "stat" => "RO",
                "statnazev" => __('Rumunsko', 'zasilkovna'),
                "nazev" => "Rumunsko DPD",
                "preklad" => __("Rumunsko DPD",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "dpd-ro",
                "prac" => "zasilkovna>dpd-ro"
            ),
            762 => array( 
                "stat" => "RO",
                "statnazev" => __('Rumunsko', 'zasilkovna'),
                "nazev" => "Rumunsko FAN",
                "preklad" => __("Rumunsko FAN",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,30),
                "pobocky" => 0,
                "slug" => "fan-ro",
                "prac" => "zasilkovna>fan-ro"
            ),
        /*    4461 => array( /
                "stat" => "RO",
                "statnazev" => __('Rumunsko', 'zasilkovna'),
                "nazev" => "Rumunsko FAN Pick-up point",
                "preklad" => __("Rumunsko FAN Pick-up point",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "pobocky" => 1,
                "slug" => "rumunsko-fanpp",
                "prac" => "zasilkovna>rumunsko-fanpp"
            ),*/
            5101 => array( 
                "stat" => "RU",
                "statnazev" => __('Rusko', 'zasilkovna'),
                "nazev" => "Ruská pošta EMS",
                "preklad" => __("Ruská pošta EMS",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(0.5,1,2,5,10),
                "pobocky" => 0,
                "slug" => "rusko-postems",
                "prac" => "zasilkovna>rusko-postems"
            ),
            5102 => array( 
                "stat" => "RU",
                "statnazev" => __('Rusko', 'zasilkovna'),
                "nazev" => "Ruská pošta Registered Parcel",
                "preklad" => __("Ruská pošta Registered Parcel",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(0.5,1,1.5,2),
                "pobocky" => 0,
                "slug" => "rusko-postrp",
                "prac" => "zasilkovna>rusko-postrp"
            ),
            4559 => array( 
                "stat" => "RU",
                "statnazev" => __('Rusko', 'zasilkovna'),
                "nazev" => "Ruská pošta",
                "preklad" => __("Ruská pošta",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15),
                "pobocky" => 0,
                "slug" => "rusko-post",
                "prac" => "zasilkovna>rusko-post"
            ),
            4827 => array( 
                "stat" => "SE",
                "statnazev" => __('Švédsko', 'zasilkovna'),
                "nazev" => "Švédsko Post Nord - doručení na adresu",
                "preklad" => __("Švédsko Post Nord - doručení na adresu",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "svedsko-pndna",
                "prac" => "zasilkovna>svedsko-pndna"
            ),
            4826 => array( 
                "stat" => "SE",
                "statnazev" => __('Švédsko', 'zasilkovna'),
                "nazev" => "Švédsko Post Nord - výdejní místo",
                "preklad" => __("Švédsko Post Nord - výdejní místo",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15),
                "pobocky" => 1,
                "slug" => "svedsko-pndvm",
                "prac" => "zasilkovna>svedsko-pndvm"
            ),
            4949 => array( 
                "stat" => "SI",
                "statnazev" => __('Slovinsko', 'zasilkovna'),
                "nazev" => "Slovinsko DPD - doručení na adresu",
                "preklad" => __("Slovinsko DPD - doručení na adresu",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 0,
                "slug" => "slovinsko-dpddna",
                "prac" => "zasilkovna>slovinsko-dpddna"
            ),
            4950 => array( 
                "stat" => "SI",
                "statnazev" => __('Slovinsko', 'zasilkovna'),
                "nazev" => "Slovinsko DPD - výdejní místo",
                "preklad" => __("Slovinsko DPD - výdejní místo",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,15,30),
                "pobocky" => 1,
                "slug" => "slovinsko-dpdvm",
                "prac" => "zasilkovna>slovinsko-dpdvm"
            ),
            132 => array( 
                "stat" => "SK",
                "statnazev" => __('Slovensko', 'zasilkovna'),
                "nazev" => "Expresní doručení Bratislava",
                "preklad" => __("Expresní doručení Bratislava",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(10,30),
                "pobocky" => 0,
                "slug" => "express-bratislava",
                "prac" => "zasilkovna>express-bratislava"
            ),
            131 => array( 
                "stat" => "SK",
                "statnazev" => __('Slovensko', 'zasilkovna'),
                "nazev" => "SK – Best delivery solution",
                "preklad" => __("SK – Best delivery solution",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,20,30,40,50),
                "pobocky" => 0,
                "slug" => "slovensko-doruceni",
                "prac" => "zasilkovna>slovensko-doruceni"
            ),
            16 => array( 
                "stat" => "SK",
                "statnazev" => __('Slovensko', 'zasilkovna'),
                "nazev" => "Slovenská pošta",
                "preklad" => __("Slovenská pošta",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "hmotnosti" => array(0.1,1,2,5,10,15),
                "pobocky" => 0,
                "slug" => "slovenska-posta",
                "prac" => "zasilkovna>slovenska-posta"
            ),
          /*  149 => array( //
                "stat" => "SK",
                "statnazev" => __('Slovensko', 'zasilkovna'),
                "nazev" => "Slovensko GLS",
                "preklad" => __("Slovensko GLS",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 0,
                "rozmery" => 0,
                "pobocky" => 0,
                "slug" => "slovensko-kuryr",
                "prac" => "zasilkovna>slovensko-kuryr"
            ),*/
            3616 => array( 
                "stat" => "UA",
                "statnazev" => __('Ukrajina', 'zasilkovna'),
                "nazev" => "Ukrajina Nova Poshta",
                "preklad" => __("Ukrajina Nova Poshta",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,20,30),
                "pobocky" => 1,
                "slug" => "ua-nova-posta",
                "prac" => "zasilkovna>ua-nova-posta",
            ),
            1160 => array( 
                "stat" => "UA",
                "statnazev" => __('Ukrajina', 'zasilkovna'),
                "nazev" => "Ukrajina Rosan",
                "preklad" => __("Ukrajina Rosan",'zasilkovna'),
                "dobirka" => 1,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(1,2,5,10,20,30),
                "pobocky" => 0,
                "slug" => "ukrajina-doruceni",
                "prac" => "zasilkovna>ukrajina-doruceni"
            ),
            6254 => array( 
                "stat" => "USA",
                "statnazev" => __('USA', 'zasilkovna'),
                "nazev" => "Americká pošta USPS",
                "preklad" => __("Americká pošta USPS",'zasilkovna'),
                "dobirka" => 0,
                "deklarace" => 1,
                "rozmery" => 0,
                "hmotnosti" => array(0.25,0.5,1,1.5,2),
                "pobocky" => 0,
                "slug" => "usa-usps",
                "prac" => "zasilkovna>usa-usps"
            ),
        );

        return $komplet;

    }
 

    static public function zasilkovna_kde(){

        $kde = array(
            'cz',
            'hu',
            'pl',
            'ro',
            'sk'
        );

        return $kde;

    }
}//End class


