<?php 
/**
 * @package   Toret Zásilkovna
 * @author    toret.cz
 * @license   GPL-2.0+
 * @link      http://toret.cz
 * @copyright 2016 Toret.cz
 */

class Zasilkovna_Outputs {

    /**
     * 
     * 
     * @since 2.0.0
     */ 
    public static function customer_order_info_table( $zasilkovna_id, $zasilkovna_mista, $zasilkovna_shipping ){
               
        $pobocky = 0;
        $komplet_data = Zasilkovna_Helper::komplet_data();
        foreach($komplet_data as $key => $data){
            if($data['prac'] == $zasilkovna_shipping){
                $pobocky = $data['pobocky'];
                $label = $data['nazev'];
            }
        }

        if( $pobocky == 1 ){

            $html = '<tr>';
            $html .= '<th colspan="2">' . $label . '</th>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<th>' . __('Ulice: ','zasilkovna') . '</th>';
            $html .= '<td>' . $zasilkovna_mista[$zasilkovna_id]['street'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<th>' . __('Město: ','zasilkovna') . '</th>';
            $html .= '<td>' . $zasilkovna_mista[$zasilkovna_id]['city'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<th>' . __('PSČ: ','zasilkovna') . '</th>';
            $html .= '<td>' . $zasilkovna_mista[$zasilkovna_id]['zip'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td></td>';

        }else{

            $html = '<tr>';
            $html .= '<th colspan="2">' . __('Zásilkovna - místo vyzvednutí: ','zasilkovna') . $zasilkovna_shipping . '</th>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<th>' . __('Název: ','zasilkovna') . '</th>';
            $html .= '<td>' . $zasilkovna_mista[$zasilkovna_id]['name'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<th>' . __('Místo: ','zasilkovna') . '</th>';
            $html .= '<td>' . $zasilkovna_mista[$zasilkovna_id]['place'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<th>' . __('Ulice: ','zasilkovna') . '</th>';
            $html .= '<td>' . $zasilkovna_mista[$zasilkovna_id]['street'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<th>' . __('Město: ','zasilkovna') . '</th>';
            $html .= '<td>' . $zasilkovna_mista[$zasilkovna_id]['city'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<th>' . __('PSČ: ','zasilkovna') . '</th>';
            $html .= '<td>' . $zasilkovna_mista[$zasilkovna_id]['zip'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td><a href="'.$zasilkovna_mista[$zasilkovna_id]['url'].'" target="_blank" class="button">'.__('Zobrazit detail místa','zasilkovna').'</a></td>';

        }
        $html = apply_filters( 'zasilkovna_vystup_html', $html, $zasilkovna_mista[$zasilkovna_id] );
        return $html;

    }

    /**
     * 
     * 
     * @since 2.0.0
     */ 
    public static function customer_email_info( $zasilkovna_id, $zasilkovna_mista, $zasilkovna_shipping ){
                
        $pobocky = 0;
        $komplet_data = Zasilkovna_Helper::komplet_data();
        foreach($komplet_data as $key => $data){
            if($data['prac'] == $zasilkovna_shipping){
                $pobocky = $data['pobocky'];
                $label = $data['nazev'];
            }
        }

        if( $pobocky == 1 ){
            
            $html = '<p><strong>' . $label . ' </strong><br />';
                $html .=$zasilkovna_mista[$zasilkovna_id]['street'].'<br />
                 '.$zasilkovna_mista[$zasilkovna_id]['city'].'<br />
                 '.$zasilkovna_mista[$zasilkovna_id]['zip'].'</p>';

        }else{

            $html = '<p><strong>' . __('Zásilkovna - místo vyzvednutí:','zasilkovna') . ' </strong><br />';
                $html .= $zasilkovna_mista[$zasilkovna_id]['name'].'<br />
                 '.$zasilkovna_mista[$zasilkovna_id]['place'].'<br />
                 '.$zasilkovna_mista[$zasilkovna_id]['street'].'<br />
                 '.$zasilkovna_mista[$zasilkovna_id]['city'].'<br />
                 '.$zasilkovna_mista[$zasilkovna_id]['zip'].'<br />
                 <a href="'.$zasilkovna_mista[$zasilkovna_id]['url'].'" target="_blank">'.__('Zobrazit detail místa','zasilkovna').'</a></p>';

        }
        $html = apply_filters( 'zasilkovna_vystup_html_email', $html, $zasilkovna_mista[$zasilkovna_id] );
        return $html;

    }

    /**
     * 
     * 
     * @since 2.0.0
     */ 
    public static function admin_customer_order_info( $zasilkovna_id, $zasilkovna_mista, $zasilkovna_shipping ){
        
        $pobocky = 0;
        $komplet_data = Zasilkovna_Helper::komplet_data();
        foreach($komplet_data as $key => $data){
            if($data['prac'] == $zasilkovna_shipping){
                $pobocky = $data['pobocky'];
                $label = $data['nazev'];
            }
        }

        if( $pobocky == 1 ){
            
            $html = '';
                $html .= '<strong>' . $label . '</strong><br />';
                $html .= '<strong>' . __('Ulice: ','zasilkovna') . '</strong>: ';
                $html .= '' . $zasilkovna_mista[$zasilkovna_id]['street'] . '<br />';
                $html .= '<strong>' . __('Město: ','zasilkovna') . '</strong>: ';
                $html .= '' . $zasilkovna_mista[$zasilkovna_id]['city'] . '<br />';
                $html .= '<strong>' . __('PSČ: ','zasilkovna') . '</strong>: ';
                $html .= '' . $zasilkovna_mista[$zasilkovna_id]['zip'] . '<br />';
                
        }else{

            $html = '';
                $html .= '<strong>' . __('Zásilkovna - místo vyzvednutí: ','zasilkovna') . '</strong><br />';
                $html .= '<strong>' . __('Název: ','zasilkovna') . '</strong>: ';
                $html .= '' . $zasilkovna_mista[$zasilkovna_id]['name'] . '<br />';
                $html .= '<strong>' . __('Místo: ','zasilkovna') . '</strong>: ';
                $html .= '' . $zasilkovna_mista[$zasilkovna_id]['place'] . '<br />'; 
                $html .= '<strong>' . __('Ulice: ','zasilkovna') . '</strong>: ';
                $html .= '' . $zasilkovna_mista[$zasilkovna_id]['street'] . '<br />';
                $html .= '<strong>' . __('Město: ','zasilkovna') . '</strong>: ';
                $html .= '' . $zasilkovna_mista[$zasilkovna_id]['city'] . '<br />';
                $html .= '<strong>' . __('PSČ: ','zasilkovna') . '</strong>: ';
                $html .= '' . $zasilkovna_mista[$zasilkovna_id]['zip'] . '<br />';
                $html .= '<a href="'.$zasilkovna_mista[$zasilkovna_id]['url'].'" target="_blank" class="button">'.__('Zobrazit detail místa','zasilkovna').'</a><br />';

        }

        return $html;

    }

    /**
     * 
     * 
     * @since 2.0.0
     */ 
    public static function sledovani_link( $order_id, $barcode = null ){

        if( !empty( $barcode ) ){
                    
            $html = '<a class="zasilkovna-sledovani" href="https://tracking.packeta.com/?id='.$barcode.'" target="_blank" class="button">'.__('Sledujte zásilku online','zasilkovna').'</a>';
            $html = apply_filters( 'zasilkovna-sledovani-link', $html, $barcode, $order_id );
            return $html;

        }else{

            return '';

        }

    }

    

}//End class

