<?php 


error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('max_execution_time', 0);
//ini_set('memory_limit', '512M');



$dirz =  dirname(dirname(dirname(dirname(__FILE__))));

require_once($dirz.'/wp-load.php');   
require_once($dirz.'/wp-includes/option.php');

if( empty( $_GET['service'] ) ){

	Zasilkovna_Pobocky::get_branches();

}else{

	$slug_key = Zasilkovna_Helper::set_shipping_ids();
	
	Zasilkovna_Pobocky::get_service_branches( $slug_key[$_GET['service']] );
	
}

exit();    