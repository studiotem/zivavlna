<?php

function getTrustPayTranslation($text, $locale = 'en') {
	
    $translate 	= '';

    $data 		= array(
        'bank_tranfer' => array(
            'en' => 'Instant bank transfers',
            'sk' => 'Okamžité bankové prevody',
            'cs' => 'Okamžité bankovní převody'
        ),
        'credit_card' => array(
            'en' => 'Card Payments',
            'sk' => 'Platby kartou',
            'cs' => 'Platby kartou'
        ),
        '100.100.303' => array(
            'en' => 'Rejected - card expired',
            'sk' => 'Exspirovaná karta',
            'cs' => 'Exspirovaná karta'
        ),
        '100.100.600' => array(
            'en' => 'Rejected - invalid CVV',
            'sk' => 'Nesprávny CVV kód',
            'cs' => 'Nesprávný CVV kód'
        ),
        '100.100.601' => array(
            'en' => 'Rejected - invalid CVV',
            'sk' => 'neplatné CVV',
            'cs' => 'neplatné CVV'
        ),
        '100.100.700' => array(
            'en' => 'Rejected - invalid card number',
            'sk' => 'Nesprávne číslo karty',
            'cs' => 'Nesprávné číslo karty'
        ),
        '100.380.401' => array(
            'en' => 'Rejected - failed 3DS authentication',
            'sk' => 'Nesprávny 3DS kód',
            'cs' => 'Nesprávný 3DS kód'
        ),
        '100.380.501' => array(
            'en' => 'Rejected - failed 3DS authentication',
            'sk' => 'Nesprávny 3DS kód',
            'cs' => 'Nesprávný 3DS kód'
        ),
        '800.100.151' => array(
            'en' => 'Rejected - invalid card number',
            'sk' => 'Nesprávne číslo karty',
            'cs' => 'Nesprávné číslo karty'
        ),
        '800.100.153' => array(
            'en' => 'Rejected - invalid CVV',
            'sk' => 'Nesprávny CVV kód',
            'cs' => 'Nesprávný CVV kód'
        ),
        '800.100.155' => array(
            'en' => 'Rejected - insufficient funds',
            'sk' => 'Nedostatok peňazí',
            'cs' => 'Nedostatek peněz'
        ),
        '800.100.157' => array(
            'en' => 'Rejected - invalid expiry date',
            'sk' => 'Exspirovaná karta',
            'cs' => 'Exspirovaná karta'
        ),
        '800.100.161' => array(
            'en' => 'Rejected - too many invalid tries',
            'sk' => 'Priveľa neúspešných pokusov',
            'cs' => 'Příliš mnoho neúspěšných pokusů'
        ),
        '800.100.162' => array(
            'en' => 'Rejected - card limit exceeded',
            'sk' => 'Prekročený limit',
            'cs' => 'Překročen limit'
        ),
        '800.100.163' => array(
            'en' => 'Rejected - card limit exceeded',
            'sk' => 'Prekročený limit',
            'cs' => 'Překročen limit'
        ),
        '800.120.101' => array(
            'en' => 'Rejected - card limit exceeded',
            'sk' => 'Prekročený limit',
            'cs' => 'Překročen limit'
        ),
        '800.120.200' => array(
            'en' => 'Rejected - card limit exceeded',
            'sk' => 'Prekročený limit',
            'cs' => 'Překročen limit'
        ),
        '800.120.201' => array(
            'en' => 'Rejected - card limit exceeded',
            'sk' => 'Prekročený limit',
            'cs' => 'Překročen limit'
        )
    );

    if(isset($data[$text]) && isset($data[$text][$locale])) {
        $translate = $data[$text][$locale];
    }

    return $translate;
    
}