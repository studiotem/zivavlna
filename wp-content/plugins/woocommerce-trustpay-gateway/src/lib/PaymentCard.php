<?php
namespace Trustpay;

class PaymentCard extends Payment {
	
    public function getUrl($paymentType, $implementation_type) {
        error_log("-----------URL:". $this->getBaseUrl($implementation_type) . '?' . http_build_query($this->getParams($paymentType)));
        return $this->getBaseUrl($implementation_type) . '?' . http_build_query($this->getParams($paymentType));
    }

    protected function getParams($paymentType) {
        $params = array(
            'AccountId'         => (string)$this->account_id,
            'Amount'            => (string)$this->order->getTotal(),
            'Currency'          => (string)$this->order->getCurrency(),
            'Reference'         => (string)$this->order->getId(),
            'PaymentType'       => (string)$paymentType,
            'Signature'         => (string)$this->getSignature()->getSignature($this->getSigData($paymentType)),
            'BillingCity'       => (string)$this->order->getBillingCity(),
            'BillingCountry'    => (string)$this->order->getBillingCountry(),
            'BillingPostcode'   => (string)$this->order->getBillingPostcode(),
            'BillingStreet'     => (string)$this->order->getBillingStreet(),
            'CardHolder'        => (string)$this->order->getCardHolder(),
            'Email'             => (string)$this->order->getEmail(),
        );

        if($this->getIsRedirect() == 'true'){
            $params['IsRedirect'] 		= $this->getIsRedirect();
        }

        if($this->notificationUrl) {
            $params['NotificationUrl'] 	= (string)$this->notificationUrl;
        }

        if($this->returnUrl) {
            $params['ReturnUrl'] 		= (string)$this->returnUrl;
        }

        if($this->cancelUrl) {
            $params['CancelUrl'] 		= (string)$this->cancelUrl;
        }

        if($this->errorUrl) {
            $params['ErrorUrl'] 		= (string)$this->errorUrl;
        }

        if($this->localization) {
            $params['Localization'] 	= (string)$this->localization;
        }

        return $params;
    }

    protected function getSigData($paymentType) {
        return array(
            $this->account_id,
            number_format($this->order->getTotal(), 2, '.', ''),
            $this->order->getCurrency(),
            $this->order->getId(),
            $paymentType,
            $this->order->getBillingCity(),
            $this->order->getBillingCountry(),
            $this->order->getBillingPostcode(),
            $this->order->getBillingStreet(),
            $this->order->getCardHolder(),
            $this->order->getEmail(),
        );
    }
    
}