<?php
namespace Trustpay;

class ResultCodes {
	
    const SUCCESS = 0;

    const PENDING = 1;

    const ANNOUNCED = 2;

    const AUTHORIZED = 3;

    const PROCESSING = 4;

    const AUTHORIZED_ONLY = 5;
    
}