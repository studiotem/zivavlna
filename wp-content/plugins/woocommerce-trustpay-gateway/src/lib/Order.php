<?php
namespace Trustpay;

class Order {
	
    protected $order_id 		= 0;
    protected $currency 		= 'EUR';
    protected $total 			= 0;
    protected $billingCity 		= '';
    protected $billingCountry 	= '';
    protected $billingPostcode 	= '';
    protected $billingStreet 	= '';
    protected $cardHolder 		= '';
    protected $email 			= '';

    public function setId($order_id) {
        $this->order_id = $order_id;
    }

    public function setCurrency($currency) {
        $this->currency = $currency;
    }

    public function setTotal($total) {
        $this->total = $total;
    }

    public function setBillingCity($billingCity) {
        $this->billingCity = $billingCity;
    }

    public function setBillingCountry($billingCountry) {
        $this->billingCountry = $billingCountry;
    }

    public function setBillingPostcode($billingPostcode) {
        $this->billingPostcode = $billingPostcode;
    }

    public function setBillingStreet($billingStreet) {
        $this->billingStreet = $billingStreet;
    }

    public function setCardHolder($cardHolder) {
        $this->cardHolder = $cardHolder;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getId() {
        return $this->order_id;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function getTotal() {
        return $this->total;
    }

    public function getBillingCity() {
        return $this->billingCity;
    }

    public function getBillingCountry() {
        return $this->billingCountry;
    }

    public function getBillingPostcode() {
        return $this->billingPostcode;
    }

    public function getBillingStreet() {
        return $this->billingStreet;
    }

    public function getCardHolder() {
        return $this->cardHolder;
    }

    public function getEmail() {
        return $this->email;
    }
    
}