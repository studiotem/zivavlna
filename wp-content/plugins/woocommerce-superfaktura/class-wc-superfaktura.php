<?php
/**
 * WooCommerce SuperFaktúra.
 *
 * @package   WooCommerce SuperFaktúra
 * @author    Webikon (Ján Bočínec) <info@webikon.sk>
 * @license   GPL-2.0+
 * @link      http://www.webikon.sk
 * @copyright 2013 Webikon s.r.o.
 */

/**
 * WC_SuperFaktura.
 *
 * @package WooCommerce SuperFaktúra
 * @author  Webikon (Ján Bočínec) <info@webikon.sk>
 */
class WC_SuperFaktura {

    /**
     * Fake payment gateway ID used to target zero value orders without payment method set.
     */
    public static $ZERO_VALUE_ORDER_FAKE_PAYMENT_METHOD_ID = 'wc_sf_zero_value_fake_gateway';

    /**
     * Plugin version, used for cache-busting of style and script file references.
     *
     * @since   1.0.0
     *
     * @var     string
     */
    protected $version = '1.9.65';

    protected $db_version = '1.0';

    /**
     * Unique identifier for your plugin.
     *
     * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
     * match the Text Domain file header in the main plugin file.
     *
     * @since    1.0.0
     *
     * @var      string
     */
    protected $plugin_slug = 'wc-superfaktura';

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Slug of the plugin screen.
     *
     * @since    1.0.0
     *
     * @var      string
     */
    protected $plugin_screen_hook_suffix = null;

    /**
     * Default product description template
     * @var string
     */
    protected $product_description_template_default;

    /**
     * Stored result of detection wc-nastavenia-skcz plugin
     * @var bool
     */
    protected $wc_nastavenia_skcz_activated;

    /**
     * Initialize the plugin by setting localization, filters, and administration functions.
     *
     * @since     1.0.0
     */
    private function __construct()
    {
        // Define custom functionality. Read more about actions and filters: http://codex.wordpress.org/Plugin_API#Hooks.2C_Actions_and_Filters
        add_action('init', [$this, 'init']);
        add_action('admin_init', [$this, 'admin_init']);
        add_action('plugins_loaded', [$this, 'plugins_loaded']);

        // Register hooks for warnings about problematic configuration
        add_action('admin_notices', [__CLASS__, 'order_number_notice_all']);
        add_action('woocommerce_settings_wc_superfaktura', [__CLASS__, 'order_number_notice']);

        // Load public-facing style sheet and JavaScript
        //add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);

        // Load admin JavaScript
        add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_scripts']);

        add_action('wp_ajax_wc_sf_api_test', [$this, 'wc_sf_api_test']);

        // backward compatibility with previous (now gone) option
        $this->product_description_template_default = '[ATTRIBUTES]' . ( 'yes' === get_option( 'woocommerce_sf_product_description_visibility', 'yes' ) ? "\n[SHORT_DESCR]" : '' );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Fired when the plugin is activated.
     *
     * @since    1.0.0
     *
     * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
     */
    public static function activate( $network_wide ) {
        // TODO: Define activation functionality here
    }

    /**
     * Fired when the plugin is deactivated.
     *
     * @since    1.0.0
     *
     * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Deactivate" action, false if WPMU is disabled or plugin is deactivated on an individual blog.
     */
    public static function deactivate( $network_wide ) {
        // TODO: Define deactivation functionality here
    }

    /**
     * Handles migration of some legacy invoice settings. It is called only from specific place as needed to avoid
     * unnecessary execution on every page load.
     */
    public static function maybe_migrate_invoice_settings()
    {
        //  previously woocommerce_sf_delivery_date_visibility and woocommerce_sf_delivery_date_order_paid
        //  now woocommerce_sf_delivery_date_value

        //  are legacy options already migrated?
        $delivery_date_value = get_option('woocommerce_sf_delivery_date_value');
        if ($delivery_date_value !== false) {
            return;
        }

        //  load previous options
        $dd_visibility = wc_string_to_bool(get_option('woocommerce_sf_delivery_date_visibility', 'yes'));  //  previous default "yes"
        $dd_paid = wc_string_to_bool(get_option('woocommerce_sf_delivery_date_order_paid', 'no'));  //  previous default "no"

        //  save them as new option, previous default was sending -1 to SF API and this was causing the delivery date NOT to show
        $new_delivery_date_value = 'none';
        if ($dd_visibility) {
            $new_delivery_date_value = $dd_paid ? 'order_paid' : 'invoice_created';
        }
        add_option('woocommerce_sf_delivery_date_value', $new_delivery_date_value, null, false);

        //  delete legacy options
        delete_option('woocommerce_sf_delivery_date_visibility');
        delete_option('woocommerce_sf_delivery_date_order_paid');
    }

    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function load_plugin_textdomain() {

        $domain = $this->plugin_slug;
        $locale = apply_filters( 'plugin_locale', get_locale(), $domain );

        load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

    /**
     * Register and enqueues public-facing JavaScript files.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {
        if (is_checkout() || is_account_page()) {
            wp_enqueue_script('wc-sf-checkout-js', plugins_url('js/checkout.js', __FILE__), ['jquery']);
        }
    }

    public function admin_enqueue_scripts() {
        wp_enqueue_script('wc-sf-admin-js', plugins_url('js/admin.js', __FILE__), ['jquery'], '1.0.5');
    }



    function admin_init() {
        if (isset($_GET['sfi_regen']) || isset($_GET['sf_invoice_proforma_create']) || isset($_GET['sf_invoice_regular_create']) || isset($_GET['sf_invoice_cancel_create'])) {
            if (!current_user_can('manage_woocommerce') || !isset($_GET['order'])) {
                wp_die('Unauthorized');
            }

            $order_id = (int)$_GET['order'];

            if (isset($_GET['sfi_regen'])) {
                $this->sf_regen_invoice($order_id);
            }

            if (isset($_GET['sf_invoice_proforma_create'])) {
                $order = new WC_Order($order_id);
                $this->sf_generate_invoice($order, 'proforma');
            }

            if (isset($_GET['sf_invoice_regular_create'])) {
                $order = new WC_Order($order_id);
                $this->sf_generate_invoice($order, 'regular');
            }

            if (isset($_GET['sf_invoice_cancel_create'])) {
                $order = new WC_Order($order_id);
                $this->sf_generate_invoice($order, 'cancel');
            }

            wp_safe_redirect(admin_url('post.php?post=' . $order_id . '&action=edit'));
            die();
        }
    }



    function plugins_loaded() {
        if (get_site_option('wc_sf_db_version') != $this->db_version) {
            $this->wc_sf_db_install();
        }
    }



    function wc_sf_db_install() {
        global $wpdb;

        $table_name = $wpdb->prefix . 'wc_sf_log';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            order_id bigint(20) unsigned NULL,
            document_type varchar(16) NULL,
            request_type varchar(16) NULL,
            response_status int(11) NULL,
            response_message varchar(255) NULL,
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        update_option('wc_sf_db_version', $this->db_version);
    }



    static function order_number_notice_all() {
        // avoid double notice on superfaktura settings tab
        if ( isset( $_GET['page'], $_GET['tab'] ) && 'wc-settings' === $_GET['page'] && 'wc_superfaktura' === $_GET['tab'] ) {
            return;
        }

        self::order_number_notice();
    }

    static function order_number_notice() {
        // display warning if we use custom numbering + [ORDER_NUMBER] variable
        // and do not have active plugin Woocommerce Sequential Order Numbers
        if ( ! is_admin() || defined('DOING_AJAX') && DOING_AJAX ) {
            return;
        }

        if ( is_plugin_active( 'woocommerce-sequential-order-numbers/woocommerce-sequential-order-numbers.php' )
            || is_plugin_active( 'woocommerce-sequential-order-numbers-pro/woocommerce-sequential-order-numbers.php' )
            || is_plugin_active( 'woocommerce-sequential-order-numbers-pro/woocommerce-sequential-order-numbers-pro.php' )
        ) {
            return;
        }

        if ( 'no' === get_option( 'woocommerce_sf_invoice_custom_num' ) ) {
            return;
        }

        $tmpl1 = get_option( 'woocommerce_sf_invoice_proforma_id' ) ?: '';
        $tmpl2 = get_option( 'woocommerce_sf_invoice_regular_id' ) ?: '';
        $tmpl3 = get_option( 'woocommerce_sf_invoice_cancel_id' ) ?: '';
        if ( false !== strpos( $tmpl1 . $tmpl2 . $tmpl3, '[ORDER_NUMBER]' ) )
        {
            ?>
            <div class="notice notice-error is-dismissible">
            <p><b>Woocommerce SuperFaktúra</b>: <?php printf( __( 'You use variable %1$s in your invoice nr. or proforma invoice nr., but the plugin "%2$s" is not activated. This may cause that your invoice numbers will not be sequential.', 'wc-superfaktura' ), '[ORDER_NUMBER]', 'WooCommerce Sequential Order Numbers' ) ?></p>
            </div>
            <?php
        }
    }

    function init() {
        // Load plugin text domain
        $this->load_plugin_textdomain();

        $this->wc_nastavenia_skcz_activated = class_exists( 'Webikon\Woocommerce_Plugin\WC_Nastavenia_SKCZ\Plugin', false );

        // woocommerce settings
        add_action( 'woocommerce_get_settings_pages', [$this, 'woocommerce_settings']);

        if ( get_option('woocommerce_sf_add_company_billing_fields', 'yes') == 'yes' && ! $this->wc_nastavenia_skcz_activated ) {
            // woo checkout billing fields
            add_filter('woocommerce_billing_fields', [$this, 'billing_fields']);
        }
        // woo checkout billing fields processing + meta has_shipping
        add_action('woocommerce_checkout_update_order_meta', [$this, 'checkout_order_meta']);

        // metabox hook
        add_action('add_meta_boxes', [$this, 'add_meta_boxes']);

        // customer order list actions filter
        add_filter( 'woocommerce_my_account_my_orders_actions', [$this, 'my_orders_actions'], 10, 2 );

        // custom order filter by wc_sf_internal_regular_id
        // see: https://github.com/woocommerce/woocommerce/wiki/wc_get_orders-and-WC_Order_Query#adding-custom-parameter-support
        add_filter( 'woocommerce_order_data_store_cpt_get_orders_query', [$this, 'filter_order_by_internal_regular_id'], 10, 2 );

        $wc_get_order_statuses = $this->get_order_statuses();

        foreach ( $wc_get_order_statuses as $key => $status )
        {
            add_action( 'woocommerce_order_status_'.$key, [$this, 'sf_new_invoice'], 5 );
        }

        add_action('woocommerce_checkout_order_processed', [$this, 'sf_new_invoice'], 5 );

        add_action( 'woocommerce_email_customer_details', [$this, 'sf_invoice_business_data_email'], 30, 3 );
        add_action( 'woocommerce_email_order_meta', [$this, 'sf_payment_link_email'], 10, 2 );
        add_action( 'woocommerce_email_order_meta', [$this, 'sf_invoice_link_email'], 10, 2 );
        add_filter( 'woocommerce_email_attachments', [$this, 'sf_invoice_attachment_email'], 10, 3);

        //add_action( 'woocommerce_order_status_on-hold_notification', array( 'WC_Email_Customer_Completed_Order', 'trigger' ) );
        add_action( 'woocommerce_thankyou', [$this, 'sf_invoice_link_page']);

        // pair invoice with order status
        add_action( 'wp_loaded', [$this, 'set_order_as_paid'] );

        add_action( 'wp_ajax_wc_sf_generate_secret_key', [$this, 'generate_secret_key'] );

        // invoice download button in the orders actions
        add_filter( 'woocommerce_admin_order_actions', array( $this, 'add_custom_order_status_actions_button'), 100, 2 );
        add_action( 'admin_head', array( $this, 'add_admin_css'));
    }

    /**
     * NOTE:  Actions are points in the execution of a page or process
     *        lifecycle that WordPress fires.
     *
     *        WordPress Actions: http://codex.wordpress.org/Plugin_API#Actions
     *        Action Reference:  http://codex.wordpress.org/Plugin_API/Action_Reference
     *
     * @since    1.0.0
     */
    public function action_method_name() {
        // TODO: Define your action hook callback here
    }



    public function generate_secret_key() {
        check_ajax_referer('wc_sf');
        echo WC_SecretKeyHelper::generate_secret_key();
        wp_die();
    }



    public function set_order_as_paid() {
        if (!isset($_GET['callback']) || 'wc_sf_order_paid' != $_GET['callback']) {
            return;
        }

        if (!isset($_GET['invoice_id']) || !isset($_GET['secret_key'])) {
            exit();
        }

        $invoiceID = $_GET['invoice_id'];
        $secretKey = $_GET['secret_key'];
        $userSecretKey = get_option('woocommerce_sf_sync_secret_key', false);

        if (is_numeric($invoiceID) && ($userSecretKey === false || $secretKey === $userSecretKey)) {

            // query order by custom field
            $orders = wc_get_orders(['wc_sf_internal_proforma_id' => $invoiceID]);
            if (count($orders) === 0) {
                $orders = wc_get_orders(['wc_sf_internal_regular_id' => $invoiceID]);
            }



            // check invoice status
            $api = $this->sf_api();
            $response = $api->getInvoiceDetails($invoiceID);

            // invoice not found
            if (!$response || !isset($response->{$invoiceID})) {
                exit();
            }

            // 1 = not paid, 2 = paid partially, 3 = paid
            if (3 != $response->{$invoiceID}->Invoice->status) {
                exit();
            }



            if (count($orders) === 1) {
                $order = $orders[0];
                $orderStatus = $order->get_status(); // https://docs.woocommerce.com/document/managing-orders/
                if ($orderStatus === 'on-hold')  {
                    // https://docs.woocommerce.com/document/payment-gateway-api/
                    // https://docs.woocommerce.com/document/woocommerce-order-status-control/#section-5
                    $order->payment_complete();
                }
            }
        }

        exit();
    }



    /**
     * Handle a custom 'wc_sf_internal_proforma_id' and 'wc_sf_internal_regular_id' query var to get orders with the 'wc_sf_internal_proforma_id' or 'wc_sf_internal_regular_id' meta respectively.
     * @param array $query - Args for WP_Query.
     * @param array $query_vars - Query vars from WC_Order_Query.
     * @return array modified $query
     */
    function filter_order_by_internal_regular_id( $query, $query_vars ) {

        if ( ! empty( $query_vars['wc_sf_internal_proforma_id'] ) ) {
            $query['meta_query'][] = array(
                'key' => 'wc_sf_internal_proforma_id',
                'value' => esc_attr( $query_vars['wc_sf_internal_proforma_id'] ),
            );
        }

        if ( ! empty( $query_vars['wc_sf_internal_regular_id'] ) ) {
            $query['meta_query'][] = array(
                'key' => 'wc_sf_internal_regular_id',
                'value' => esc_attr( $query_vars['wc_sf_internal_regular_id'] ),
            );
        }

        return $query;
    }



    /**
     * @return SFAPIclient|SFAPIclientAT|SFAPIclientCZ
     */
    public function sf_api($credentials = null) {
        if (is_array($credentials)) {
            $sf_lang = $credentials['woocommerce_sf_lang'];
            $sf_email = $credentials['woocommerce_sf_email'];
            $sf_key = $credentials['woocommerce_sf_apikey'];
            $sf_company_id = $credentials['woocommerce_sf_company_id'];
        }
        else {
            $sf_lang = get_option('woocommerce_sf_lang', 'sk');
            $sf_email = get_option('woocommerce_sf_email');
            $sf_key = get_option('woocommerce_sf_apikey');
            $sf_company_id = get_option('woocommerce_sf_company_id');
        }

        $module_id = sprintf('WordPress %s (WC %s, WC SF %s)', get_bloginfo('version'), WC()->version, $this->version);

        switch ($sf_lang) {
            case 'at':
                return new SFAPIclientAT( $sf_email, $sf_key, $_SERVER['SERVER_NAME'], $module_id, $sf_company_id );

            case 'cz':
                return new SFAPIclientCZ( $sf_email, $sf_key, $_SERVER['SERVER_NAME'], $module_id, $sf_company_id );

            default:
                return new SFAPIclient( $sf_email, $sf_key, $_SERVER['SERVER_NAME'], $module_id, $sf_company_id );
        }
    }

    /**
     * @param int $invoice_id
     * @param SFAPIclient|SFAPIclientAT|SFAPIclientCZ $api
     */
    public function sf_clean_invoice_items($invoice_id, $api) {
        $response = $api->invoice($invoice_id);
        if(!isset($response->error) || $response->error==0) {
            if(isset($response->InvoiceItem) && is_array($response->InvoiceItem))
            {
                foreach($response->InvoiceItem as $item)
                    $api->deleteInvoiceItem($invoice_id, $item->id);
            }
        }
    }

    /**
     * @param int $order_id
     */
    public function sf_new_invoice($order_id) {
        $order = new WC_Order($order_id);

        foreach(['regular','proforma'] as $type ) {
            $payment_method = $this->get_order_field('payment_method',$order);
            if (empty($payment_method) && 0 == abs(floatval($this->get_order_field('total', $order)))) {
                //  payment method is missing for zero value orders, we check the "Zero value invoices" status from
                //  settings using fake payment method name
                $payment_method = self::$ZERO_VALUE_ORDER_FAKE_PAYMENT_METHOD_ID;
            }

            if ( ! $invoice_status = $this->generate_invoice_status($payment_method, $type) )
                continue;

            $order_status = $this->get_order_field('status',$order);
            if ( $invoice_status != $order_status) {
                //  the order status does not match the status in which invoice should be generated
                if ('proforma' === $type) {
                    continue;
                }

                /*
                 * Workaround for orders that don't need processing. Invoice won't be generated in some cases because
                 * the "Processing" order state is skipped. We need to allow the generation of invoice in "Completed"
                 * order state instead.
                 */
                $workaround_enabled = wc_string_to_bool(get_option('woocommerce_sf_invoice_regular_processing_skipped_fix'));
                if (!$workaround_enabled) {
                    continue;
                }

                $is_exception = ('processing' == $invoice_status && 'completed' == $order_status && !$order->needs_processing());
                if (!$is_exception) {
                    continue;
                }

            }

            $this->sf_generate_invoice( $order, $type );
        }
    }



    /**
     * @param int $order_id
     */
    public function sf_regen_invoice($order_id) {
        $order = new WC_Order($order_id);

        foreach(['proforma', 'regular', 'cancel'] as $type ) {
            $sf_id = get_post_meta($this->get_order_field('id', $order), 'wc_sf_internal_' . $type . '_id', true);
            if (!empty($sf_id)) {
                $this->sf_generate_invoice($order, $type);
            }
        }
    }



    /**
     * @param WC_Order $order
     * @param string $type
     */
    private function sf_generate_invoice($order, $type) {

        // filter to allow skipping invoice creation
        $skip_invoice = apply_filters('sf_skip_invoice', false, $order);
        if ($skip_invoice) {
            return false;
        }



        $api = $this->sf_api();

        $sf_id = get_post_meta($this->get_order_field('id', $order), 'wc_sf_internal_' . $type . '_id', true);

        // try to get $sf_id from deprecated meta data
        if (!$sf_id) {
            $old_sf_id = get_post_meta($this->get_order_field('id', $order), 'wc_sf_internal_id', true);
            if ($old_sf_id) {

                // if regular invoice link exists, it's a regular id
                if (get_post_meta($this->get_order_field('id', $order), 'wc_sf_invoice_regular', true)) {
                    update_post_meta($this->get_order_field('id', $order), 'wc_sf_internal_regular_id', $old_sf_id);

                    // use only if we are generating regular invoice
                    if ('regular' === $type) {
                        $sf_id = $old_sf_id;
                    }
                }

                // if proforma invoice link exists, it's a proforma id
                elseif (get_post_meta($this->get_order_field('id', $order), 'wc_sf_invoice_proforma', true)) {
                    update_post_meta($this->get_order_field('id', $order), 'wc_sf_internal_proforma_id', $old_sf_id);

                    // use only if we are generating proforma invoice
                    if ('proforma' === $type) {
                        $sf_id = $old_sf_id;
                    }
                }
            }
        }

        $edit = false;
        if (!empty($sf_id)) {
            if (!$this->sf_can_regenerate($order)) {
                return false;
            }

            $this->sf_clean_invoice_items($sf_id, $api);
            $edit = true;
        }

        self::maybe_migrate_invoice_settings();

        // CLIENT DATA

        if ($this->wc_nastavenia_skcz_activated) {
            $plugin = Webikon\Woocommerce_Plugin\WC_Nastavenia_SKCZ\Plugin::get_instance();
            $details = $plugin->get_customer_details($this->get_order_field('id', $order));
            $ico = $details->get_company_id();
            $ic_dph = $details->get_company_vat_id();
            $dic = $details->get_company_tax_id();
        }
        else {
            $ico = get_post_meta($this->get_order_field('id', $order), 'billing_company_wi_id', true);
            $ic_dph = get_post_meta($this->get_order_field('id', $order), 'billing_company_wi_vat', true);
            $dic = get_post_meta($this->get_order_field('id', $order), 'billing_company_wi_tax', true);
        }

        if (empty($ic_dph)) {
            include_once(ABSPATH . 'wp-admin/includes/plugin.php');

            // compatibility with WooCommerce EU VAT Number plugin
            if (is_plugin_active('woocommerce-eu-vat-number/woocommerce-eu-vat-number.php') && 'true' == get_post_meta($this->get_order_field('id', $order), '_vat_number_is_valid', true)) {
                $ic_dph = get_post_meta($this->get_order_field('id', $order), '_vat_number', true);
            }

            // compatibility with WooCommerce EU VAT Assistant plugin
            if (is_plugin_active('woocommerce-eu-vat-assistant/woocommerce-eu-vat-assistant.php') && 'valid' == get_post_meta($this->get_order_field('id', $order), '_vat_number_validated', true)) {
                $ic_dph = get_post_meta($this->get_order_field('id', $order), 'vat_number', true);
            }
        }

        $client_data = [
            'name'                  => ($this->get_order_field('billing_company', $order)) ? $this->get_order_field('billing_company', $order) : $this->get_order_field('billing_first_name', $order) . ' ' . $this->get_order_field('billing_last_name', $order),
            'ico'                   => $ico,
            'dic'                   => $dic,
            'ic_dph'                => $ic_dph,
            'email'                 => $this->get_order_field('billing_email', $order),
            'address'               => $this->get_order_field('billing_address_1', $order) . (($this->get_order_field('billing_address_2', $order)) ? ' ' . $this->get_order_field('billing_address_2', $order) : ''),
            'country_iso_id'        => $this->get_order_field('billing_country', $order),
            'city'                  => $this->get_order_field('billing_city', $order),
            'zip'                   => $this->get_order_field('billing_postcode', $order),
            'phone'                 => $this->get_order_field('billing_phone', $order),
            'update_addressbook'    => (get_option('woocommerce_sf_invoice_update_addressbook', 'no') == 'yes'),
        ];

        if ($order->get_formatted_billing_address() != $order->get_formatted_shipping_address()) {
            if ($this->get_order_field('shipping_company', $order)) {
                if (get_option('woocommerce_sf_invoice_delivery_name') == 'yes') {
                    $shipping_name = sprintf('%s - %s %s', $this->get_order_field('shipping_company', $order), $this->get_order_field('shipping_first_name', $order), $this->get_order_field('shipping_last_name', $order));
                }
                else {
                    $shipping_name = $this->get_order_field('shipping_company', $order);
                }
            }
            else {
                $shipping_name = $this->get_order_field('shipping_first_name', $order) . ' ' . $this->get_order_field('shipping_last_name', $order);
            }

            $client_data['delivery_address']        = $this->get_order_field('shipping_address_1', $order)  . (($this->get_order_field('shipping_address_2', $order)) ? ' ' . $this->get_order_field('shipping_address_2', $order) : '');
            $client_data['delivery_city']           = $this->get_order_field('shipping_city', $order);
            $client_data['delivery_country_iso_id'] = $this->get_order_field('shipping_country', $order);
            $client_data['delivery_name']           = $shipping_name;
            $client_data['delivery_zip']            = $this->get_order_field('shipping_postcode', $order);
        }

        $client_data = apply_filters('sf_client_data', $client_data, $order);

        $api->setClient($client_data);

        // INVOICE DATA

        $shipping_methods = $order->get_shipping_methods();
        $shipping_method = reset($shipping_methods);
        if (class_exists( 'WC_Shipping_Zones')) {
            $delivery_type = get_option('woocommerce_sf_shipping_' . $shipping_method['method_id'] . ':' . $shipping_method['instance_id']);
        }
        else {
            $delivery_type = get_option('woocommerce_sf_shipping_' . $shipping_method['method_id']);
        }

        $set_invoice_data = [
            'invoice_currency'  => ($this->version_check()) ? $order->get_currency() : get_post_meta($this->get_order_field('id', $order), '_order_currency', true),
            'payment_type'      => get_option('woocommerce_sf_gateway_' . $this->get_order_field('payment_method', $order)),
            'delivery_type'     => $delivery_type,
            'rounding'          => (wc_prices_include_tax()) ? 'item_ext' : 'document',
            'issued_by'         => get_option('woocommerce_sf_issued_by'),
            'issued_by_phone'   => get_option('woocommerce_sf_issued_phone'),
            'issued_by_web'     => get_option('woocommerce_sf_issued_web'),
            'issued_by_email'   => get_option('woocommerce_sf_issued_email'),
            'internal_comment'  => $this->get_order_field('customer_note', $order),
            'order_no'          => $order->get_order_number(),
        ];

        // document relations
        switch ($type) {
            case 'regular':
                $sf_proforma_id = get_post_meta($this->get_order_field('id', $order), 'wc_sf_internal_proforma_id', true);
                if ($sf_proforma_id) {
                    $set_invoice_data['proforma_id'] = $sf_proforma_id; // set proforma_id to link regular invoice to proforma invoice
                }
                break;

            case 'cancel':
                $sf_invoice_id = get_post_meta($this->get_order_field('id', $order), 'wc_sf_internal_regular_id', true);
                if ($sf_invoice_id) {
                    $set_invoice_data['parent_id'] = $sf_invoice_id;
                }
                break;
        }

        // sequence
        switch ($type) {
            case 'proforma':
                $set_invoice_data['sequence_id'] = get_option('woocommerce_sf_proforma_invoice_sequence_id');
                break;

            case 'regular':
                $set_invoice_data['sequence_id'] = get_option('woocommerce_sf_invoice_sequence_id');
                break;

            case 'cancel':
                $set_invoice_data['sequence_id'] = get_option('woocommerce_sf_cancel_sequence_id');
                break;
        }

        // logo
        $set_invoice_data['logo_id'] = get_option('woocommerce_sf_logo_id');

        // bank account
        $bank_account_id = get_option('woocommerce_sf_bank_account_id', null);
        if ($bank_account_id) {
            $set_invoice_data['bank_accounts'] = [
                ['id' => $bank_account_id]
            ];
        }

        // create date
        if (get_option('woocommerce_sf_created_date_as_order') == 'yes') {
            $set_invoice_data['created'] = (string)$this->get_order_field('date_created', $order);
        }

        if ('order_nr' === get_option('woocommerce_sf_variable_symbol')) {
            $set_invoice_data['variable'] = $order->get_order_number();
        }

        // delivery date
        switch (get_option('woocommerce_sf_delivery_date_value', 'invoice_created')) {

            case 'invoice_created':
                // do nothing, SF API will use invoice creation date by default
                break;

            case 'order_paid':
                $delivery_date = $order->get_date_paid();
                if ($delivery_date) {
                    $set_invoice_data['delivery'] = $delivery_date->date('Y-m-d');
                }
                break;

            case 'order_created':
                $delivery_date = $order->get_date_created();
                if ($delivery_date) {
                    $set_invoice_data['delivery'] = $delivery_date->date('Y-m-d');
                }
                break;

            case 'none':
                $set_invoice_data['delivery'] = -1;
                break;

        }

        // comment
        if (get_option('woocommerce_sf_comments') == 'yes') {
            $comment_parts = [];

            if (($ic_dph && WC()->countries->get_base_country() != $this->get_order_field('billing_country', $order)) || (in_array(WC()->countries->get_base_country(), ['SK', 'CZ']) && !in_array($this->get_order_field('billing_country', $order), ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK']))) {
                $sf_tax_liability = get_option('woocommerce_sf_tax_liability');
                if ($sf_tax_liability) {
                    $comment_parts[] = $sf_tax_liability;
                }
            }

            $sf_comment = get_option('woocommerce_sf_comment');
            if ($sf_comment) {
                $comment_parts[] = $sf_comment;
            }

            if (get_option('woocommerce_sf_comment_add_order_note') == 'yes') {
                $customer_note = $this->get_order_field('customer_note',$order);
                if ($customer_note) {
                    $comment_parts[] = $customer_note;
                }
            }

            $set_invoice_data['comment'] = implode("\r\n\r\n", $comment_parts);
        }

        // override invoice settings for specific countries based on customer billing address
        $country_settings = json_decode(get_option('woocommerce_sf_country_settings', false), true);
        if ($country_settings) {
            $billing_country = $this->get_order_field('billing_country', $order);

            foreach ($country_settings as $country) {
                if ($billing_country == $country['country']) {

                    // override VAT and TAX ID
                    $client_country_data = [
                        'ic_dph' => $country['vat_id'],
                        'dic' => $country['tax_id'],
                    ];

                    //  Webikon, 20201001: Added a filter that allows to modify client data based on country 
                    $client_country_data = apply_filters('sf_client_country_data', $client_country_data, $order);

                    if(!empty($client_country_data)) {
                        $api->setMyData($client_country_data);
                    }

                    // override bank account
                    if ($country['bank_account_id']) {
                        $set_invoice_data['bank_accounts'] = [
                            [
                                'id' => $country['bank_account_id']
                            ]
                        ];
                    }

                    // override sequences
                    switch ($type) {
                        case 'proforma':
                            if ($country['proforma_sequence_id']) {
                                $set_invoice_data['sequence_id'] = $country['proforma_sequence_id'];
                            }
                            break;

                        case 'regular':
                            if ($country['invoice_sequence_id']) {
                                $set_invoice_data['sequence_id'] = $country['invoice_sequence_id'];
                            }
                            break;

                        case 'cancel':
                            if ($country['cancel_sequence_id']) {
                                $set_invoice_data['sequence_id'] = $country['cancel_sequence_id'];
                            }
                            break;
                    }
                }
            }
        }

        //  Webikon, 20200521: added extra attribute $type to be able to identify credit note in the hook
        $set_invoice_data = apply_filters('sf_invoice_data', $set_invoice_data, $order, $type);

        $api->setInvoice($set_invoice_data);



        // INVOICE SETTINGS

        $settings = [
            'language' => $this->get_language( $this->get_order_field( 'id', $order ), get_option( 'woocommerce_sf_invoice_language' ), true ),
            'signature' => true,
            'payment_info' => true,
            'bysquare' => get_option( 'woocommerce_sf_bysquare', 'yes' ) == 'yes',
        ];

        if ('multi' == get_option('woocommerce_sf_sync_type', 'single')) {
            $settings['callback_payment'] = site_url('/') . '?callback=wc_sf_order_paid&secret_key=' . get_option('woocommerce_sf_sync_secret_key', false);
        }

        $api->setInvoiceSettings($settings);




        // PAYMENT STATUS

        if ($this->order_is_paid($order) || 'yes' == get_option('woocommerce_sf_invoice_' . $type . '_' . $this->get_order_field('payment_method', $order) . '_set_as_paid', 'no')) {

            // check if proforma was already paid
            $proforma_already_paid = false;
            if ($set_invoice_data['proforma_id']) {
                $proforma = $api->invoice($set_invoice_data['proforma_id']);
                if (isset($proforma->Invoice) && 1 != $proforma->Invoice->status) {
                    $proforma_already_paid = true;
                }
            }

            if (!$proforma_already_paid) {
                $api->setInvoice([
                    'already_paid' => true,
                    'cash_register_id' => get_option('woocommerce_sf_cash_register_' . $this->get_order_field('payment_method', $order)),
                ]);
            }
        }



        // :FIX: use woocommerce tax rates
        $tax_rates = [];
        foreach ($order->get_items('tax') as $tax_item) {
            if ('WC_Order_Item_Tax' == get_class($tax_item)) {
                $tax_rates[$tax_item->get_rate_id()] = $tax_item->get_rate_percent();
            }
        }



        // ITEMS

        $items = $order->get_items(); // WC_Order_Item_Product[]

        foreach ($items as $item_id => $item) {
            $product = $item->get_product();

            $processed_item_meta = $item['item_meta'];

            // remove meta from WooCommerce Product Add-Ons plugin
            unset($processed_item_meta['product_extras']);

            // compatibility with N-Media WooCommerce PPOM plugin
            if (function_exists('ppom_woocommerce_order_key')) {
                $processed_item_meta = [];
                foreach ($item['item_meta'] as $meta_key => $meta_value) {
                    $meta_key = ppom_woocommerce_order_key($meta_key, null, $item);
                    $processed_item_meta[$meta_key] = html_entity_decode(strip_tags($meta_value));
                }
            }

            $item_meta = new WC_Order_Item_Meta($processed_item_meta);

            /* :FIX: use woocommerce tax rates
            $item_subtotal = $order->get_item_subtotal($item, false, false);
            $item_tax = ($item_subtotal > 0) ? round(($item['line_subtotal_tax'] / max( 1, $item['qty'])) / $item_subtotal * 100) : 0;
            */
            $item_tax = 0;
            $taxes = $item->get_taxes();
            foreach ($taxes['subtotal'] as $rate_id => $tax) {
                if (empty($tax)) {
                    continue;
                }
                $item_tax = $tax_rates[$rate_id];
            }

            $quantity = $item['qty'];

            // subtract refunded items quantity
            if ('yes' == get_option('woocommerce_sf_product_subtract_refunded_qty', 'no')) {
                $quantity -= abs($order->get_qty_refunded_for_item($item_id));

                // skip item if whole quantity was refunded
                if ($quantity <= 0) {
                    continue;
                }
            }

            $item_data = [
                'name'        => html_entity_decode($item['name']),
                'quantity'    => $quantity,
                'sku'         => $product->get_sku(),
                'unit'        => 'ks',

                /* :FIX: use woocommerce tax rates
                'unit_price'  => $order->get_item_subtotal($item, true) / (1 + $item_tax / 100),
                */
                'unit_price'  => $order->get_item_subtotal($item, false, false),

                'tax'         => $item_tax,
            ];

            if ('per_item' == get_option('woocommerce_sf_coupon_invoice_items', 'total')) {
                $item_discount = $order->get_item_subtotal($item, true, false) - $order->get_item_total($item, true, false);
                if ($item_discount) {
                    $item_discount_percent = $item_discount / $order->get_item_subtotal($item, true, false) * 100;
                    $item_data['discount'] = $item_discount_percent;
                    $item_data['discount_description'] = __(get_option('woocommerce_sf_discount_name', 'Zľava'), 'wc-superfaktura');

                    $discount_description = $this->get_discount_description($order);
                    if ($discount_description) {
                        $item_data['discount_description'] .= ', ' . $discount_description;
                    }
                }
            }

            $product_id = (isset($item['variation_id']) &&  $item['variation_id'] > 0) ? $item['variation_id'] : $item['product_id'];
            $product = wc_get_product($product_id);

            $attributes = ($item_meta->meta) ? $item_meta->display(true, true, '_', ', ') : '';
            $non_variations_attributes = $this->get_non_variations_attributes($item['product_id']);
            if ($product->is_type('variation')) {
                $variation = $this->convert_to_plaintext($product->get_description());

                $parent_product = wc_get_product($item['product_id']);
                $short_descr = $this->convert_to_plaintext($parent_product->get_short_description());
            }
            else {
                $variation = '';
                $short_descr = $this->convert_to_plaintext($product->get_short_description());
            }

            $template = get_option('woocommerce_sf_product_description', $this->product_description_template_default);

            $item_data['description'] = strtr( $template, [
                '[ATTRIBUTES]' => $attributes,
                '[NON_VARIATIONS_ATTRIBUTES]' => $non_variations_attributes,
                '[VARIATION]' => $variation,
                '[SHORT_DESCR]' => $short_descr,
                '[SKU]' => $product->get_sku(),
            ]);

            // compatibility with WooCommerce Wholesale Pricing plugin
            $wprice = get_post_meta($product->get_id(), 'wholesale_price', true);

            if (!$wprice && $product->is_on_sale()) {
                $tax = 1 + ((wc_get_price_excluding_tax($product) == 0) ? 0 : round(((wc_get_price_including_tax($product) - wc_get_price_excluding_tax($product)) / wc_get_price_excluding_tax($product)), 2));
                $discount = $product->get_regular_price() - $product->get_sale_price();

                if ('yes' == get_option('woocommerce_sf_product_description_show_discount', 'yes') && $discount) {
                    $item_data['description'] = trim($item_data['description'] . PHP_EOL . __(get_option('woocommerce_sf_discount_name', 'Zľava'), 'wc-superfaktura') . ' -' . $discount . ' ' . html_entity_decode(get_woocommerce_currency_symbol()));
                }
            }

            // accounting
            $item_type_product = get_option('woocommerce_sf_item_type_product');
            if ($item_type_product) {
                $item_data['AccountingDetail']['type'] = $item_type_product;
            }

            $analytics_account_product = get_option('woocommerce_sf_analytics_account_product');
            if ($analytics_account_product) {
                $item_data['AccountingDetail']['analytics_account'] = $analytics_account_product;
            }

            $synthetic_account_product = get_option('woocommerce_sf_synthetic_account_product');
            if ($synthetic_account_product) {
                $item_data['AccountingDetail']['synthetic_account'] = $synthetic_account_product;
            }

            $preconfidence_product = get_option('woocommerce_sf_preconfidence_product');
            if ($preconfidence_product) {
                $item_data['AccountingDetail']['preconfidence'] = $preconfidence_product;
            }

            $item_data = apply_filters('sf_item_data', $item_data, $order);

            $api->addItem($item_data);
        }



        // FEES

        if ($order->get_fees()) {
            foreach ($order->get_fees() as $fee) {
                $fee_total = $fee->get_total();
                $fee_taxes = $fee->get_taxes();
                $fee_tax_total = array_sum($fee_taxes['total']);

                $item_data = [
                    'name'              => $fee['name'],
                    'quantity'          => '',
                    'unit'              => '',
                    'unit_price'        => $fee_total,
                    'tax'               => (0 == $fee_total) ? 0 : round(($fee_tax_total / $fee_total) * 100),
                ];

                // accounting
                $item_type_fees = get_option('woocommerce_sf_item_type_fees');
                if ($item_type_fees) {
                    $item_data['AccountingDetail']['type'] = $item_type_fees;
                }

                $analytics_account_fees = get_option('woocommerce_sf_analytics_account_fees');
                if ($analytics_account_fees) {
                    $item_data['AccountingDetail']['analytics_account'] = $analytics_account_fees;
                }

                $synthetic_account_fees = get_option('woocommerce_sf_synthetic_account_fees');
                if ($synthetic_account_fees) {
                    $item_data['AccountingDetail']['synthetic_account'] = $synthetic_account_fees;
                }

                $preconfidence_fees = get_option('woocommerce_sf_preconfidence_fees');
                if ($preconfidence_fees) {
                    $item_data['AccountingDetail']['preconfidence'] = $preconfidence_fees;
                }

                $api->addItem($item_data);
            }
        }



        // SHIPPING

        $shipping_price = $this->get_shipping_total($order) + $order->get_shipping_tax();

        /* :FIX: use woocommerce tax rates
        $shipping_tax = ($shipping_price > 0) ? round($order->get_shipping_tax() / $this->get_shipping_total($order) * 100) : 0;
        */
        $shipping_tax = 0;
        foreach($order->get_items('tax') as $tax_item) {
            if (!empty($tax_item->get_shipping_tax_total())) {
                $shipping_tax = $tax_item->get_rate_percent();
            }
        }

        $shipping_item_name = ($shipping_price > 0) ? __(get_option('woocommerce_sf_shipping_item_name', 'Poštovné'), 'wc-superfaktura') : __(get_option('woocommerce_sf_free_shipping_name'), 'wc-superfaktura');

        if ($shipping_item_name) {
            $item_data = [
                'name'              => $shipping_item_name,
                'quantity'          => '',
                'unit'              => '',
                'unit_price'        => $shipping_price / (1 + $shipping_tax / 100),
                'tax'               => $shipping_tax,
            ];

            // accounting
            $item_type_shipping = get_option('woocommerce_sf_item_type_shipping');
            if ($item_type_shipping) {
                $item_data['AccountingDetail']['type'] = $item_type_shipping;
            }

            $analytics_account_shipping = get_option('woocommerce_sf_analytics_account_shipping');
            if ($analytics_account_shipping) {
                $item_data['AccountingDetail']['analytics_account'] = $analytics_account_shipping;
            }

            $synthetic_account_shipping = get_option('woocommerce_sf_synthetic_account_shipping');
            if ($synthetic_account_shipping) {
                $item_data['AccountingDetail']['synthetic_account'] = $synthetic_account_shipping;
            }

            $preconfidence_shipping = get_option('woocommerce_sf_preconfidence_shipping');
            if ($preconfidence_shipping) {
                $item_data['AccountingDetail']['preconfidence'] = $preconfidence_shipping;
            }

            $api->addItem($item_data);
        }



        // DISCOUNT

        if ('total' == get_option('woocommerce_sf_coupon_invoice_items', 'total')) {
            if ($order->get_total_discount()) {

                /* :FIX: use woocommerce tax rates
                $discount_price = $order->get_total_discount(false);
                $discount_tax = round(($order->get_total_discount(false) - $order->get_total_discount()) / $order->get_total_discount() * 100);
                */
                $discount_tax = max($tax_rates); // we use highest tax rate (in case there are several different tax rates in the order)

                $discount_description = $this->get_discount_description($order);

                $discount_data = [
                    'name'        => __(get_option('woocommerce_sf_discount_name', 'Zľava'), 'wc-superfaktura'),
                    'description' => $discount_description ? $discount_description : '',
                    'quantity'    => '',
                    'unit'        => '',

                    /* :FIX: use woocommerce tax rates
                    'unit_price'  => ($discount_price / (1 + $discount_tax / 100)) * -1,
                    */
                    'unit_price'  => $order->get_total_discount(false) / (1 + $discount_tax / 100) * -1,

                    'tax'         => $discount_tax,
                ];
                $discount_data = apply_filters( 'sf_discount_data', $discount_data, $order );

                $api->addItem($discount_data);
            }
        }



        // REFUNDS

        $refunds = $order->get_refunds();
        if ($refunds) {
            foreach ($refunds as $refund) {

                // get refunded amount for items by quantity
                $refund_items_price = 0;
                $refund_items_price_without_tax = 0;

                // subtract refunded amount for items by quantity, because quantity was already subtracted by get_qty_refunded_for_item() above
                if ('yes' == get_option('woocommerce_sf_product_subtract_refunded_qty', 'no')) {
                    $refunded_items = $refund->get_items();
                    if ($refunded_items) {
                        foreach ($refunded_items as $item) {
                            $refund_items_price += abs($item['qty']) * $refund->get_item_subtotal($item, true);
                            $refund_items_price_without_tax += abs($item['qty']) * $refund->get_item_subtotal($item, false);
                        }
                    }
                }

                $refund_price = abs($refund->get_total()) - $refund_items_price;

                // skip refund if whole amount was refunded with items by quantity
                if ($refund_price <= 0) {
                    continue;
                }

                $refund_price_without_tax = abs($refund->get_total()) - abs($refund->get_total_tax()) - $refund_items_price_without_tax;
                $refund_tax = round(($refund_price - $refund_price_without_tax) / $refund_price_without_tax * 100);
                $refund_description = $refund->get_reason();

                $refund_data = [
                    'name'          => __('Refunded', 'wc-superfaktura'),
                    'description'   => $refund_description ? $refund_description : '',
                    'quantity'      => '',
                    'unit'          => '',
                    'unit_price'    => $refund_price_without_tax * -1,
                    'tax'           => $refund_tax,
                ];
                $refund_data = apply_filters('sf_refund_data', $refund_data, $order);

               $api->addItem($refund_data);
            }
        }



        // 2019/05/31 webikon: added invoice items as an extra argument
        // 2020/05/07 webikon: added document type as an extra argument
        foreach (apply_filters('woocommerce_sf_invoice_extra_items', [], $order, $api->data['InvoiceItem'], $type) as $extra_item) {
            $api->addItem($extra_item);
        }



        if ($edit) {
            $api->setInvoice([
                'type' => apply_filters('woocommerce_sf_invoice_type', $type, 'edit'),
                'id' => $sf_id,
            ]);

            $response = $api->edit();
        }
        else {
            $args = [
                'type' => apply_filters('woocommerce_sf_invoice_type', $type, 'create'),
            ];

            $sequence_id = apply_filters('wc_sf_sequence_id', false, $type, $order);

            if (!$sequence_id) {
                $sequence_id = '';
            }

            if ($sequence_id) {
                $args['sequence_id'] = $sequence_id;
            }
            else {
                $invoice_id = apply_filters('wc_sf_invoice_id', false, $type, $order);

                if (!$invoice_id) {
                    $invoice_id = get_option('woocommerce_sf_invoice_custom_num')=='yes' ? $this->generate_invoice_id($order, $type) : '';
                }

                $args['invoice_no_formatted'] = $invoice_id;
            }

            $api->setInvoice($args);

            $response = $api->save();
        }



        // add log entry
        global $wpdb;

        $log_data = [
            'order_id' => $this->get_order_field('id', $order),
            'document_type' => $type,
            'request_type' => ($edit) ? 'edit' : 'create',
            'time' => current_time('mysql'),
        ];
        if (empty($response)) {
            $error = $api->getLastError();
            $log_data['response_status'] = (isset($error['status'])) ? $error['status'] : 999;
            $log_data['response_message'] = (isset($error['message'])) ? $error['message'] : "Request failed without further information.";
        }

        $wpdb->insert($table_name = $wpdb->prefix . 'wc_sf_log', $log_data);



        if ($response->error === 0) {

            // save payment link if there is one and the invoice is not paid yet
            if ($response->data->PaymentLink && 3 != $response->data->Invoice->status) {
                update_post_meta($this->get_order_field('id', $order), 'wc_sf_payment_link', $response->data->PaymentLink);
            }
            // delete payment link otherwise
            else {
                delete_post_meta($this->get_order_field('id', $order), 'wc_sf_payment_link');
            }

            // save sf document id
            $internal_id = $response->data->Invoice->id;
            update_post_meta($this->get_order_field('id', $order), 'wc_sf_internal_' . $type . '_id', $internal_id);

            //  save the formatted invoice number
            $invoice_number = $response->data->Invoice->invoice_no_formatted;
            update_post_meta($this->get_order_field('id', $order), 'wc_sf_' . $type . '_invoice_number', $invoice_number);

            // save sf pdf url
            $language = $this->get_language($this->get_order_field('id', $order), get_option('woocommerce_sf_invoice_language'), true);
            $pdf = $api::SFAPI_URL . '/' . $language . '/invoices/pdf/' . $internal_id . '/token:' . $response->data->Invoice->token;
            update_post_meta($this->get_order_field('id', $order), 'wc_sf_invoice_' . $type, $pdf);
        }
        else {
            $pdf = $order->get_view_order_url(); // ?
        }
    }



    function get_discount_description($order) {
        $coupons_codes = $order->get_used_coupons();
        if ( !$coupons_codes ) {
            return false;
        }

        $coupons = [];
        foreach( $coupons_codes as $coupon_code ) {
            $coupon = new WC_Coupon( $coupon_code );

            $sign = '';
            if ( $coupon->is_type( 'fixed_cart' ) ) {
                $sign = ' ' . $order->get_currency();
            } elseif ( $coupon->is_type( 'percent' ) ) {
                $sign = '%';
            }

            if ( 'yes' == get_option( 'woocommerce_sf_product_description_show_coupon_code', 'yes' ) ) {
                $coupons[] = $coupon_code . ' (' . $coupon->get_amount() . $sign . ')';
            }
            else {
                $coupons[] = $coupon->get_amount() . $sign;
            }
        }

        $result = 'Kupóny: ' . implode( ', ', $coupons );
        return $result;
    }



    /**
     * @param int $order_id
     * @param $woocommerce_sf_invoice_language
     * @param bool $strict
     * @return mixed|string
     */
    function get_language( $order_id, $woocommerce_sf_invoice_language, $strict = false )
    {
        $locale_map = [
            'sk' => 'slo',
            'cs' => 'cze',
            'en' => 'eng',
            'de' => 'deu',
            'ru' => 'rus',
            'uk' => 'ukr',
            'hu' => 'hun',
            'pl' => 'pol',
            'ro' => 'rom',
            'hr' => 'hrv',
            'sl' => 'slv'
        ];

        $language = $woocommerce_sf_invoice_language;
        switch ( $language ) {
            case 'locale':
                $locale = substr( get_locale(), 0, 2 );
                if ( isset( $locale_map[ $locale ] ) ) {
                    $language = $locale_map[ $locale ];
                }
                break;

            case 'wpml':
                $wpml_language = get_post_meta( $order_id, 'wpml_language', true );
                if ( isset( $locale_map[ $wpml_language ] ) ) {
                    $language = $locale_map[ $wpml_language ];
                }

                if (class_exists('sitepress')) {
                    global $sitepress;
                    $sitepress->switch_lang($wpml_language, false);
                }
                break;

            case 'endpoint':
            default:
                // nothing to do
                break;
        }

        if ($strict) {
             if ( ! in_array( $language, $locale_map ) ) {
                $language = ( 'cz' === get_option( 'woocommerce_sf_lang' ) ) ? 'cze' : 'slo';
            }
        }

        return $language;
    }


    /**
     * Get non-variation product attributes
     *
     * @since 1.6.17
     * @param int $product_id
     * @return false|string
     */
    function get_non_variations_attributes($product_id)
    {
        $attributes = get_post_meta($product_id, '_product_attributes');
        if (!$attributes) {
            return false;
        }
        $result = [];
        foreach ($attributes[0] as $attribute) {
            if ($attribute['is_variation']) {
                continue;
            }

            $result[] = $attribute['name'] . ': ' . $attribute['value'];
        }

        return implode(', ', $result);
    }

    /**
     * Save our meta data to an order.
     *
     * @since 1.0.0
     * @param int $order_id
     */
    function checkout_order_meta($order_id)
    {
        if(isset($_POST['shiptobilling']) && $_POST['shiptobilling']=='1')
            update_post_meta($order_id, 'has_shipping', '0');
        else
            update_post_meta($order_id, 'has_shipping', '1');


        if(isset($_POST['wi_as_company']) && $_POST['wi_as_company']=='1')
        {
            $valid = ['billing_company_wi_id', 'billing_company_wi_vat', 'billing_company_wi_tax'];

            foreach($valid as $attr)
            {
                if(isset($_POST[$attr]))
                    update_post_meta($order_id, $attr, esc_attr($_POST[$attr]));
            }
        }
    }

    /**
     * Add company information fields on checkout page.
     *
     * @since 1.0.0
     * @param $fields
     * @return array
     */
    function billing_fields($fields)
    {
        $required = false;
        if(get_option('woocommerce_sf_invoice_checkout_required', false)=='yes')
            $required = true;

        $new_fields = [];
        foreach($fields as $key=>$value)
        {
            // add pay as company checkbox
            if($key=='billing_company')
            {
                $new_fields['wi_as_company'] = [
                    'type' => 'checkbox',
                    'label' => __('Buy as Business client', 'wc-superfaktura'),
                    'class' => ['form-row-wide'],
                ];
            }

            $new_fields[$key] = $value;

            if($key=='billing_company')
            {
                $new_fields[$key]['required'] = $required;

                if(get_option('woocommerce_sf_invoice_checkout_id', false)=='yes')
                {
                    $new_fields['billing_company_wi_id'] = [
                        'type' => 'text',
                        'label' => __('ID #', 'wc-superfaktura'),
                        'required' => $required,
                        'class' => ['form-row-wide'],
                    ];
                }

                if(get_option('woocommerce_sf_invoice_checkout_vat', false)=='yes')
                {
                    $new_fields['billing_company_wi_vat'] = [
                        'type' => 'text',
                        'label' => __('VAT #', 'wc-superfaktura'),
                        'required' => $required,
                        'class' => ['form-row-wide'],
                    ];
                }

                if(get_option('woocommerce_sf_invoice_checkout_tax', false)=='yes')
                {
                    $new_fields['billing_company_wi_tax'] = [
                        'type' => 'text',
                        'label' => __('TAX ID #', 'wc-superfaktura'),
                        'required' => $required,
                        'class' => ['form-row-wide'],
                    ];
                }
            }
        }

        return $new_fields;
    }


    /**
     * Create tab in WooCommerce settings for this plugin.
     *
     * @since 1.8.0
     * @param array $settings
     * @return array
     */
    public function woocommerce_settings( $settings ) {
        require_once plugin_dir_path( __FILE__ ) . 'includes/class-wc-settings-superfaktura.php';
        $settings[] = new WC_Settings_SuperFaktura();
        return $settings;
    }



    function add_meta_boxes()
    {
        add_meta_box('wc_sf_invoice_box', __('Invoices', 'wc-superfaktura'), [$this, 'add_box'], 'shop_order', 'side');
    }

    /**
     * @param array $actions
     * @param WC_Order $order
     * @return array
     */
    function my_orders_actions( $actions, $order )
    {
        $pdf = get_post_meta( $this->get_order_field('id', $order), 'wc_sf_invoice_proforma', true );
        if ( $pdf ) {
            $actions['wc_sf_invoice_proforma'] = [
                'url' => $pdf,
                'name' => __( 'Proforma', 'wc-superfaktura' ),
            ];
        }
        $pdf = get_post_meta( $this->get_order_field('id', $order), 'wc_sf_invoice_regular', true );
        if ( $pdf ) {
            $actions['wc_sf_invoice_regular'] = [
                'url' => $pdf,
                'name' => __( 'Invoice', 'wc-superfaktura' ),
            ];
        }

        return $actions;
    }

    /**
     * @param WC_Order $order
     * @return bool
     */
    function sf_can_regenerate($order)
    {
        if($this->get_order_field('status',$order)=='completed')
            return false;

        if($this->get_order_field('status',$order)=='processing' && $this->get_order_field('payment_method',$order) != 'cod')
            return false;

        return true;
    }



    /**
     * @param WP_Post $post
     */
    function add_box($post) {
        $order = new WC_Order($post->ID);

        $proforma = get_post_meta($post->ID, 'wc_sf_invoice_proforma', true);
        $invoice = get_post_meta($post->ID, 'wc_sf_invoice_regular', true);
        $cancel = get_post_meta($post->ID, 'wc_sf_invoice_cancel', true);

        echo '<p><strong>' . __('View Generated Invoices', 'wc-superfaktura') . '</strong>:';
        if (empty($proforma) && empty($invoice)) {
            echo '<br>' . __('No invoice was generated', 'wc-superfaktura');
        }
        echo '</p>';



        if (!empty($proforma)) {
            echo '<p><a href="' . $proforma . '" class="button" target="_blank">' . __('Proforma', 'wc-superfaktura') . '</a></p>';
        }
        elseif (get_option('woocommerce_sf_invoice_proforma_manual', 'no') == 'yes') {
            echo '<p><a href="' . admin_url('admin.php?sf_invoice_proforma_create=1&order=' . $post->ID) . '">' . __('Create proforma invoice', 'wc-superfaktura') . '</a></p>';
        }



        if (!empty($invoice)) {
            echo '<p><a href="' . $invoice . '" class="button" target="_blank">' . __('Invoice', 'wc-superfaktura') . '</a></p>';
        }
        elseif (get_option('woocommerce_sf_invoice_regular_manual', 'no') == 'yes') {
            echo '<p><a href="' . admin_url('admin.php?sf_invoice_regular_create=1&order=' . $post->ID) . '">' . __('Create invoice', 'wc-superfaktura') . '</a></p>';
        }


        if (!empty($cancel)) {
            echo '<p><a href="' . $cancel . '" class="button" target="_blank">' . __('Credit note', 'wc-superfaktura') . '</a></p>';
        }
        elseif (!empty($invoice) && in_array($order->get_status(), ['cancelled', 'refunded'])) {
            echo '<p><a href="' . admin_url('admin.php?sf_invoice_cancel_create=1&order=' . $post->ID) . '">' . __('Create credit note', 'wc-superfaktura') . '</a></p>';
        }



        if ((!empty($proforma) || !empty($invoice)) && $this->sf_can_regenerate($order)) {
            echo '<p><a href="' . admin_url('admin.php?sfi_regen=1&order=' . $post->ID) . '">' . __('Regenerate existing invoices', 'wc-superfaktura') . '</a></p>';
        }

        // 2020/07/01 webikon: Added an action that allows to add content after the invoice button
        do_action('sf_metabox_after_invoice_generate_button', $order, $invoice);
    }



    /**
     * @param WC_Order $order
     * @param string $key
     * @return mixed|string|void
     */
    function generate_invoice_id( $order, $key = 'regular' )
    {
        $order_id = $this->get_order_field('id', $order);

        $invoice_id = get_post_meta($order_id, 'wc_sf_invoice_'.$key.'_id', true);
        if(!empty($invoice_id))
            return $invoice_id;

        $invoice_id_template = get_option('woocommerce_sf_invoice_'.$key.'_id', true);
        if(empty($invoice_id_template))
            $invoice_id_template = '[YEAR][MONTH][COUNT]';

        $num_decimals = get_option('woocommerce_sf_invoice_count_decimals', true);
        if(empty($num_decimals))
            $num_decimals = 4;

        $count = get_option('woocommerce_sf_invoice_'.$key.'_count', true);
        update_option('woocommerce_sf_invoice_'.$key.'_count', intval($count)+1);
        $count = str_pad($count, intval($num_decimals), '0', STR_PAD_LEFT);

        $date = current_time('timestamp');

        $template_tags = [
            '[YEAR]' => date( 'Y', $date ),
            '[YEAR_SHORT]' => date( 'y', $date ),
            '[MONTH]' => date( 'm', $date ),
            '[DAY]' => date( 'd', $date ),
            '[COUNT]' => $count,
            '[ORDER_NUMBER]' => $order->get_order_number(),
        ];
        $invoice_id = strtr( $invoice_id_template, $template_tags );

        $invoice_id = apply_filters('superfaktura_invoice_id', $invoice_id, $template_tags, $key);

        update_post_meta($order_id, 'wc_sf_invoice_'.$key.'_id', $invoice_id);

        return $invoice_id;
    }

    /**
     * @param string $payment_method
     * @param string $type
     * @return string
     */
    function generate_invoice_status($payment_method, $type = 'regular')
    {
        if($type!='regular' && $type!='proforma')
            $type = 'regular';

        $generate = get_option('woocommerce_sf_invoice_'.$type.'_'.$payment_method);

        // if(!in_array($generate, array('new_order', 'processing', 'completed')))
        //     $generate = false;

        return $generate;
    }

    /**
     * @param int $order_id
     */
    function sf_invoice_link_page( $order_id )
    {
        if ( get_option('woocommerce_sf_order_received_invoice_link', 'yes') == 'yes' ) {

            if ( $pdf = get_post_meta( $order_id, 'wc_sf_invoice_regular', true ) ) {
                echo "<h2>" . __('Invoice', 'wc-superfaktura') . "</h2>\n\n"
                   . '<a href="' . esc_attr( $pdf ) . '">' . esc_html( $pdf ) . "</a>\n\n";
            }
            elseif ( $pdf = get_post_meta( $order_id, 'wc_sf_invoice_proforma', true ) ) {
                echo "<h2>" . __('Proforma invoice', 'wc-superfaktura') . "</h2>\n\n"
                    . '<a href="' . esc_attr( $pdf ) . '">' . esc_html( $pdf ) . "</a>\n\n";
            }

        }
    }


    /**
     * @param int $order_id
     * @return array|bool
     */
    function get_invoice_data( $order_id )
    {
        if ( $pdf = get_post_meta( $order_id, 'wc_sf_invoice_regular', true ) ) {
            return [
                'type' => 'regular',
                'pdf' => $pdf,
                'invoice_id' => (int) get_post_meta( $order_id, 'wc_sf_internal_regular_id', true ),
            ];
        }

        if ( $pdf = get_post_meta( $order_id, 'wc_sf_invoice_proforma', true ) ) {
            return [
                'type' => 'proforma',
                'pdf' => $pdf,
                'invoice_id' => (int) get_post_meta( $order_id, 'wc_sf_internal_proforma_id', true ),
            ];
        }

        return false;
    }



    function sf_invoice_business_data_email( $order, $sent_to_admin, $plain_text ) {

        if ( get_option('woocommerce_sf_email_billing_details', 'no') == 'no' ) {
            return;
        }

        if ( $this->wc_nastavenia_skcz_activated ) {
            $plugin = Webikon\Woocommerce_Plugin\WC_Nastavenia_SKCZ\Plugin::get_instance();
            $details = $plugin->get_customer_details( $this->get_order_field( 'id', $order ) );
            $ico = $details->get_company_id();
            $ic_dph = $details->get_company_vat_id();
            $dic = $details->get_company_tax_id();
        }
        else {
            $ico = get_post_meta($this->get_order_field('id', $order), 'billing_company_wi_id', true);
            $ic_dph = get_post_meta($this->get_order_field('id', $order), 'billing_company_wi_vat', true);
            $dic = get_post_meta($this->get_order_field('id', $order), 'billing_company_wi_tax', true);
        }

        $result = '';

        if ($ico) {
            $result .= sprintf('%s: %s<br>', __('ID #', 'wc-superfaktura'), $ico);
        }

        if ($ic_dph) {
            $result .= sprintf('%s: %s<br>', __('VAT #', 'wc-superfaktura'), $ic_dph);
        }

        if ($dic) {
            $result .= sprintf('%s: %s<br>', __('TAX ID #', 'wc-superfaktura'), $dic);
        }

        if ($result) {
            echo '<p>' . $result . '</p>';
        }
    }



    function sf_payment_link_email($order, $sent_to_admin = false) {

        if ($order->get_status() == 'cancelled') {
            return;
        }

        $payment_link = get_post_meta($this->get_order_field('id', $order), 'wc_sf_payment_link', true);
        if (!$payment_link) {
            return;
        }

        if (get_option('woocommerce_sf_email_payment_link', 'yes') == 'yes') {
            echo '<h2>' . __('Online payment link', 'wc-superfaktura') . '</h2>';
            echo '<p><a href="' . $payment_link . '">' . $payment_link . '</a></p>';
        }
    }



    function sf_invoice_link_email($order, $sent_to_admin = false) {

        // filter allows to cancel invoice link
        $skip_link = apply_filters('sf_skip_email_link', false, $order);
        if ($skip_link) {
            return;
        }

        if ($order->get_status() == 'cancelled') {
            return;
        }

        if ($order->get_status() == 'completed' && get_option('woocommerce_sf_completed_email_skip_invoice', 'no') == 'yes') {
            return;
        }

        if ($this->get_order_field('payment_method', $order) == 'cod' && get_option('woocommerce_sf_cod_email_skip_invoice', 'no') == 'yes') {
            return;
        }

        $invoice_data = $this->get_invoice_data($this->get_order_field('id', $order));
        if (!$invoice_data) {
            return;
        }

        // check if proforma was already paid
        if ($invoice_data['type'] == 'proforma') {
            $proforma = $this->sf_api()->invoice($invoice_data['invoice_id']);
            if (isset($proforma->Invoice) && 1 != $proforma->Invoice->status) {
                return;
            }
        }

        if (get_option('woocommerce_sf_email_invoice_link', 'yes') == 'yes') {
            echo "<h2>" . ( ( $invoice_data['type'] == 'regular' ) ? __('Download invoice', 'wc-superfaktura') : __('Download proforma invoice', 'wc-superfaktura') ) . "</h2>\n\n";
            echo '<p><a href="' . esc_attr( $invoice_data['pdf'] ) . '">' . esc_html( $invoice_data['pdf'] ) . "</a></p>\n\n";

            if (!$sent_to_admin && !empty($invoice_data['invoice_id'])) {
                try {
                    $this->sf_api()->markAsSent($invoice_data['invoice_id'], $this->get_order_field('billing_email',$order));
                } catch (Exception $e) {
                    // do not report anything
                }
            }
        }
    }



    function sf_invoice_attachment_email ($attachments , $status, $order) {

        // filter allows to cancel pdf attachment
        $skip_attachment = apply_filters('sf_skip_email_attachment', false, $order);
        if ($skip_attachment) {
            return;
        }

        if (!($order instanceof WC_Order)) {
            return $attachments;
        }

        if ($order->get_status() == 'cancelled') {
            return $attachments;
        }

        if ($order->get_status() == 'completed' && get_option('woocommerce_sf_completed_email_skip_invoice', 'no') == 'yes') {
            return $attachments;
        }

        if ($this->get_order_field('payment_method', $order) == 'cod' && get_option('woocommerce_sf_cod_email_skip_invoice', 'no') == 'yes') {
            return $attachments;
        }

        $invoice_data = $this->get_invoice_data($this->get_order_field('id', $order));
        if (!$invoice_data) {
            return $attachments;
        }

        // check if proforma was already paid
        if ($invoice_data['type'] == 'proforma') {
            $proforma = $this->sf_api()->invoice($invoice_data['invoice_id']);
            if (isset($proforma->Invoice) && 1 != $proforma->Invoice->status) {
                return $attachments;
            }
        }

        if (get_option('woocommerce_sf_invoice_pdf_attachment') == 'yes') {
            $tmp_dir = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();
            $pdf_path = $tmp_dir . DIRECTORY_SEPARATOR . $invoice_data['invoice_id'] . '.pdf';
            file_put_contents($pdf_path, fopen($invoice_data['pdf'], 'r'));
            $attachments[] = $pdf_path;

            if (!empty($invoice_data['invoice_id'])) {
                try {
                    $this->sf_api()->markAsSent($invoice_data['invoice_id'], $this->get_order_field('billing_email',$order));
                } catch (Exception $e) {
                    // do not report anything
                }
            }
        }

        return $attachments;
    }



    /**
     * @return array
     */
    function get_order_statuses()
    {
        if ( function_exists( 'wc_order_status_manager_get_order_status_posts' ) ) // plugin WooCommerce Order Status Manager
        {
            $wc_order_statuses = array_reduce(
                wc_order_status_manager_get_order_status_posts(),
                function($result, $item)
                {
                    $result[$item->post_name] = $item->post_title;
                    return $result;
                },
                []
            );

            return $wc_order_statuses;
        }

        if ( function_exists( 'wc_get_order_statuses' ) )
        {
            $wc_get_order_statuses = wc_get_order_statuses();

            return $this->alter_wc_statuses( $wc_get_order_statuses );
        }

        $order_status_terms = get_terms('shop_order_status','hide_empty=0');

        $shop_order_statuses = [];
        if ( ! is_wp_error( $order_status_terms ) )
        {
            foreach ( $order_status_terms as $term )
            {
                $shop_order_statuses[$term->slug] = $term->name;
            }
        }

        return $shop_order_statuses;
    }

    /**
     * @param array $array
     * @return array
     */
    function alter_wc_statuses( $array )
    {
        $new_array = [];
        foreach ( $array as $key => $value )
        {
            $new_array[substr($key,3)] = $value;
        }

        return $new_array;
    }

    /**
     * @param string $id
     * @param WC_Order $order
     * @return mixed
     */
    function get_order_field( $id, $order ) {
        if ( $this->version_check() ) {
            $fn = "get_{$id}";
            return $order->$fn($order);
        } else {
            return $order->{$id};
        }
    }

    /**
     * @param WC_Order $order
     * @return mixed
     */
    function get_shipping_total( $order ) {
        if ( $this->version_check() ) {
            return $order->get_shipping_total();
        } else {
            return $order->order_shipping;
        }
    }

    /**
     * @param string $version
     * @return bool
     */
    function version_check( $version = '3.2' ) {
        if ( version_compare( WC()->version, $version, ">=" ) ) {
            return true;
        }

        return false;
    }



    /**
     * @param WC_Order $order
     * @return bool
     */
    function order_is_paid( $order ) {

        $is_paid = false;
        $set_as_paid_statuses = get_option('woocommerce_sf_invoice_set_as_paid_statuses', false);

        // WooCommerce Order Status Manager
        if (function_exists('wc_order_status_manager')) {
            $order_status = new WC_Order_Status_Manager_Order_Status($this->get_order_field('status', $order));

            if (false !== $set_as_paid_statuses) {
                if (in_array($order_status, $set_as_paid_statuses) || $order_status->is_paid()) {
                    $is_paid = true;
                }
            }

            // backward compatibility with previous options woocommerce_sf_invoice_regular_processing_set_as_paid and woocommerce_sf_invoice_regular_dont_set_as_paid
            else {

                switch ($order_status->get_slug()) {

                    case 'processing':
                        if (get_option('woocommerce_sf_invoice_regular_processing_set_as_paid', 'no') == 'yes') {
                            $is_paid = true;
                        }
                        break;

                    case 'completed':
                        if (get_option('woocommerce_sf_invoice_regular_dont_set_as_paid', 'no') == 'no') {
                            $is_paid = true;
                        }
                        break;

                    default:
                        $is_paid = $order_status->is_paid();
                        break;
                }
            }
        }

        // WooCommerce
        else {

            if (false !== $set_as_paid_statuses) {
                if (in_array($this->get_order_field( 'status', $order ), $set_as_paid_statuses)) {
                    $is_paid = true;
                }
            }

            // backward compatibility with previous options woocommerce_sf_invoice_regular_processing_set_as_paid and woocommerce_sf_invoice_regular_dont_set_as_paid
            else {

                switch ($this->get_order_field( 'status', $order )) {

                    case 'processing':
                        if (get_option('woocommerce_sf_invoice_regular_processing_set_as_paid', 'no') == 'yes') {
                            $is_paid = true;
                        }
                        break;

                    case 'completed':
                        if (get_option('woocommerce_sf_invoice_regular_dont_set_as_paid', 'no') == 'no') {
                            $is_paid = true;
                        }
                        break;
                }
            }
        }

        return apply_filters('woocommerce_sf_order_is_paid', $is_paid, $order);
    }



    /**
     * @param string $string
     * @return string
     */
    function convert_to_plaintext( $string )
    {
        return html_entity_decode( wp_strip_all_tags( $string ), ENT_QUOTES, get_option( 'blog_charset' ) );
    }

    /**
     * @param array $actions
     * @param WC_Order $order
     * @return array $actions
     */
    function add_custom_order_status_actions_button( $actions, $order ) {
        if (get_option('woocommerce_sf_invoice_download_button_actions', false)=='yes') {
            $action_slug = 'invoice';

            $pdf = get_post_meta($this->get_order_field('id', $order), 'wc_sf_invoice_proforma', true);
            if ($pdf) {
                $actions[$action_slug] = array(
                'url' => $pdf,
                'name' => __('Proforma', 'wc-superfaktura'),
                'action'    => $action_slug,
                );
            }
            $pdf = get_post_meta($this->get_order_field('id', $order), 'wc_sf_invoice_regular', true);
            if ($pdf) {
                $actions[$action_slug] = array(
                'url' => $pdf,
                'name' => __('Invoice', 'wc-superfaktura'),
                'action'    => $action_slug,
                );
            }
        }

        return $actions;
    }



    function add_admin_css() {
        $action_slug = "invoice"; // The key slug defined for your action button
        echo '
        <style>
        .wc-action-button-'.$action_slug.'::after {
            font-family: woocommerce !important;
            content: "\e00a" !important;
        }
        p.description .button {
            font-style: normal;
        }
        .wc-sf-api-test-loading,
        .wc-sf-api-test-ok,
        .wc-sf-api-test-fail {
            display: none;
            vertical-align: middle;
        }
        table.wc-sf-api-log {
            border-spacing: 0;
        }
        table.wc-sf-api-log th,
        table.wc-sf-api-log td {
            margin: 0;
            padding: 6px 12px;
            text-align: left;
        }
        table.wc-sf-api-log tr.odd td {
            background: #fff;
        }
        table.wc-sf-api-log tr.error td {
            color: #f00;
        }

        #woocommerce_wi_invoice_creation1-description + .form-table tr:nth-child(odd) th,
        #woocommerce_wi_invoice_creation1-description + .form-table tr:nth-child(odd) td {
            padding-bottom: 0;
        }
        #woocommerce_wi_invoice_creation1-description + .form-table tr:nth-child(even) th,
        #woocommerce_wi_invoice_creation1-description + .form-table tr:nth-child(even) td {
            padding-top: 0;
        }
        </style>
        ';
    }



    function wc_sf_api_test() {

        $api = $this->sf_api([
            'woocommerce_sf_lang' => $_POST['woocommerce_sf_lang'],
            'woocommerce_sf_email' => $_POST['woocommerce_sf_email'],
            'woocommerce_sf_apikey' => $_POST['woocommerce_sf_apikey'],
            'woocommerce_sf_company_id' => $_POST['woocommerce_sf_company_id']
        ]);

        $result = $api->getSequences();

        if (empty($result)) {
            $error = $api->getLastError();
            echo $error['message'];
        }
        else {
            echo "OK";
        }

        wp_die();
    }

}
