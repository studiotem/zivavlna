<?php

class WC_SecretKeyHelper {
    public static function generate_secret_key() {
        return str_shuffle(SHA1(random_int(PHP_INT_MIN, PHP_INT_MAX)));
    }
}
