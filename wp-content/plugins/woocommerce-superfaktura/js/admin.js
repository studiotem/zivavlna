jQuery(document).ready(function($) {

	// custom invoice numbering
	function wc_sf_show_custom_invoice_numbering(show) {
		var $items = $('.custom-invoice-numbering-item').closest('tr');
		if (show) {
			$items.show();
		}
		else {
			$items.hide();
		}
	}

	$('input[name=woocommerce_sf_invoice_custom_num]').on('click', function(e) {
		wc_sf_show_custom_invoice_numbering($(this).prop('checked'));
	});
	wc_sf_show_custom_invoice_numbering($('input[name=woocommerce_sf_invoice_custom_num]').prop('checked'));



	// add country settings
	$('body').on('click', 'a.sf-add-country-settings', function(e) {
		e.preventDefault();

		$('tbody#sf-countries').append(
			'<tr>' + $('tbody#sf-countries tr[data-name=template]').html() + '</tr>'
		);
	});



	// delete country settings
	$('body').on('click', 'a.sf-delete-country-settings', function(e) {
		e.preventDefault();

		$(this).closest('tr').remove();
	});



	// process country settings
	if ($('input[name=woocommerce_sf_country_settings]').length) {
		$('body').on('submit', 'form', function(e) {
			var country_settings = [];

			$('tbody#sf-countries tr:not([data-name=template])').each(function() {
				country_settings.push({
					'country': $(this).find('select[name=_country_country]').val(),
					'vat_id': $(this).find('input[name=_country_vat]').val(),
					'tax_id': $(this).find('input[name=_country_tax]').val(),
					'bank_account_id': $(this).find('input[name=_country_bank_account_id]').val(),
					'proforma_sequence_id': $(this).find('input[name=_country_proforma_invoice_sequence_id]').val(),
					'invoice_sequence_id': $(this).find('input[name=_country_invoice_sequence_id]').val(),
					'cancel_sequence_id': $(this).find('input[name=_country_cancel_sequence_id]').val()
				});
			});

			$('input[name=woocommerce_sf_country_settings]').val(JSON.stringify(country_settings));
		});
	}



	// test api connection
	$('a.wc-sf-api-test').on('click', function(e) {
		e.preventDefault();

		$('span.wc-sf-api-test-loading').show();
		$('span.wc-sf-api-test-ok').hide();
		$('span.wc-sf-api-test-fail').hide();
		$('span.wc-sf-api-test-fail-message').hide();

		var data = {
			'action': 'wc_sf_api_test',
			'woocommerce_sf_lang': $('input[name=woocommerce_sf_lang]:checked').val(),
			'woocommerce_sf_email': $('input[name=woocommerce_sf_email]').val(),
			'woocommerce_sf_apikey': $('input[name=woocommerce_sf_apikey]').val(),
			'woocommerce_sf_company_id': $('input[name=woocommerce_sf_company_id]').val()
		};

		jQuery.post(ajaxurl, data, function(response) {
			$('span.wc-sf-api-test-loading').hide();

			if ('OK' == response) {
				$('span.wc-sf-api-test-ok').show();
			}
			else {
				$('span.wc-sf-api-test-fail').show();
				$('span.wc-sf-api-test-fail-message').text(response).show();
			}
		});
	});

});
