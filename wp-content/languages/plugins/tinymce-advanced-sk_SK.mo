��    $      <  5   \      0  E   1     w     �  
   �     �     �     �  7   �          *     B  
   R  g   ]     �     �     �  K        R  g   Z     �     �     �     �       5     �   R  1   �     0      A  �   b     �     �     �  3     -   J  q  x  N   �	     9
  
   H
  
   S
     ^
     {
  &   �
  Y   �
                7     O  �   `     �     �       P   -     ~  p   �  "   �          -     :     U  @   k  �   �  7   [     �  "   �  �   �     g     �     �  3   �  -   �                                                                                                      #                                        
   !                 $       	       "    A stylesheet file named editor-style.css was not added by your theme. Administration Advanced Options Andrew Ozz Back to Editor Settings Context Menu Default settings restored. ERROR: All toolbars are empty. Default settings loaded. Editor Settings Enable the editor menu. Export Settings Font sizes However it may behave unexpectedly in rare cases, so test it thoroughly before enabling it permanently. Import Settings Importing of settings failed. List Style Options Load the CSS classes used in editor-style.css and replace the Formats menu. Options Please upgrade your WordPress installation or download an <a href="%s">older version of the plugin</a>. Restore Default Settings Save Changes Settings Settings import and export Settings saved. The Classic Editor (Add New and Edit posts and pages) The [Toolbar toggle] button shows or hides the second, third, and forth button rows. It will only work when it is in the first row and there are buttons in the second row. The settings are exported as a JSON encoded file. TinyMCE Advanced TinyMCE Advanced Settings Import TinyMCE Advanced requires WordPress version %1$s or newer. It appears that you are running %2$s. This can make the editor unstable. Unused Buttons Verify http://www.laptoptips.ca/ http://www.laptoptips.ca/projects/tinymce-advanced/ https://wordpress.org/plugins/classic-editor/ PO-Revision-Date: 2020-09-25 08:02:19+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n >= 2 && n <= 4) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: sk
Project-Id-Version: Plugins - Advanced Editor Tools (previously TinyMCE Advanced) - Stable (latest release)
 Súbor štýlov s názvom editor-style.css nebol pridaný do vašej šablóny. Administrácia Nastavenia Andrew Ozz Späť na nastavenie editora Kontextové menu Predvolené nastavenia boli obnovené. CHYBA: Všetky panely s nástrojmi sú prázdne. Boli načítané predvolené nastavenia. Nastavenie editora Povoliť menu editora. Exportovanie nastavení Veľkosť písma Avšak v zriedkavých prípadoch sa to môže správať neočakávane, takže to dôkladne vyskúšajte pred trvalým zapnutím. Importovanie nastavení Import nastavenia zlyhal. Možnosti štýlu zoznamu Načítať CSS triedy používané v editor-style.css a nahradiť formáty menu. Voľby Prosím aktualizujte si vašu WordPress inštaláciu alebo si stiahnite <a href="%s">staršiu verziu modulu</a>. Obnovenie predvolených nastavení Uložiť nastavenia nastaveniach Import a export nastavení Nastavenia uložené. Klasický editor (Pridať nové a editovať články a stránky) [Toolbar toggle] tlačidlo zobrazí alebo skryje druhý, tretí a štvrtý riadok tlačidiel. Toto bude fungovať len ak bude v prvom riadku a v druhom riadku sú tlačidlá. Nastavenia sú exportované ako JSON kódovaný súbor. TinyMCE Advanced TinyMCE Advanced import nastavení TinyMCE Advanced vyžaduje WordPress verziu %1$s alebo novšiu. Zdá sa, že používate verziu %2$s. Toto môže spôsobiť nestabilné správanie sa editoru. Nepoužívané tlačidlá Overiť http://www.laptoptips.ca/ http://www.laptoptips.ca/projects/tinymce-advanced/ https://wordpress.org/plugins/classic-editor/ 