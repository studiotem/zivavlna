<?php
global $query_shop;
$atts['orderby']= get_option('woocommerce_default_catalog_orderby');
$atts['order'] 	= 'asc';  
$query_shop 	= new WP_Query( $atts ); 
?>
<div class="products_shortcode_wrap items-grid type-tabShop main-content">
    <ul class="products-block row clearfix" data-pages="<?php echo esc_attr($query_shop->max_num_pages);?>">
        <?php while ( $query_shop->have_posts() ){
            $query_shop->the_post();?>
            <li <?php post_class('col-item col-md-4 col-sm-6'); ?>>
                <?php wc_get_template_part( 'layouts/content-product-grid'); ?>
            </li>
        <?php }
        wp_reset_postdata(); ?>
    </ul>
    <?php if ( empty($atts['link']) ) {
        nano_pagination(3, $query_shop);
    }else{
	    $link = explode('|', str_replace(['url:', 'title:'], '', urldecode($atts['link'])));
	    if($link){
	    	echo '<a href="'.urldecode($link[0]).'" class="btn">'.$link[1].'</a>';
	    }
    } ?> 
</div>