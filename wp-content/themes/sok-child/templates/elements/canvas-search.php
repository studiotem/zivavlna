<div class="search-wrap search-box canvas-warp sok-hidden">
    <div class="canvas-header clearfix">
        <span class="btn-close close pull-right"><i class="ion-android-close"></i></span>
    </div>
    <div class="canvas-inner">
        <h5 class="search-title"><?php echo esc_html__('What are you looking for?','sok');?></h5>
        <?php
        if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            if ( function_exists( 'aws_get_search_form' ) ) {
                aws_get_search_form();
            }else{
                 get_product_search_form();
            }
        }
        else
            get_search_form();
        ?>
    </div>
</div>