<?php
$sok_user          = get_theme_mod('sok_user',false);
?>
<div class="login-wrap login-box canvas-warp sok-hidden">
    <div class="canvas-header clearfix">
        <span class="btn-close close pull-right"><i class="ion-android-close"></i></span>
    </div>
    <div class="login-inner">
        <?php if (!is_user_logged_in() && $sok_user): ?>
            <?php wc_get_template( 'myaccount/form-login-sidebar.php' ); ?> 
        <?php else: ?>

        <div class="logined">
            <div class="my-account clearfix">
                <div class="author-img avatar">
                    <?php echo get_avatar( wp_get_current_user()->user_email,50); ?>
                </div>
                <div class="name-account">
                    <?php
                    echo sprintf( esc_html__( 'Hello %s', 'sok' ),
                        '<strong>'.wp_get_current_user()->display_name.'</strong>'
                    );
                    ?>
                </div>
            </div>
            <?php do_action( 'woocommerce_account_navigation' ); ?>
        </div>

        <?php endif; ?>

    </div>
</div>