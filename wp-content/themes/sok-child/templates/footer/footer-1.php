<footer id="na-footer" class="na-footer footer-1">
    <?php  if(is_active_sidebar( 'footer-top' ) && is_front_page() ){ ?>
        <div class="footer-top clearfix">
            <div class="container-inner">
                <?php dynamic_sidebar('footer-top'); ?>
            </div>
        </div>
    <?php }?>
    <?php if(is_active_sidebar( 'footer-1' )){ ?>
        <div class="footer-center clearfix">
            <div class="container">
                <div class="container-inner">
                    <div class="row flx">

                        <div class="col col-md-3">
                            <?php dynamic_sidebar('footer-1'); ?>
                        </div>
                        <div class="col col-md-3">
                            <?php dynamic_sidebar('footer-2'); ?>

                        </div>
                        <div class="col col-md-3">
                            <?php dynamic_sidebar('footer-3'); ?>
                        </div>  
                                                                                         
                    </div>
                        <div class="copyright-footer">
                          
							<?php if(get_theme_mod('sok_copyrights_config')) { ?> 
                                <div class="footer-bottom-center" style="text-align:left;">
                                    <?php if(get_theme_mod('sok_copyright_text')) {?>
                                        <span><?php echo do_shortcode(wp_kses_post(get_theme_mod('sok_copyright_text'))); ?></span>
                                    <?php } ?>
                                </div>  
							<?php } ?>
                                                 
                        </div>                    
                </div>
            </div>
        </div>
    <?php } ?> 
</footer>