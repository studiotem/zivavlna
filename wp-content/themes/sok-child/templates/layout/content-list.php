<?php
/**
 * The default template for displaying content
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
$format = get_post_format();
$add_class='';
$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "sok-blog-list" );
$placeholder_image = get_template_directory_uri(). '/assets/images/layzyload-list.jpg';
?>
<article <?php post_class('post-item post-list clearfix'); ?>>

    <div class="entry-header clearfix">
        <div class="article-meta">
            <div class="meta-author">
                <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
                    <?php echo esc_attr(get_the_author()); ?>
                </a>
            </div>

            <div class="meta-date">
                <?php sok_entry_date(); ?>
            </div>

            <div class="meta-cat">
                <?php echo sok_category(', '); ?>
            </div>
        </div>
        <?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
    </div>

    <?php if(has_post_thumbnail()) : ?>
        <div class="article-image">
            <div class="post-image">
                <a href="<?php echo get_permalink() ?>">
                    <img  class="lazy" src="<?php echo esc_url($placeholder_image);?>"  data-lazy="<?php echo esc_attr($thumbnail_src[0]);?>" data-src="<?php echo esc_attr($thumbnail_src[0]);?>" alt="<?php esc_attr_e('Post Image','sok')?>"/>
                </a>
            </div>

            <?php if(has_post_format('gallery')) : ?>
                <?php $images = get_post_meta( get_the_ID(), '_format_gallery_images', true );?>
                <?php if($images) : ?>
                    <div class="post-gallery">
                        <a  href="<?php echo get_post_format_link( $format ); ?>" class="post-format"><i class="ti-camera"></i></a>
                    </div>
                <?php endif; ?>

            <?php elseif(has_post_format('video')) : ?>
                <div class="post-video">
                    <a  href="<?php echo get_post_format_link( $format ); ?>" class="post-format"><i class="ti-control-play"></i></a>
                </div>
            <?php elseif(has_post_format('audio')) : ?>
                <div class="post-audio">
                    <a href="<?php echo get_permalink() ?>"><?php the_post_thumbnail('sok-blog-grid'); ?></a>
                    <a  href="<?php echo get_post_format_link( $format ); ?>" class="post-format"><i class="ti-headphone"></i></a>
                </div>
            <?php elseif(has_post_format('quote')) :?>
                <div class="post-quote <?php echo esc_attr($add_class);?>">
                    <?php $sp_quote = get_post_meta( get_the_ID(), '_format_quote_source_name', true ); ?>
                    <a href="<?php echo get_permalink() ?>"><?php the_post_thumbnail('sok-blog-grid'); ?></a>
                    <a  href="<?php echo get_post_format_link( $format ); ?>" class="post-format"><i class="ti-quote-left"></i></a>
                </div>
            <?php endif; ?>
        </div>
    <?php else :
        $add_class='full-width';
    endif; ?>
    <div class="article-content <?php echo esc_attr($add_class);?>">
        <div class="entry-content">
            <?php
                if ( has_excerpt()){
                    sok_excerpt();
                }
                else{
                    echo sok_content(32);
                }
                wp_link_pages( array(
                    'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'sok' ) . '</span>',
                    'after'       => '</div>',
                    'link_before' => '<span class="page-numbers">',
                    'link_after'  => '</span>',
                    'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'sok' ) . ' </span>%',
                    'separator'   => '<span class="screen-reader-text">, </span>',
                ) );
            ?>

        </div>
        <a class="btn-readmore" href="<?php echo get_permalink() ?>"><?php esc_html_e('Read More','sok') ?></a>
    </div>

</article>
