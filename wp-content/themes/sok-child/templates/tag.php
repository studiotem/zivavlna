<?php if (has_tag()) { ?> 
    <div class="meta-cat"> 
        <span class="byline"><?php echo esc_html__('Tagy','sok-child');?></span>    
		<?php the_tags(' ', ', ', ' '); ?>
	</div>
<?php } ?>