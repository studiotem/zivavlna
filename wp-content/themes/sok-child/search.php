<?php
/**
 * The template for displaying Category pages
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

get_header(); ?>
<?php if ( have_posts() ) : ?>
    <header class="page-header">
        <h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'sok' ), get_search_query() ); ?></h1>
    </header><!-- .page-header -->
	<div class="wrap-content container" role="main">
		<div class="row content-category">
            <div class="main-content col-md-12 col-lg-12">
				<div class="row archive-blog">
					<div class="affect-isotope">
						<?php get_template_part( 'loop' );?>
					</div>
				</div>
			<?php
			the_posts_pagination( array(
				'prev_text'          => '<i class="ion-ios-arrow-thin-left"></i>',
				'next_text'          => '<i class="ion-ios-arrow-thin-right"></i>',
				'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
			) );
			?>
            </div>
		</div>

	</div>
<?php  else :?>
    <div class="wrap-content container" role="main">
        <div class="row content-category">
            <div class="main-content col-md-12 col-lg-12">
                <?php  get_template_part( 'content', 'none' );?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php get_footer();
