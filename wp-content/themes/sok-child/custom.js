(function($){ 
    jQuery(document).on('click tap','.cart-panel .btn-close', function(){

        if( jQuery('body').hasClass('cart-box-open') ) {
            //jQuery('body').addClass('cart-box-open');
            console.log('cart-box-open');
            jQuery('body').removeClass('cart-box-open');
        } else {
            //jQuery('body').removeClass('cart-box-open');
        }
                
    });
	
    jQuery(document).ready(function($) { 
	     
	    if(jQuery('.single-product .woocommerce-notices-wrapper').length){ 
		    $(".single-product .woocommerce-notices-wrapper").prependTo(".sticky-product-detail").fadeIn();
		    $(".single-product .woocommerce-notices-wrapper").delay(5000).fadeOut();
		    
	    }
	     
        jQuery('.single-autors-product').each(function() {
            var sefl= $(this);
            sefl.find('.show-autors').on( 'click', function(){
                jQuery('.single-autors-product-inner').slideToggle(200);
                sefl.find('.show-autors').toggleClass('open');
            });
        });
        
        //var lang = jQuery('#billing_country').val(); 
        //jQuery('.woocommerce-checkout .checkout-review th.zasikovna-ico img').attr('src', location.protocol + "//" + location.host+'/wp-content/themes/sok-child/images/zasilkovna-'+lang+'.png').show();
     
		function checkForBusiness(){
			if($('.form-row.for-business input[name="for_business"]').is(':checked')){
				$('#billing_company_field, #billing_ic_field, #billing_dic_field, #billing_dic_dph_field').fadeIn();
			}else{ 	
				$('#billing_company_field, #billing_ic_field, #billing_dic_field, #billing_dic_dph_field').fadeOut();	
			}
		}
	  
	    function based_on_country( country ) {  
	 
	        switch( country ) {
	            case 'SK':
	                $('#billing_dic_dph_field').show();
	                break;
	            case 'CZ':
	                $('#billing_dic_dph_field').hide(); 
	                break;
	            default:
	                $('#billing_dic_dph_field').hide();
	        }
	
	    }
	 
        based_on_country( $('#billing_country').val() );
        
        $( 'body' ).bind( 'country_to_state_changing', function( event, country, wrapper ){            
            based_on_country( country );
        });

		checkForBusiness();
		
		$( '.form-row.for-business input[name="for_business"]' ).on( 'change', function( event ) {	
			checkForBusiness();	
			based_on_country( $('#billing_country').val() );
		});
		
		jQuery( ".woocommerce-checkout #payment ul.payment_methods li label" ).each(function() { 
		    var element = $(this);
		    element.html(element.html().replace(":", "")); 
		});
		
		jQuery( "tr.woocommerce-shipping-totals.shipping ul.woocommerce-shipping-methods li label" ).each(function() { 
		    var element = $(this);
		    element.html(element.html().replace(":", "")); 
		}); 
		 
    });   
     
	jQuery( document ).ajaxComplete(function() {  
		 
		jQuery( ".woocommerce-checkout #payment ul.payment_methods li label" ).each(function() { 
		    var element = $(this);
		    element.html(element.html().replace(":", "")); 
		});
		
		jQuery( "tr.woocommerce-shipping-totals.shipping ul.woocommerce-shipping-methods li label" ).each(function() { 
		    var element = $(this);
		    element.html(element.html().replace(":", "")); 
		}); 
		
		//var lang = jQuery('#billing_country').val(); 
		//jQuery('.woocommerce-checkout .checkout-review th.zasikovna-ico img').attr('src', location.protocol + "//" + location.host+'/wp-content/themes/sok-child/images/zasilkovna-'+lang+'.png').show();  
	
	});

	$(document).ready(function () {
		if ($('.info-popup').length > 0) {
			let popupWindow = $('.info-popup');
			let closePopupWindow = $('.close-popup');
			let popUpCookie = getCookie("popUpInfo");

			if(popUpCookie != 1 ) {
				setTimeout(function () {
					popupWindow.fadeIn();
				}, 3000);
			}


			function setCookie(cname,cvalue) {
				let date = new Date();
				date.setTime(date.getTime() + (5 * 60 * 60 * 1000));
				let expires = "expires=" + date.toGMTString();
				document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
				console.log(date.toGMTString());
			}

			function getCookie(cname) {
				let name = cname + "=";
				let decodedCookie = decodeURIComponent(document.cookie);
				let ca = decodedCookie.split(';');
				for(let i = 0; i < ca.length; i++) {
					let c = ca[i];
					while (c.charAt(0) === ' ') {
						c = c.substring(1);
					}
					if (c.indexOf(name) === 0) {
						return c.substring(name.length, c.length);
					}
				}
				return "";
			}

			function checkCookie() {
				let user = getCookie("popUpInfo");
				if (user === "") {
					setCookie("popUpInfo", 1);
				}
			}

			closePopupWindow.on('click', function () {
				popupWindow.fadeOut();
				checkCookie();
			});
		}
	})
	
})(jQuery);