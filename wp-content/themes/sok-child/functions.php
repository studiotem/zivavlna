<?php 
require_once(get_stylesheet_directory() .'/inc/widgets/social.php');
require_once(get_stylesheet_directory() .'/inc/vc.php');
require_once(get_stylesheet_directory() .'/inc/woocommerce.php');
	
function ddp($string){
	echo '<pre>';
	print_r($string);
	echo '</pre>';
}

function sok_child_theme_styles() {
    wp_enqueue_style( 'sok-child-theme', get_stylesheet_directory_uri() . '/style.css?v='.rand(0,50000) ); 
    wp_enqueue_script('sok-custom', get_stylesheet_directory_uri() . '/custom.js', array('jquery'),null, true);
	wp_deregister_script( 'sok-slick' );
    wp_register_script( 'sok-slick', get_stylesheet_directory_uri().'/assets/js/dev/sok-slick.js', array('jquery'),null, true );  
}
add_action( 'wp_enqueue_scripts', 'sok_child_theme_styles', 1000 );
	 
function remove_some_widgets(){ 
    //unregister_sidebar( 'footer-2' );
    //unregister_sidebar( 'footer-3' );
    unregister_sidebar( 'footer-4' );
}
add_action( 'widgets_init', 'remove_some_widgets', 11 );

function iconic_remove_password_strength() {
	wp_dequeue_script( 'wc-password-strength-meter' );
}
add_action( 'wp_print_scripts', 'iconic_remove_password_strength', 10 );

function remove_update_notifications($value) {
    if ( isset( $value ) && is_object( $value ) ) {
        unset($value->response[ 'js_composer_theme/js_composer.php' ]);
    }
}
add_filter('site_transient_update_plugins', 'remove_update_notifications');

add_action('admin_init', function () { 
    global $pagenow; 
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    } 
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); 
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
}); 

add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2); 
add_filter('comments_array', '__return_empty_array', 10, 2); 
add_filter('show_admin_bar', '__return_false');
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
}); 
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme',
		'menu_title'	=> 'Website settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position'		=> '50',
		'icon_url'		=> 'dashicons-screenoptions'
	));
}

if ( is_admin() ) {
	function remove_revolution_slider_meta_boxes() {
		remove_meta_box( 'mymetabox_revslider_0', 'page', 'normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'post', 'normal' );
	}
	add_action( 'do_meta_boxes', 'remove_revolution_slider_meta_boxes' );
}

function halena_year_shortcode() {
   return date('Y');
}
add_shortcode( 'year', 'halena_year_shortcode' );

function sok_custom_woocommerce_checkout_fields( $fields ) 
{
    $fields['order']['order_comments']['placeholder'] 	= '';
    $fields['order']['order_comments']['label'] 		= __('Poznámka k objednávke', 'sok-child');

    return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'sok_custom_woocommerce_checkout_fields' );
 
add_filter('gettext', 'translate_reply');
add_filter('ngettext', 'translate_reply');

function translate_reply($translated) { 
    $translated = str_ireplace('Doručenie', 'Dodanie', $translated);
    $translated = str_ireplace('Vyskytli sa problémy s položkami vo vašom košíku. Pred pokračovaním do pokladne sa vráťte sa späť do košíka a pokúste sa to napraviť.', 'Tovar je pravdepodobne už vypredaný, posledný zákazník dokončuje objednávku. Ak ju nedokončí, tovar sa vráti naspäť do predaja.', $translated);
	return $translated;
}
 
add_action( 'woocommerce_no_products_found', function(){
    remove_action( 'woocommerce_no_products_found', 'wc_no_products_found', 10 ); 
    $message = __( 'Je nám ľúto, aktuálne tu nič nemáme.', 'sok-child' ); 
    echo '<p class="woocommerce-info">' . $message .'</p>';
}, 9 ); 

function addPopUpWindow() {
    $show = get_field('show_window', 'option');
    $pop_up_title = get_field('pup_up_title', 'option');
    $pop_up_text = get_field('pop_up_text', 'option');
    if ($show[0] === 'true') {
        echo '<div class="info-popup">
                <div class="info-popup-wrapper">
                    <div class="info-popup-box">
                        <h2>'. $pop_up_title .'</h2>
                        '. $pop_up_text .'
                        <span class="close-popup">X</span>
                    </div>
                </div>
               </div>';
    }
}
add_action('wp_footer', 'addPopUpWindow');


function zivavlna_theme_scripts() {
 
	wp_enqueue_style( 'zivavlna_style', get_stylesheet_directory_uri() . '/zivavlna_style.css', array(), '1.0.0' );
 
     
}

add_action( 'wp_enqueue_scripts', 'zivavlna_theme_scripts' );



