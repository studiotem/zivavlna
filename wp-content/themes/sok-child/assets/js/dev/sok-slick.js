(function($){
    "use strict";
    jQuery(document).ready(function($) {
        //horizontal
        jQuery('.product-layout-vertical').each(function(){
            jQuery('.gallery-main').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                dots: true,
                asNavFor: '.gallery-nav',
                fade: true,
            });
            jQuery('.gallery-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.gallery-main',
                focusOnSelect: true,
                arrows: false,
                infinite: false,
                vertical: true,
                verticalSwiping: true
            });

        });
        jQuery('.product_sticky').each(function(){
            jQuery('.gallery-carousel').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                fade: true,
            });

        });

        //carousel
        jQuery('.product_carousel_left').each(function() {
            var number = jQuery(this).data('number');
            jQuery('.gallery-main').slick({
                dots: false,
                lazyLoad: 'ondemand',
                slidesToShow: 2,
                arrows: true,
                slidesToScroll: 1,
                centerPadding: '25%',
                autoplay: false,
                autoplaySpeed:4000,
            });
        });

        //Show Description/Reviews
        jQuery('.single-description-product').each(function() {
            var sefl= $(this);
            sefl.find('.show-detail').on( 'click', function(){
                jQuery('.single-description-product-inner').slideToggle(200);
                sefl.find('.show-detail').toggleClass('open');
            });
        });
        jQuery('.single-shipping-product').each(function() {
            var sefl= $(this);
            sefl.find('.show-shipping').on( 'click', function(){
                jQuery('.single-shipping-product-inner').slideToggle(200);
                sefl.find('.show-shipping').toggleClass('open');
            });
        });        
        jQuery('.single-comment-product').each(function() {
            var sefl= $(this);
            sefl.find('.show-reviews').on( 'click', function(){
                jQuery('.single-comment-product-inner').slideToggle(200);
                sefl.find('.show-reviews').toggleClass('open');
            });
        });

    });
})(jQuery);