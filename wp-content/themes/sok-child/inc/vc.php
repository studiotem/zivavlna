<?php
if ( ! class_exists( 'sok_child_categories' ) ) {

	class sok_child_categories {
 
		public function __construct() { 
			add_shortcode( 'sok_child_categories', array( 'sok_child_categories', 'output' ) ); 
			if ( function_exists( 'vc_lean_map' ) ) {
				vc_lean_map( 'sok_child_categories', array( 'sok_child_categories', 'map' ) );
			} 
		}
 
		public static function output( $atts, $content = null ) { 
			extract( vc_map_get_attributes( 'sok_child_categories', $atts ) ); 
			$output = ''; 
			$args 	= array(
				'taxonomy'     	=> 'product_cat',
				'orderby'      	=> 'name',
				'show_count'   	=> 0,
				'pad_counts'   	=> 0,
				'child_of'		=> 0,
				'parent' 		=> 0,
				'hierarchical' 	=> false,
				'title_li'     	=> '',
				'hide_empty'   	=> 1
			); 
			$categories = get_categories( $args ); 
			$output .= '<div class="sok-child-categories"><ul>'; 
			foreach  ($categories as $category) {  
				$output .= '<li><a href="'.get_category_link($category).'">'.$category->name.'</a></li>';  
			}
			$output .= '</ul></div>'; 
			return $output; 
		}
 
		public static function map() {
			return array(
				'name'        => esc_html__( 'Sok Child Categories', 'locale' ),
				'description' => '',
				'base'        => 'sok_child_categories',
				'params'      => array(),
			);
		}

	}

}
new sok_child_categories;