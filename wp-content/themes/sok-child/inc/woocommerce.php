<?php

remove_action( 'sok_yith_wishlist', 'sok_wishlist_button');
remove_action('woocommerce_single_product_summary', 'sok_popup_image_product', 31);
remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');

function terms_checkbox( $args ) {
	$privacy_policy_page 	= get_option( 'wp_page_for_privacy_policy' );
	$permalink				= '';
	if( $privacy_policy_page ) {
	    $permalink 			= esc_url( get_permalink( $privacy_policy_page ) );
	} ?>
	<p class="form-row terms wc-terms-and-conditions">
		<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
			<input type="checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" name="personal" id="personal">
			<?php echo sprintf( __( 'I have read and agree to %s processing my personal data %s', 'sok' ), '<a href="'.$permalink.'" target="_blank">', '</a>' ); ?>
			<span class="required">*</span>
		</label>
		<input type="hidden" name="personal-field" value="1">
	</p>
<?php }
add_action( 'woocommerce_checkout_after_terms_and_conditions', 'terms_checkbox', 10, 1 );

function theme_woocommerce_checkout_process() {
	if ( ! isset( $_POST['personal'] ) || $_POST['personal'] !== 'on' ) {
		wc_add_notice( __( 'You must agree to the processing of your personal data to continue.', 'sok' ), 'error' );
	}
}
add_action('woocommerce_checkout_process', 'theme_woocommerce_checkout_process');

add_filter( 'woocommerce_available_payment_gateways', 'bbloomer_gateway_disable_shipping_326' );

function bbloomer_gateway_disable_shipping_326( $available_gateways ) {
	if ( ! is_admin() ) {
		$chosen_methods	= WC()->session->get( 'chosen_shipping_methods' );
		$chosen_shipping= $chosen_methods[0];
		if ( isset( $available_gateways['cod'] ) && 0 === strpos( $chosen_shipping, 'local_pickup' ) ) {
			unset( $available_gateways['trustcard'] );
			unset( $available_gateways['trustpay'] );
		}
	}
	return $available_gateways;
}

function disable_shipping_calc_on_cart( $show_shipping ) {
    if( is_cart() ) {
        return false;
    }
    return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );

function register_awaiting_shipment_order_status() {
    register_post_status( 'wc-reminder', array(
        'label'                     => 'Pripomenutie vyzdvihnutia',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Pripomenutie vyzdvihnutia (%s)', 'Pripomenutie vyzdvihnutia (%s)' )
    ) );
}
add_action( 'init', 'register_awaiting_shipment_order_status' );

function add_awaiting_shipment_to_order_statuses( $order_statuses ) {
    $new_order_statuses = array();
    foreach ( $order_statuses as $key => $status ) {
        $new_order_statuses[ $key ] = $status;
        if ( 'wc-completed' === $key ) {
            $new_order_statuses['wc-reminder'] = 'Pripomenutie vyzdvihnutia';
        }
    }
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_awaiting_shipment_to_order_statuses' );

function custom_dropdown_bulk_actions_shop_order( $actions ) {
    $actions['mark_reminder'] = 'Zmeniť stav na pripomenutie vyzdvihnutia';
    return $actions;
}
add_filter( 'bulk_actions-edit-shop_order', 'custom_dropdown_bulk_actions_shop_order', 20, 1 );

function add_expedited_order_woocommerce_email( $email_classes ) {
	require( get_stylesheet_directory() .'/inc/class-wc-pickup-reminder-email.php' );
	require( get_stylesheet_directory() .'/inc/class-wc-cancelled-email.php' );
	$email_classes['WC_Pickup_Reminder_Email'] 	= new WC_Pickup_Reminder_Email();
	$email_classes['WC_Cancelled_Email'] 		= new WC_Cancelled_Email();
	return $email_classes;
}
add_filter( 'woocommerce_email_classes', 'add_expedited_order_woocommerce_email', 10, 1);

function add_ready_to_ship_woocommerce_email_action( $email_actions ) {
	$email_actions[] = 'woocommerce_order_status_cancelled';
	$email_actions[] = 'woocommerce_order_status_reminder';
	return $email_actions;
}
add_filter( 'woocommerce_email_actions', 'add_ready_to_ship_woocommerce_email_action' );

function theme_flat_rate_shipping_by_weight_field($form_fields) {
	$form_fields['shipping_by_weight'] = array(
		'title'       => 'Cena na základe váhy',
		'type'        => 'textarea',
		'description' => 'Upresnite nastavenie cien pomocou intervalov.<br>Napríklad zadanie <b>0.5-1|4</b> znamená že cena poštovného bude 4 EUR ak váha objednávky je od 0.5kg do 1kg.<br>Intervaly musia byť oddelené novým riadkom!',
		'default'     => ''
	);
	return $form_fields;
}
add_filter('woocommerce_shipping_instance_form_fields_flat_rate', 'theme_flat_rate_shipping_by_weight_field');

function theme_flat_rate_description_field($form_fields) {
	$form_fields['description'] = array(
		'title'       => 'Popis',
		'type'        => 'text',
		'description' => '',
		'default'     => ''
	);
	return $form_fields;
}
add_filter('woocommerce_shipping_instance_form_fields_flat_rate', 'theme_flat_rate_description_field');
add_filter('woocommerce_shipping_instance_form_fields_local_pickup', 'theme_flat_rate_description_field');
add_filter('woocommerce_shipping_instance_form_fields_zasilkovna', 'theme_flat_rate_description_field');


add_filter( 'woocommerce_general_settings','hs_woo_minimum_order_settings', 10, 2 );
function hs_woo_minimum_order_settings( $settings ) {

	$settings[] = array(
		'title' => __( 'Minimum order settings', 'wc_minimum_order_amount' ),
		'type' 	=> 'title',
		'desc' 	=> 'Set the minimum order amount and adjust notifications',
		'id' 	=> 'wc_minimum_order_settings',
	);

	$settings[] = array(
		'title'    	=> __( 'Minimum order amount', 'woocommerce' ),
		'desc'      => __( 'Leave this empty if all orders are accepted, otherwise set the minimum order amount', 'wc_minimum_order_amount' ),
		'id'       	=> 'wc_minimum_order_amount_value',
		'default'  	=> '',
		'type'      => 'number',
		'desc_tip'  => true,
		'css'		=> 'width:70px;',
	);

	$settings[] = array(
		'title'    => __( 'Cart message', 'woocommerce' ),
		'desc'     => __( 'Show this message if the current order total is less than the defined minimum - for example "50".', 'wc_minimum_order_amount' ),
		'id'       => 'wc_minimum_order_cart_notification',
		'default'  => 'Your current order total is %s — your order must be at least %s.',
		'type'     => 'text',
		'desc_tip' => true,
		'css'      => 'width:500px;',
	);

	$settings[] = array(
		'title'    => __( 'Checkout message', 'woocommerce' ),
		'desc'     => __( 'Show this message if the current order total is less than the defined minimum', 'wc_minimum_order_amount' ),
		'id'       => 'wc_minimum_order_checkout_notification',
		'default'  => 'Your current order total is %s — your order must be at least %s.',
		'type'     => 'text',
		'desc_tip' => true,
		'css'      => 'width:500px;',
	);

	$settings[] = array( 'type' => 'sectionend', 'id' => 'wc_minimum_order_settings' );
	return $settings;
}

add_action( 'woocommerce_checkout_process', 'hs_wc_minimum_order_amount' );
add_action( 'woocommerce_before_cart' , 'hs_wc_minimum_order_amount' );

function hs_wc_minimum_order_amount() {
	$minimum = get_option( 'wc_minimum_order_amount_value' );
	if ($minimum) {
		if ( WC()->cart->total < $minimum ) {
			if( is_cart() ) {
				wc_print_notice(
				    sprintf( get_option( 'wc_minimum_order_cart_notification' ),
				        wc_price( WC()->cart->total ),
				        wc_price( $minimum )
				    ), 'error'
				);
			} else {
				wc_add_notice(
				    sprintf( get_option( 'wc_minimum_order_checkout_notification' ) ,
				        wc_price( WC()->cart->total ),
				        wc_price( $minimum )
				    ), 'error'
				);
		    }
		}
	}
}

add_filter( 'woocommerce_package_rates', 'woocommerce_package_rates', 10, 2 );
function woocommerce_package_rates( $rates ) {
	$cart_contents_weight				= WC()->cart->cart_contents_weight;
	if($cart_contents_weight>0){
		$cart_contents_weight			= $cart_contents_weight/1000;
		foreach($rates as $key => $rate ) {
			$price						= $rates[$key]->cost;
			$settings					= get_option( 'woocommerce_'.$rate->method_id.'_'.$rate->instance_id.'_settings' );
			if($settings && isset($settings['shipping_by_weight'])){
				$cena_dopravy_hodnoty	= $settings['shipping_by_weight'];
				$cena_dopravy_tmp		= array_values( array_filter( explode( PHP_EOL, $cena_dopravy_hodnoty ) ) );
				foreach ( $cena_dopravy_tmp as $cena_dopravy_hodnota ) {
					$explode_prices		= explode( "|", $cena_dopravy_hodnota );
					if($explode_prices && isset($explode_prices[0]) && isset($explode_prices[1])){
						$explode_weights= explode( "-", $explode_prices[0] );
						if($explode_weights && isset($explode_weights[0]) && isset($explode_weights[1])){
							$weight_from= $explode_weights[0];
							$weight_to	= $explode_weights[1];
							if($cart_contents_weight >= $weight_from && $cart_contents_weight <= $weight_to){
								$price	= $explode_prices[1];
							}
						}
					}
				}
			}
			$rates[$key]->cost = number_format( str_replace( '\r', '', $price ), 2, '.', '' );
		}
	}
	return $rates;
}

//add_action('woocommerce_cart_totals_after_order_total', 'myprefix_cart_extra_info');
function myprefix_cart_extra_info() {
	global $woocommerce;
	echo '<tr class="order-total">
		<th>' . __('Váha objednávky', 'sok-child').'</th>
		<td data-title="' . __('Váha objednávky:', 'sok-child').'"><strong><span class="amount">' . $woocommerce->cart->cart_contents_weight . ' ' . get_option('woocommerce_weight_unit') . '</span></strong> </td>
    </tr>';
}

add_filter( 'woocommerce_checkout_redirect_empty_cart', 'redirect_empty_cart' );
function redirect_empty_cart(  ){
	return false;
}

function manage_product_columns( $column ) {
    $column['menu_order'] = 'Poradie';
    return $column;
}
add_filter( 'manage_product_posts_columns', 'manage_product_columns' );

function manage_product_custom_column( $column, $post_id ) {
	if ( 'menu_order' === $column ) {
		$post = get_post($post_id);
		echo $post->menu_order;
	}
}
add_filter( 'manage_product_posts_custom_column', 'manage_product_custom_column', 10, 3 );

function order_old_invoice_backend($order){
	if($old_invoice = get_post_meta( $order->get_id(), 'old_invoice', true )){
    	echo '<p><strong>OpenCart faktúra:</strong> <a target="_blank" href="'.get_site_url().'/wp-content/uploads/invoices/' . $old_invoice . '">' . $old_invoice . '</a></p><br>';
    }
}
add_action( 'woocommerce_admin_order_data_after_billing_address', 'order_old_invoice_backend', 10, 1 );

function order_old_invoice_my_account_order_actions( $actions, $order ) {
	if($old_invoice = get_post_meta( $order->get_id(), 'old_invoice', true )){
	    $actions['invoice'] = array(
	        'url'  	=> get_site_url().'/wp-content/uploads/invoices/' . $old_invoice,
	        'name'	=> __('Faktúra', 'sok-child'),
	        'target'=> '_blank',
	    );
    }
    return $actions;
}
add_filter( 'woocommerce_my_account_my_orders_actions', 'order_old_invoice_my_account_order_actions', 10, 2 );

function status_changed_completed( $order_id, $checkout = null ) {
	global $woocommerce;
	$order = new WC_Order( $order_id );
	if($order->status == 'completed'){
		update_post_meta($order_id, 'completed_date', time());
	}
}
add_action('woocommerce_order_status_changed', 'status_changed_completed');
