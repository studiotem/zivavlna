<?php 
class WC_Cancelled_Email extends WC_Email {
 
	public function __construct() { 
		$this->id 					= 'wc_cancelled'; 
		$this->title 				= 'Zrušená objednávka'; 
		$this->description 			= ''; 
		$this->heading 				= 'Zrušená objednávka';
		$this->additional_content 	= '';
		$this->subject 				= 'Zrušená objednávka';
		$this->customer_email 		= true; 
		$this->template_html  		= 'emails/customer-cancelled-order.php';
		$this->template_plain 		= 'emails/plain/customer-cancelled-order.php'; 
		add_action( 'woocommerce_order_status_cancelled_notification', array( $this, 'trigger' ), 20, 2 );   
		parent::__construct(); 
	}
 
	public function trigger( $order_id ) { 
		if ( ! $order_id ){
			return;
		} 
		$this->object 		= new WC_Order( $order_id );  
		$order_email 		= $this->object->get_billing_email(); 
		$this->find[] 		= '{order_date}';
		$this->replace[] 	= date_i18n( woocommerce_date_format(), strtotime( $this->object->order_date ) );
		$this->find[] 		= '{order_number}';
		$this->replace[] 	= $this->object->get_order_number(); 
		$this->recipient 	= $order_email; 
		if ( ! $this->is_enabled() || ! $this->get_recipient() ){
			return;
		}  
		$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() ); 
	}
 
	public function get_content_html() {
		ob_start();
		wc_get_template( $this->template_html, array(
			'order'         	=> $this->object,
			'email_heading' 	=> $this->get_heading(),
			'additional_content'=> $this->get_additional_content()
		) );
		return ob_get_clean();
	}
 
	public function get_content_plain() {
		ob_start();
		wc_get_template( $this->template_plain, array(
			'order'         	=> $this->object,
			'email_heading' 	=> $this->get_heading(),
			'additional_content'=> $this->get_additional_content()
		) );
		return ob_get_clean();
	}
 
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled' 		=> array(
				'title'   		=> 'Enable/Disable',
				'type'    		=> 'checkbox',
				'label'   		=> 'Enable this email notification',
				'default' 		=> 'yes'
			), 
			'subject'    	=> array(
				'title'       	=> 'Subject',
				'type'        	=> 'text',
				'description' 	=> sprintf( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', $this->subject ),
				'placeholder' 	=> '',
				'default'     	=> ''
			),
			'heading'    	=> array(
				'title'       	=> 'Email Heading',
				'type'        	=> 'text',
				'description' 	=> sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.' ), $this->heading ),
				'placeholder' 	=> '',
				'default'     	=> ''
			), 
			'additional_content'=> array(
				'title'       	=> 'Dodatočný obsah',
				'type'        	=> 'textarea',
				'description' 	=> '',
				'placeholder' 	=> '',
				'default'     	=> ''
			), 
			'email_type' 	=> array(
				'title'       	=> 'Email type',
				'type'        	=> 'select',
				'description' 	=> 'Choose which format of email to send.',
				'default'     	=> 'html',
				'class'       	=> 'email_type',
				'options'     	=> array(
					'plain'	    	=> __( 'Plain text', 'woocommerce' ),
					'html' 	    	=> __( 'HTML', 'woocommerce' ),
					'multipart' 	=> __( 'Multipart', 'woocommerce' ),
				)
			)
		);
	}
} 