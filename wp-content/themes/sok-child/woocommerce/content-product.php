<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product;
global $na_count;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
    return;
}
$sok_layout_cat           =   get_theme_mod('sok_layout_cat','grid');
if(isset($_GET['layout-product'])){
    $sok_layout_cat=$_GET['layout-product'];
}

?>
<?php if($sok_layout_cat == 'duel'){?>
    <?php if($na_count == 1 || $na_count == 2 || (($na_count - 1)%4)==0 || (($na_count - 2)%4)==0 ) {
        $padding='duel-right';
    }else{
        $padding='duel-left';
    }
    if($na_count == 1 || $na_count == 2 || ((($na_count - 1)%4)==0 || (($na_count - 2)%4)==0) ) {
        $padding_top='duel-top';
    }else{
        $padding_top='duel-bottom';
    }
    ?>
        <li <?php post_class('col-item '.$padding.' '.$padding_top.' '.$sok_layout_cat.'-'.$na_count); ?>>
            <?php wc_get_template_part( 'layouts/content-product-scattered');?>
        </li>
<?php }
else{?>
    <li <?php post_class('col-item'); ?>>
        <?php wc_get_template_part( 'layouts/content-product','grid'); ?>
    </li>
<?php } ?>

<?php $na_count++;?>