<?php
/**
 * Checkout login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( is_user_logged_in() || 'no' === get_option( 'woocommerce_enable_checkout_login_reminder' ) ) {
	return;
}

$info_message  = apply_filters( 'woocommerce_checkout_login_message', esc_html__( 'Returning customer?', 'sok' ) );
$info_message .= ' <a href="#" class="showlogin">' . esc_html__( 'Click here to login', 'sok' ) . '</a>';
?>
<div class="col-md-7">
	<div class="woocommerce_checkout_login">
		<?php wc_print_notice( $info_message, 'notice' );

		woocommerce_login_form(
			array(
				'message'  => 'Ak ste u nás už nakupovali, nižšie sa môžete prihlásiť. V prípade, že si želáte nakúpiť bez registrácie, len pokračujte vo vypĺňaní. Pre registráciu nového účtu kliknite na stránku <a href="'.get_permalink( get_option('woocommerce_myaccount_page_id')).'" style="text-decoration:underline;">zákaznícky účet</a>.',
				'redirect' => wc_get_page_permalink( 'checkout' ),
				'hidden'   => true,
			)
		);?>
	</div>
</div>
