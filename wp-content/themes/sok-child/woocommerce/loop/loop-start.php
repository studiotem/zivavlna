<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */
global $woocommerce_loop;
global $wp_query;
global $na_count;

$na_count 		= 1;
$cate   		= get_queried_object();
$col            = get_theme_mod('sok_woo_product_per_row','4');
$sok_layout_cat = get_theme_mod('sok_layout_cat','grid');

if(isset($_GET['layout-product'])){
    $sok_layout_cat	= $_GET['layout-product'];
}
if(isset($_GET['col'])){
    $col			= $_GET['col'];
}

$li			= '';
$affect		= 'affect-isotopes';
if($sok_layout_cat=='masonry'){
    $col	= 3;
    $affect	= 'affect-isotope';
}elseif ($sok_layout_cat=='trans'){
    $col	= 4;
    $affect	= 'affect-isotope-cats';
    $li		= '<li class="grid-sizer"></li>';
}
elseif ($sok_layout_cat=='scattered'){
    $col	= 4;
    $affect	= 'affect-isotopes';
}
elseif ($sok_layout_cat=='duel'){
    $col	= 2;
    $affect	= 'affect-isotope';
}
if(isset($_GET['col'])){
    $col	= $_GET['col'];
}
$affect		= '';
?>
<ul class="products-block row <?php echo esc_attr($affect); ?> product-cat-<?php echo esc_attr($sok_layout_cat);?>  col<?php echo esc_attr($col)?> clearfix" data-paged="<?php echo esc_attr($wp_query->max_num_pages);?>">
    <?php echo ent2ncr($li);?>
