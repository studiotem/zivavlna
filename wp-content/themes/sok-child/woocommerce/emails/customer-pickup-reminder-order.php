<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$days		= get_field('local_pickup_days', 'option');

$completed_date		= get_post_meta($order->get_id(), 'completed_date', true);
if($completed_date){
	$completed_date = date('Y-m-d H:i:s', $completed_date);
	$date 	= date('d.m.Y', strtotime($completed_date. ' +'.$days.' days'));
}else{
	$date 	= date('d.m.Y', strtotime($order->get_date_modified(). ' +'.$days.' days'));	
}

do_action( 'woocommerce_email_header', $email_heading, '' ); ?>
 
<p><?php printf( esc_html__( 'Milá/milý %s,', 'sok' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
 
<p><?php printf( get_field('pickup_reminder_text', 'option'), esc_html( $date ) ); ?></p>
 
<?php
 
//do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, '' );
 
//do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, '' );
 
//do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, '' );
 
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}
 
do_action( 'woocommerce_email_footer', '' );