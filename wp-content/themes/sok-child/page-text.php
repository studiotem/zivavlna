<?php
/*
* Template Name: Page text 
*/
?>   
<?php
/**
 * The template for displaying pages
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

get_header();

$sok_title      = get_post_meta(get_the_ID(), 'sok_show_title',true);
$padding        = get_post_meta(get_the_ID(), 'padding_content','global');
$sok_shop_menu  =  get_post_meta(get_the_ID(), 'sok_shop_menu','');

$sok_breadcrumb =  get_post_meta(get_the_ID(),'sok_img_breadcrumb','');
$sok_breadcrumb = wp_get_attachment_url(implode(",",$sok_breadcrumb));
$style_css ='';
if($sok_breadcrumb){
    $bg_image          ="background-image:url('$sok_breadcrumb')";
    $style_css            ='style = '.$bg_image.'';
}
?>

<div class="site-main" role="main">
    <?php if( ($sok_title=== '1') || ($sok_title=='')){?>
        <div class="page-header clearfix" <?php echo esc_attr($style_css);?>>
            <?php the_title( '<h1 class="page-title">', '</h1>' );?>
        </div>
    <?php }?>
    <div class="page-content <?php echo esc_attr($padding);?> clearfix">
        <?php
        if(isset($sok_shop_menu) && !empty($sok_shop_menu)){?>
            <div class="left-fixed">
                <?php
                wp_nav_menu( array(
                    'menu'           => implode(",", $sok_shop_menu),
                    'menu_class'     => 'nav navbar-nav na-menu mega-menu',
                    'container_id'   => 'vertical-primary',
                    'walker'         => new Sok_Menu_Maker_Walker(),
                ) );?>
            </div>
        <?php } ?>
        <div class="container">
	        <div class="entry-content">
	            <?php while ( have_posts() ) : the_post();?>
	                <?php get_template_part( 'content', 'page' );
	                if ( comments_open() || get_comments_number() ) :
	                    comments_template();
	                endif;
	            endwhile; ?>
	        </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>