<?php
/**
 * @package     Over
 * @version     0.1
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$share_facebook     = get_theme_mod('sok_share_facebook',false);
$share_twitter      = get_theme_mod('sok_share_twitter',false);
$share_google       = get_theme_mod('sok_share_google',false);
$share_linkedin     = get_theme_mod('sok_share_linkedin',false);
$share_pinterest    = get_theme_mod('sok_share_pinterest',false);
?>
<?php if ($share_facebook || $share_twitter || $share_google):?>
<div class="meta-social clearfix">
    <div class="btn-share" data-toggle="modal" data-target="#btnShare">
        <span class="byline"><i class="icon icon-share"></i></span>
        <span class="posted-share "><?php echo esc_html__('Share','sok');?></span>
    </div>
    <div id="btnShare" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">

            <div class="modal-header btn-close-popup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ion-ios-close-empty"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="social-modal share-links clearfix">
                    <span class="modal-share"><?php echo esc_html__('Share','sok');?></span>
                    <header class="entry-header-title">
                        <?php  the_title( '<h3 class="entry-title">', '</h3>' ); ?>
                    </header>
                    <div class="count-share">
                        <ul class="social-icons list-unstyled list-inline">
                            <?php if ($share_facebook):?>
                                <li class="social-item facebook">
                                    <a href="http://www.facebook.com/sharer.php?url=<?php the_permalink(); ?>" title="<?php echo esc_attr__('facebook', 'sok'); ?>" class="post_share_facebook facebook" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=220,width=600');return false;">
                                        <i class="ion-social-facebook"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ($share_twitter):?>
                                <li class="social-item twitter">
                                    <a href="https://twitter.com/share?url=<?php the_permalink(); ?>" title="<?php echo esc_attr__('twitter', 'sok'); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=260,width=600');return false;" class="product_share_twitter twitter">
                                        <i class="ion-social-twitter"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ($share_google):?>
                                <li class="social-item google">
                                    <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" class="googleplus" title="<?php echo esc_attr__('google +', 'sok'); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                        <i class="ion-social-googleplus"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ($share_linkedin):?>
                                <li class="social-item linkedin">
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>&title=<?php echo esc_attr__('pinterest', 'sok'); ?>&summary=<?php echo urlencode(get_the_title()); ?>&source=<?php echo urlencode(get_the_title()); ?>">
                                        <i class="ion-social-linkedin"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ($share_pinterest):?>
                                <li class="social-item pinterest">
                                    <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php if(function_exists('the_post_thumbnail')) echo wp_get_attachment_url(get_post_thumbnail_id()); ?>&description=<?php echo urlencode(get_the_title()); ?>" title="<?php echo esc_attr__('pinterest', 'sok'); ?>" class="pinterest">
                                        <i class="ion-social-pinterest"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<?php endif; ?>

