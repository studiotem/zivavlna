<div class="post-author">
		
	<div class="author-img">
		<?php echo get_avatar( get_the_author_meta('email'), '130' ); ?>
	</div>
	
	<div class="author-content">
		<h5><?php the_author_posts_link(); ?></h5>
        <p><?php the_author_meta('description'); ?></p>

        <?php if(get_the_author_meta('facebook')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('//facebook.com/');?><?php echo the_author_meta('facebook'); ?>"><i class="ion-social-facebook"></i></a><?php endif; ?>
        <?php if(get_the_author_meta('twitter')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('//twitter.com/');?><?php echo the_author_meta('twitter'); ?>"><i class="ion-social-twitter"></i></a><?php endif; ?>
        <?php if(get_the_author_meta('instagram')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('//instagram.com/');?><?php echo the_author_meta('instagram'); ?>"><i class="ion-social-instagram"></i></a><?php endif; ?>
        <?php if(get_the_author_meta('google')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('//plus.google.com/');?><?php echo the_author_meta('google'); ?>?rel=author"><i class="ion-social-googleplus"></i></a><?php endif; ?>
        <?php if(get_the_author_meta('pinterest')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('//pinterest.com/');?><?php echo the_author_meta('pinterest'); ?>"><i class="ion-social-pinterest"></i></a><?php endif; ?>
	</div>
	
</div>