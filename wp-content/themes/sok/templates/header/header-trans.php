<?php
/**
 * @package     Sok Theme
 * @version     1.0
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2018 NanoAgency
 * @license     GPL v2
 */
$keepMenu           = str_replace('','',sok_keep_menu() );
$sok_cart          = get_theme_mod('sok_cart',true);
$sok_search        = get_theme_mod('sok_search',true);
$sok_user          = get_theme_mod('sok_user',false);

$detailLayouts      = get_theme_mod('sok_detail_layouts', 'vertical');
if(isset($_GET['detail-layouts'])){
    $detailLayouts=$_GET['detail-layouts'];
}
if($detailLayouts=='carousel' || $detailLayouts=='carousel-left'){
    $sok_breadcrumb = false;
}

?>
<div class="sok-header-placeholder  placeholder-<?php echo esc_attr($keepMenu);?>"></div>
<header id="masthead" class="site-header header-transparent header-center <?php echo esc_attr($keepMenu);?>">
    <div id="sok-header" class="sok-header">
        <!--Header Menu-->
        <div class="container-fluid">
            <div class="sok-header-content">

                <!-- Menu-->
                <div class="header-content-menu">
                    <div id="na-menu-primary" class="nav-menu clearfix">
                        <nav class="text-center na-menu-primary clearfix">
                            <?php
                            if (has_nav_menu('primary_navigation')) :
                                // Main Menu
                                wp_nav_menu( array(
                                    'theme_location' => 'primary_navigation',
                                    'menu_class'     => 'nav navbar-nav na-menu',
                                    'container'      => '',
                                ) );
                            endif;
                            ?>
                        </nav>
                    </div>
                </div>
                <!--Logo-->
                <div class="header-content-logo">
                    <?php
                    get_template_part('templates/logo');
                    ?>
                </div>
                <!--Seacrch & Cart-->
                <div class="header-content-right">
                    <?php if($sok_search):?>
                        <div class="searchform-mini">
                            <span class="btn-mini-search"><i class="icon-magnifier"></i></span>
                        </div>
                    <?php endif;?>

                    <?php if($sok_user):?>
                        <?php if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) { ?>
                            <div class="user-mini">
                                <span class="btn-user"><i class="icon-user"></i></span>
                            </div>
                        <?php } ?>
                    <?php endif;?>

                    <?php if($sok_cart):?>
                        <div class="cart-wrap">
                            <?php
                            if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
                                <?php sok_cartbox();?>
                            <?php }
                            ?>
                        </div>
                    <?php endif;?>

                    <div class="mobile-mini hidden-md hidden-lg">
                        <span class="btn-mobile"><i class="icon ion-android-menu"></i></span>
                    </div>
                </div>

            </div>
        </div><!-- .container -->
    </div>
</header>

<?php get_template_part('templates/elements/canvas-search'); ?>
<?php get_template_part('templates/elements/canvas-intro'); ?>
<?php get_template_part('templates/elements/canvas-login'); ?>
<?php get_template_part('templates/elements/header-mobile'); ?>

<!-- .site-header -->
<div id="content" class="site-content">
