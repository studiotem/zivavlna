<?php
/**
 * The template for displaying Header mobile
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

?>

<div class="mobile-wrap mobile-box canvas-warp sok-hidden">
    <div class="canvas-header clearfix">
        <span class="btn-close close pull-right"><i class="ion-android-close"></i></span>
    </div>
    <div class="mobile-inner">
            <div id="mobile-header" class="mobile-header">
                <nav class="menu-mobile-primary">
                    <nav class="na-menu-primary clearfix">
                        <?php
                        if (has_nav_menu('primary_navigation')) :
                            // Main Menu
                            wp_nav_menu( array(
                                'theme_location' => 'primary_navigation',
                                'menu_class'     => 'nav navbar-nav na-menu mega-menu',
                                'container_id'   => 'mobile-primary',
                                'walker'         => new Sok_Menu_Maker_Walker()
                            ) );
                        endif;
                        ?>
                    </nav>
                </nav>
            </div>
    </div>
</div>