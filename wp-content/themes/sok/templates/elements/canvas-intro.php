<?php if(is_active_sidebar( 'intro')){?>
<div class="intro-wrap intro-box canvas-warp sok-hidden">
    <div class="canvas-header clearfix">
        <span class="btn-close close pull-right"><i class="ion-android-close"></i></span>
    </div>
    <div class="intro-inner">
           <?php  dynamic_sidebar('intro'); ?>
    </div>
</div>
<?php } ?>