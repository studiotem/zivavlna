<?php

// ADD attributes Parallax
add_action( 'vc_after_init', 'sok_add_parallax_param' );
function sok_add_parallax_param() {
    $setting = array(
        'type' => 'dropdown',
        'heading' => esc_html__( 'Parallax', 'sok' ),
        'param_name' => 'parallax',
        'value' => array(
            esc_html__( 'None', 'sok' )      => '',
            esc_html__( 'Simple', 'sok' )    => 'content-moving',
            esc_html__( 'Image', 'sok' )     => 'content-moving-image',
            esc_html__( 'With fade', 'sok' ) => 'content-moving-fade',
        ),
        'description' => esc_html__( 'Add parallax type background for row (Note: If no image is specified, parallax will use background image from Design Options).', 'sok' ),
        'dependency' => array(
            'element' => 'video_bg',
            'is_empty' => true,
        ),
    );
    vc_add_param( 'vc_row', $setting );
}


