(function($){
    "use strict";
    jQuery(document).ready(function(){

        jQuery(".nano-showcase").each(function(){
            var number = jQuery('.nano-showcase .widgetcontent').data('number');
            var galleryThumbs  = new Swiper ('.gallery-thumbs', {
                slidesPerView: number,
                direction: "horizontal",
                loop: true,
                grabCursor: true,
                resistance : true,
                resistanceRatio : 0,
                speed:800,
                autoplay: true,
                effect: "slide",
                mousewheel: true,
                freeMode: true,
                centeredSlides: true,
                //watchSlidesVisibility: true,
                watchSlidesProgress: true,

            });
            var galleryTop = new Swiper ('.gallery-top', {
                slidesPerView: 1,
                direction: "horizontal",
                loop: true,
                grabCursor: true,
                resistance : true,
                resistanceRatio : 0,
                speed:800,
                autoplay: true,
                effect: "slide",
                mousewheel: true,
                watchSlidesProgress: true,
                thumbs: {
                    swiper: galleryThumbs
                },
                breakpoints: {                                        
                    640: {
                        slidesPerView: 'auto',
                    }
                  }
            })
        });


        jQuery(".items-carousel").each(function(){
            var rtl = jQuery(this).data('rtl');
            var number = jQuery(this).data('number');
            jQuery(this).slick({
                autoplay: true,
                dots: false,
                rtl:rtl,
                slidesToShow: number,
                autoplaySpeed: 10000,
                arrows: true,
                infinite: true,
                responsive: [
                    {
                        breakpoint: 900,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                ]
            });
        });
        jQuery(".article-carousel").each(function(){
            var number = jQuery(this).data('number');
            var dots = jQuery(this).data('dots');
            var arrows = jQuery(this).data('arrows');
            var table = jQuery(this).data('table');
            var rtl = jQuery(this).data('rtl');
            var mobile = jQuery(this).data('mobile');
            var mobilemin = jQuery(this).data('mobilemin');

            jQuery(this).slick({                
                lazyLoad: 'ondemand',
                dots: dots,
                rtl:rtl,
                slidesToShow: number,
                arrows: arrows,
                autoplaySpeed: 4000,
                responsive: [
                    {
                        breakpoint: 900,
                        settings: {
                            slidesToShow: table
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: mobile
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: mobilemin
                        }
                    }
                ]
            });
        });
        jQuery(".article-carousel-center").each(function(){
            var number = jQuery(this).data('number');
            var dots = jQuery(this).data('dots');
            var rtl = jQuery(this).data('rtl');
            var arrows = jQuery(this).data('arrows');
            jQuery(this).slick({
                dots: dots,
                lazyLoad: 'ondemand',
                slidesToShow: number,
                arrows: arrows,
                slidesToScroll: 1,
                centerPadding: '25%',
                autoplay: false,
                rtl:rtl,
                autoplaySpeed:4000,
                centerMode: true,
                // focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            variableWidth: false
                        }
                    },
                    {
                        breakpoint: 770,
                        settings: {
                            centerPadding: '15%',
                            centerMode: false,
                            variableWidth: false
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            centerPadding: '0%',
                            centerMode: false,
                            variableWidth: false
                        }
                    }
                ]

            });
        });

        //horizontal NavFor
        jQuery('.slider_horizontal').each(function(){
            var number = jQuery(this).data('number');
            jQuery('.gallery-main').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: false,
                asNavFor: '.gallery-nav',
                fade: true,
            });
            jQuery('.gallery-nav').slick({
                slidesToShow:1,
                slidesToScroll: 1,
                asNavFor: '.gallery-main',
                focusOnSelect: true,
                arrows: true,
                infinite: false
            });

        });

        //MENU ------------------------------------------------------------------------------------------------------- /

        // Sticky Menu
        var scrollTop = $(document).scrollTop();
        var contentHeight = $('.site-header').outerHeight();
        $(window).scroll(function() {
            var headerscroll = $(document).scrollTop();

            if (headerscroll > contentHeight){
                jQuery('.header-fixed').addClass('fixed');

            }else{
                jQuery('.header-fixed').removeClass('fixed');
            }
            scrollTop = $(document).scrollTop();
        });
        // Sticky Menu ------------------------------------------------------------------------------------------------/
            jQuery(".btn-mini-search").on( 'click', function(){
                jQuery(".searchform-wrap").removeClass('sok-hidden');
            });
            jQuery(".btn-mini-close").on( 'click', function(){
                jQuery(".searchform-wrap").addClass('sok-hidden');
            });


        // parallax ---------------------------------------------------------------------------------------------------/
        jQuery(".js-vc_parallax-o-image").each(function(){
            var parallax_image_src = jQuery(this).data('vc-parallax-image');
            jQuery(this).css( 'background-image', 'url(' + parallax_image_src + ')' );
            // jQuery(this).parallax("60%", 0.6);
        });

        // Quantity ---------------------------------------------------------------------------------------------------/
        jQuery(".form-inline .quantity").each(function(){
            var self    =  jQuery(this);
            var p       = self.find('.qty-plus');
            var m       = self.find('.qty-minus');
            p.on( 'click', function(){
                    self.find('.qty').val( parseInt( self.find('.qty').val()) + 1 );
                    self.find('.qty').trigger( 'change' );
            } );
            m.on( 'click', function(){
                if( parseInt(self.find('.qty').val())  > 1 ) {
                    self.find('.qty').val( parseInt( self.find('.qty').val()) - 1 );
                    self.find('.qty').trigger( 'change' );
                }
            } );
        });

        // Woo --------------------------------------------------------------------------------------------------------/
        //carousel
        jQuery(".na-carousel").each(function(){
            var number = jQuery(this).data('number');
            var auto = jQuery(this).data('auto');
            var pagination =jQuery(this).data('pagination');
            jQuery(this).slick({
                autoplay: auto,
                slidesToShow: number,
                slidesToScroll: 1,
                dots:pagination,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });

        // Canvas -----------------------------------------------------------------------------------------------------/
        var
            btnSearch       = jQuery('body').find('.btn-mini-search'),
            btnIntro        = jQuery('body').find('.btn-intro'),
            btnLogin        = jQuery('body').find('.btn-user'),
            btnMobile       = jQuery('body').find('.btn-mobile'),
            btnSidebar      = jQuery('body').find('.filter-mini'),
            mainWrapper     = jQuery('body'),
            iconClose       = jQuery('.btn-close'),
            canvasOverlay   = jQuery('.canvas-overlay');

        function offCanvas(){
            if ( canvasOverlay.hasClass('show')) {
                canvasOverlay.removeClass('show');
            }
            if ( mainWrapper.hasClass('canvas-search')) {
                $('body').removeClass('canvas-search');
            }
            if ( mainWrapper.hasClass('canvas-intro')) {
                $('body').removeClass('canvas-intro');
            }
            if ( mainWrapper.hasClass('canvas-mobile')) {
                $('body').removeClass('canvas-mobile');
            }

            if ( mainWrapper.hasClass('canvas-login')) {
                $('body').removeClass('canvas-login');
            }

            if ( mainWrapper.hasClass('canvas-filter')) {
                $('body').removeClass('canvas-filter');
            }

        }

        iconClose.on( 'click', function(){
            offCanvas();
        });
         
         
         jQuery(document).on( 'click', '.na-cart .btn-close', function(){
            if ( canvasOverlay.hasClass('show')) {
                canvasOverlay.removeClass('show');
            }
            if ( mainWrapper.hasClass('cart-box-open')) {
                jQuery('body').removeClass('cart-box-open');
            }
        });

        canvasOverlay.on( 'click', function(){
            offCanvas();
        });

        //login
        btnLogin.on( 'click', function(){
            mainWrapper.toggleClass('canvas-login');
            canvasOverlay.addClass('show');
        });
        //search
        btnSearch.on( 'click', function(){
            mainWrapper.toggleClass('canvas-search');
            canvasOverlay.addClass('show');
        });
        //intro
        btnIntro.on( 'click', function(){
            mainWrapper.toggleClass('canvas-intro');
            canvasOverlay.addClass('show');
        });
        //mobile
        btnMobile.on( 'click', function(){
            mainWrapper.toggleClass('canvas-mobile');
            canvasOverlay.addClass('show');
        });
        //filter
        btnSidebar.on( 'click', function(){
            mainWrapper.toggleClass('canvas-filter');
            canvasOverlay.addClass('show');
        });

        //cart
        jQuery(document).on( 'click','.mini-cart',function() {
            $('.canvas-overlay').addClass('show');
            $('body').toggleClass('cart-box-open');
        });
        function widgetPanelShowCart (showLoader) {
            var $this = $(this);
            $this.$widgetPanelOverlay = $('.canvas-overlay');
            $this.classWidgetPanelOpen = 'cart-box-open';
            $this.$body = $('body');
            if (showLoader) {
                $('#cart-panel-loader').addClass('show');
            }
            $this.$body.addClass($this.classWidgetPanelOpen);
            $this.$widgetPanelOverlay.addClass('show');

        }
        function widgetPanelHiddenCart () {
            var $this = $(this);
            $this.$widgetPanelOverlay = $('.canvas-overlay');
            $this.classWidgetPanelOpen = 'cart-box-open';
            $this.$body = $('body');

            $this.$body.removeClass('cart-box-open');
            $this.$widgetPanelOverlay.removeClass('show');
            jQuery("html, body").animate({ scrollTop: 0 }, 200);

        }
        function widgetPanelHideCartLoader (showLoader) {
            $('#cart-panel-loader').addClass('fade-out');
            setTimeout(function() { $('#cart-panel-loader').removeClass('fade-out show'); }, 500);
        }

        //Ajax Remove Cart ===================================
        $(document).on('click', '.product_remove', function(event) {
            var $this = $(this);
            var product_key = $this.data('cart-item-key');

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: ajax_object.ajaxurl,
                data: { action: "cart_remove_product",
                    product_key: product_key
                },success: function(data){
                    var $cart = $('.cart-box');

                    if(data.cart_count==0){
                        $cart.find('.cart_list').html('<li class="empty">'+$cart.find('.cart_container').data('text-emptycart')+'</li>');
                        $cart.find('.cart-bottom').remove();
                    }else{
                        $cart.find('.total .amount').remove();
                        $('.mini-cart-subtotal').html(data.cart_subtotal);
                        $this.parent().remove();
                    }
                    $('.text-items .mini-cart-items').html(data.cart_count);
                }
            });
            return false;
        });


        //Ajax Add to Cart ===================================
        $(document).on('click', '.add_to_cart_button', function(event) {
            widgetPanelShowCart(true);
            setTimeout(function() {
                widgetPanelHideCartLoader(true); // Args: (showLoader)
            }, 350);
        });
        //Ajax Add to Cart Detail ===================================
        $(document).on('click', '.single_add_to_cart_button', function(event) {

            var $this = $(this);
            var $productForm = $this.closest('form');

            var	data = {
                product_id:				$productForm.find("[name*='add-to-cart']").val(),
                product_variation_data: $productForm.serialize()
            };
            if (!data.product_id) {
                console.log('(Error): No product id found');
                return;
            }
            widgetPanelShowCart(true);
            $.ajax({
                type: 'POST',
                dataType: 'html',
                url: '?add-to-cart=' + data.product_id + '& ajax-add-to-cart=1',
                data: data.product_variation_data,
                cache: false,
                headers: {'cache-control': 'no-cache'},
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log('AJAX error - SubmitForm() - ' + errorThrown);
                },
                success: function(response){
                    var $response = $('<div>' + response + '</div>'),
                        $shopNotices = $response.find('#shop-notices-wrap'), // Shop notices
                        hasError = ($shopNotices.find('.woocommerce-error').length) ? true : false,
                        cartHash = '';
//                      Get replacement elements/values

                    var fragments = {
                        '.mini-cart-items': $response.find('.mini-cart-items'), // Header menu cart count
                        '#shop-notices-wrap': $shopNotices,
                        '.cart-box': $response.find('.cart-box') // Widget panel mini cart
                    };

                    // Replace elements
                    $.each(fragments, function(selector, $element) {
                        if ($element.length) {
                            $(selector).replaceWith($element);
                        }
                    });

                    setTimeout(function() {
                        widgetPanelHideCartLoader(true); // Args: (showLoader)
                    }, 550);
                    if (hasError) {
                        setTimeout(function() {
                            widgetPanelHiddenCart(true); // Args: (showLoader)
                        }, 500);
                    }
                }
            });
            return false;
        });


        var initPhotoSwipeFromDOM = function(gallerySelector) {

            // parse slide data (url, title, size ...) from DOM elements
            // (children of gallerySelector)
            var parseThumbnailElements = function(el) {
                var thumbElements = el.childNodes,
                    numNodes = thumbElements.length,
                    items = [],
                    figureEl,
                    linkEl,
                    size,
                    item;

                for(var i = 0; i < numNodes; i++) {

                    figureEl = thumbElements[i]; // <figure> element

                    // include only element nodes
                    if(figureEl.nodeType !== 1) {
                        continue;
                    }

                    linkEl = figureEl.children[0]; // <a> element

                    size = linkEl.getAttribute('data-size').split('x');

                    // create slide object
                    item = {
                        src: linkEl.getAttribute('href'),
                        w: parseInt(size[0], 10),
                        h: parseInt(size[1], 10)
                    };



                    if(figureEl.children.length > 1) {
                        // <figcaption> content
                        item.title = figureEl.children[1].innerHTML;
                    }

                    if(linkEl.children.length > 0) {
                        // <img> thumbnail element, retrieving thumbnail url
                        item.msrc = linkEl.children[0].getAttribute('src');
                    }

                    item.el = figureEl; // save link to element for getThumbBoundsFn
                    items.push(item);
                }

                return items;
            };

            // find nearest parent element
            var closest = function closest(el, fn) {
                return el && ( fn(el) ? el : closest(el.parentNode, fn) );
            };

            // triggers when user clicks on thumbnail
            var onThumbnailsClick = function(e) {
                e = e || window.event;
                e.preventDefault ? e.preventDefault() : e.returnValue = false;

                var eTarget = e.target || e.srcElement;

                // find root element of slide
                var clickedListItem = closest(eTarget, function(el) {
                    return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                });

                if(!clickedListItem) {
                    return;
                }

                // find index of clicked item by looping through all child nodes
                // alternatively, you may define index via data- attribute
                var clickedGallery = clickedListItem.parentNode,
                    childNodes = clickedListItem.parentNode.childNodes,
                    numChildNodes = childNodes.length,
                    nodeIndex = 0,
                    index;

                for (var i = 0; i < numChildNodes; i++) {
                    if(childNodes[i].nodeType !== 1) {
                        continue;
                    }

                    if(childNodes[i] === clickedListItem) {
                        index = nodeIndex;
                        break;
                    }
                    nodeIndex++;
                }



                if(index >= 0) {
                    // open PhotoSwipe if valid index found
                    openPhotoSwipe( index, clickedGallery );
                }
                return false;
            };

            // parse picture index and gallery index from URL (#&pid=1&gid=2)
            var photoswipeParseHash = function() {
                var hash = window.location.hash.substring(1),
                    params = {};

                if(hash.length < 5) {
                    return params;
                }

                var vars = hash.split('&');
                for (var i = 0; i < vars.length; i++) {
                    if(!vars[i]) {
                        continue;
                    }
                    var pair = vars[i].split('=');
                    if(pair.length < 2) {
                        continue;
                    }
                    params[pair[0]] = pair[1];
                }

                if(params.gid) {
                    params.gid = parseInt(params.gid, 10);
                }

                return params;
            };

            var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
                var pswpElement = document.querySelectorAll('.pswp')[0],
                    gallery,
                    options,
                    items;

                items = parseThumbnailElements(galleryElement);

                // define options (if needed)
                options = {

                    // define gallery index (for URL)
                    galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                    getThumbBoundsFn: function(index) {
                        // See Options -> getThumbBoundsFn section of documentation for more info
                        var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                            rect = thumbnail.getBoundingClientRect();

                        return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                    }

                };

                // PhotoSwipe opened from URL
                if(fromURL) {
                    if(options.galleryPIDs) {
                        // parse real index when custom PIDs are used
                        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                        for(var j = 0; j < items.length; j++) {
                            if(items[j].pid == index) {
                                options.index = j;
                                break;
                            }
                        }
                    } else {
                        // in URL indexes start from 1
                        options.index = parseInt(index, 10) - 1;
                    }
                } else {
                    options.index = parseInt(index, 10);
                }

                // exit if index not found
                if( isNaN(options.index) ) {
                    return;
                }

                if(disableAnimation) {
                    options.showAnimationDuration = 0;
                }

                // Pass data to PhotoSwipe and initialize it
                gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();
            };

            // loop through all gallery elements and bind events
            var galleryElements = document.querySelectorAll( gallerySelector );

            for(var i = 0, l = galleryElements.length; i < l; i++) {
                galleryElements[i].setAttribute('data-pswp-uid', i+1);
                galleryElements[i].onclick = onThumbnailsClick;
            }

            // Parse URL and open gallery if it contains #&pid=3&gid=1
            var hashData = photoswipeParseHash();
            if(hashData.pid && hashData.gid) {
                openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
            }
        };

// execute above function
        initPhotoSwipeFromDOM('.gallery-main');

        // Filter -----------------------------------------------------------------------------------------------------/
        function filter (showLoader) {
            var $this = $(this);
            $this.$widgetPanelOverlay = $('.canvas-overlay');
            $this.classWidgetPanelOpen = 'cart-box-open';
            $this.$body = $('body');

            if (showLoader) {
                $('#cart-panel-loader').addClass('show');
            }

            $this.$body.addClass($this.classWidgetPanelOpen);
            $this.$widgetPanelOverlay.addClass('show');
        }

        $('.canvas-overlay').on('click',function() {
            if ( $(this).hasClass('show') && $('body').hasClass('filter-open')) {
                $(this).removeClass('show');
                $('body').removeClass('filter-open');
            }
        });
        jQuery(document).on( 'click','.btn-mini-close',function() {        
            $('.canvas-overlay').removeClass('show');
            $('body').removeClass('filter-open');
        });


        jQuery(document).on( 'click','.btn-filter-left .btn-filter',function() {        
            $('.canvas-overlay').toggleClass('show');
            $('body').toggleClass('filter-open');
        });

        jQuery(document).on( 'click','.btn-filter-full .btn-filter',function() {
            $('.filter-full').slideToggle(100);
        });
    });

    //carousel
    jQuery('.cat-carousel').each(function() {
        var number = jQuery(this).data('col');
        jQuery(this).slick({
            dots: false,
            lazyLoad: 'ondemand',
            slidesToShow: number,
            arrows: true,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed:4000,
            responsive: [                
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]           
        });
    });
})(jQuery);



