(function($){
    "use strict";

    function sok_isotope($this){
        var $grid =$this.isotope({
            transitionDuration: '0.4s',
            masonry: {
                columnWidth:'.col-item'
            },
            fitWidth: true
        });
        $grid.imagesLoaded().progress( function() {
            $grid.isotope('layout');
        });
    }

    function sok_isotope_percentage($this){
        var $grid = $this.isotope({
            itemSelector: '.col-item',
            percentPosition: true,
            masonry: {
                columnWidth: '.grid-sizer',

            }
        });
        $this.next('.product-cat-duel').isotope({
            layoutMode: 'fitRows',
        });

        $grid.imagesLoaded().progress( function() {
            $grid.isotope('layout');
        });
    }
    function resizeLayout($this) {
        var col                 =$this.data('col');
        var w_warp_product      =$this.outerWidth();
        if(w_warp_product > 991 ) {
            if(col > 0 ) {
                var w_width   =Math.floor(100/col);
                $this.find(".col-item").css({"width": (w_width)+"%","height":"auto"});
            }else{
                $this.find(".col-item").css({"width": "","height":"auto"});
            }
        }
        if(w_warp_product < 992 ) {
            var w_width   =50;
            $this.find(".col-item").css({"width": (w_width)+"%","height":"auto"});
        }

    }

    function resizeDetail($this) {
            var w_warp_product      =$this.outerWidth();
            if(w_warp_product < 1200 ) {
                $this.find('.gallery-main').addClass('gallery-carousel');
            }
            else{
                if ( $this.find('.gallery-main').hasClass('gallery-carousel')) {
                    $this.find('.gallery-main').removeClass('gallery-carousel');
                }
            }
                    
    }

    jQuery(document).ready(function($) {

        jQuery(".products-block").each(function(){
            resizeLayout( $(this) );
        });

        jQuery(".affect-isotope").each(function(){
            sok_isotope( $(this) );
        });

        jQuery(".affect-isotope-cats").each(function(){
            sok_isotope_percentage( $(this) );
        });

        jQuery(".product-layout-sticky").each(function(){
            resizeDetail( $(this) );
        });

        $('.lazy').lazy({
            effect: "fadeIn",
            effectTime: 400,
            threshold: 0
        });
    });

    jQuery(window).on( 'resize', function() {
        jQuery(".products-block").each(function(){
            resizeLayout( $(this) );
        });

        jQuery(".affect-isotope-cats").each(function(){
            sok_isotope_percentage( $(this) );
        });

        jQuery(".affect-isotope").each(function(){
            sok_isotope( $(this) );
        });

        jQuery(".product-layout-sticky").each(function(){
            resizeDetail( $(this) );
        });

    }).resize();

    jQuery(window).load(function(){
        jQuery(".products-block").each(function(){
            resizeLayout( $(this) );
        });
        jQuery(".affect-isotope-cats").each(function(){
            sok_isotope_percentage( $(this) );
        });

        jQuery(".affect-isotope").each(function(){
            sok_isotope( $(this) );
        });

        $('.lazy').lazy({
            effect: "fadeIn",
            effectTime: 400,
            threshold: 0
        });
    });

    //animation scroll
    jQuery.fn.visible = function(partial) {

        var $t = $(this),
            $w = $(window),
            viewTop = $w.scrollTop(),
            viewBottom = viewTop + $w.height(),
            _top = $t.offset().top,
            _bottom = _top + $t.height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

    };

})(jQuery);



//scroll invi tabs
jQuery(window).scroll(function() {
    var $t = jQuery(this),
        $w = jQuery(window);
    var scroll = $w.scrollTop(),
        scrollBottom = $w.height() + scroll,
        space= (jQuery(document).height() - scrollBottom);

    if ( scroll>=$w.height() && space >= 700) {
        jQuery(".left-fixed").addClass("view");
    }
    else {
        jQuery(".left-fixed").removeClass("view");
    }
});



