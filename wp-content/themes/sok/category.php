<?php
/**
 * The template for displaying Category pages
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
$layout_content      = get_theme_mod('sok_cat_content_layout', 'list');

//get url
if(isset($_GET['content'])){
    $layout_content=$_GET['content'];
}
get_header(); ?>
    <?php

        do_action('category-title'); ?>

        <div class="wrap-content container" role="main">

            <div class="row content-category">
                <?php do_action('archive-sidebar-left'); ?>
                <?php do_action('archive-content-before'); ?>
                <?php if ( have_posts() ) : ?>
                    <div class="row archive-blog layout-<?php echo esc_attr($layout_content)?>">
                        <div class="affect-isotope clearfix">
                            <?php get_template_part( 'loop' );?>
                        </div>
                    </div>

                    <?php
                    the_posts_pagination( array(
                        'prev_text'          => '<i class="ion-ios-arrow-thin-left"></i>',
                        'next_text'          => '<i class="ion-ios-arrow-thin-right"></i>',
                        'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
                    ) );
                    ?>

                <?php  else :
                    // If no content, include the "No posts found" template.
                    get_template_part( 'content', 'none' );
                endif; ?>

                <?php do_action('archive-content-after'); ?>
                <?php do_action('archive-sidebar-right'); ?>
            </div>

    </div>

<?php get_footer();
