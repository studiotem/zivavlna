<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.6.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! comments_open() ) {
	return;
}

?>
<div id="reviews" class="woocommerce-Reviews">
	<div id="comments">
		<?php if ( have_comments() ) : ?>

			<ol class="commentlist">
				<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				echo '<nav class="woocommerce-pagination">';
				paginate_comments_links( apply_filters( 'woocommerce_comment_pagination_args', array(
					'prev_text' => '&larr;',
					'next_text' => '&rarr;',
					'type'      => 'list',
				) ) );
				echo '</nav>';
			endif; ?>

		<?php else : ?>

			<p class="woocommerce-noreviews"><?php esc_html_e( 'There are no reviews yet.', 'sok' ); ?></p>

		<?php endif; ?>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>

		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
					$commenter = wp_get_current_commenter();

					$comment_form = array(
						'title_reply'          => have_comments() ? esc_html__( 'Add a review', 'sok' ) : sprintf( esc_html__( 'Be the first to review', 'sok' ),'' ),
						'title_reply_to'       => esc_html__( 'Leave a Reply to %s', 'sok' ),
						'title_reply_before'   => '<span id="reply-title" class="comment-reply-title">',
						'title_reply_after'    => '</span>',
						'comment_notes_after'  => '',
						'fields'               => array(
							'author' => '<div class="row clearfix">
											<div class="form-group col-md-6">
												<p class="comment-form-author">' .
												'<input id="author" class="form-control" name="author" type="text" placeholder="'.esc_attr__('Name', 'sok').'" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" required />
												</p>
											</div>',
							'email'  => '	<div class="form-group col-md-6">
												<p class="comment-form-email">' .
												'<input id="email" name="email" class="form-control" type="email" placeholder="'.esc_attr__('Email', 'sok').'" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" aria-required="true" required />
												</p>
											</div>
										</div>',
						),

						'label_submit'  => esc_html__( 'Submit', 'sok' ),
						'logged_in_as'  => '',
						'comment_field' => '',
					);
					if ( $account_page_url = wc_get_page_permalink( 'myaccount' ) ) {
						$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( esc_html__( 'You must be <a href="%s">logged in</a> to post a review.', 'sok' ), esc_url( $account_page_url ) ) . '</p>';
					}

					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
						$comment_form['comment_field'] = '<div class="comment-form-rating"><label for="rating">' . esc_html__( 'Your rating', 'sok' ) . '</label><select name="rating" id="rating" aria-required="true" required>
							<option value="">' . esc_html__( 'Rate&hellip;', 'sok' ) . '</option>
							<option value="5">' . esc_html__( 'Perfect', 'sok' ) . '</option>
							<option value="4">' . esc_html__( 'Good', 'sok' ) . '</option>
							<option value="3">' . esc_html__( 'Average', 'sok' ) . '</option>
							<option value="2">' . esc_html__( 'Not that bad', 'sok' ) . '</option>
							<option value="1">' . esc_html__( 'Very poor', 'sok' ) . '</option>
						</select></div>';
					}

					$comment_form['comment_field'] .= '<p class="comment-form-comment"><textarea id="comment" class="form-control" placeholder="'.esc_attr__('Your review *', 'sok').'" name="comment" cols="45" rows="8" aria-required="true" required></textarea></p>';

					comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>

	<?php else : ?>

		<p class="woocommerce-verification-required"><?php esc_html_e( 'Only logged in customers who have purchased this product may leave a review.', 'sok' ); ?></p>

	<?php endif; ?>

	<div class="clear"></div>
</div>
