<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$display_type = get_option( 'woocommerce_shop_page_display', '' );
$page_title = get_theme_mod('sok_show_page_title',true);
$shop_layout = get_theme_mod('sok_show_page_layout','container-fluid');

$woo_catLayout=get_theme_mod( 'sok_filter_woo', 'full' );
if(isset($_GET['filter'])){
    $woo_catLayout=$_GET['filter'];
}

$sok_layout_cat           =   get_theme_mod('sok_layout_cat','grid');


if(isset($_GET['shop-layout'])){
    $shop_layout=$_GET['shop-layout'];
}
if(isset($_GET['shop-type'])){
    $display_type=$_GET['shop-type'];
}
if(isset($_GET['title'])){
    $page_title=$_GET['title'];
}
if(isset($_GET['layout-product'])){
    $sok_layout_cat=$_GET['layout-product'];
}
$sok_breadcrumb         = get_theme_mod('sok_shop_breadcrumb','');
$style_css ='';

if($sok_breadcrumb){
    $bg_image          ="background-image:url('$sok_breadcrumb')";
    $style_css            ='style = '.$bg_image.'';
}

get_header( 'shop' ); ?>
<div class="site-main clearfix">
    <?php if ($page_title) : ?>
        <div class="page-header clearfix" <?php echo esc_attr($style_css);?>>
                <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
                <div class="wrapper-breadcrumb clearfix">
                    <?php do_action( 'sok_breadcrumb'); ?>
                </div>
        </div>
    <?php endif;?>
    <?php if($display_type=='subcategories' || $display_type=='both' ){?>
        <div class="shop-subcategories clearfix">
            <?php sok_subcategories_loop_start();?>
        </div>
    <?php  }?>
    <div class="page-content <?php echo esc_attr($shop_layout);?>">

        <div class="row shop-content type-loadShop sok-<?php echo esc_attr($display_type);?> layout-filter-<?php echo esc_attr($woo_catLayout);?> layout-shop-<?php echo esc_attr($sok_layout_cat);?> clearfix">

        <?php do_action('woo-sidebar-left'); ?>
        <?php do_action('woo-content-before'); ?>

            <?php if ( woocommerce_product_loop() )  { ?>

                    <div class="shop-content-top clearfix">
                        <?php
                        /**
                         * woocommerce_before_shop_loop hook.
                         *
                         * @hooked wc_print_notices - 10
                         * @hooked sok_nav_category - 17
                         * @hooked sok_nav_filters - 18
                         * @hooked sok_btn_filter - 19
                         * @hooked woocommerce_catalog_ordering - 30
                         * @hooked sok_filter_down - 31
                         * @hooked sok_search - 32
                         * @hooked sok_filter_full - 33
                         */
                        do_action( 'woocommerce_before_shop_loop' );
                        ?>
                    </div>
                    <?php  woocommerce_product_loop_start();
                    if ( wc_get_loop_prop( 'total' ) ) {

                        while ( have_posts() ) {
                            the_post();

                            /**
                             * Hook: woocommerce_shop_loop.
                             *
                             * @hooked WC_Structured_Data::generate_product_data() - 10
                             */

                            do_action( 'woocommerce_shop_loop' );

                            wc_get_template_part( 'content', 'product' );
                        }

                    }
                    woocommerce_product_loop_end(); ?>
                    <?php
                    /**
                     * woocommerce_after_shop_loop hook.
                     *
                     * @hooked woocommerce_pagination - 10
                     */
                    do_action( 'woocommerce_after_shop_loop' );
                    ?>

            <?php } else {
            /**
             * Hook: woocommerce_no_products_found.
             *
             * @hooked wc_no_products_found - 10
             */
            do_action( 'woocommerce_no_products_found' );
            } ?>

        <?php do_action('woo-content-after'); ?>
    </div>
    </div>

</div>

<?php
/**
 * woocommerce_sidebar hook.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );
?>

<?php get_footer( 'shop' ); ?>




