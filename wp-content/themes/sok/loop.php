<?php
/**
 * The template for displaying Category pages
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$layout_content      = get_theme_mod('sok_cat_content_layout', 'list');
$content_col         = get_theme_mod('sok_sidebar_cat', 'right');

//get url
if(isset($_GET['content'])){
    $layout_content=$_GET['content'];
}
if(isset($_GET['layout'])){
    $content_col=$_GET['layout'];
}

if ($content_col && ($content_col==='right' || $content_col==='left') && is_active_sidebar( 'archive') ){
    $class='col-md-6 col-sm-6 col-xs-12';
} else{
    $class='col-md-4 col-sm-6 col-xs-12';
}



$na=1;
// Start the Loop.
while ( have_posts() ) : the_post(); ?>
    <?php if($layout_content=='tran'){
        if($na%2==0){?>
            <div class="item-post item-post-masonry col-item <?php echo esc_attr($class);?>">
                <?php get_template_part( 'templates/layout/content-tran'); ?>
            </div>
        <?php } else{ ?>
            <div class="item-post item-post-grid col-item <?php echo esc_attr($class);?>">
                <?php get_template_part( 'templates/layout/content-grid'); ?>
            </div>
        <?php }	?>
    <?php }
    elseif ($layout_content=='list'){?>
        <div class="item-post col-item col-md-12 col-lg-12">
            <?php get_template_part( 'templates/layout/content-list'); ?>
        </div>
    <?php }
    else{ ?>
        <div class="item-post col-item <?php echo esc_attr($class);?>">
            <?php get_template_part( 'templates/layout/content' ,$layout_content); ?>
        </div>
    <?php }
    $na++;
endwhile; ?>
