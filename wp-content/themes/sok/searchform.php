<form method="get"  class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">

    <div class="input-group">
        <label class="screen-reader-text"><?php echo esc_attr__( 'Search for:','sok' ); ?></label>
        <input type="text" class="form-control" placeholder="<?php echo esc_attr__( 'Search ... ','sok' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
        <span class="input-group-btn">
            <button class="btn btn-variant"><i class="ion-ios-search-strong"></i></button>
        </span>
    </div>

</form>