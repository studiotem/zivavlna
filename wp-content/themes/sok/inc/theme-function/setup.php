<?php
/**
 * @package     sok
 * @version     1.0
 * @author      NanoAgency
 * @link        http://www.nanoagency.co
 * @copyright   Copyright (c) 2016 NanoAgency
 * @license     GPL v2
 */

/*  Setup Theme ===================================================================================================== */
add_action( 'after_setup_theme', 'sok_theme_setup' );
if ( ! function_exists( 'sok_theme_setup' ) ) :
    function sok_theme_setup() {
        load_theme_textdomain( 'sok', get_template_directory() . '/languages' );

        //  Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        //  Let WordPress manage the document title.
        add_theme_support( 'title-tag' );

        //  Enable support for Post Thumbnails on posts and pages.
        add_theme_support( 'post-thumbnails' );

        //Enable support for Post Formats.
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ) );

        add_theme_support( 'post-formats', array(
            'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
        ) );

        add_theme_support( 'custom-header' );

        add_theme_support( 'custom-background' );

        add_theme_support( "title-tag" );

        add_theme_support( 'woocommerce' );

        // Indicate widget sidebars can use selective refresh in the Customizer.
        add_theme_support( 'customize-selective-refresh-widgets' );

        add_theme_support( 'wp-block-styles' );

        add_theme_support( 'align-wide' );

        add_theme_support( 'editor-styles' );

        add_editor_style( array( 'assets/css/editor-style.css', sok_font_url() ) );

        add_theme_support( 'responsive-embeds' );
    }
endif;

/* Thumbnail Sizes ================================================================================================== */
set_post_thumbnail_size( 825, 510, true );
add_image_size( 'thumb-image', 600, 450, true);

//layout blog
add_image_size( 'sok-blog-sidebar', 150, 150, true);
add_image_size( 'sok-related-image',370,247,true);
add_image_size( 'sok-blog-list', 1100 ,600, true);
add_image_size( 'sok-blog-grid', 600 ,400, true);
add_image_size( 'sok-blog-tran', 500 ,700, true);

//layout sub-cate
add_image_size( 'sok-category-thumb-large', 770, 400,true );
add_image_size( 'sok-category-thumb-wide', 1170, 300,true );

//layout product  shop
add_image_size( 'sok-product-grid', 500, 583,true );///
add_image_size( 'sok-product-vertical', 1200, 1470,true );///
add_image_size( 'sok-product-large', 700, 700,true );//
add_image_size( 'sok-vertical-thumbnail-size', 500, 700,true );
add_image_size( 'sok-trans-lg-thumbnail-size', 800, 800,true );


/* Setup Font ======================================================================================================= */
function sok_font_url() {
    $fonts_url = '';
    $roboto    = _x( 'on', 'Roboto font: on or off', 'sok' );
    if ( 'off' !== $roboto ) {
        $font_families = array();
        if ( 'off' !== $roboto) {
            $font_families[] = 'Roboto:300,300i,400,400i,500,700';
        }
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
        $fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
    }
    return esc_url_raw( $fonts_url );
}


/* Load Front-end scripts  ========================================================================================== */
add_action( 'wp_enqueue_scripts', 'sok_theme_scripts');
function sok_theme_scripts() {

    // Add  fonts, used in the main stylesheet.
    wp_enqueue_style('now-font',get_template_directory_uri().'/assets/css/plugins/font-now.css', array(), null);

    //register style CSS
    wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/css/bootstrap.min.css', array(), '3.0.2 ');
    wp_enqueue_style('ionicons-font',get_template_directory_uri().'/assets/css/plugins/ionicons.min.css', array(), '2.0.0 ');
    wp_enqueue_style('simple-line-icons-font',get_template_directory_uri().'/assets/css/plugins/simple-line-icons.min.css', array(), '2.0.0 ');
    wp_enqueue_style('slick',get_template_directory_uri().'/assets/css/plugins/slick.min.css', array(), '1.0.0');
    wp_enqueue_style('slick-theme',get_template_directory_uri().'/assets/css/plugins/slick-theme.min.css', array(), '1.0.0');
    wp_enqueue_style('swiper',get_template_directory_uri().'/assets/css/plugins/swiper.min.css', array(), '4.4.6');
    wp_enqueue_style('slicknav',get_template_directory_uri().'/assets/css/plugins/slicknav.min.css', array(), '1.0.3');
    wp_enqueue_style('jquery-ui', get_template_directory_uri() . '/inc/customize/assets/css/jquery-ui.min.css', array(),null);

    //style MAIN THEME
    wp_enqueue_style( 'sok-main', get_template_directory_uri(). '/style.css', array(), null );
    //style SKIN THEME
    wp_enqueue_style('sok-style', get_template_directory_uri().'/assets/css/style-default.min.css' );

    //register all plugins JS
    wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/assets/js/plugins/bootstrap.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'html5', get_template_directory_uri().'/assets/js/plugins/html5.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'skip-link-focus', get_template_directory_uri().'/assets/js/plugins/skip-link-focus-fix.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'lazy', get_template_directory_uri().'/assets/js/plugins/jquery.lazy.js', array(), '2.2.0', true );

    wp_enqueue_script( 'accordion-menu', get_template_directory_uri().'/assets/js/plugins/accordion-menu.js', array(), 'null', true );//menu
    wp_enqueue_script( 'slick', get_template_directory_uri().'/assets/js/plugins/slick.min.js', array(), '2.2.0', true ); //slick
    wp_enqueue_script( 'swiper', get_template_directory_uri().'/assets/js/plugins/swiper.min.js', array(), '4.4.6', true ); //swiper

    wp_enqueue_script('masonry');
    wp_enqueue_script('jquery-masonry');
    wp_enqueue_script( 'isotope', get_template_directory_uri().'/assets/js/plugins/isotope.pkgd.min.js', array(), '2.2.0', true );

    wp_register_script( 'parallax', get_template_directory_uri().'/assets/js/plugins/parallax.min.js', array(), '1.1.3', true ); //parallax
    wp_register_script( 'videoController', get_template_directory_uri().'/assets/js/plugins/jquery.videoController.min.js', array(), '2.2.0', true );//video

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    if ( is_singular() && wp_attachment_is_image() ) {
        wp_enqueue_script( 'sok-theme-keyboard-image-navigation', get_template_directory_uri() . '/assets/js/plugins/keyboard-image-navigation.min.js', array( 'jquery' ), '20141010' );
    }

    if( function_exists('is_product') && is_product()){
        wp_enqueue_script( 'wc-add-to-cart-variation' ); //woo
        wp_register_script( 'sok-slick', get_template_directory_uri().'/assets/js/dev/sok-slick.js', array('jquery'),null, true );
        //photoswipe
        wp_enqueue_script( 'photoswipe', get_template_directory_uri().'/assets/js/plugins/photoswipe.min.js', array(), null, true );
        wp_enqueue_script( 'photoswipe-ui-default', get_template_directory_uri().'/assets/js/plugins/photoswipe-ui-default.min.js', array(), null, true );
        wp_enqueue_style('photoswipe',get_template_directory_uri().'/assets/css/plugins/photoswipe.min.css', array(), null);
        wp_enqueue_style('photoswipe-skin',get_template_directory_uri().'/assets/css/plugins/photoswipe-skin/default-skin.css', array(), null);
    }

    //jquery MAIN THEME
    wp_enqueue_script('sok-isotope', get_template_directory_uri() . '/assets/js/dev/sok-isotope.js', array('jquery'),null, true);
    wp_enqueue_script('sok-main', get_template_directory_uri() . '/assets/js/dev/sok.js', array('jquery'),null, true);
    
     wp_localize_script( 'sok-main', 'ajax_object',
            array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

}

/* Load Back-end SCRIPTS============================================================================================= */
function sok_js_enqueue()
{
    wp_enqueue_media();
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    // moved the js to an external file, you may want to change the path
    wp_enqueue_script('information',get_template_directory_uri(). '/assets/js/plugins/widget.min.js', 'jquery', '1.0', true);
}
add_action('admin_enqueue_scripts', 'sok_js_enqueue');

/* Register the required plugins    ================================================================================= */
add_action( 'tgmpa_register', 'sok_register_required_plugins' );
function sok_register_required_plugins() {

    $plugins = array(
        // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'      => esc_html__( 'Nano Core Plugin', 'sok' ),
            'slug'      => 'theme-core',
            'source'    => get_template_directory() . '/inc/theme-plugins/theme-core.zip',
            'required'  => true,
            'version'   => '1.0.1',
            'force_activation' => false,
            'force_deactivation' => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/nano.jpg',

        ),
        //WooSwatches
        array(
            'name'      =>  esc_html__('WooSwatches - WooCommerce Color or Image Variation Swatches', 'sok' ),
            'slug'      => 'woocommerce-colororimage-variation-select',
            'required'  => false,
            'source'    => get_template_directory() . '/inc/theme-plugins/woocommerce-colororimage-variation-select.zip',
            'version'   => ' 2.8.7',
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/wooswatches.jpg',
        ),
        //Slider Revolution
        array(
            'name'      =>  esc_html__('Slider Revolution', 'sok' ),
            'slug'      => 'revslider',
            'source'    => get_template_directory() . '/inc/theme-plugins/revslider.zip',
            'required'  => false,
            'version'   => '5.4.8',
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/revolution.jpg',
        ),
        //WPBakery Page Builder
        array(
            'name'      =>  esc_html__('WPBakery Page Builder', 'sok' ),
            'slug'      => 'js_composer',
            'source'    => get_template_directory() . '/inc/theme-plugins/js_composer.zip',
            'required'  => true,
            'version'   => '6.0.5',
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/vc.jpg',
        ),
        //Contact form 7
        array(
            'name'      => esc_html__('Contact Form 7', 'sok' ),
            'slug'      => 'contact-form-7',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/contact-form7.jpg',
        ),
        //MailChimp for WordPress
        array(
            'name'      =>  esc_html__('MailChimp for WordPress ', 'sok' ),
            'slug'      => 'mailchimp-for-wp',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/mailchimp.jpg',
        ),
        //woocommerce
        array(
            'name'      =>  esc_html__('WooCommerce', 'sok' ),
            'slug'      => 'woocommerce',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/woo.jpg',
        ),

        //YITH  Wishlist
        array(
            'name'      =>  esc_html__('YITH WooCommerce Wishlist', 'sok' ),
            'slug'      => 'yith-woocommerce-wishlist',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/yith.jpg',
        ),
        //Advanced Woo Search
        array(
            'name'      =>  esc_html__('Advanced Woo Search', 'sok' ),
            'slug'      => 'advanced-woo-search',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/ad-search.jpg',
        ),
        //Loco Translate
        array(
            'name'      =>  esc_html__('Loco Translate', 'sok' ),
            'slug'      => 'loco-translate',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/loco.jpg',
        ),

    );

    $config = array(
        'id'           => 'sok',                   // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                       // Default absolute path to pre-packaged plugins.
        'has_notices'  => true,
        'menu'         => 'tgmpa-install-plugins',  // Menu slug.
        'dismiss_msg'  => '',                       // If 'dismissable' is false, this message will be output at top of nag.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'is_automatic' => true,                     // Automatically activate plugins after installation or not.
        'message'      => '',                       // Message to output right before the plugins table.

    );

    tgmpa( $plugins, $config );

}

/* Register Navigation ============================================================================================== */
register_nav_menus( array(
    'primary_navigation'    => esc_html__( 'Primary Navigation', 'sok' ),
) );

/* Register Sidebar ================================================================================================= */
function sok_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Archive', 'sok' ),
        'id'            => 'archive',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar for the Archive.', 'sok' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Shop', 'sok' ),
        'id'            => 'shop',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar for the Shop', 'sok' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );


    register_sidebar(array(
        'name' => esc_html__('Footer 1','sok'),
        'id'   => 'footer-1',
        'description'   => esc_html__( 'Add the widget here to appear in the first Footer column.', 'sok' ),
        'before_widget' => '<div id="%1$s" class="widget last %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 2','sok'),
        'id'   => 'footer-2',
        'description'   => esc_html__( 'Add the widget here to appear in the second Footer column.', 'sok' ),
        'before_widget' => '<div id="%1$s" class="widget last %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 3','sok'),
        'id'   => 'footer-3',
        'description'   => esc_html__( 'Add the widget here to appear in the 3rd Footer column.', 'sok' ),
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 4','sok'),
        'id'   => 'footer-4',
        'description'   => esc_html__( 'Add the widget here to appear in the 4rd Footer column.', 'sok' ),
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer top','sok'),
        'id'   => 'footer-top',
        'description'   => esc_html__( 'Add the widget here to appear in the Range Footer column.', 'sok' ),
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar( array(
        'name'          => esc_html__( 'Intro Sidebar ', 'sok' ),
        'id'            => 'intro',
        'description'   => esc_html__( 'Add the widget here to appear in the Intro Canvas', 'sok' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'sok_widgets_init' );