<?php
/**
 * @package     over
 * @version     1.0
 * @author      Nanoagency
 * @link        http://www.nanoagency.co
 * @copyright   Copyright (c) 2016 Nanoagency
 * @license     GPL v2
 */

/* WooCommerce - Disable the default stylesheet WooCommerce ========================================================= */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/** Disable Ajax Call from WooCommerce on front page and posts*/
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11);
function dequeue_woocommerce_cart_fragments() {
if (is_front_page() || is_single() ) wp_dequeue_script('wc-cart-fragments');
}

// WooCommerce - Product Meta ==========================================================================================
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

// WooCommerce - remove add_to_cart from link_close ====================================================================
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
add_action('woocommerce_add_to_cart_item', 'woocommerce_template_loop_add_to_cart', 10);

// WooCommerce - remove Star ===========================================================================================
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

// WooCommerce - cross_sell_display ====================================================================================
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
add_action( 'woo_cart_collaterals', 'woocommerce_cross_sell_display' );

// WooCommerce - remove result_count ===================================================================================
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count',20 );



// WooCommerce - Output the WooCommerce Breadcrumb =====================================================================
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('sok_breadcrumb', 'sok_wp_breadcrumbs', 10);
add_action('sok_breadcrumb', 'sok_woocommerce_breadcrumb',10);
function sok_woocommerce_breadcrumb( $args = array() ) {
    $args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
        'delimiter'   => '',
        'wrap_before' => '<div class = "breadcrumb"'.'>'. '<nav class="woocommerce-breadcrumb">',
        'wrap_after'  => '</nav></div>',
        'before'      => '',
        'after'       => '',
        'home'        => _x( 'Home', 'breadcrumb', 'sok' )
    ) ) );
    $breadcrumbs = new WC_Breadcrumb();
    if ( $args['home'] ) {
        $breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
    }
    $args['breadcrumb'] = $breadcrumbs->generate();
    wc_get_template( 'global/breadcrumb.php', $args );

}

// WooCommerce - Next / Prev nav on Product Pages ======================================================================
add_action( 'sok_breadcrumb', 'sok_post_link_product',20);
function sok_post_link_product()
{
    global $post;
    $next_post = get_next_post(true, '', 'product_cat');
    if ( is_product() ){ ?>
            <div class="product-nav">
                <?php if (is_a($next_post, 'WP_Post')) { ?>
                    <div class="nav-product">
                        <a title="<?php esc_attr_e('Prev','sok')?>" href="<?php echo get_the_permalink($next_post->ID); ?>" class="ion-ios-arrow-thin-left"></a>
                    </div>
                <?php }
                $prev_post = get_previous_post(true, '', 'product_cat');
                if (is_a($prev_post, 'WP_Post')) { ?>
                    <div class="nav-product">
                        <a title="<?php esc_attr_e('Next','sok')?>" href="<?php echo get_the_permalink($prev_post->ID); ?>" class="ion-ios-arrow-thin-right"></a>
                    </div>
                <?php }?>
            </div>
    <?php }
}

// WooCommerce -Sidebar Detail==========================================================================================
add_action( 'woo-sidebar-detail-left', 'sok_woo_sidebar_left' );
function sok_woo_sidebar_left() {
    $woo_sidebar=get_theme_mod( 'sok_sidebar_woo_single', 'full' );
    if ( $woo_sidebar && $woo_sidebar == 'left') { ?>
        <div id="archive-sidebar" class="sidebar sidebar-left hidden-sx hidden-sm col-sx-12 col-sm-12 col-md-3 col-lg-3 detail-sidebar">
            <?php get_sidebar('shop'); ?>
        </div>
    <?php }
}

add_action( 'woo-sidebar-detail-right', 'sok_woo_sidebar_right' );
function sok_woo_sidebar_right() {
    $woo_sidebar=get_theme_mod( 'sok_sidebar_woo_single', 'full' );
    if ( $woo_sidebar && $woo_sidebar == 'right') {?>
        <div id="archive-sidebar" class="sidebar sidebar-right hidden-sx hidden-sm col-sx-12 col-sm-12 col-md-3 col-lg-3 detail-sidebar">
            <?php get_sidebar('shop'); ?>
        </div>
    <?php }
}

//content
add_action( 'woo-content-detail-before', 'sok_woo_content_before' );
function sok_woo_content_before() {
    $woo_sidebar=get_theme_mod( 'sok_sidebar_woo_single', 'full' );
    if ( $woo_sidebar && $woo_sidebar == 'full') {?>
        <div class="main-content">
    <?php }
    else{?>
        <div class="main-content col-sx-12 col-sm-12 col-md-9  col-lg-9 padding-content-<?php echo esc_attr($woo_sidebar)?>">
    <?php }
}
add_action( 'woo-content-detail-after', 'sok_woo_content_after' );
function sok_woo_content_after() {
    $woo_sidebar=get_theme_mod( 'sok_sidebar_woo_single', 'full' );
    if ( $woo_sidebar){?>
        </div>
    <?php }
}

// WooCommerce - Layout Categories =====================================================================================
add_action( 'woo-sidebar-left', 'sok_woo_sidebar_cat_left' );
function sok_woo_sidebar_cat_left() {
    $woo_catLayout=get_theme_mod( 'sok_filter_woo', 'full' );
    if(isset($_GET['filter'])){
        $woo_catLayout=$_GET['filter'];
    }
    if ( $woo_catLayout && $woo_catLayout == 'left') { ?>
        <div id="shop-sidebar" class="sidebar shop-sidebar shop-filter sidebar-left  col-sx-12 col-sm-12 col-md-4 col-lg-4">
            <div class="filter-header clearfix">
                    <span class="title-filter">
                        <?php  esc_html_e('Filter','sok');?>
                    </span>
                    <button class="btn-mini-close pull-right"><i class="ion-ios-close-empty"></i></button>
            </div>
            <?php get_sidebar('shop'); ?>
        </div>
    <?php }
    if ( $woo_catLayout && $woo_catLayout == 'sidebar-left') {?>
        <div id="archive-sidebar" class="sidebar sidebar-left shop-sidebars col-sx-12 col-sm-12 col-md-2 col-lg-2 archive-sidebar">
            <div class="filter-header hidden-lg hidden-md clearfix">
                        <span class="title-filter">
                            <?php  esc_html_e('Filter','sok');?>
                        </span>
                        <span class="btn-close pull-right"><i class="ion-ios-close-empty"></i></span>
             </div>
            <?php get_sidebar('shop'); ?>
        </div>
    <?php }
}

//content
add_action( 'woo-content-before', 'sok_woo_content_cat_before' );
function sok_woo_content_cat_before() {
        $woo_catLayout=get_theme_mod( 'sok_filter_woo', 'full' );
        if(isset($_GET['filter'])){
            $woo_catLayout=$_GET['filter'];
        }
        if ( $woo_catLayout && $woo_catLayout == 'sidebar-left') { ?>
            <div class="main-content hidden-filters col-sx-12 col-sm-12 col-md-10 col-lg-10">

                    <div class="filter-mini hidden-md hidden-lg">
                     <span class="title-filter">
                           <?php  esc_html_e('Filter','sok');?>
                     </span>
                        <span class="btn-filter"><i class="icon ion-ios-arrow-thin-right"></i></span>
                    </div>
        <?php }else{?>
            <div class="main-content col-sx-12 col-sm-12 col-md-12 col-lg-12">
        <?php }
}
add_action( 'woo-content-after', 'sok_woo_content_cat_after' );
function sok_woo_content_cat_after() {?>
        </div>
<?php }


/* WooCommerce - Show Nav Filter ========================================================================================= */
add_action('woocommerce_before_shop_loop', 'sok_nav_filters', 18);
if ( ! function_exists( 'sok_nav_filters' ) ) {
	function sok_nav_filters() {?>
	    <?php if (get_theme_mod( 'sok_woo_layered_nav', false )):; ?>
            <div class="woocommerce-nav-filters">
                <?php the_widget('WC_Widget_Layered_Nav_Filters', array('title' => esc_html__('Active Filters','sok'))); ?>
            </div>
         <?php endif; ?>
	<?php
	}
}

/* WooCommerce - Show Filter ========================================================================================= */
add_action('woocommerce_before_shop_loop', 'sok_btn_filter', 19);
if ( ! function_exists( 'sok_btn_filter' ) ) {
	function sok_btn_filter() {?>
	    <?php $woo_catLayout=get_theme_mod( 'sok_filter_woo', 'full' );
	        if(isset($_GET['filter'])){
                $woo_catLayout=$_GET['filter'];
            }
	    ?>
		<div class="shop-btn-filter pull-left <?php echo esc_attr('btn-filter-'.$woo_catLayout)?>">
            <span class="btn-filter">
                <i class="ion-android-options" aria-hidden="true"></i>
                <?php  esc_html_e('Filters','sok');?>
            </span>
        </div>
	<?php
	}
}

/* WooCommerce - Layout Filter ====================================================================================== */
add_action('woocommerce_before_shop_loop', 'sok_filter_full', 33);
if ( ! function_exists( 'sok_filter_full' ) ) {
	function sok_filter_full() {?>
	    <?php $woo_catLayout=get_theme_mod( 'sok_filter_woo', 'full' );
	        if(isset($_GET['filter'])){
                $woo_catLayout=$_GET['filter'];
            }
	    ?>
        <?php if ( $woo_catLayout && $woo_catLayout == 'full') { ?>
            <div id="filter-full" class="shop-filter filter-full col-sx-12 col-sm-12 col-md-12 col-lg-12">
                <?php get_sidebar('shop'); ?>
            </div>
        <?php }?>
	<?php
	}
}
add_action('woocommerce_before_shop_loop', 'sok_filter_down',31);
if ( ! function_exists( 'sok_filter_down' ) ) {
	function sok_filter_down() {?>
	    <?php
	        $woo_catLayout=get_theme_mod( 'sok_filter_woo', 'full' );
	        if(isset($_GET['filter'])){
                $woo_catLayout=$_GET['filter'];
            }
	    ?>
        <?php if ( $woo_catLayout && $woo_catLayout == 'down') { ?>
            <div id="filter-down" class="shop-filter filter-down pull-left col-sx-12 col-sm-12 col-md-12 col-lg-12">
                <?php get_sidebar('shop'); ?>
            </div>
        <?php }?>
	<?php
	}
}

// WooCommerce - Products per page ================================================================================== */
function sok_products_per_page(){
    global $woocommerce;
    $sok_get_products_per_page  = get_theme_mod('sok_woo_product_per_page','16');
    $default = $sok_get_products_per_page;
    $count = $default;
    return $count;
}
add_filter('loop_shop_per_page','sok_products_per_page');

// Related Products: Change "related" ==================================================================================
add_filter( 'woocommerce_output_related_products_args', 'sok_related_products_args' );
function sok_related_products_args( $args ) {
$args['posts_per_page'] = 6; // 6 related products
$args['columns'] = 5; // arranged in 4 columns
return $args;
}
/* WooCommerce - Ajax add to cart =================================================================================== */
add_action('woocommerce_ajax_added_to_cart', 'sok_ajax_added_to_cart');
function sok_ajax_added_to_cart() {
    add_filter( 'add_to_cart_fragments', 'sok_header_add_to_cart_fragment' );
    function sok_header_add_to_cart_fragment( $fragments ) {
        ob_start();
        sok_cartbox();
        $fragments['.na-cart'] = ob_get_clean();
        return $fragments;
    }
}
if(!function_exists('sok_cartbox')){
    function sok_cartbox(){
        global $woocommerce;
        $cart_image = get_theme_mod('sok_cart_image', '');
        ?>
        <div class="na-cart">
            <div  class="mini-cart btn-header clearfix" role="contentinfo">
                <span class="icon-cart text-items">
                    <?php if($cart_image){ ?>
                        <img class="cart_image" src="<?php echo esc_url($cart_image); ?>" alt="<?php esc_attr_e('Cart Image','sok')?>">
                    <?php } else{?>
                        <i class="icon-basket"></i>
                    <?php }?>
                    <?php echo sprintf(_n(' <span class="mini-cart-items">%d</span> ', '<span class="mini-cart-items">%d</span>', $woocommerce->cart->cart_contents_count, 'sok'), $woocommerce->cart->cart_contents_count); ?>
                </span>
                <div class="group-mini-cart">
                    <div class="text-cart"><?php esc_html_e('My Cart','sok'); ?></div>
                    <div class="text-items">
                        <?php echo sprintf(_n(' <span class="mini-cart-items"></span> ', '', $woocommerce->cart->cart_contents_count, 'sok'), $woocommerce->cart->cart_contents_count);?> <?php echo esc_attr($woocommerce->cart->get_cart_total()); ?>
                    </div>
                </div>
            </div>
            <div class="cart-box">
                <?php woocommerce_mini_cart(); ?>
            </div>
        </div>
    <?php }
}
/* WooCommerce - Ajax remover cart ================================================================================== */
add_action( 'wp_ajax_cart_remove_product', 'nano_woo_remove_product' );
add_action( 'wp_ajax_nopriv_cart_remove_product', 'nano_woo_remove_product' );
function nano_woo_remove_product() {
    $product_key = $_POST['product_key'];

    $cart = WC()->instance()->cart;
    $removed = $cart->remove_cart_item( $product_key );

    if ( $removed )	{
        $output['status'] = '1';
        $output['cart_count'] = $cart->get_cart_contents_count();
        $output['cart_subtotal'] = $cart->get_cart_subtotal();
    } else {
        $output['status'] = '0';
    }
    echo json_encode( $output );
    exit;
}

/* WooCommerce - Product: Get sale percentage ======================================================================= */
function sok_product_get_sale_percent( $product ) {
    if ( $product->is_type( 'variable' ) ) {
        // Get product variation prices (regular and sale)
        $product_variation_prices = $product->get_variation_prices();
        $highest_sale_percent = 0;
        foreach( $product_variation_prices['regular_price'] as $key => $regular_price ) {
            // Get sale price for current variation
            $sale_price = $product_variation_prices['sale_price'][$key];

            // Is product variation on sale?
            if ( $sale_price < $regular_price ) {
                $sale_percent = round( ( ( $regular_price - $sale_price ) / $regular_price ) * 100 );

                // Is current sale percent highest?
                if ( $sale_percent > $highest_sale_percent ) {
                    $highest_sale_percent = $sale_percent;
                }
            }
        }
        return $highest_sale_percent;
    } else {
        $sale_percent = 0;
        if ( intval( $product->get_regular_price() ) > 0 ) {
            $sale_percent = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
        }
        return $sale_percent;
    }
}

/* WooCommerce - Register Size Thumbnail ============================================================================ */
remove_action( 'woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail', 10 );

add_action( 'sok-category-thumb-large', 'sok_subcategory_thumbnail_large' );
if ( ! function_exists( 'sok_subcategory_thumbnail_large' ) ) {
	function sok_subcategory_thumbnail_large( $category ) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'sok-category-thumb-large' );
		$placeholder_image    = get_template_directory_uri(). '/assets/images/layzyload-category-thumb-large.jpg';
		$dimensions           = wc_get_image_size( $small_thumbnail_size );
		$thumbnail_id         = get_term_meta ( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
            $image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
            $thumb_size   = wp_get_attachment_image_src($thumbnail_id,'sok-category-thumb-large');
		} else {
			$image        = $placeholder_image;
			$image_srcset = false;
			$image_sizes  = false;
		}

		if ( $image ) {
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available.
			if ($image_sizes ) {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '"  srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
			} else {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" />';
			}
		}
	}
}

add_action( 'sok-category-thumb-wide', 'sok_subcategory_thumbnail_wide' );
if ( ! function_exists( 'sok_subcategory_thumbnail_wide' ) ) {
	function sok_subcategory_thumbnail_wide( $category ) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'sok-category-thumb-wide' );
		$placeholder_image    = get_template_directory_uri(). '/assets/images/layzyload-category-thumb-wide.jpg';
		$dimensions           = wc_get_image_size( $small_thumbnail_size );
		$thumbnail_id         = get_term_meta ( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
			$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
			$thumb_size   = wp_get_attachment_image_src($thumbnail_id,'sok-category-thumb-wide');
		} else {
			$image        = wc_placeholder_img_src();
			$image_srcset = false;
			$image_sizes  = false;
		}
		if ( $image ) {
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available.
			if ( $image_srcset && $image_sizes ) {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '"  srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
			} else {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" />';
			}
		}
	}
}
add_action( 'sok-category-thumb-vertical', 'sok_subcategory_thumbnail_vertical' );
if ( ! function_exists( 'sok_subcategory_thumbnail_vertical' ) ) {
	function sok_subcategory_thumbnail_vertical( $category ) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'sok-trans-lg-thumbnail-size' );
		$placeholder_image    = get_template_directory_uri(). '/assets/images/layzyload-category-thumb-vertical.jpg';
		$dimensions           = wc_get_image_size( $small_thumbnail_size );
		$thumbnail_id         = get_term_meta ( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
			$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
			$thumb_size   =wp_get_attachment_image_src($thumbnail_id,'sok-trans-lg-thumbnail-size');
		} else {
			$image        = $placeholder_image;
			$image_srcset = false;
			$image_sizes  = false;
		}
		if ( $image ) {
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available.
			if ( $image_srcset && $image_sizes ) {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '"  srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
			} else {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '"  />';
			}
		}
	}
}

add_action( 'sok-category-thumb', 'sok_subcategory_thumbnail' );
if ( ! function_exists( 'sok_subcategory_thumbnail' ) ) {
	function sok_subcategory_thumbnail( $category ) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'sok-product-grid' );
		$placeholder_image    = get_template_directory_uri(). '/assets/images/layzyload-category-thumb.jpg';
		$dimensions           = wc_get_image_size( $small_thumbnail_size );
		$thumbnail_id         = get_term_meta ( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
			$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
			$thumb_size   =wp_get_attachment_image_src($thumbnail_id,'sok-product-grid');
		} else {
			$image        = $placeholder_image;
			$image_srcset = false;
			$image_sizes  = false;
		}
		if ( $image ) {
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available.
			if ( $image_srcset && $image_sizes ) {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '"  width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
			} else {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '"  width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_attr( $dimensions['height'] ) . '" />';
			}
		}
	}
}

/* WooCommerce - Register subcategory_title ========================================================================= */

remove_action( 'woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title', 10 );
add_action( 'woocommerce_shop_loop_subcategory_title', 'sok_template_loop_category_title', 10 );

if ( ! function_exists( 'sok_template_loop_category_title' ) ) {

	/**
	 * Show the subcategory title in the product loop.
	 *
	 * @param object $category Category object.
	 */
	function sok_template_loop_category_title( $category ) {
		?>
		<h2 class="woocommerce-loop-category__title">
			<a href="<?php echo esc_url(get_term_link($category)); ?>" ><?php echo esc_html( $category->name ); ?></a>
			<?php
			if ( $category->count > 0 ) {
				echo apply_filters( 'woocommerce_subcategory_count_html', ' <mark class="count">' . esc_html( $category->count ) . ' '.esc_html__('items','sok').'</mark>', $category ); // WPCS: XSS ok.
			}
			?>
			<span class="btn-shop"><?php  esc_html_e('Shop Now','sok');?></span>
		</h2>
	<?php }
}

//Remove sale_flash to after the title
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
add_action( 'woocommerce_single_product_summary', 'sok_template_single_title', 6 );

if ( ! function_exists( 'sok_template_single_title' ) ) {

	function sok_template_single_title(){ ?>
		<div class="single-title-product">
            <?php woocommerce_template_single_title();?>
            <?php woocommerce_show_product_sale_flash();?>
        </div>
	<?php }
}

//Remove Tabs description_tab
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
// Remove the additional information tab
function sok_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );
    return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'sok_remove_product_tabs', 98 );

//woocommerce_product_description_tab
if ( ! function_exists( 'sok_template_single_description' ) ) {
	function sok_template_single_description(){ ?>
		<div class="single-description-product">
		    <span class="show-detail"><?php echo esc_html__('Description','sok');?></span>
            <div class="single-description-product-inner">
                <?php woocommerce_product_description_tab();?>
                <?php woocommerce_template_single_meta();?>
            </div>
         </div>
	<?php }
}
if ( ! function_exists( 'sok_template_single_comment' ) ) {
	function sok_template_single_comment(){ ?>
		<div class="single-comment-product">
		    <span class="show-reviews"><?php echo esc_html__('Reviews','sok');?></span>
            <div class="single-comment-product-inner">
                <?php comments_template();?>
            </div>
         </div>
	<?php }
}


/**
 * Product layout.
 */

$detailLayouts = get_theme_mod('sok_detail_layouts', 'vertical');
// Vertical layout Default

// Grid layout
add_action( 'sok_before_single_product_summary_grid', 'sok_show_product_images_grid', 20 );

if ( ! function_exists( 'sok_show_product_images_grid' ) ) {

	/**
	 * Output the product image before the single product summary.
	 */
	function sok_show_product_images_grid() {
		wc_get_template( 'single-product/layouts/grid/product-image.php' );
	}
}
function sok_variation_image_size_grid( $child_id, $instance, $variation ) {
  $attachment_id = get_post_thumbnail_id( $variation->get_id() );
  $attachment    = wp_get_attachment_image_src( $attachment_id, 'sok-product-grid' );
  $image_src     = $attachment ? current( $attachment ) : '';
  $child_id['image']['src'] = $image_src;
  $child_id['image']['srcset'] = $image_src;
  $child_id['image']['src_w'] = '320';
  $child_id['image']['src_h'] = '320';
  return $child_id;
}
if($detailLayouts == 'grid'){
    add_filter( 'woocommerce_available_variation', 'sok_variation_image_size_grid', 10, 3 );
}

// Carousel  Left layout
add_action( 'sok_before_single_product_summary_carousel_left', 'sok_show_product_images_carousel_left', 20 );

if ( ! function_exists( 'sok_show_product_images_carousel_left' ) ) {

	/**
	 * Output the product image before the single product summary.
	 */
	function sok_show_product_images_carousel_left() {
		wc_get_template( 'single-product/layouts/carousel-left/product-image.php' );
	}
}
function sok_variation_image_size_carousel_left( $child_id, $instance, $variation ) {
  $attachment_id = get_post_thumbnail_id( $variation->get_id() );
  $attachment    = wp_get_attachment_image_src( $attachment_id, 'sok-product-large' );
  $image_src     = $attachment ? current( $attachment ) : '';
  $child_id['image']['src'] = $image_src;
  $child_id['image']['srcset'] = $image_src;
  $child_id['image']['src_w'] = '920';
  $child_id['image']['src_h'] = '920';
  return $child_id;
}
if($detailLayouts == 'carousel-left'){
    add_filter( 'sok_variation_image_size_carousel_left', 'full', 10, 3 );
}

//Layout  Sticky
add_action( 'sok_before_single_product_summary_carousel_sticky', 'sok_show_product_images_sticky', 20 );

if ( ! function_exists( 'sok_show_product_images_sticky' ) ) {
    /**
     * Output the product image before the single product summary.
     */
    function sok_show_product_images_sticky() {
        wc_get_template( 'single-product/layouts/sticky/product-image.php' );
    }
}



/* WooCommerce - Layout SHOP ======================================================================================== */

//vertical
add_action( 'sok_vertical_before_shop_loop_item_title', 'sok_vertical_template_loop_product_thumbnail', 10 );
add_action( 'sok_vertical_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );

if ( ! function_exists( 'sok_vertical_template_loop_product_thumbnail' ) ) {
    function sok_vertical_template_loop_product_thumbnail() {
        echo sok_vertical_get_product_thumbnail();
    }
}
function sok_vertical_get_product_thumbnail() {
    $image_src                      = wp_get_attachment_image_src(get_post_thumbnail_id(), 'sok-vertical-thumbnail-size');
    $placeholder_image              = get_template_directory_uri(). '/assets/images/layzyload-product-vertical.jpg';
    $placeholder_fallback = wc_placeholder_img_src();
    $alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);

    $size = $image_src;
    $woo_width = $size[1];
    $woo_height = $size[2];

    if (!empty($image_src)) {
        echo '<img src="' . $placeholder_image . '" data-lazy="' . $image_src[0] . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
    } else {
        echo '<img src="' . $placeholder_fallback . '" data-lazy="' . $image_src[0] . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
    }
}

//grid
if (!function_exists('woocommerce_template_loop_product_thumbnail')) {
    remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
    add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

    function woocommerce_template_loop_product_thumbnail() {
        $image_src = wp_get_attachment_image_src(get_post_thumbnail_id(), 'shop_catalog');
        $placeholder_image              = get_template_directory_uri(). '/assets/images/layzyload-product-grid.jpg';

        $placeholder_fallback = wc_placeholder_img_src();
        $alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
        $size = wc_get_image_size('shop_catalog');
        $woo_width = $size['width'];
        $woo_height = $size['height'];

        if (!empty($image_src)) {
            echo '<img src="' . $placeholder_image . '" data-lazy="' . $image_src[0] . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
        } else {
            echo '<img src="' . $placeholder_fallback . '" data-lazy="' . $image_src[0] . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
        }
    }
}
//trans lg
add_action( 'sok_trans_lg_before_shop_loop_item_title', 'sok_trans_lg_template_loop_product_thumbnail', 10 );
add_action( 'sok_trans_lg_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );

if ( ! function_exists( 'sok_trans_lg_template_loop_product_thumbnail' ) ) {
    function sok_trans_lg_template_loop_product_thumbnail() {
        echo sok_trans_lg_get_product_thumbnail();
    }
}

function sok_trans_lg_get_product_thumbnail() {

    $image_src                      = wp_get_attachment_image_src(get_post_thumbnail_id(), 'sok-trans-lg-thumbnail-size');
    $placeholder_image              = get_template_directory_uri(). '/assets/images/layzyload-category-thumb-vertical.jpg';

    $placeholder_fallback = wc_placeholder_img_src();
    $alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);

    $size = $image_src;
    $woo_width = $size[1];
    $woo_height = $size[2];

    if (!empty($image_src)) {
        echo '<img src="' . $placeholder_image . '" data-lazy="' . $image_src[0] . '" srcset="' . esc_attr( $image_src[0] ) . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
    } else {
        echo '<img src="' . $placeholder_fallback . '" data-lazy="' . $image_src[0] . '" srcset="' . esc_attr( $image_src[0] ) . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
    }
}

function sok_zoom_thumbnail() { ?>
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="<?php echo esc_attr__('Close (Esc)','sok')?>"></button>

                    <button class="pswp__button pswp__button--share" title="<?php echo esc_attr__('Share','sok')?>"></button>

                    <button class="pswp__button pswp__button--fs" title="<?php echo esc_attr__('Toggle Full screen','sok')?>"></button>

                    <button class="pswp__button pswp__button--zoom" title="<?php echo esc_attr__('Zoom in/out','sok')?>"></button>

                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="<?php echo esc_attr__('Previous (arrow left)','sok')?>">
                </button>

                <button class="pswp__button pswp__button--arrow--right" title="<?php echo esc_attr__('Next (arrow right)','sok')?>">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>

        </div>

    </div>
    <?php
}

/**
 * Product loop start.
 */

remove_filter( 'woocommerce_product_loop_start', 'woocommerce_maybe_show_product_subcategories' );

add_filter( 'sok_subcategories_loop_start', 'woocommerce_maybe_show_product_subcategories' );

if ( ! function_exists( 'sok_subcategories_loop_start' ) ) {

    /**
     * Output the start of a product loop. By default this is a UL.
     *
     * @param bool $echo Should echo?.
     * @return string
     */
    function sok_subcategories_loop_start( $echo = true ) {
        ob_start();

        wc_set_loop_prop( 'loop', 0 );

        wc_get_template( 'loop/loop-start-subcate.php' );

        if(isset($_GET['shop-type']) && ($_GET['shop-type']=='both')){
            $args = array(
                'hide_empty' => 1,
                'pad_counts' => true,
                'child_of'   => 0,
                'parent'  => 0
            );
            $product_categories = get_terms( 'product_cat', $args);
            if ( $product_categories ) {
                foreach ( $product_categories as $category ) {
                    wc_get_template( 'content-product_cat.php', array(
                        'category' => $category,
                    ) );
                }
            }
        }

        $loop_start = apply_filters( 'sok_subcategories_loop_start', ob_get_clean() );

        if ( $echo ) {
            echo $loop_start; // WPCS: XSS ok.
        } else {
            return $loop_start;
        }
    }
}
