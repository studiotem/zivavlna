<?php
/**
 * @package     sok
 * @version     1.0
 * @author      NanoAgency
 * @link        http://www.nanoagency.co
 * @copyright   Copyright (c) 2016 NanoAgency
 * @license     GPL v2
 */

/* Customize font Google  =========================================================================================== */
if(!function_exists('sok_googlefont')){
    function sok_googlefont(){
        global $wp_filesystem;
        $filepath = get_template_directory().'/assets/googlefont/webfonts.json';
        if( empty( $wp_filesystem ) ) {
            require_once( ABSPATH .'/wp-admin/includes/file.php' );
            WP_Filesystem();
        }
        if( $wp_filesystem ) {
            $listGoogleFont=$wp_filesystem->get_contents($filepath);
        }
        if($listGoogleFont){
            $gfont = json_decode($listGoogleFont);
            $fontarray = $gfont->items;
            $options = array();
            foreach($fontarray as $font){
                $options[$font->family] = $font->family;
            }
            return $options;
        }
        return false;
    }
}

function sok_validate_pagination( $template ) {
    return str_replace( 'role="navigation"', '', $template );
}
add_filter( 'navigation_markup_template', 'sok_validate_pagination' );

/* Header Class =================================================================================================== */
function sok_header_class( $class = '' ) {
    echo 'class="'.join( ' ',sok_get_header_class( $class ) ).'"';
}
function sok_get_header_class( $class = '' ){
    $classes = array();
    if ( ! empty( $class ) ) {
        if ( !is_array( $class ) )
            $class = preg_split( '#\s+#', $class );
        $classes = array_merge( $classes, $class );
    } else {
        // Ensure that we always coerce class to being an array.
        $class = array();
    }

    $classes = array_map( 'esc_attr', $classes );

    $classes = apply_filters( 'header_class', $classes, $class );

    return array_unique( $classes );
}

add_filter( 'body_class', 'sok_class' );
function sok_class( $classes ) {
    global  $post;
    //check layout footer
    $class_footer = get_theme_mod('sok_footer', 'footer-1');
    if(is_page()){
        $class_footer=get_post_meta($post->ID, 'layout_footer',true);
    }

    if ( $class_footer) {
        $classes[] = 'sok-'.$class_footer;
    }

    return $classes;
}

add_filter( 'body_class', 'sok_header_products_class' );
function sok_header_products_class( $classes ) {
    global  $post;
    //check layout footer
    $header_products = get_theme_mod('sok_woo_header_products', 'fixed');

    if(isset($_GET['detail-header'])){
        $header_products=$_GET['detail-header'];
    }
    if ( $header_products) {
        $classes[] = 'sok-product-'.$header_products;
    }

    return $classes;
}

/* Post Thumbnail =================================================================================================== */
if ( ! function_exists( 'sok_post_thumbnail' ) ) :
    function sok_post_thumbnail() {
        if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
            return;
        }

        if ( is_singular() ) :
            ?>

            <div class="post-thumbnail">
                <?php the_post_thumbnail(); ?>
            </div>
            <!-- .post-thumbnail -->

        <?php else : ?>

            <a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
                <?php the_post_thumbnail( 'post-thumbnail', array( 'alt' => the_title_attribute( 'echo=0' ) ) ); ?>
            </a>

        <?php endif; // End is_singular()
    }
endif;

/* Excerpt more  ==================================================================================================== */
function sok_new_excerpt_more($more)
{
    return '...';
}
add_filter('excerpt_more', 'sok_new_excerpt_more');

if(!function_exists('sok_string_limit_words')){
    function sok_string_limit_words($string, $word_limit)
    {
        $words = explode(' ', $string, ($word_limit + 1));
        if(count($words) > $word_limit) {
            array_pop($words);
        }
        return implode(' ', $words);
    }
}

function sok_excerpt( $class = 'entry-excerpt' ) {
    if ( has_excerpt() || is_search() ) : ?>
        <div class="<?php echo esc_attr( $class ); ?>">
            <?php the_excerpt(); ?>
        </div>
    <?php endif;
}


/* Header ============================================================================================================*/
add_action( 'sok_header_layout', 'sok_header_layout_config');
if(!function_exists('sok_header_layout_config')){
    function sok_header_layout_config() {
        global  $post;
        $layout_header = get_theme_mod('sok_header', 'full');
        if(is_page()){
            $layout_header = get_post_meta($post->ID, 'layout_header', true);
        }
        if($layout_header == 'global' || empty($layout_header)){
            $layout_header = get_theme_mod('sok_header', 'full');
        }
        get_template_part('templates/header/header', $layout_header);
    }
}

/* Keep Menu =========================================================================================================*/
if(!function_exists('sok_keep_menu')){
    function sok_keep_menu() {
        $configMenu = get_theme_mod('sok_menu_keep',false);
        if(isset($configMenu) & $configMenu == '1'){
            $configMenu='header-fixed';
        }
        return $configMenu;
    }
}

/* Breadcrumb ========================================================================================================*/
function sok_wp_breadcrumbs(){?>
            <nav id="sok_breadcrumbs" class="breadcrumb">
                <?php
                if(in_array("search-no-results",get_body_class())){ ?>
                    <div class="breadcrumb-iner" class="col-sm-12">
                        <a href="<?php get_template_directory_uri().'/'; ?>"><?php echo esc_html__('Home','sok'); ?></a>
                        <span class="delimiter">/</span>
                        <span class="current"><?php echo esc_html__('Search results for','sok'); ?> "<?php echo get_search_query(); ?>"</span> </div>
                    <?php
                }else{
                    $delimiter = '&nbsp;&nbsp;';
                    $home = esc_html__('Home','sok');
                    global $post;
                    global $wp_query;
                    $homeLink = home_url();
                    echo '<a href="'.$homeLink.'">'.$home .'</a>'.$delimiter.'';
                    if ( is_category() ) {
                        global $wp_query;
                        $cat_obj = $wp_query->get_queried_object();
                        $thisCat = $cat_obj->term_id;
                        $thisCat = get_category($thisCat);
                        $parentCat = get_category($thisCat->parent);
                        if ($thisCat->parent != 0) echo(get_category_parents($parentCat,TRUE, ''.$delimiter.''));
                        echo single_cat_title('', false);
                    } elseif ( is_day() ) {
                        echo '<a href="'.get_year_link(get_the_time('Y')).'">'.get_the_time('Y').'</a>'.$delimiter.'';
                        echo '<a href="'.get_month_link(get_the_time('Y'),get_the_time('m')).'">'.get_the_time('F').'</a>'.$delimiter.'';
                        echo esc_html__('Archive by date','sok').'"'.get_the_time('d') ;
                    } elseif ( is_month() ) {
                        echo '<a href="'.get_year_link(get_the_time('Y')).'">'.get_the_time('Y').'</a>'.$delimiter.'';
                        echo esc_html__('Archive by month ','sok').'"'.get_the_time('F').'"' ;
                    } elseif ( is_year() ) {
                        echo esc_html__('Archive by year ','sok').'"' . get_the_time('Y').'"';
                    } elseif ( is_single() && !is_attachment() ) {
                        if ( get_post_type() != 'post' ) {
                            $post_type = get_post_type_object(get_post_type());
                            $slug = $post_type->rewrite;
                            echo '<a href="'.$homeLink .'/'.$slug['slug'].'/">'.$post_type->labels->singular_name.'</a>'.$delimiter.'';
                            echo get_the_title();
                        } else {
                            $cat = get_the_category(); $cat = $cat[0];
                            echo ' ' . get_category_parents($cat,TRUE,''.$delimiter.'').'';
                            echo '' . get_the_title() . '';
                        }
                    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
                        $post_type = get_post_type_object(get_post_type());
                        echo esc_html($post_type->labels->singular_name);
                    } elseif ( is_attachment() ) {
                        $parent_id  = $post->post_parent;
                        $breadcrumbs = array();
                        while ($parent_id) {
                            $page = get_page($parent_id);
                            $breadcrumbs[] = '<a href="'.get_permalink($page->ID).'">'.get_the_title($page->ID).'</a>';
                            $parent_id    = $page->post_parent;
                        }
                        $breadcrumbs = array_reverse($breadcrumbs);
                        foreach ($breadcrumbs as $crumb) echo ''.$crumb.''.$delimiter.'';
                        echo get_the_title() . '';
                    } elseif ( is_page() && !$post->post_parent ) {
                        echo get_the_title();
                    } elseif ( is_page() && $post->post_parent ) {
                        $parent_id  = $post->post_parent;
                        $breadcrumbs = array();
                        while ($parent_id) {
                            $page = get_page($parent_id);
                            $breadcrumbs[] = '<a href="' . get_permalink($page->ID).'">'.get_the_title($page->ID).'</a>';
                            $parent_id    = $page->post_parent;
                        }
                        $breadcrumbs = array_reverse($breadcrumbs);
                        foreach ($breadcrumbs as $crumb) echo ''.$crumb.''.$delimiter.'';
                        echo get_the_title();
                    } elseif ( is_search()) {
                        echo esc_html__('Search results for','sok').'"'. get_search_query();
                    } elseif ( is_tag() ) {
                        echo esc_html__('Archive by tag','sok').'"'.single_tag_title('', false);
                    } elseif ( is_author() ) {
                        global $author;
                        $userdata = get_userdata($author);
                        echo esc_html__('Articles posted by','sok').'"'.$userdata->display_name;
                    } elseif ( is_404() ) {
                        echo esc_html__('You got it ','sok').'"'.esc_html__('Error 404 not Found','sok').'"&nbsp;';
                    }
                    if ( get_query_var('paged') ) {
                        if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' ';
                        if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo '';
                    }
                }
                ?>
            </nav>
<?php }
add_action('sok_breadcrumb', 'sok_wp_breadcrumbs',10);

/* Sub String Content =============================================================================================== */
if(!function_exists('sok_content')) {
    function sok_content($limit) {
        $excerpt = explode(' ', get_the_excerpt(), $limit);
        if (count($excerpt)>=$limit) {
            array_pop($excerpt);
            array_pop($excerpt);
            $excerpt = implode(" ",$excerpt).'...';
        } else {
            $excerpt = implode(" ",$excerpt);
        }
        $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
        return $excerpt;
    }
}

/* Get Category  ==================================================================================================== */
if(!function_exists('sok_category')) {
    function sok_category($separator)
    {
        $first_time = 1;
        foreach ((get_the_category()) as $category) {
            if ($first_time == 1) {?>
               <a href="<?php echo esc_url(get_category_link($category->term_id));?>" title="<?php  sprintf(esc_attr__('View all posts in %s', 'sok'), $category->name); ?>" ><?php echo esc_attr($category->name);?></a>
               <?php $first_time = 0; ?>
            <?php } else {
                echo esc_attr($separator) ?>
                <a href="<?php echo esc_url(get_category_link($category->term_id)); ?>" title="<?php  sprintf(esc_attr__('View all posts in %s', 'sok'), $category->name) ?>" ><?php  echo esc_attr($category->name); ?></a>
            <?php }
        }
    }
}

/* Add login outlink ================================================================================================ */
add_filter( 'wp_nav_menu_items', 'sok_loginout_link', 10, 2 );
function sok_loginout_link($output='',$args) {

    if (!is_user_logged_in() && $args->theme_location == 'top_navigation') {
        $output .='<li><a href="'.get_permalink(get_option('woocommerce_myaccount_page_id')).'">';
        $output .='<span>'.esc_html__(' Login', 'sok').'</span>';
        $output .='</a></li>';
    }
    elseif (is_user_logged_in() && $args->theme_location == 'top_navigation')
    {
        $output .='<li><a href="'.wp_logout_url(get_permalink(get_option('woocommerce_myaccount_page_id'))).'">';
        $output .='<span>'.esc_html__(' Logout', 'sok').'</span>';
        $output .='</a></li>';
    }
    return $output;

}

/* Config Sidebar Blog ============================================================================================== */
//content
add_action( 'single-content-before', 'sok_single_content_before' );
function sok_single_content_before() {?>
        <div class="main-content col-sx-12 col-sm-12 col-md-12 col-lg-12">
<?php }
add_action( 'single-content-after', 'sok_single_content_after' );
function sok_single_content_after() {?>
        </div>
<?php
}

/* Config Sidebar archive =========================================================================================== */
add_action( 'archive-sidebar-left', 'sok_cat_sidebar_left' );
function sok_cat_sidebar_left() {
    $cat_sidebar=get_theme_mod( 'sok_sidebar_cat', 'right' );
    if(isset($_GET['layout'])){
        $cat_sidebar=$_GET['layout'];
    }
    if (  isset($cat_sidebar) && is_active_sidebar( 'archive' ) && $cat_sidebar == 'left') {?>
         <div id="archive-sidebar" class="sidebar sidebar-left hidden-sx hidden-sm col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar">
            <?php get_sidebar('sidebar'); ?>
        </div>
    <?php }
}

add_action( 'archive-sidebar-right', 'sok_cat_sidebar_right' );
function sok_cat_sidebar_right() {
    $cat_sidebar=get_theme_mod( 'sok_sidebar_cat', 'right' );

    if(isset($_GET['layout'])){
        $cat_sidebar=$_GET['layout'];
    }
    if ( isset($cat_sidebar) && is_active_sidebar( 'archive' ) && $cat_sidebar == 'right') {?>
         <div id="archive-sidebar" class="sidebar sidebar-right hidden-sx hidden-sm col-sx-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar">
            <?php get_sidebar('sidebar'); ?>
        </div>
    <?php }
}

//content
add_action( 'archive-content-before', 'sok_cat_content_before' );
function sok_cat_content_before() {
    $cat_sidebar=get_theme_mod( 'sok_sidebar_cat', 'right' );
    if(isset($_GET['layout'])){
        $cat_sidebar=$_GET['layout'];
    }
    if ($cat_sidebar && is_active_sidebar( 'archive')  && $cat_sidebar != 'full') {?>
        <div class="main-content col-sx-12 col-sm-12 col-md-9 padding-content-<?php echo esc_attr($cat_sidebar)?> col-lg-9">
    <?php }
    else{?>
            <div class="main-content col-md-12 col-lg-12">
    <?php }


}

add_action( 'archive-content-after', 'sok_cat_content_after' );
function sok_cat_content_after() {
    $cat_sidebar=get_theme_mod( 'sok_sidebar_cat', 'right' );
    if ( $cat_sidebar){?>
        </div>
    <?php }
}

/* Category Title ===================================================================================================== */
add_action( 'category-title', 'sok_category_title' );
function sok_category_title() {
    $sok_breadcrumb         = get_theme_mod('sok_blog_breadcrumb','');
    $style_css ='';

    if($sok_breadcrumb){
        $bg_image          ="background-image:url('$sok_breadcrumb')";
        $style_css            ='style = '.$bg_image.'';
    }
    $title  = get_theme_mod('sok_post_title_heading',true);
    if ($title):?>
        <div class="page-header clearfix" <?php echo esc_attr($style_css);?>>
            <h1 class="page-title">
                <?php single_cat_title(); ?>
            </h1>
            <div class="wrapper-breadcrumb clearfix">
                <?php do_action( 'sok_breadcrumb'); ?>
            </div>
        </div>
    <?php endif;
}

/* Archive Title ===================================================================================================== */
add_action( 'archive-title', 'sok_archive_title' );
function sok_archive_title() {
    $sok_breadcrumb         = get_theme_mod('sok_blog_breadcrumb','');
    $style_css ='';

    if($sok_breadcrumb){
        $bg_image          ="background-image:url('$sok_breadcrumb')";
        $style_css            ='style = '.$bg_image.'';
    }
    $title  = get_theme_mod('sok_post_title_heading',true);
    if ($title):?>
        <div class="page-header clearfix" <?php echo esc_attr($style_css);?>>
            <h1 class="page-title">
                <?php the_archive_title(); ?>
            </h1>
             <div class="description-page">
                <?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
            </div>
            <div class="wrapper-breadcrumb clearfix">
                <?php do_action( 'sok_breadcrumb'); ?>
            </div>
        </div>
    <?php endif; ?>
<?php }

/* Comment Form ===================================================================================================== */
function sok_comment_form($arg,$class='btn-variant',$id='submit'){
    ob_start();
    comment_form($arg);
    $form = ob_get_clean();
    echo str_replace('id="submit"','id="'.$id.'"', $form);
}

function sok_list_comments($comment, $args, $depth){
    $path = get_template_directory() . '/templates/list_comments.php';
    if( is_file($path) ) require ($path);
}

/* PostViews =========================================================================================================*/
function sok_post_views($post_ID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($post_ID, $count_key, true);
    if ($count == '') {
        delete_post_meta($post_ID, $count_key);
        add_post_meta($post_ID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($post_ID, $count_key, $count);
    }
}

function sok_track_post_views($post_id)
    {
        if (!is_single()) return;
        if (empty ($post_id)) {
        global $post;
        $post_id = $post->ID;
    }
    sok_post_views($post_id);
}
add_action('wp_head', 'sok_track_post_views');

function sok_get_PostViews($post_ID)
    {
        $count_key = 'post_views_count';
        $count = get_post_meta($post_ID, $count_key, true);
        return $count;
    }

function sok_post_column_views($newcolumn)
    {
        $newcolumn['post_views'] = esc_html__('Views', 'sok');
        return $newcolumn;
    }

function sok_post_custom_column_views($column_name, $id)
    {
        if ($column_name === 'post_views') {
        echo esc_attr(sok_get_PostViews(get_the_ID()));
    }
}
add_filter('manage_posts_columns', 'sok_post_column_views');
add_action('manage_posts_custom_column', 'sok_post_custom_column_views', 10, 2);


/* Move comment field to bottom ======================================================================================*/
function sok_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}
add_filter( 'comment_form_fields', 'sok_move_comment_field_to_bottom' );
?>
