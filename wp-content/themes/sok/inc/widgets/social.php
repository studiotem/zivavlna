<?php
/**
 * The template for displaying the social
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
?>
<?php
if (!class_exists('sok_social')) {
    class sok_social extends WP_Widget
    {
        public $socials = array(
            'social-facebook' => array(
                'title' => 'Facebook',
                'name' => 'facebook_username',
                'link' => '*',
                'icon'=>'ion-social-facebook',
            ),
            'social-googleplus' => array(
                'title' => 'Googleplus',
                'name' => 'googleplus_username',
                'link' => '*',
                'icon'=>'ion-social-googleplus',
            ),
            'social-twitter' => array(
                'title' => 'Twitter',
                'name' => 'twitter_username',
                'link' => '*',
                'icon'=>'ion-social-twitter',
            ),
            'social-instagram' => array(
                'title' => 'Instagram',
                'name' => 'instagram_username',
                'link' => '*',
                'icon'=>'ion-social-instagram',
            ),
            'social-pinterest' => array(
                'title' => 'Pinterest',
                'name' => 'pinterest_username',
                'link' => '*',
                'icon'=>'ion-social-pinterest',
            ),
            'social-skype' => array(
                'title' => 'Skype',
                'name' => 'skype_username',
                'link' => '*',
                'icon'=>'ion-social-skype',
            ),
            'social-vimeo' => array(
                'title' => 'Vimeo',
                'name' => 'vimeo_username',
                'link' => '*',
                'icon'=>'ion-social-vimeo-square',
            ),
            'social-youtube' => array(
                'title' => 'Youtube',
                'name' => 'youtube_username',
                'link' => '*',
                'icon'=>'ion-social-youtube',
            ),
            'social-dribbble' => array(
                'title' => 'Dribbble',
                'name' => 'dribbble_username',
                'link' => '*',
                'icon'=>'ion-sociala-dribbble',
            ),
            'social-linkedin' => array(
                'title' => 'Linkedin',
                'name' => 'linkedin_username',
                'link' => '*',
                'icon'=>'ion-social-linkedin',
            ),
            'social-rss' => array(
                'title' => 'Rss',
                'name' => 'rss_username',
                'link' => '*',
                'icon'=>'ion-social-rss',
            )
        );

        public function __construct()
        {
            $widget_ops = array('classname' => 'sok_social', 'description' => esc_html__('Displays your social profile.', 'sok'));

            parent::__construct(false, esc_html__('+NA: Social', 'sok'), $widget_ops);
        }

        function widget($args, $instance)
        {
            extract($args);
            $title = apply_filters('widget_title', $instance['title']);
            echo ent2ncr($args['before_widget']);
            if($title) {
                echo ent2ncr($args['before_title']) . esc_html($title) . ent2ncr($args['after_title']);
            }
            echo '<div class="social-footer clearfix">';
            echo "<ul class='social-icons list-unstyled list-inline'>";
            foreach ($this->socials as $key => $social) {
                if (!empty($instance[$social['name']])) {
                    echo "<li class='social-item'>";
                    echo '<a href="' . str_replace('*', esc_attr($instance[$social['name']]), $social['link']) . '" target="_blank" class="' . esc_attr($key) . '"><i class="' . esc_attr( $social['icon']) . '"></i>' . esc_attr($social['title']) . '</a>';
                    echo "</li>";
                }
            }echo "</ul>";
            echo '</div>';
            echo ent2ncr($args['after_widget']);
        }

        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance = $new_instance;
            /* Strip tags (if needed) and update the widget settings. */
            $instance['title'] = strip_tags($new_instance['title']);
            return $instance;
        }

        function form($instance)
        {
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">Title:</label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" type="text"
                       name="<?php echo esc_attr($this->get_field_name('title')); ?>"
                       value="<?php echo isset($instance['title']) ? esc_attr($instance['title']) : ''; ?>"/>
            </p> <?php
            foreach ($this->socials as $key => $social) {
                ?>
                <p>
                <label for="<?php echo esc_attr($this->get_field_id($social['name'])); ?>"><?php echo esc_html($key); ?>
                    :</label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id($social['name'])); ?>" type="text"
                       name="<?php echo esc_attr($this->get_field_name($social['name'])); ?>"
                       value="<?php echo isset($instance[$social['name']]) ? esc_attr($instance[$social['name']]) : ''; ?>"/>
                </p><?php
            }
        }
    }
}

add_action('widgets_init', 'sok_social_widgets');

function sok_social_widgets()
{
    register_widget('sok_social');
}
