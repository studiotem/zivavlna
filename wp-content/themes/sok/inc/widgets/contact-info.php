<?php
 /* *
  * widgets contact info
  **/
  class sok_contact_info extends WP_Widget{

      /*function construct*/
      public function __construct() {
          $widget_ops = array('classname' => ' sok_contact_info widget_contact_info clearfix', 'description' => esc_html__( "Display Contact info.",'sok') );
          parent::__construct( 'contact_info', esc_attr__( '+NA: Contact info', 'sok' ), $widget_ops );
      }
      /**
       * font-end widgets
      */
      public function widget($args, $instance) {
          extract($args);
          $image = $instance['image'];

          echo ent2ncr($args['before_widget']);

      ?>

          <?php if($image): ?>
                <div class="about-image">
                    <img class="logo-image-footer" src="<?php echo esc_url($image)?>" alt="<?php esc_attr_e('Logo Footer','sok')?>" />
                </div>
          <?php endif; ?>
          <div class="group-des">
              <?php if($instance['description']): ?>
                  <p class="description">
                      <span><?php echo esc_attr($instance['description']); ?></span>
                  </p>
              <?php endif; ?>

              <?php  if($instance['address']): ?>
                  <div class="address">
                      <span><?php  echo esc_attr($instance['address']);  ?></span>
                  </div>
              <?php  endif; ?>
          </div>
          <ul class="contact-info">

                <?php if($instance['skype']): ?>
                  <li>
                      <a href="skype:<?php echo esc_attr($instance['skype']);?>?chat" ><span><?php echo esc_attr($instance['skype']); ?></span></a>
                  </li>
                <?php endif; ?>

                <?php if($instance['email']): ?>
                  <li>
                      <a href="mailto:<?php echo esc_attr($instance['email']);?>" ><span><?php echo esc_attr($instance['email']); ?></span></a>
                  </li>
                <?php endif; ?>

                <?php if($instance['mobile']): ?>
                  <li class="mobile">
                      <span><?php   echo esc_attr($instance['mobile']);  ?></span>
                  </li>

                <?php endif; ?>
                <?php if($instance['phone']): ?>
                  <li class="phone">
                      <span><?php   echo esc_attr($instance['phone']);  ?></span>
                  </li>

                <?php endif; ?>


          </ul>



      <?php
          echo ent2ncr($args['after_widget']);
      }

      /**
       * Back-end widgets form
      */
      public function form($instance){
          $instance =   wp_parse_args($instance,array(
              'address'     =>  '',
              'phone'       =>  '',
              'mobile'      =>  '',
              'skype'       =>  '',
              'email'       =>  '',
              'description' =>'',
          ));
          ?>
          <p id="<?php echo esc_attr($this->get_field_id('image').'-wrapp'); ?>">
              <label for="<?php echo esc_attr($this->get_field_id('image')); ?>"><?php esc_html_e('Image:', 'sok'); ?></label>
              <img id="<?php echo esc_attr($this->get_field_id('image').'-img'); ?>" src="<?php echo esc_url($instance['image'])?>" class="custom_media_image <?php echo esc_attr($instance['image']==''?  esc_attr('hidden'):''); ?>"/>
              <input type="text" class="widefat custom_media_url hidden" name="<?php echo esc_attr($this->get_field_name('image')); ?>" id="<?php echo esc_attr($this->get_field_id('image')); ?>" value="<?php echo esc_attr($instance['image']); ?>" />
              <br>
              <input type="button" class="button button-primary custom_media_button" id="<?php echo esc_attr($this->get_field_id('image').'-button'); ?>" value="Select Image" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php echo esc_html_e('Address:','sok'); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('address')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('address')); ?>" value="<?php echo esc_attr($instance['address']); ?>" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php echo esc_html_e( 'Phone:', 'sok' ); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" value="<?php echo esc_attr($instance['phone']); ?>" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('mobile')); ?>"><?php echo esc_html_e( 'Mobile:', 'sok' ); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('mobile')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('mobile')); ?>" value="<?php echo esc_attr($instance['mobile']); ?>" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('skype')); ?>"><?php echo esc_html_e('Skype:', 'sok'); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('skype')); ?>" name="<?php echo esc_attr($this->get_field_name('skype')); ?>" class="widefat" value="<?php echo esc_attr($instance['skype']); ?>" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php echo esc_html_e('Email:', 'sok'); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" class="widefat" value="<?php echo esc_attr($instance['email']); ?>" />
          </p>

          <!-- description -->
          <p>
              <label for="<?php echo esc_attr($this->get_field_id( 'description' )); ?>"><?php echo esc_html_e('About me text::', 'sok'); ?></label>
              <textarea id="<?php echo esc_attr($this->get_field_id( 'description')); ?>" name="<?php echo esc_attr($this->get_field_name( 'description' )); ?>" style="width:95%;" rows="6"><?php echo esc_attr($instance['description']); ?></textarea>
          </p>


      <?php
      }

      /**
      * function update widget
      */
      public function update( $new_instance, $old_instance ) {
          $instance = $old_instance;
          $instance['image'] = $new_instance['image'];
          $instance['address'] = $new_instance['address'];
          $instance['phone']    =   $new_instance['phone'];
          $instance['mobile'] = $new_instance['mobile'];
          $instance['skype'] = $new_instance['skype'];
          $instance['email']    =   $new_instance['email'];
          $instance['description']    =   $new_instance['description'];
          return $instance;
      }
  }
  function sok_contact_info(){
      register_widget('sok_contact_info');
  }
  add_action('widgets_init','sok_contact_info');
?>