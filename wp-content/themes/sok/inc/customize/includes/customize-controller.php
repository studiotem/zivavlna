<?php
/**
 * @package     Over
 * @version     0.1
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
if (!class_exists('sok_Customize')) {
    class sok_Customize
    {
        public $customizers = array();

        public $panels = array();

        public function init()
        {
            $this->customizer();
            add_action('customize_controls_enqueue_scripts', array($this, 'sok_customizer_script'));
            add_action('customize_register', array($this, 'sok_register_theme_customizer'));
            add_action('customize_register', array($this, 'remove_default_customize_section'), 20);
        }

        public static function &getInstance()
        {
            static $instance;
            if (!isset($instance)) {
                $instance = new sok_Customize();
            }
            return $instance;
        }

        protected function customizer()
        {

            $this->panels = array(

                'blog_panel' => array(
                    'title'             => esc_html__('Blog','sok'),
                    'description'       => esc_html__('Blog >','sok'),
                    'priority'          =>  101,
                ),
                'sidebar_panel' => array(
                    'title'             => esc_html__('Sidebar','sok'),
                    'description'       => esc_html__('Sidebar Setting','sok'),
                    'priority'          => 103,
                ),
                'sok_option_panel' => array(
                    'title'             => esc_html__('Option','sok'),
                    'description'       => '',
                    'priority'          => 104,
                ),
            );

            $this->customizers = array(
                'title_tagline' => array(
                    'title' => esc_html__('Site Identity', 'sok'),
                    'priority'  =>  1,
                    'settings' => array(
                        'sok_logo' => array(
                            'class' => 'image',
                            'label' => esc_html__('Logo', 'sok'),
                            'description' => esc_html__('Upload Logo Image', 'sok'),
                            'priority' => 12
                        ),
                        'sok_logo_theme' => array(
                            'type'          => 'checkbox',
                            'label'         => esc_html__('Logo Theme', 'sok'),
                            'priority'      => 99,
                            'params'        => array(
                                'default'   => false,
                            ),
                        ),
                    )
                ),

                //2.General ============================================================================================
             'sok_general' => array(
                'title' => esc_html__('General', 'sok'),
                'description' => '',
                'priority' => 2,
                'settings' => array(

                'sok_bg_body' => array(
                    'label'         => esc_html__('Background - Body', 'sok'),
                    'description'   => '',
                    'class'         => 'color',
                    'priority'      => 2,
                    'params'        => array(
                        'default'   => '',
                    ),
                    
                ),
                'sok_primary_body' => array(
                    'label'         => esc_html__('Primary - Color', 'sok'),
                    'description'   => '',
                    'class'         => 'color',
                    'priority'      => 1,
                    'params'        => array(
                        'default'   => '',
                    ),
                    
                ),
                'sok_cart' => array(
                    'class' => 'toggle',
                    'label' => esc_html__('Enable Cart','sok'),
                    'priority' => 4,
                    'params' => array(
                        'default' => true,
                    ),
                ),
                'sok_search' => array(
                    'class' => 'toggle',
                    'label' => esc_html__('Enable Search','sok'),
                    'priority' => 5,
                    'params' => array(
                        'default' => true,
                    ),
                ),
                'sok_user' => array(
                    'class' => 'toggle',
                    'label' => esc_html__('Enable User','sok'),
                    'priority' => 6,
                    'params' => array(
                        'default' => false,
                    ),
                ),
            )
        ),

                //3.Header =============================================================================================
                'sok_header' => array(
                    'title' => esc_html__('Header', 'sok'),
                    'description' => '',
                    'priority' => 3,
                    'settings' => array(
                        //header
                        'sok_header_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Header', 'sok'),
                            'priority' => 0,
                        ),
                        'sok_header' => array(
                            'type' => 'select',
                            'label' => esc_html__('Choose Header Layout', 'sok'),
                            'description' => '',
                            'priority' => 1,
                            'choices' => array(
                                'full'      => esc_html__('Header Full Width', 'sok'),
                                'center'    => esc_html__('Header Center', 'sok'),
                                 'simple'    => esc_html__('Header Default', 'sok'),
                            ),
                            'params' => array(
                                'default' => 'full',
                            ),
                        ),

                        'sok_menu_keep' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Fixed Menu', 'sok'),
                            'priority' =>4,
                            'params' => array(
                                'default' => false,
                            ),
                            
                        ),

                        'sok_bg_header' => array(
                            'label'         => esc_html__('Background - Header', 'sok'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 5,
                            'params'        => array(
                                'default'   => '',
                            ),
                            
                        ),

                        'sok_color_menu' => array(
                            'label'         => esc_html__('Color - Text', 'sok'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 6,
                            'params'        => array(
                                'default'   => '',
                            ),
                        ),

                    )
                ),

                //4.Footer =============================================================================================
                'sok_new_section_footer' => array(
                    'title' => esc_html__('Footer', 'sok'),
                    'description' => '',
                    'priority' => 4,
                    'settings' => array(
                        'sok_footer' => array(
                            'type' => 'select',
                            'label' => esc_html__('Choose Footer Style', 'sok'),
                            'description' => '',
                            'priority' => -1,
                            'choices' => array(
                                'footer-1'     => esc_html__('Footer 1', 'sok'),
                            ),
                            'params' => array(
                                'default' => 'footer-1',
                            ),
                            
                        ),
                        'sok_copyrights_config' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Show Copyrights', 'sok'),
                            'priority' => 1,
                            'params' => array(
                                'default' => true,
                            ),

                        ),

                        'sok_bg_footer' => array(
                            'label'         => esc_html__('Background - Footer', 'sok'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 2,
                            'params'        => array(
                                'default'   => '',
                            ),
                            
                        ),
                        'sok_color_footer' => array(
                            'label'         => esc_html__('Color - Text ', 'sok'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 3,
                            'params'        => array(
                                'default'   => '',
                            ),
                            
                        ),
                        'sok_copyright_text' => array(
                            'type' => 'textarea',
                            'label' => esc_html__('Footer Copyright Text', 'sok'),
                            'description' => '',
                            'priority' => 0,
                            
                        ),
                    )
                ),

                //6.Font  ==============================================================================================
                'sok_new_section_font_size' => array(
                    'title' => esc_html__('Font', 'sok'),
                    'description' => 'Font Option',
                    'priority' => 5,
                    'settings' => array(

                        'sok_body_font_google' => array(
                            'type'          => 'select',
                            'label'         => esc_html__('Use Google Font', 'sok'),
                            'choices'       => sok_googlefont(),
                            'priority'      => 1,
                            'params'        => array(
                                'default'       => 'ABeeZee',
                            ),
                        ),

                        'sok_body_font_size' => array(
                            'type'          => 'number',
                            'label'         => esc_html__('Body Font Size', 'sok'),
                            'description'   => '',
                            'priority'      => 2,
                            'params'        => array(
                                'default'       => '',
                            ),
                        ),
                        'sok_menu_font_size' => array(
                            'type'          => 'number',
                            'label'         => esc_html__('Menu Font Size', 'sok'),
                            'description'   => '',
                            'priority'      =>3,
                            'params'        => array(
                                'default'    => '',
                            ),
                        ),
                    ),
                ),

                //7.Categories Blog ====================================================================================
                'sok_blog' => array(
                    'title' => esc_html__('Blogs Categories', 'sok'),
                    'description' => '',
                    'panel'        =>'blog_panel',
                    'priority' => 6,
                    'settings' => array(

                        'sok_sidebar_cat' => array(
                            'class'         => 'layout',
                            'label'         => esc_html__('Sidebar Layout', 'sok'),
                            'priority'      =>3,
                            'choices'       => array(
                                'left'         => get_template_directory_uri().'/assets/images/left.png',
                                'right'        => get_template_directory_uri().'/assets/images/right.png',
                                'full'         => get_template_directory_uri().'/assets/images/full.png',
                            ),
                            'params' => array(
                                'default' => 'right',
                            ),
                        ),
                        'sok_siderbar_cat_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'sok'),
                            'description' => esc_html__( 'Please goto Appearance > Widgets > drop drag widget to the sidebar Article.', 'sok' ),
                            'priority' => 4,
                        ),

                        //layout content
                        'sok_cat_content_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Content', 'sok'),
                            'priority' =>19,
                        ),
                        'sok_post_title_heading' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Category Title','sok'),
                            'priority' => 20,
                            'params' => array(
                                'default' => true,
                            ),
                        ),
                        'sok_blog_breadcrumb' => array(
                            'class' => 'image',
                            'label' => esc_html__('Breadcrumb Background', 'sok'),
                            'description' => esc_html__('Default 1920x675 px', 'sok'),
                            'priority' => 20,
                        ),

                        'sok_cat_content_layout' => array(
                            'class'         => 'layout',
                            'priority'      =>21,
                            'choices'       => array(
                                'grid'         => get_template_directory_uri().'/assets/images/box-grid.jpg',
                                'list'         => get_template_directory_uri().'/assets/images/box-list.jpg',
                                'tran'         => get_template_directory_uri().'/assets/images/box-tran.jpg',
                            ),
                            'params' => array(
                                'default' => 'list',
                            ),
                        ),
                        'sok_post_meta_author' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Author','sok'),
                            'priority' => 24,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'sok_post_meta_date' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Date','sok'),
                            'priority' => 24,
                            'params' => array(
                                'default' =>true,
                            ),

                        ),
                        'sok_post_meta_view' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('View','sok'),
                            'priority' => 25,
                            'params' => array(
                                'default' => false,
                            ),
                            
                        ),
                        'sok_post_meta_comment' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Comments','sok'),
                            'priority' => 26,
                            'params' => array(
                                'default' => false,
                            ),
                            
                        ),
                    ),
                 ),

                //8.Single blog ========================================================================================
                'sok_blog_single' => array(
                    'title' => esc_html__('Blog Single', 'sok'),
                    'description' => '',
                    'panel'        =>'blog_panel',
                    'priority' => 7,
                    'settings' => array(
                        'sok_sidebar_single' => array(
                            'class'         => 'layout',
                            'label'         => esc_html__('Sidebar Layout', 'sok'),
                            'priority'      =>13,
                            'choices'       => array(
                                'left'         => get_template_directory_uri().'/assets/images/left.png',
                                'right'        => get_template_directory_uri().'/assets/images/right.png',
                                'full'         => get_template_directory_uri().'/assets/images/full.png',
                            ),
                            'params' => array(
                                'default' => 'right',
                            ),
                        ),
                        'sok_siderbar_single_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'sok'),
                            'description' => esc_html__( 'Please goto Appearance > Widgets > drop drag widget to the sidebar Article.', 'sok' ),
                            'priority' => 14,
                        ),
                        //share
                        'sok_single_share' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Share', 'sok'),
                            'priority' =>15,
                        ),
                        'sok_share_facebook' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Facebook  ','sok'),
                            'priority' => 17,
                            'params' => array(
                                'default' =>false,
                            ),
                            
                        ),
                        'sok_share_twitter' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Twitter  ','sok'),
                            'priority' => 18,
                            'params' => array(
                                'default' => false,
                            ),
                            
                        ),
                        'sok_share_google' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Google  ','sok'),
                            'priority' => 19,
                            'params' => array(
                                'default' => false,
                            ),
                            
                        ),

                        'sok_share_linkedin' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Linkedin  ','sok'),
                            'priority' => 20,
                            'params' => array(
                                'default' => false,
                            ),
                            
                        ),

                        'sok_share_pinterest' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Pinterest  ','sok'),
                            'priority' => 21,
                            'params' => array(
                                'default' => false,
                            ),
                            
                        ),
                        //comments
                        'sok_single_comments' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Comments', 'sok'),
                            'priority' =>22,
                        ),
                        'sok_comments_single_facebook' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Enable Facebook Comments ','sok'),
                            'priority' => 23,
                            'params' => array(
                                'default' => false,
                            ),
                            
                        ),
                        'sok_comments_single' => array(
                            'type'          => 'text',
                            'label'         => esc_html__('Your app id :', 'sok'),
                            'priority'      => 24,
                            'params'        => array(
                                'default'   => '',
                            ),
                            
                        ),
                        'sok_comments_single_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'sok'),
                            'description' => esc_html__('If you want show notification on  your facebook , please input app id ...', 'sok' ),
                            'priority' => 25,
                        ),
                    ),
                ),

                //9.WooCommerces  Category =============================================================================
                'woocommerce_product_catalog' => array(
                    'title' => esc_html__('Product Catalog', 'sok'),
                    'description' => '',
                    'panel' =>'woocommerce',
                    'priority' => 10,
                    'settings' => array(
                        //siderbar
                        'sok_subcate_woo' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Category Layout:', 'sok'),
                            'priority' =>98,
                            'choices' => array(
                                'cat-grid'              => get_template_directory_uri().'/assets/images/cat-3.jpg',
                                'cat-carousel'          => get_template_directory_uri().'/assets/images/filter-left.jpg',
                            ),
                            'params' => array(
                                'default' => 'cat-carousel',
                            ),
                        ),
                        'sok_subcate_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'sok'),
                            'description' => esc_html__( 'Choose what to display Category layout  on the main shop page.', 'sok' ),
                            'priority' => 99,
                        ),
                        'sok_show_page_title' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Title Page Shop', 'sok'),
                            'priority' =>100,
                            'params' => array(
                                'default' => true,
                            ),

                        ),
                        'sok_shop_breadcrumb' => array(
                            'class' => 'image',
                            'label' => esc_html__('Breadcrumb Background', 'sok'),
                            'description' => esc_html__('Default 1920x675 px', 'sok'),
                            'priority' => 100,
                        ),

                        //category
                        'sok_header_woocat' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Categories Product', 'sok'),
                            'priority' =>100,
                        ),
                        //Filter
                        'sok_filter_woo' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Filter Layout', 'sok'),
                            'priority' =>101,
                            'choices' => array(
                                'left'                  => get_template_directory_uri().'/assets/images/shop/filter-left.jpg',
                                'full'                  => get_template_directory_uri().'/assets/images/shop/filter-full.jpg',
                                'down'                  => get_template_directory_uri().'/assets/images/shop/filter-down.jpg',
                                'sidebar-left'          => get_template_directory_uri().'/assets/images/left.png',
                            ),
                            'params' => array(
                                'default' => 'full',
                            ),
                        ),
                        'sok_siderbar_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'sok'),
                            'description' => esc_html__( 'Please goto Appearance > Widgets > drop drag widget to the sidebar SHOP.', 'sok' ),
                            'priority' => 102,
                        ),
                        'sok_woo_layered_nav' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Enable Layered Nav Filters', 'sok'),
                            'priority' =>103,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                    ),
                ),

                //10.WooCommerces Product Layouts  =====================================================================
                'sok_woo_product' => array(
                    'title' => esc_html__('Product Layouts', 'sok'),
                    'description' => '',
                    'panel' =>'woocommerce',
                    'priority' => 9,
                    'settings' => array(

                        //category
                        'sok_header_layout_cat' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Product Layouts', 'sok'),
                            'priority' =>1,
                        ),
                        'sok_show_page_layout' => array(
                            'type' => 'select',
                            'label' => esc_html__('Layout Shop', 'sok'),
                            'description' => '',
                            'priority' => 100,
                            'choices' => array(
                                'container'         => esc_html__('Container', 'sok'),
                                'container-fluid'   => esc_html__('Container Fluid', 'sok'),
                                'container-full'    => esc_html__('Container Full Width', 'sok'),
                            ),
                            'params' => array(
                                'default' => 'container-fluid',
                            ),

                        ),
                        'sok_layout_cat' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Products Layouts', 'sok'),
                            'priority' =>2,
                            'choices' => array(
                                'grid'              => get_template_directory_uri().'/assets/images/box-grid.jpg',
                                'duel'              => get_template_directory_uri().'/assets/images/shop/cat-duel.jpg',
                            ),
                            'params' => array(
                                'default' => 'grid',
                            ),
                        ),
                        'sok_woo_product_sale_flash' => array(
                            'type'          => 'checkbox',
                            'label'         => esc_html__('Show Discount', 'sok'),
                            'priority'      => 4,
                            'params'        => array(
                                'default'   => false,
                            ),

                        ),

                        'sok_woo_cart' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Show button Add To Cart','sok'),
                            'priority' => 4,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'sok_woo_price' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Show Price','sok'),
                            'priority' => 5,
                            'params' => array(
                                'default' => true,
                            ),

                        ),
                        'sok_woo_product_per_row' => array(
                            'class' => 'slider',
                            'label' => esc_html__('Products per row', 'sok'),
                            'description' => '',
                            'priority' =>108,
                            'choices' => array(
                                'max' => 6,
                                'min' => 1,
                                'step' => 1
                            ),
                            'params'      => array(
                                'default' => 4
                            ),
                        ),
                        'sok_woo_product_per_page' => array(
                            'class' => 'slider',
                            'label' => esc_html__('Number products per page', 'sok'),
                            'description' => '',
                            'priority' =>109,
                            'choices' => array(
                                'max' => 36,
                                'min' => 9,
                                'step' => 1
                            ),
                            'params'      => array(
                                'default' => 24
                            ),
                        ),
                        'sok_woo_pagination' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Enable Load More', 'sok'),
                            'priority' =>110,
                            'params' => array(
                                'default' => false,
                            ),

                        ),


                        //share
                        'sok_woo_share_twitter' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Twitter','sok'),
                            'priority' => 27,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'sok_woo_share_pinterest' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Pinterest','sok'),
                            'priority' => 28,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'sok_woo_share_google' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Google +','sok'),
                            'priority' => 29,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'sok_woo_share_linkedin' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('linkedin','sok'),
                            'priority' => 30,
                            'params' => array(
                                'default' => false,
                            ),

                        ),

                    ),
                ),
                //11.WooCommerces Detail  ==============================================================================
                'sok_woo_detail' => array(
                    'title' => esc_html__('Product Detail', 'sok'),
                    'description' => '',
                    'panel' =>'woocommerce',
                    'priority' => 10,
                    'settings' => array(
                        //siderbar
                        'sok_woo_header_products' => array(
                            'type' => 'select',
                            'label'         => esc_html__('Header Product', 'sok'),
                            'description'   => '',
                            'priority'      => 1,
                            'choices'       => array(
                                'trans' => esc_html__('Transparent Menu', 'sok'),
                                'fixed' => esc_html__('Fixed Menu', 'sok'),
                            ),
                            'params' => array(
                                'default' => 'fixed',
                            ),

                        ),
                        'sok_sidebar_woo_single' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Sidebar', 'sok'),
                            'priority' =>2,
                            'choices' => array(
                                'full'         => get_template_directory_uri().'/assets/images/full.png',
                            ),
                            'params' => array(
                                'default' => 'full',
                            ),
                        ),
                        'sok_woo_related_products' => array(
                            'type'          => 'checkbox',
                            'label'         => esc_html__('Display Related Products', 'sok'),
                            'priority'      => 3,
                            'params'        => array(
                                'default'   => true,
                            ),

                        ),

                        //Detail layouts
                        'sok_detail_layouts_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Detail Layouts', 'sok'),
                            'priority' =>10,
                        ),
                        'sok_detail_layouts' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Layouts', 'sok'),
                            'priority' =>11,
                            'choices' => array(
                                'vertical'              => get_template_directory_uri().'/assets/images/shop/detail-vertical.jpg',
                                'grid'                  => get_template_directory_uri().'/assets/images/shop/detail-grid.jpg',
                                'sticky'                => get_template_directory_uri().'/assets/images/shop/detail-sticky.jpg',
                            ),
                            'params' => array(
                                'default' => 'vertical',
                            ),
                        ),
                        //Share products
                        'sok_cat_content_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Share', 'sok'),
                            'priority' =>25,
                        ),
                        'sok_woo_share_facebook' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Facebook','sok'),
                            'priority' => 26,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'sok_woo_share_twitter' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Twitter','sok'),
                            'priority' => 27,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'sok_woo_share_pinterest' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Pinterest','sok'),
                            'priority' => 28,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'sok_woo_share_google' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Google +','sok'),
                            'priority' => 29,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'sok_woo_share_linkedin' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('linkedin','sok'),
                            'priority' => 30,
                            'params' => array(
                                'default' => false,
                            ),

                        ),

                    ),
                ),
            );

        }

        public function sok_customizer_script()
        {
            // Register
            wp_enqueue_style('sok-customize', get_template_directory_uri() . '/inc/customize/assets/css/customizer.css', array(),null);
            wp_enqueue_style('jquery-ui', get_template_directory_uri() . '/inc/customize/assets/css/jquery-ui.min.css', array(),null);
            wp_enqueue_script('sok-customize', get_template_directory_uri() . '/inc/customize/assets/js/customizer.js', array('jquery'), null, true);
        }

        public function add_customize($customizers) {
            $this->customizers = array_merge($this->customizers, $customizers);
        }

        public function sok_register_theme_customizer()
        {
            global $wp_customize;

            foreach ($this->customizers as $section => $section_params) {

                //add section
                $wp_customize->add_section($section, $section_params);
                if (isset($section_params['settings']) && count($section_params['settings']) > 0) {
                    foreach ($section_params['settings'] as $setting => $params) {

                        //add setting
                        $setting_params = array();
                        if (isset($params['params'])) {
                            $setting_params = $params['params'];
                            unset($params['params']);
                        }
                        $wp_customize->add_setting($setting, array_merge( array( 'sanitize_callback' => null ), $setting_params));
                        //Get class control
                        $class = 'WP_Customize_Control';
                        if (isset($params['class']) && !empty($params['class'])) {
                            $class = 'WP_Customize_' . ucfirst($params['class']) . '_Control';
                            unset($params['class']);
                        }

                        //add params section and settings
                        $params['section'] = $section;
                        $params['settings'] = $setting;

                        //add controll
                        $wp_customize->add_control(
                            new $class($wp_customize, $setting, $params)
                        );
                    }
                }
            }

            foreach($this->panels as $key => $panel){
                $wp_customize->add_panel($key, $panel);
            }

            return;
        }

        public function remove_default_customize_section()
        {
            global $wp_customize;
//            // Remove Sections
//            $wp_customize->remove_section('title_tagline');
            $wp_customize->remove_control('woocommerce_catalog_columns');
            $wp_customize->remove_section('header_image');
            $wp_customize->remove_section('nav');
            $wp_customize->remove_section('static_front_page');
            $wp_customize->remove_section('colors');
            $wp_customize->remove_section('background_image');
        }
    }
}